Brentwood
=========

My website : [nickthecoder.co.uk](http://nickthecoder.co.uk), written in Kotlin.
With a wiki, photo album, family tree, search, music player and a catalogue of my software.
While it is designed specifically for my needs, there are reusable components
which others may find useful.

![Screenshot](screenshot.jpeg)

Status
------

Complete, and stable. I can't remember it ever failing ;-)
So much easier to set up compared to my previous site, which used Apache and Tomcat. Yuck!

Components
----------

 * **Wiki**, which can have many "namespaces", which I use for :
   * Gardening
   * Recipes
   * Other more general stuff
 * **Image Browser** (mainly for photos)
 * **Music Browser** (play songs from any client within my intranet).
 * **Player** - A music player (plays the music on the server, not on the client!).
   
 * **Family Album** (Walk through my family tree. Each photo has meta-data of each person in the photo )
 * **Search** (crawls over all pages, and stores meta-data in a lucene database)
 * **Files** - A web-based view of a directory. Download files, navigate up/down the tree of directories.
 * Some on-line games
 * Maps (using Google Maps API), without the clutter of Google Maps, and with easy
   addition of markers.
 * Time - Useful for IoT, which need to update their date

Technology
----------

Uses the [Ktor](https://ktor.io/) web server framework to server the pages.

Uses Kotlin's [HTML DSL](https://github.com/Kotlin/kotlinx.html) to generate the HTML.
I really like thi way of generating HTML. 

Uses [Lucene](https://lucene.apache.org/) for the "search" feature.

Uses [SQLite](https://www.sqlite.org/index.html)
to hold meta-data about photos, as well as the data for the FamilyAlbum component,
and the artists/albums/songs/playlists in the Player component.

Code Structure
--------------

There are several subprojects :

### brentwood-core

Contains all the core components, which are reasonably generic.
(wiki, images, files, music & time)


### brentwood-familyalbum

A single component, for displaying and editing a family tree, and family photos.

### brentwood-player

A single component, for playing music on the server (not on the client!).
I have an always-on server in my house, and it's hooked up to my main stereo.
Anyone with a mobile phone can choose music from my collection.
No special _app_ needed. Great for social gatherings.

### brentwood-home

Very specific to me, and will be of no use to anybody else!
brentwood-home creates the two websites that I host on a single machine,
one which is public (http://nickthecoder.co.uk), and the other is available on my LAN only.

While there are features which may be useful to others, this project
does have quite a lot of quirks specific to my needs.

For example, the music browser assumes the music is rigidly
organised in directory structures with 4 layers of hierarchy :
**genre / artist / album / track**.
Also, the album cover images are hard-coded to
".meta/cover_100.jpg" and ".meta/cover_400.jpg".

Components
----------

Each component has its own package :

    games, images, music, wiki

In each package, there is a class with the same name, which supplies the Ktor routing.

Notes
-----

Configuration is done directly in Kotlin, not through configuration files.

If you want to use this for your own website, then you will need to create something
similar to the functions in package uk.co.nickthecoder.brentwood.home.

I use [systemd.md](SystemD) to man
