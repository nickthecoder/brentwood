package uk.co.nickthecoder.brentwood

class BrentwoodException(message: String) : Exception(message)
