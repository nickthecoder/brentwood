package uk.co.nickthecoder.brentwood

import io.ktor.routing.*
import uk.co.nickthecoder.brentwood.layout.Layout

/**
 * The web site is composed of parts, each part is a [Component].
 * A component had a [webPath], which is the route for all requests within the component.
 */
abstract class Component(val webPath: String) {

    init {
        if (!webPath.startsWith("/")) {
            throw RuntimeException("webPath ($webPath) must start with a /")
        }
        if (webPath.endsWith("/")) {
            throw RuntimeException("webPath ($webPath) must not end with a /")
        }
    }

    /**
     * If we want trailing slash removed, call super.
     */
    abstract fun route(route: Route)
}

abstract class LayoutComponent(val layout: Layout, webPath: String) : Component(webPath)

fun Route.component(component: Component) {
    route(component.webPath) { component.route(this) }
}
