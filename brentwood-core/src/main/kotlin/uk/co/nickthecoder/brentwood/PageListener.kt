package uk.co.nickthecoder.brentwood

interface PageListener {

    /**
     * [localURL] is the url, excluding the protocol/hostname/port.
     * This is because the sender will not always KNOW the protocol/hostname/port,
     * and also because one page change may cause changes to two different sites
     * (if they share the same page).
     * In this case, one pageChanged will be propagated to TWO different search
     * instances, with different hostnames.
     */
    fun pageChanged(localURL: String)

}

class DoNothingPageListener : PageListener {
    override fun pageChanged(localURL: String) {
    }
}

class CompoundPageListener : PageListener {
    val children = mutableListOf<PageListener>()

    override fun pageChanged(localURL: String) {
        for (child in children) {
            child.pageChanged(localURL)
        }
    }
}
