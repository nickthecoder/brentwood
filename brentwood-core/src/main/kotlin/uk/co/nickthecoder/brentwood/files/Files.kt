package uk.co.nickthecoder.brentwood.files

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.html.*
import org.intellij.markdown.IElementType
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.layout.PageTemplate
import uk.co.nickthecoder.brentwood.util.*
import java.io.File
import java.text.DecimalFormat
import java.util.*

class Files(layout: Layout, val config: FilesConfig) :
    LayoutComponent(layout, config.webPath) {

    override fun route(route: Route) {

        with(route) {

            get("/") {
                fileOrDirectory(call, layout, config, emptyList())
            }

            get("/{path...}") {
                val pathParts = call.safePathParts("path")
                fileOrDirectory(call, layout, config, pathParts)
            }
        }
    }

}


suspend fun fileOrDirectory(call: ApplicationCall, layout: Layout, config: FilesConfig, pathParts: List<String>) {

    val path = pathParts.joinPath()
    val fileOrDir = File(config.baseDirectory, path)

    if (fileOrDir.exists() && ! fileOrDir.isHidden) {
        if (fileOrDir.isFile) {
            val extension = fileOrDir.extension.lowercase()
            if (config.textExtensions.contains(extension)) {
                if (config.rawText) {
                    call.respondFile(fileOrDir)
                } else {
                    textFile(call, layout, config, fileOrDir, pathParts)
                }
            } else if (config.markdownExtensions.contains(extension)) {
                markdownFile(call, layout, config, fileOrDir, pathParts)
            } else {
                call.respondFile(fileOrDir)
            }
        } else {
            // A directory
            for (index in config.indexFiles) {
                val indexFile = File(fileOrDir, index)
                if (indexFile.exists() && indexFile.isFile) {
                    call.respondRedirect("${config.webPath}$path/$index")
                    return
                }
            }
            directory(call, layout, config, fileOrDir, pathParts)
        }
    } else {
        throw NotFoundException()
    }

}

suspend fun markdownFile(
    call: ApplicationCall,
    layout: Layout,
    config: FilesConfig,
    file: File,
    pathParts: List<String>
) {
    val markdown = file.readText()
    val parsedTree = MarkdownParser(config.markdownFlavour).parse(IElementType("ROOT"), markdown)
    val html = HtmlGenerator(markdown, parsedTree, config.markdownFlavour).generateHtml()
    val uri = call.request.uri
    call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
        pageHeading(config, pathParts)

        content {
            div("markdown") {
                unsafe { + html }
            }
        }
    }
}

suspend fun textFile(call: ApplicationCall, layout: Layout, config: FilesConfig, file: File, pathParts: List<String>) {
    val text = file.readText()
    val uri = call.request.uri
    call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
        pageHeading(config, pathParts)

        content {
            pre {
                + text
            }
        }
    }
}

suspend fun directory(call: ApplicationCall, layout: Layout, config: FilesConfig, dir: File, pathParts: List<String>) {
    val path = pathParts.joinPath()
    val directories = dir.listFiles()?.filter { it.isDirectory && ! it.isHidden }?.sorted() ?: emptyList()
    val files = dir.listFiles()?.filter { it.isFile && ! it.isHidden }?.sorted() ?: emptyList()

    val pathSlash = if (path.endsWith('/')) path else "$path/"
    val pathString = if (pathSlash == "/") "${config.webPath}/" else "${config.webPath}$pathSlash"

    val parent = File("/$path").parent

    call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
        pageHeading(config, pathParts)

        pageTools {
            if (parent != null) {
                layout.imageTool(this, "${config.webPath}$parent") {
                    title = "Parent Directory"
                    img("folder", "/style/up.png")
                }
            }
        }

        content {

            if (files.isEmpty() && directories.isEmpty()) {
                p { + "Empty" }
            } else {
                table("plain") {
                    tr {
                        th { + "Name" }
                        th { + "Last Modified" }
                        th {
                            colSpan = "2"
                            + "Size"
                        }
                    }

                    for (directory in directories) {
                        tr {
                            td {
                                a("$pathString${directory.name}") {
                                    img("folder", "${config.resourcesPath}/folder.png")
                                    + " "
                                    + directory.name
                                }
                            }
                            td { + Date(directory.lastModified()).toString() }
                            td {
                                colSpan = "2"
                            }
                        }
                    }

                    for (file in files) {
                        tr {
                            td {
                                a("$pathString${file.name}") {
                                    fileIcon(this, config, file)
                                    + " "
                                    + file.name
                                }
                            }
                            td { + Date(file.lastModified()).toString() }
                            humanReadableSize(this, file.length())
                        }
                    }

                }
            }
        }

    }
}

fun PageTemplate.pageHeading(config: FilesConfig, pathParts: List<String>) {
    val path = pathParts.joinPath()
    val pathSlash = if (path.endsWith('/')) path else "$path/"
    val pathString = if (pathSlash == "/") "${config.webPath}/" else "${config.webPath}/$pathSlash"
    pageTitle { + pathString }

    pageHeading {
        if (config.prefix != null) {
            if (config.prefixLink != null) {
                a(config.prefixLink) { + config.prefix }
            } else {
                + config.prefix
            }
            + " "
        }
        a("${config.webPath}/") { + "/" }
        + " "
        for (i in 0 until pathParts.size - 1) {
            val part = pathParts[i]

            a("${config.webPath}/${escapeURLPathParams(pathParts.subList(0, i + 1))}") { + part }
            + " / "
        }
        pathParts.lastOrNull()?.let { + it }
    }
}

private fun fileIcon(parent: FlowContent, config: FilesConfig, file: File) {
    val ext = file.extension.lowercase()
    if (config.textExtensions.contains(ext)) {
        parent.img("text", "${config.resourcesPath}/text.png")
    } else if (config.markdownExtensions.contains(ext)) {
        parent.img("html", "${config.resourcesPath}/markdown.png")
    } else if (config.htmlExtensions.contains(ext)) {
        parent.img("html", "${config.resourcesPath}/html.png")
    } else if (config.imageExtensions.contains(ext)) {
        parent.img("image", "${config.resourcesPath}/image.png")
    } else {
        parent.img("file", "${config.resourcesPath}/file.png")
    }
}

/**
 * Standard units of file sizes. I've chosen to use the SI decimal convention, rather than
 * the older powers of 2 convention.
 *
 * NOTE, the order is important! Largest first.
 */
private var sizes = listOf(
    1_000_000_000_000.0 to "TB",
    1_000_000_000.0 to "GB",
    1_000_000.0 to "MB",
    1_000.0 to "kB"
)

/**
 * Used to display the file size
 */
private var sizeFormat = DecimalFormat("0.0#")

private fun humanReadableSize(tr: TR, size: Long) {
    val sizeD = size.toDouble()
    for (s in sizes) {
        if (sizeD >= s.first) {
            tr.td("right") { + sizeFormat.format(sizeD / s.first) }
            tr.td { + s.second }
            return
        }

    }
    tr.td("right") { + "$size" }
    tr.td { + "bytes" }
}
