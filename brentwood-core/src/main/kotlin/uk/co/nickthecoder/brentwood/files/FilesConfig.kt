package uk.co.nickthecoder.brentwood.files

import org.intellij.markdown.flavours.MarkdownFlavourDescriptor
import org.intellij.markdown.flavours.commonmark.CommonMarkFlavourDescriptor
import org.intellij.markdown.flavours.gfm.GFMFlavourDescriptor
import java.io.File

class FilesConfig(
    val webPath: String,
    val baseDirectory: File,
    val prefix: String? = null,
    val prefixLink: String? = null,

    /**
     * When true, files with an extension in [textExtensions] are displayed in the browser in their "raw" form.
     * Otherwise, they are displayed within a PRE tag in a HTML page.
     */
    var rawText: Boolean = true

) {

    val resourcesPath = "/fileResources"

    /**
     * Which files in a directory should switch from listing the directory, to showing the
     * directory using the index file.
     */
    val indexFiles = mutableListOf("index.html")

    /**
     * Files with this extension are displayed with a "html" icon.
     */
    val htmlExtensions = mutableListOf("html")

    /**
     * Files with this extension are converted from markdown to html.
     */
    val markdownExtensions = mutableListOf("md")

    /**
     * Files with this extension are displayed with a "text" icon.
     */
    val textExtensions = mutableListOf("txt", "html", "css", "js", "kt", "java", "css", "kts", "properties", "gradle")

    /**
     * Files with this extension are displayed with an "image" icon.
     */
    val imageExtensions = mutableListOf("png", "jpeg", "jpg")


    var markdownFlavour: MarkdownFlavourDescriptor = GFMFlavourDescriptor() //CommonMarkFlavourDescriptor()
}
