package uk.co.nickthecoder.brentwood.images

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.io.IOException
import java.sql.DriverManager

fun createImagesDB(sqliteDB: File) {

    if (sqliteDB.exists()) {
        throw IOException("File $sqliteDB already exists.")
    }

    val db = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$sqliteDB") })
    transaction(db) {
        SchemaUtils.create(
            Photos
        )
    }
}

fun dumpImagesSchema(sqliteDB: File) {

    ProcessBuilder(
        "sh",
        "-c",
        "sqlite3 $sqliteDB .schema | sed -e 's/(/(\\n    /'  -e 's/, /,\\n    /g' -e 's/);/\\n);\\n/' -e 's/REFERENCES/\\n        REFERENCES/g' -e 's/CONSTRAINT/\\n    CONSTRAINT/g'\n"
    ).apply {
        inheritIO()
    }.start()
}

/**
 * Creates the tables in the (empty?) database called familyAlbum.db in the current directory.
 *
 * Prints the resulting schema.
 */
fun main(/*vararg args: String*/) {
    createImagesDB(File("images.db"))
    dumpImagesSchema(File("images.db"))
}
