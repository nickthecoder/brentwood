package uk.co.nickthecoder.brentwood.images

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.html.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.*
import java.io.File
import java.util.*

// Valid values for the size request parameter.
// If size is not specified, then NORMAL is assumed.
private const val THUMBNAIL = "thumbnail"
private const val NORMAL = "normal"
private const val FULL = "full"

/**
 * Browse images within a part of the filesystem.
 *
 * [webPath] is the ktor route's path. On my server this is /gidea/images (for backwards compatibility with
 * a previous implementation of the site). If I were to do it again, I would make it just "/images".
 *
 * [baseDirectory] is the top-level directory of the images. Images in sub-directories will be
 * accessible too, but parent and sibling directories are not accessible.
 */
class Images(

    layout: Layout,
    webPath: String = "/images",
    private val baseDirectory: File,

    private val thumbnailer: ImageScaler,

    private val quickScaler: ImageScaler,

    /**
     * Stores notes of each folder/image.
     */
    private val db: Database,

    /**
     * Either null, or a list of authentication configuration names, such as "auth-basic"
     * as defined in the web server's Application.install(Authentication) block.
     */
    private val authenticate: List<String>? = null

) : LayoutComponent(layout, webPath) {

    private val imageExtensions = mutableListOf("png", "jpeg", "jpg")

    var normalSizeImagesPerPage = 10
    var fullSizeImagesPerPage = 5

    override fun route(route: Route) {

        with(route) {

            staticResources("/resources/", this@Images.javaClass.packageName)

            get("/") { directory(call, emptyList(), baseDirectory) }

            get("/{resource...}") {
                imageOrDirectory(call, call.safePathParts("resource"))
            }

            get("/media/{resource...}") {
                val pathParts = call.safePathParts("resource")
                media(call, pathParts)
            }

            optionalAuthenticate(authenticate) {
                route("/editNotes/{resource...}") {
                    get { editNotes(call, call.safePathParts("resource")) }
                    post { saveNotes(call, call.safePathParts("resource")) }
                }
            }
        }
    }

    /**
     * webPath/resource/{...}
     *
     * Responds with the contents of the file (usually an image).
     */
    private suspend fun media(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()

        val file = File(baseDirectory, path)
        if (file.exists() && file.isFile) {
            call.respondFile(file)
        } else {
            throw NotFoundException("Image media not found : '$path'")
        }
    }

    private suspend fun imageOrDirectory(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()

        suspendTransaction {
            val file = File(baseDirectory, path)
            if (file.exists()) {
                if (file.isDirectory) {
                    directory(call, pathParts, file)
                } else {
                    if (imageExtensions.contains(file.extension.lowercase())) {
                        image(call, pathParts, file)
                    }
                }
            } else {
                throw NotFoundException(file.path)
            }
        }
    }

    private suspend fun image(call: ApplicationCall, pathParts: List<String>, file: File) {
        val notes = notes(file)
        call.respondHtmlTemplate2(layout.createFullScreenTemplate()) {

            content {
                div("imageContainer") {
                    img(file.name, media(pathParts, FULL))
                    br()
                    +file.name
                    editNotesLink(pathParts)
                    br()
                    notes(notes)
                }
            }
        }
    }

    private suspend fun directory(call: ApplicationCall, pathParts: List<String>, directory: File) {
        val size = call.optionalStringParameter("size", NORMAL)
        val pageNumber = call.optionalIntParameter("page", 1)
        val uri = call.request.uri

        suspendTransaction {

            call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

                pageTitle { +"${pathParts.joinPath()} (Images)" }

                pageHeading {
                    a(webPath) { +"/" }
                    for (i in pathParts.indices) {
                        a(href2(pathParts.subList(0, i + 1), size)) {
                            +pathParts[i]
                        }
                        if (i < pathParts.size - 1) {
                            +"/"
                        }
                    }
                }

                pageTools {
                    edit("$webPath/editNotes/${escapeURLPathParams(pathParts)}", "Edit Notes")

                    if (pathParts.isNotEmpty()) {
                        layout.imageTool(this, href2(pathParts.subList(0, pathParts.size - 1), size)) {
                            title = "Back to Parent Directory"
                            img("Up", "/style/up.png")
                        }
                    }
                    layout.imageTool(this, href2(pathParts, THUMBNAIL)) {
                        title = "View as Thumbnails"
                        img("Thumbnails", "/style/thumbnails.png")
                    }
                    layout.imageTool(this, href2(pathParts, NORMAL)) {
                        title = "View Normal Size"
                        img("Normal Size", "/style/normal.png")
                    }
                    layout.imageTool(this, href2(pathParts, FULL)) {
                        title = "View Full Size"
                        img("Full Size", "/style/full.png")
                    }
                }


                content {

                    notes(notes(directory))

                    layout.thumbnails(this) {
                        for (child in directory.listOnlyDirectories()) {
                            layout.thumbnail(this, main = { directoryThumbnail(pathParts, child, size) }) {
                                a(childHref2(pathParts, child.name, size)) {
                                    +child.name
                                }
                            }
                        }
                    }

                    if (size == THUMBNAIL) {
                        layout.thumbnails(this) {
                            for (child in directory.listFilesWithExtension(imageExtensions)) {
                                layout.thumbnail(this, main = { imageThumbnail(pathParts, child) }) {
                                    a(childHref2(pathParts, child.name, NORMAL)) {
                                        +child.name
                                    }
                                }
                            }
                        }
                    } else {
                        val imageFiles = directory.listFilesWithExtension(imageExtensions)
                        val itemsPerPage = if (size == NORMAL) normalSizeImagesPerPage else fullSizeImagesPerPage
                        val pager = ListPager(href2(pathParts, size), imageFiles, itemsPerPage)
                        images {
                            for (child in pager.pageItems(pageNumber)) {
                                image(pathParts, child, size, notes(child))
                            }
                        }
                        pager(pager, layout, pageNumber)
                    }

                }
            }
        }
    }

    private fun FlowContent.editNotesLink(pathParts: List<String>) {
        +" "
        a("$webPath/editNotes/${escapeURLPathParams(pathParts)}") {
            title = "Edit Notes"
            img("edit", "$webPath/resources/editTiny.png", "plain")
        }
    }

    private fun FlowContent.editChildNotesLink(pathParts: List<String>, child: String) {
        +" "
        a("$webPath/editNotes/${escapeURLPathParams(pathParts)}/${escapeURLParam(child)}") {
            title = "Edit Notes"
            img("edit", "$webPath/resources/editTiny.png", "plain")
        }
    }

    private suspend fun editNotes(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()
        val file = File(baseDirectory, path)

        suspendTransaction {

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"Edit Notes : $path" }
                pageHeading { +"Notes for $path" }

                content {
                    form("$webPath/editNotes/${escapeURLPathParams(pathParts)}", method = FormMethod.post) {

                        input(InputType.hidden, name = "ref") {
                            value = call.request.header(HttpHeaders.Referrer) ?: ""
                        }

                        div("form") {

                            if (file.isFile) {
                                imageThumbnail(pathParts.subList(0, pathParts.size - 1), file)
                                br()

                                // Allow this image to be used as the default image for a folder.
                                label {
                                    this.htmlFor = "useAsDefault"
                                    +"Use as the folder's default thumbnail"
                                }
                                input(InputType.checkBox, name = "useAsDefault") {
                                    id = "useAsDefault"
                                }
                                br()
                                br()
                            }

                            +"Notes"
                            textArea {
                                name = "notes"
                                rows = "10"
                                autoFocus = true

                                +(notes(file) ?: "")
                            }
                        }

                        div("buttons") {
                            input(InputType.submit, name = "ok", classes = "button ok") { value = "OK" }
                            input(InputType.submit, name = "cancel", classes = "button cancel") { value = "Cancel" }
                        }
                    }
                }

            }
        }
    }

    private suspend fun saveNotes(call: ApplicationCall, pathParts: List<String>) {
        val parameters = call.receiveParameters()
        val path = pathParts.joinPath()
        val file = File(baseDirectory, path)

        // Make the image the default image for the parent folder.
        // i.e. ln -s $file/../.thumbnails/${file.name}  $file/../.thumbnails/default
        if (parameters["useAsDefault"] != null) {
            val thumbnailFile = File(file.parentFile, "/.thumbnails/${file.name}")
            val folderDefaultThumbnailFile = File(thumbnailFile.parentFile, "default.jpg")
            JobQueue.add {
                if (folderDefaultThumbnailFile.exists()) {
                    folderDefaultThumbnailFile.delete()
                }
                commandToString("ln", "-s", thumbnailFile.path, folderDefaultThumbnailFile.path)
            }
        }

        val notes = parameters["notes"]
        if (notes != null) {
            suspendTransaction {
                val photo = Photo.findById(file.path)
                if (photo == null) {
                    Photo.new(file.path) {
                        this.notes = notes
                    }
                } else {
                    photo.notes = notes
                }
            }

            // Save the notes in a hidden file .notes
            if (file.isDirectory) {
                File(file, ".notes").writeText(notes)
            }

            // Use image magik to set the comment inside the jpg file.
            if (file.isFile && imageExtensions.contains(file.extension)) {
                JobQueue.add {
                    commandToString("mogrify", "-set", "comment", notes, file.path)
                }
            }
        }

        var ref = parameters["ref"]
        if (ref.isNullOrBlank()) ref = href2(pathParts, NORMAL)
        call.respondRedirect(ref)
    }

    private fun notes(file: File): String? {
        val photo = Photo.findById(file.path)

        // If the database doesn't know about the file, then update it from the jpeg comments,
        // or the folder's .notes file.

        if (photo == null) {
            // Each directory can have a hidden file called ".notes"
            try {
                if (file.isDirectory) {
                    val notesFile = File(file, ".notes")
                    if (notesFile.exists() && notesFile.isFile) {
                        Photo.new(file.path) {
                            this.notes = notesFile.readText()
                        }
                    }
                }
            } catch (e: Exception) {
                // Maybe locked. No matter, we might update next time!
            }

            // Update the database with comments from the jpeg image.
            // If an image is renamed, or moved to a different directory, then the notes will not be found in the
            // database, but in a new thread, the notes will be extracted from the image, and stored in the db.
            // Therefore, on the next request, the notes WILL be found.
            if (file.isFile && file.exists() && imageExtensions.contains(file.extension)) {
                JobQueue.add {
                    val result = commandToString("identify", "-format", "%c", file.path)
                    try {
                        transaction(db) {
                            val photo2 = Photo.findById(file.path)
                            if (photo2 != null) {
                                photo2.notes = result
                            } else {
                                Photo.new(file.path) {
                                    this.notes = result
                                }

                            }
                        }
                    } catch (e: Exception) {
                        // Maybe locked. No matter, we might update next time!
                    }
                }
            }
        }

        return photo?.notes
    }

    private fun FlowContent.notes(notes: String?) {

        if (!notes.isNullOrBlank()) {
            div("notes") {
                p {
                    +notes
                }
            }
        }
    }

    private fun FlowContent.directoryThumbnail(parentPathParts: List<String>, dir: File, size: String) {
        val defaultThumbnail = File(dir, ".thumbnails/default.jpg")
        a(childHref2(parentPathParts, dir.name, size)) {
            if (defaultThumbnail.exists()) {
                val src = "$webPath/media/${escapeURLPathParams(parentPathParts)}/${
                    escapeURLParam(dir.name)
                }/.thumbnails/default.jpg"
                img(dir.name, src)
                span("emblem") {
                    img(dir.name, "/style/folderEmblem.png", "folder")
                }
            } else {
                img(dir.name, "/style/folderThumbnail.png", classes = "folder")
            }
        }
    }

    /**
     * Generates the thumbnail image if it doesn't already exists.
     * NOTE. This may NOT be in time for the current request, so the user will have to refresh the page to see the thumbnails.
     */
    private fun ensureThumbnail(image: File) {
        thumbnailer.scaleImage(image, File(image.parentFile, ".thumbnails/${image.name}"))
    }

    /**
     * Generates the "quick" image if it doesn't already exists.
     * NOTE. This may NOT be in time for the current request, so the user will have to refresh the page to see the thumbnails.
     */
    private fun ensureQuick(image: File) {
        quickScaler.scaleImage(image, File(image.parentFile, ".quick/${image.name}"))
    }

    private fun FlowContent.imageThumbnail(parentPathParts: List<String>, image: File) {
        ensureThumbnail(image)
        a(childHref2(parentPathParts, image.name, NORMAL)) {
            img(image.name, media(parentPathParts, image.name, THUMBNAIL), classes = "thumbnail")
        }
    }

    private fun FlowContent.images(content: DIV.() -> Unit) {
        div("images") {
            content()
        }
    }

    private fun FlowContent.image(parentPathParts: List<String>, image: File, size: String, notes: String?) {
        div("imageContainer") {
            a(childHref2(parentPathParts, image.name, NORMAL)) {
                img(image.name, media(parentPathParts, image.name, size))
            }
            br()
            a(childHref2(parentPathParts, image.name, NORMAL)) {
                +image.name
            }
            editChildNotesLink(parentPathParts, image.name)
            notes(notes)
        }
    }


    private fun href2(pathParts: List<String>, size: String) =
        if (size == THUMBNAIL || size == FULL) {
            "$webPath/${escapeURLPathParams(pathParts)}?size=$size"
        } else {
            "$webPath/${escapeURLPathParams(pathParts)}"
        }


    private fun childHref2(pathParts: List<String>, child: String, size: String) =
        if (size == THUMBNAIL || size == FULL) {
            "$webPath/${escapeChildURLPathParams(pathParts, child)}?size=$size"
        } else {
            "$webPath/${escapeChildURLPathParams(pathParts, child)}"
        }

    private fun media(pathParts: List<String>, size: String): String {
        val path = pathParts.joinPath()
        val file = File(baseDirectory, path)
        return when (size) {
            THUMBNAIL -> {
                ensureThumbnail(file)
                "$webPath/media/${escapeURLPathParams(pathParts.subList(0, pathParts.size - 1))}/.thumbnails/${
                    escapeURLParam(pathParts.last())
                }"
            }
            NORMAL -> {
                ensureQuick(file)
                "$webPath/media/${escapeURLPathParams(pathParts.subList(0, pathParts.size - 1))}/.quick/${
                    escapeURLParam(pathParts.last())
                }"
            }
            else -> "$webPath/media/${escapeURLPathParams(pathParts)}"
        }
    }


    private fun media(pathParts: List<String>, child: String, size: String): String {
        val path = pathParts.joinPath()
        val file = File(File(baseDirectory, path), child)
        return when (size) {
            THUMBNAIL -> {
                ensureThumbnail(file)
                "$webPath/media/${escapeURLPathParams(pathParts)}/.thumbnails/${escapeURLParam(child)}"
            }
            NORMAL -> {
                ensureQuick(file)
                "$webPath/media/${escapeURLPathParams(pathParts)}/.quick/${escapeURLParam(child)}"
            }
            else -> {
                "$webPath/media/${escapeURLPathParams(pathParts)}/${escapeURLParam(child)}"
            }
        }
    }

    internal fun FlowContent.edit(href: String, aTitle: String = "Edit") {
        layout.imageTool(this, href) {
            title = aTitle
            img("edit", "$webPath/resources/edit.png")
        }
    }

    internal suspend fun <T> suspendTransaction(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO, db = db) {
            block()
        }

}

