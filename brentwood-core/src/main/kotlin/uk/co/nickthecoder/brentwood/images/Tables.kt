package uk.co.nickthecoder.brentwood.images

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable
import org.jetbrains.exposed.sql.Column

object Photos : IdTable<String>("photo") {

    override val id: Column<EntityID<String>> = varchar("filename", 400).entityId()
    val notes = text("notes")
}

class Photo(id: EntityID<String>) : Entity<String>(id) {
    companion object : EntityClass<String, Photo>(Photos)

    var notes by Photos.notes
}
