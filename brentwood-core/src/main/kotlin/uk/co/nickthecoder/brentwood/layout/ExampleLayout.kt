package uk.co.nickthecoder.brentwood.layout

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.util.GroupBy
import uk.co.nickthecoder.brentwood.util.Pager
import uk.co.nickthecoder.brentwood.util.addParameter

/**
 * A minimal example of a Layout.
 */
class ExampleLayout : Layout {

    override fun createTemplate(fullWidth: Boolean, uri: String) = ExampleTemplate()
    override fun createFullScreenTemplate() = ExampleTemplate()

    override fun groupByAnchor(parent: FlowContent, groupBy: GroupBy<*>, initial: Char) {
        parent.h2 {
            id = "groupBy_$initial"
            +"$initial"
        }
    }

    override fun groupByInitials(parent: FlowContent, groupBy: GroupBy<*>) {

        parent.div("groupByInitials") {
            ul("infoList") {
                for (initial in groupBy.allInitials()) {
                    li {
                        if (groupBy.isUsed(initial)) {
                            a("#groupBy_$initial") {
                                id = "groupByInitial_$initial"
                                +"$initial"
                            }
                        } else {
                            +"$initial"
                        }
                    }
                    +" "
                }
            }
        }
    }

    override fun pager(parent: FlowContent, pager: Pager<*>, pageNumber: Int) {
        if (pager.pageCount() > 1) {
            parent.div("pager") {

                if (pageNumber > 1) {
                    a(pager.href.addParameter("page=${pageNumber - 1}")) {
                        +"Previous Page"
                    }
                }

                for (n in 1..pager.pageCount()) {
                    if (n == pageNumber) {
                        span("currentPage") { +"$n" }
                    } else {
                        a(pager.href.addParameter("page=$n")) { +"$n" }
                    }
                }

                if (pageNumber < pager.pageCount()) {
                    a(pager.href.addParameter("page=${pageNumber + 1}")) {
                        +"Next Page"
                    }
                }
            }
        }
    }

}
