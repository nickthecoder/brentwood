package uk.co.nickthecoder.brentwood.layout

import io.ktor.html.*
import kotlinx.html.*

/**
 * This is a minimal example of a page template.
 */
class ExampleTemplate : PageTemplate() {

    override fun HTML.apply() {
        head {
            title { insert(pageTitle) }
            insert(extraHeader)
        }
        body {
            div("content") {
                insert(pageTools)
                h1 { insert(pageHeading) }
                insert(content)
            }
            div("navigation") {
                insert(navigation)
            }
        }
    }

}
