package uk.co.nickthecoder.brentwood.layout

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.request.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.BrentwoodException
import uk.co.nickthecoder.brentwood.util.GroupBy
import uk.co.nickthecoder.brentwood.util.Pager
import uk.co.nickthecoder.brentwood.util.respondHtmlTemplate2

/**
 * Determines the websites overall appearance.
 * Chooses the template (which is responsible for the overall structure of the page.
 * Also has minor features, such as how to render a [Pager], a [GroupBy] and a grid of thumbnails.
 */
interface Layout {

    fun createTemplate(fullWidth: Boolean, uri: String): PageTemplate
    fun createFullScreenTemplate(): PageTemplate

    fun pager(parent: FlowContent, pager: Pager<*>, pageNumber: Int)
    fun groupByInitials(parent: FlowContent, groupBy: GroupBy<*>)
    fun groupByAnchor(parent: FlowContent, groupBy: GroupBy<*>, initial: Char)

    /**
     * Adds a link, whose body will contain an image.
     * See also [textTool] (we need two methods, because they may be styled differently).
     * For nickthecoder.co.uk, we add "[" and "]" around [textTool], but not around [imageTool].
     *
     * Used this within the [PageTemplate.pageTools] section of the [PageTemplate].
     */
    fun imageTool(parent: FlowContent, href: String? = null, block: A.() -> Unit = {}): Unit =
        parent.a(href, classes = "toolImg", block = block)

    /**
     * Adds a link, whose body will be plain text.
     * See also [imageTool] (we need two methods, because they may be styled differently).
     * For nickthecoder.co.uk, we add "[" and "]" around [textTool], but not around [imageTool].
     *
     * Used this within the [PageTemplate.pageTools] section of the [PageTemplate].
     */
    fun textTool(parent: FlowContent, href: String? = null, block: A.() -> Unit = {}): Unit =
        parent.span("tool") {
            parent.a(href, block = block)
        }


    fun thumbnail(parent: FlowContent, icons:DIV.() -> Unit = {}, main: DIV.() -> Unit = {}, label: DIV.() -> Unit = {}): Unit =
        parent.div("thumbnailBox") {
            div("thumbnailContainer") {
                div("thumbnailWrapper") {
                    main()
                }
                div( "thumbnailIcons" ) {
                    icons()
                }
            }
            div("thumbnailLabel") {
                label()
            }
        }
    fun thumbnail(parent: FlowContent, main: DIV.() -> Unit = {}, label: DIV.() -> Unit = {}): Unit =
        parent.div("thumbnailBox") {
            div("thumbnailContainer") {
                div("thumbnailWrapper") {
                    main()
                }
            }
            div("thumbnailLabel") {
                label()
            }
        }

    fun thumbnails(parent: FlowContent, plain: Boolean = false, content: DIV.() -> Unit) {
        if (plain) {
            parent.div("thumbnails plain") {
                content()
            }
        } else {
            parent.div("thumbnails") {
                content()
            }
        }
    }

    suspend fun exception(call: ApplicationCall, cause: Throwable) {

        when (cause) {
            is NotFoundException -> {
                call.respondHtmlTemplate2(createTemplate(true, call.request.uri)) {
                    pageTitle {
                        +"Not Found : ${cause.message ?: ""}"
                    }
                    content {
                        h1 {
                            +"Not Found : ${cause.message ?: ""}"
                        }
                    }
                }
            }

            is BrentwoodException -> {
                call.respondHtmlTemplate2(createTemplate(true, call.request.uri)) {
                    pageTitle {
                        +"Error"
                    }
                    content {
                        h1 { +(cause.message ?: "Error") }
                    }
                }
            }

            else -> {
                // Unless commented out, this will print a stack trace for all exceptions thrown.
                cause.printStackTrace()

                call.respondHtmlTemplate2(createTemplate(true, call.request.uri)) {
                    pageTitle {
                        +"Error"
                    }
                    content {
                        h1 {
                            +"Error"
                        }
                    }
                }
            }
        }
    }

    fun route(route: Route) {
        route.install(StatusPages) {
            exception<Throwable> { cause ->
                exception(call, cause)
            }
        }
        // If no other routes match, throw an exception which will be picked up by in [StatusPage] feature.
        route.get("/{...}") {
            throw NotFoundException()
        }
    }
}

fun Route.layout( layout : Layout ) {
    layout.route(this)
}
