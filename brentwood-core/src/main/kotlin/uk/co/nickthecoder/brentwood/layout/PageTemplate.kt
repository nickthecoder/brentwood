package uk.co.nickthecoder.brentwood.layout

import io.ktor.html.*
import kotlinx.html.FlowContent
import kotlinx.html.HEAD
import kotlinx.html.HTML
import kotlinx.html.TITLE

abstract class PageTemplate : Template<HTML> {

    // HEAD SECTION
    val pageTitle = Placeholder<TITLE>()
    val extraHeader = Placeholder<HEAD>()

    // BODY SECTION

    /**
     * Used for extra controls e.g. Up, Thumbnails, Full-Size, Normal-Size in the Images Component.
     */
    val preContent = Placeholder<FlowContent>()

    /**
     * Typically a h1 tag with the title of the page
     */
    val pageHeading = Placeholder<FlowContent>()

    /**
     * Tools related to the [pageHeading], e.g. "Edit" and "Info" links in the Wiki Component.
     */
    val pageTools = Placeholder<FlowContent>()

    /**
     * The main content of the page
     */
    val content = Placeholder<FlowContent>()

    /**
     * At the bottom of the page (but not part of the PageTemplate defined footer)
     * Used for a Pager (i.e. the "Next" and "Prev" pages to change page number (e.g. in Search results).
     */
    val postContent = Placeholder<FlowContent>()

    /**
     * 2nd level navigation (typically a side-bar). Used by the Wiki Component to place the "navigation" page.
     */
    val navigation = Placeholder<FlowContent>()

}
