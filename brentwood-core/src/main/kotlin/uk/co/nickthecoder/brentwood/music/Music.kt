package uk.co.nickthecoder.brentwood.music

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.listFilesWithExtension
import uk.co.nickthecoder.brentwood.util.listOnlyDirectories
import uk.co.nickthecoder.brentwood.util.*
import java.io.File

class Music(

    layout: Layout,
    webPath: String = "/music",
    val baseDirectory: File

) : LayoutComponent(layout, webPath) {

    private val musicExtensions = mutableListOf("wav", "flac", "ogg", "mp3")

    var defaultGenre = "default"

    override fun route(route: Route) {

        with(route) {

            get("/") {
                showGenre(call, defaultGenre)
            }

            get("/{genre}") {
                val genre = call.safePart("genre")

                showGenre(call, genre)
            }

            get("/{genre}/{artist}") {
                val genre = call.safePart("genre")
                val artist = call.safePart("artist")

                showArtist(call, genre, artist)
            }

            get("/{genre}/{artist}/{album}") {
                val genre = call.safePart("genre")
                val artist = call.safePart("artist")
                val album = call.safePart("album")

                showAlbum(call, genre, artist, album)
            }

            get("/playMusic/{genre}/{artist}/{album}") {
                val genre = call.safePart("genre")
                val artist = call.safePart("artist")
                val album = call.safePart("album")
                val albumDir = File(File(File(baseDirectory, genre), artist), album)

                playAlbum(call, albumDir)
            }
            get("/playMusic/{genre}/{artist}/{album}/{sub...}") {
                val genre = call.safePart("genre")
                val artist = call.safePart("artist")
                val album = call.safePart("album")
                val sub = call.joinedSafeParts("sub")

                val track = File(File(File(File(baseDirectory, genre), artist), album), sub)
                playTrack(call, track)
            }

            // Returns actual files (including album coverts and the music
            get("/{genre}/{artist}/{album}/{sub...}") {
                val genre = call.safePart("genre")
                val artist = call.safePart("artist")
                val album = call.safePart("album")
                val sub = call.joinedSafeParts("sub")

                val file = File(File(File(File(baseDirectory, genre), artist), album), sub)
                if (file.exists() && file.isFile) {
                    call.respondFile(file)
                } else {
                    throw NotFoundException(file.path)
                }
            }
        }
    }

    private fun FlowContent.listGenres() {
        div("center") {
            ul("infoList") {
                for (child in baseDirectory.listOnlyDirectories()) {
                    li {
                        a("$webPath/${escapeURLParam(child.name)}") { +child.name }
                    }
                    +" "
                }
            }
        }
    }

    private suspend fun showGenre(call: ApplicationCall, genre: String) {

        val artistsDirectories = File(baseDirectory, genre).listOnlyDirectories()

        val groupBy = GroupBy(artistsDirectories) { it.name.first() }

        val uri = call.request.uri
        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            pageTitle { +"Music : $genre" }
            pageHeading { +"Artists" }

            preContent {
                listGenres()
                groupByLinks(groupBy, layout)
            }

            content {

                for (initial in groupBy.usedInitials()) {
                    groupByAnchor(groupBy, layout, initial)

                    for (artistDir in groupBy.itemsForInitial(initial)) {
                        val artist = artistDir.name

                        div("artist") {
                            h3 {
                                a("$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}") {
                                    title = "View Artist"
                                    +artist
                                }
                            }
                            layout.thumbnails(this) {
                                for (albumDir in artistDir.listOnlyDirectories()) {
                                    val album = albumDir.name
                                    layout.thumbnail(this, main = {
                                        a("$webPath/playMusic/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}") {
                                            title = "Click to Play"
                                            img(
                                                "cover",
                                                "$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}/.meta/cover_100.jpg"
                                            )
                                        }
                                    }) {
                                        a("$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}") {
                                            title = "View Album"
                                            +album
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }

    }

    private suspend fun showArtist(call: ApplicationCall, genre: String, artist: String) {
        val artistDir = File(File(baseDirectory, genre), artist)

        with(layout) {
            val uri = call.request.uri
            call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
                pageTitle { +artist }

                preContent {
                    listGenres()
                }

                pageHeading { +artist }
                content {

                    for (albumDir in artistDir.listOnlyDirectories()) {
                        val album = albumDir.name

                        h2 {
                            a("$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}") {
                                title = "View Album"
                                +album
                            }
                        }

                        album(genre, artist, album, albumDir)
                    }
                }
            }
        }
    }

    private suspend fun showAlbum(call: ApplicationCall, genre: String, artist: String, album: String) {
        val albumDir = File(File(File(baseDirectory, genre), artist), album)

        with(layout) {
            val uri = call.request.uri
            call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

                pageTitle { +"$artist - $album" }

                preContent {
                    listGenres()
                }

                pageHeading {
                    a("$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}") {
                        +artist
                    }
                }

                content {
                    h2 { +album }
                    album(genre, artist, album, albumDir)
                }

            }
        }
    }

    private fun FlowContent.album(genre: String, artist: String, album: String, albumDir: File) {
        div("albumCover") {
            a("$webPath/playMusic/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}") {
                title = "Click to Play"
                img(
                    "cover",
                    "$webPath/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}/.meta/cover_400.jpg"
                )
            }
        }

        div("trackListing") {
            ol {
                for (trackFile in albumDir.listFilesWithExtension(musicExtensions)) {
                    val trackName = trackFile.nameWithoutExtension
                    li {
                        a("$webPath/playMusic/${escapeURLParam(genre)}/${escapeURLParam(artist)}/${escapeURLParam(album)}/${escapeURLParam(trackFile.name)}") {
                            title = "Click to Play"
                            formatTrackName(trackName)
                        }
                    }
                }
            }
        }
    }

    private suspend fun playAlbum(call: ApplicationCall, albumDir: File) {

        val text = buildString {
            for (trackFile in albumDir.listFilesWithExtension(musicExtensions)) {
                appendLine(trackFile.path)
            }
        }
        val contentType = ContentType("audio", "x-mpegurl")
        call.response.header(
            HttpHeaders.ContentDisposition,
            "inline; filename=\"playlist.m3u8\""
        )
        call.respondText(text, contentType, HttpStatusCode.OK)
    }

    private suspend fun playTrack(call: ApplicationCall, track: File) {
        // Prevent malicious attempts to peek around the filesystem

        val contentType = ContentType("audio", "x-mpegurl")
        call.response.header(
            HttpHeaders.ContentDisposition,
            "inline; filename=\"playlist.m3u8\""
        )
        call.respondText(track.path + "\n", contentType, HttpStatusCode.OK)
    }

    private fun FlowContent.formatTrackName(track: String) {
        val stripped = regexCurlyBraces.replace(regexTrackNumber.replaceFirst(track, ""), "")
        var start = 0
        regexSquareBrackets.findAll(stripped).forEach {
            if (it.range.first > start) {
                +stripped.substring(start, it.range.first)
            }
            span("grey") { +it.value }
            start = it.range.last + 1
        }
        if (start < stripped.length) {
            +stripped.substring(start)
        }
    }
}

private val regexTrackNumber = Regex("^\\(?[0-9]*\\)? *")

private val regexCurlyBraces = Regex("\\{.*?}")

private val regexSquareBrackets = Regex("\\[.*?]")


