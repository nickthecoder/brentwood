package uk.co.nickthecoder.brentwood.search

import kotlinx.html.FlowContent
import kotlinx.html.b

// This code was copy/pasted from an old project that was written in Java.
// So there may well be lots of anti-patterns that Java code tends to have
// (which Kotlin tends to avoid).
// I have since kotlin-ified it somewhat.


/**
 * Creates a short summary of some text, with certain words highlighted.
 * We want to show the highlighted words within their context
 * (i.e. show a certain number of words before or after the highlighted word)
 *
 * This is used to display a summary for each search result.
 *
 * [content] is the full text of the document which we need to summarise.
 *
 * [analysedSearchWords] is the list of words the user search for,
 * AFTER begin put through the analyser.
 *
 * How it works :
 *
 * We split [content] into words, then pass each word through the analyser.
 * We can now compare each analysed word in the [content] with [analysedSearchWords]
 * to see if that word should be matched.
 *
 * We build a representation of the whole document as a list of either
 * a list of unmatched words, or a single matched word.
 *
 * Finally we can "compress" the unmatched words, removing long passages where
 * search terms are absent.
 */
fun FlowContent.buildSummary(content: String, config: SearchConfiguration, analysedSearchWords: List<String>) {

    val sections = mutableListOf<SummarySection>()

    var unMatchedWords = mutableListOf<String>()
    val words = content.split(Regex("\\s+"))
    var matchCount = 0

    // Build up a list of SummarySection objects. These are either a single
    // matched word,
    // or a set of 1 or more unmatched words.
    for (word in words) {
        if (analysedSearchWords.contains(config.analyzeWord(word))) {
            matchCount++
            if (unMatchedWords.size > 0) {
                sections.add(UnmatchedSummarySection(unMatchedWords))
                unMatchedWords = mutableListOf()
            }
            if (matchCount >= config.maxMatchedSections) {
                break
            }
            sections.add(MatchedSummarySection(word))
        } else {
            unMatchedWords.add(word)
        }
    }
    if (unMatchedWords.size > 0) {
        sections.add(UnmatchedSummarySection(unMatchedWords))
    }
    if (sections.size > 0) {
        val firstSection = sections.get(0)
        val lastSection = sections.get(sections.size - 1)
        for (section in sections) {
            if (section !== firstSection) {
                +" "
            }
            if (section.isMatched) {
                b { +section.compress(config, section === firstSection, section === lastSection) }
            } else {
                +section.compress(config, section === firstSection, section === lastSection)
            }
        }
    }
}


private interface SummarySection {

    val isMatched: Boolean

    fun compress(config: SearchConfiguration, isFirst: Boolean, isLast: Boolean): String
}

private class MatchedSummarySection(val text: String) : SummarySection {
    override val isMatched: Boolean
        get() = true

    override fun compress(config: SearchConfiguration, isFirst: Boolean, isLast: Boolean) = text
}

private class UnmatchedSummarySection(

    private val words: List<String>

) : SummarySection {

    override val isMatched: Boolean
        get() = false

    /**
     * Build up the text summary. In general the output will be the first n
     * words, followed by a separator, followed by the last m words.
     *
     * n = wordsAfterMatch, m = wordsBeforeMatch
     *
     * If word count < n + m, then all the words are added, and there is no
     * separator.
     *
     * The first summary section ignores the first n words. The last summary
     * section ignores the last m words.
     */
    override fun compress(config: SearchConfiguration, isFirst: Boolean, isLast: Boolean): String {
        var nowIsFirst = isFirst
        val buffer = StringBuffer()
        var firstEnd = 0
        var secondStart = 0
        var addSep = false
        val max =
            if (nowIsFirst) config.wordsBeforeMatch else if (isLast) config.wordsAfterMatch else config.wordsBeforeMatch + config.wordsAfterMatch + 2
        if (words.size > max) {
            addSep = true
            if (nowIsFirst && isLast) {
                // No keywords, so output more words than usual at the
                // beginning, and
                // non after the "...".
                firstEnd = config.wordsAfterMatch + config.wordsBeforeMatch
                secondStart = words.size
                // Pretend there is a keyword BEFORE us, so that the first
                // part IS used.
                nowIsFirst = false
            } else {
                firstEnd = config.wordsAfterMatch
                secondStart = words.size - config.wordsBeforeMatch
            }
        }

        // Check bounds. I think the only time this is needed is when
        // isFirst == isLast == true, and
        // the number wordCount > wordsAfterMatch and wordCount <
        // wordsAfterMatch + wordsBeforeMatch.
        if (firstEnd > words.size || secondStart < 0) {
            firstEnd = 0
            secondStart = 0
            addSep = false
        }

        // Useful for debugging.
        // buffer.append( " ((0.." + firstEnd +") & (" + secondStart + ".."
        // + _words.size() + ")) ");
        if (!nowIsFirst) {
            for (i in 0 until firstEnd) {
                buffer.append(words[i])
                buffer.append(" ")
            }
        }
        if (addSep) {
            buffer.append(config.separator)
        }
        if (!addSep || !isLast) {
            for (i in secondStart until words.size) {
                buffer.append(" ")
                buffer.append(words[i])
            }
        }
        return buffer.toString()
    }

}
