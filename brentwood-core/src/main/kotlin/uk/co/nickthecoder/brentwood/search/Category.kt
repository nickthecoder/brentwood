package uk.co.nickthecoder.brentwood.search

import uk.co.nickthecoder.brentwood.util.Filter
import java.net.URL

class Category(
    val id: String,
    val title: String,
    val filter: Filter<URL>
) {
}
