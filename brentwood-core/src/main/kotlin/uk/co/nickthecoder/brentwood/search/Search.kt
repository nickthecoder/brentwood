package uk.co.nickthecoder.brentwood.search

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlinx.html.*
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.lucene.document.Document
import org.apache.lucene.index.Term
import org.apache.lucene.search.BooleanClause.Occur
import org.apache.lucene.search.BooleanQuery
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.search.Query
import org.apache.lucene.search.TermQuery
import uk.co.nickthecoder.brentwood.BrentwoodException
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.escapeURLParam
import uk.co.nickthecoder.brentwood.util.respondHtmlTemplate2
import uk.co.nickthecoder.brentwood.util.unescapeURLParam
import uk.co.nickthecoder.brentwood.util.url
import java.io.PrintWriter
import java.io.Reader
import java.io.StringReader
import java.lang.Integer.min
import java.net.URL
import java.util.*


class Search(
    layout: Layout,
    webPath: String = "/search",
    private val config: SearchConfiguration

) : LayoutComponent(layout, webPath) {

    private val spiderInstance by lazy { Spider(config) }

    override fun route(route: Route) {

        with(route) {

            get("/") { search(call) }

            authenticate("auth-basic") {
                get("/info") { info(call) }
                get("/debug") { debug(call) }
                get("/list") { list(call) }

                route("/spider") {
                    get { spiderForm(call) }
                    post { startSpider(call) }
                }
            }

        }

    }


    private suspend fun spiderForm(call: ApplicationCall) {
        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Search Spider" }
            if (!spiderInstance.isRunning) {
                pageHeading { +"Start the Search Spider" }

                content {

                    p {
                        +"The current search index contains "
                        b { +"${config.reader().numDocs()}" }
                        +" pages."
                    }

                    form("$webPath/spider", method = FormMethod.post) {
                        div("buttons") {
                            input(InputType.submit, name = "start") {
                                value = "Start"
                            }
                        }
                    }
                }

            } else {
                pageHeading { +"The Spider is Running" }

                content {

                    p {
                        +"So far, the spider has scanned "
                        b { +"${spiderInstance.documentsAdded}" }
                        +" pages."
                    }

                    if (spiderInstance.isPurging) {
                        p { +"Purging out-of-date entries" }
                    }

                    p {
                        +"The current search index contains "
                        b { +"${config.reader().numDocs()}" }
                        +" pages."
                    }
                }

            }
        }
    }

    fun scanSinglePage(url: String) {
        spiderInstance.singlePage(url)
    }

    private suspend fun startSpider(call: ApplicationCall) {

        if (spiderInstance.isRunning) {
            throw BrentwoodException("The Spider is already running")
        }

        spiderInstance.fullScan()

        call.respondRedirect("$webPath/spider")
    }

    private suspend fun search(call: ApplicationCall) {
        val uri = call.request.uri
        val queryString = call.parameters["q"]
        val categoryIDs = call.parameters.getAll("category") ?: emptyList()
        val pageNumber = call.parameters["page"]?.toIntOrNull() ?: 1

        val queryTerms: List<String> = queryString?.split(Regex("\\s+"))
            ?.map { config.analyzeWord(it) ?: "" }
            ?.filter { it.isNotBlank() }
            ?: emptyList()

        val pager = queryString?.let {
            withContext(Dispatchers.IO) {

                val searcher = IndexSearcher(config.reader())
                val query = createQuery(queryString, categoryIDs)
                SearchPager(
                    searcher,
                    query,
                    config.resultsPerPage,
                    "${webPath}?q=${escapeURLParam(queryString)}",
                    pageNumber
                )
            }
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

            if (queryString == null) {
                pageTitle { +"Search" }
            } else {
                pageTitle { +"Search : $queryString" }
            }

            pageHeading { +"Search" }

            content {

                form(webPath) {
                    id = "searchForm"

                    div("form") {

                        input(type = InputType.text, name = "q") {
                            id = "largeSearchBox"
                            size = "50"
                            value = queryString ?: ""
                            autoFocus = true
                        }
                        br()
                        for (category in config.categories) {
                            input(type = InputType.checkBox, name = "category", classes = "category") {
                                id = "category_${category.id}"
                                value = category.id
                                checked = categoryIDs.contains(category.id)
                            }
                            label {
                                htmlFor = "category_${category.id}"
                                +category.title
                            }
                        }
                        div("buttons") {
                            input(type = InputType.submit, name = "go", classes = "button ok") {
                                value = "Search"
                            }
                        }
                    }

                }

                pager?.let { pager ->

                    div {
                        id = "searchResults"

                        h2 { +"Results" }

                        for (document in pager.pageItems(pageNumber)) {
                            val url = document.url()

                            div("searchResult") {
                                h3 {
                                    a(url.toString()) {
                                        +document.title()
                                    }
                                }
                                div("url") {
                                    +unescapeURLParam(url.toString())
                                    div("searchInfo") {
                                        a("$webPath/info?url=${escapeURLParam(url)}") {
                                            +"(info)"
                                        }
                                    }
                                }
                                buildSummary(document.content(), config, queryTerms)
                            }

                        }

                        layout.pager(this, pager, pageNumber)
                    }
                }
            }


        }
    }

    /**
     * Displays the search meta-data of a single page.
     * The page in question is given using the request parameter "url".
     * This can be the full url, or excluding the protocol,host and port (in which case the
     * full url will be reconstructed, before looking up the search database).
     */
    private suspend fun info(call: ApplicationCall) {
        withContext(Dispatchers.IO) {
            // The uri of THIS request.
            val uri = call.request.uri

            // The url of the page we want information about
            // If it is given without protocol host etc, then reconstruct the whole url.
            var url = call.parameters["url"] ?: ""
            if (url.startsWith("/")) {
                url = URL(call.request.origin.url(), url).toString()
            }
            val query = TermQuery(Term("url", url))

            val searcher = IndexSearcher(config.reader())
            val topDocs = searcher.search(query, 1)
            val scoreDocs = topDocs.scoreDocs
            if (scoreDocs.isEmpty()) {
                throw NotFoundException()
            }
            val document: Document = searcher.doc(scoreDocs[0].doc)

            val backQuery = TermQuery(Term("link", url))
            val backTopDocs = searcher.search(backQuery, 100)
            val backScoreDocs = backTopDocs.scoreDocs

            call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
                pageTitle { +"Search Info : ${document.title()}" }
                pageHeading {
                    +"Search Info : "
                    a(document.url()) {
                        +document.title()
                    }
                }

                content {

                    div("url") {
                        a(document.url()) {
                            +unescapeURLParam(url)
                        }
                    }
                    +"Last scanned : ${document.lastUpdatedString()}"

                    h3 { +"Categories" }
                    +document.getFields("category").joinToString(separator = " , ") { it.stringValue() }

                    h3 { +"Content" }
                    +(document.content())

                    h3 { +"Links from Other Pages" }
                    div("links") {
                        for (i in backScoreDocs.indices) {
                            val backDoc = searcher.doc(backScoreDocs[i].doc)
                            val backURL = backDoc.url()

                            div {
                                a(backURL) {
                                    +unescapeURLParam(backURL)
                                }
                                span("searchInfo") {
                                    a("$webPath/info?url=${backURL.encodeURLParameter()}") {
                                        +"(info)"
                                    }
                                }
                            }
                        }
                    }

                    h3 { +"Links to Other Pages" }
                    div("links") {
                        for (linkField in document.getFields("link")) {
                            val link = linkField.stringValue()

                            div {
                                a(link) {
                                    +unescapeURLParam(link)
                                }
                                span("searchInfo") {
                                    a("$webPath/info?url=${link.encodeURLParameter()}") {
                                        +"(info)"
                                    }
                                }
                            }

                        }
                    }

                }
            }
        }
    }

    private suspend fun list(call: ApplicationCall) {

        withContext(Dispatchers.IO) {
            call.respondOutputStream(ContentType.Text.Plain, HttpStatusCode.OK) {
                val out = PrintWriter(this)
                for (i in 0 until config.reader().numDocs()) {
                    val document = config.reader().document(i)
                    out.println(document.url())
                }
            }
        }

    }


    private suspend fun debug(call: ApplicationCall) {
        val uri = call.request.uri
        val page = call.parameters["page"]?.toIntOrNull() ?: 1
        val from = (page - 1) * config.resultsPerPage
        val to = min(from + config.resultsPerPage, config.reader().numDocs())

        withContext(Dispatchers.IO) {
            call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
                pageTitle { +"Search Debug" }
                pageHeading { +"Debug Search Database" }

                content {

                    for (i in from until to) {
                        val document = config.reader().document(i)

                        div("searchResult") {
                            h3 {
                                a(document.url()) {
                                    +(document.title())
                                }
                            }
                            div("url") {
                                +(document.url())
                                span("categories") {
                                    +document.getValues("category")
                                        .joinToString(prefix = "(", separator = " , ", postfix = ")")
                                }
                            }
                            div("content") {
                                val content = document.content()
                                if (content.length > 200) {
                                    +content.substring(0, 200)
                                    +"..."
                                } else {
                                    +content
                                }
                            }
                        }
                    }
                    br()
                    br()
                    div("center") {
                        a("$webPath/debug?page=${page + 1}") { +"Next Page" }
                    }
                }
            }
        }
    }


    private fun createQuery(queryString: String, categoryIDs: List<String>): Query {

        return if (categoryIDs.isNotEmpty()) {
            andQuery(createTextQuery(queryString), createCategoryQuery(categoryIDs))
        } else {
            createTextQuery(queryString)
        }

    }

    private fun createTextQuery(queryString: String): Query {
        return BooleanQuery.Builder().apply {

            val st = StringTokenizer(queryString)
            while (st.hasMoreTokens()) {
                var occur: Occur = Occur.SHOULD
                var word = st.nextToken()
                when {
                    word.startsWith("-") -> {
                        occur = Occur.MUST_NOT
                        word = word.substring(1)
                    }
                    word.startsWith("+") -> {
                        occur = Occur.MUST
                        word = word.substring(1)
                    }
                }

                analyzeWord(word)?.let { keyword ->
                    add(TermQuery(Term("content", keyword)), occur)

                    // If we are using MUST, then don't force the title to include this word.
                    val titleOccur = if (occur == Occur.MUST) Occur.SHOULD else occur
                    add(TermQuery(Term("title", keyword)), titleOccur)
                }
            }
        }.build()

    }

    private fun createCategoryQuery(categoryIDs: List<String>): Query {
        return BooleanQuery.Builder().apply {

            for (categoryCode in categoryIDs) {
                // NOTE. If we used Occur.MUST, and we ticked two categories,
                // it would only match pages with BOTH categories, but we want it
                // to match pages with EITHER category.
                add(TermQuery(Term("category", categoryCode)), Occur.SHOULD)
            }
        }.build()
    }

    private fun andQuery(vararg queries: Query?): BooleanQuery {
        return BooleanQuery.Builder().apply {
            for (q in queries) {
                add(q, Occur.MUST)
            }
        }.build()
    }

    private fun analyzeWord(word: String): String? {
        val reader: Reader = StringReader(word)
        var tokenStream: TokenStream? = null
        try {
            tokenStream = config.analyzer.tokenStream("content", reader)
            val charTermAttribute = tokenStream.addAttribute(CharTermAttribute::class.java)
            tokenStream.reset()
            while (tokenStream.incrementToken()) {
                return charTermAttribute.toString()
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        } finally {
            if (tokenStream != null) {
                tokenStream.end()
                tokenStream.close()
            }
            reader.close()
        }
        return null
    }


    fun backLinks(fromURL: String, maxResults: Int = 100): List<String> {

        val query = TermQuery(Term("link", fromURL))

        val searcher = IndexSearcher(config.reader())
        val topDocs = searcher.search(query, maxResults)
        val scoreDocs = topDocs.scoreDocs

        val result = mutableListOf<String>()
        for (scoreDoc in scoreDocs) {
            val doc = searcher.doc(scoreDoc.doc)
            result.add(doc["url"])
        }

        return result
    }

}

fun Document.url() = getOrDefault("url", "#")
fun Document.title() = getOrBlank("title")
fun Document.content() = getOrBlank("content")
fun Document.lastUpdated() = getField("lastUpdateStoreField")?.numericValue()?.let { Date(it.toLong()) }
fun Document.lastUpdatedString() = lastUpdated()?.toString() ?: ""

fun Document.getOrBlank(fieldName: String) = get(fieldName) ?: ""
fun Document.getOrDefault(fieldName: String, default: String) = get(fieldName) ?: default
