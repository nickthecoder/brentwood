package uk.co.nickthecoder.brentwood.search

import org.apache.lucene.analysis.Analyzer
import org.apache.lucene.analysis.TokenStream
import org.apache.lucene.analysis.en.EnglishAnalyzer
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.store.NIOFSDirectory
import uk.co.nickthecoder.brentwood.util.Filter
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.net.URL
import java.nio.file.Paths


class SearchConfiguration(

    /**
     * The directory holding the lucene database.
     */
    val dir: File,

    /**
     * The starting point for the spider. Often this is a single URL - the root of the web site.
     * Only pages that can be accessed via html a tags from these origins will be included
     * in the search database.
     */
    val origins: List<URL>,

    /**
     * Determines if a page is eligible for entry into the lucene database.
     * At a very minimum, you should reject urls from outside of your web site,
     * otherwise the Spider may attempt to index the whole of the internet!
     */
    val urlFilter: Filter<URL>,

    /**
     * When searching the web site, if may be useful to search only part of it.
     * For example, search only images, or search only music.
     * Each page can be placed into one or more categories (often only one)
     * based on the page's URL. For example all pages who's url is /gidea/music*
     * will be placed in the "music" category.
     */
    val categories: List<Category>,

    /**
     * The HTML element IDs of the page's main content.
     * Links within this div are added to the lucene meta-data for the page.
     * Links outside of this div will still be visited by the spider, but will NOT
     * appear in the meta-data.
     *
     * If this is empty, then all of the page is considered.
     *
     * NOTE [ignoredIDs] and [ignoredCssClass] "reject" filters are also applied.
     */
    val contentID: List<String>,

    /**
     * A list of HTML element IDs, whose contents should be ignored, but whose links are still followed.
     */
    val ignoredIDs: List<String>,

    /**
     * A css class name. Any text or links will be ignored.
     * I use "noSearch", and then add this css class to all tags which can be ignored by the Search Spider.
     */
    val ignoredCssClass: String?,

    val resultsPerPage: Int,

    /**
     * The Lucene [Analyzer]. This should be specific to your language.
     * The default being [EnglishAnalyzer].
     */
    val analyzer: Analyzer = EnglishAnalyzer(),

    /**
     * Stop after n words have been matched.
     */
    val maxMatchedSections: Int = 5,

    /**
     * The number of words to output before a matched word. If there are more
     * than `wordsBeforeMatch + wordsAfterMath` between matched
     * words, then an ellipsis replaces the words that are excluded.
     */
    val wordsBeforeMatch: Int = 10,

    /**
     * The number of words to output after a matched word
     */
    val wordsAfterMatch: Int = 10,

    /**
     * The string used in place of missed words. The default is an ellipsis ….
     */
    val separator: String = "…"

) {

    val directory = NIOFSDirectory(Paths.get(dir.path));

    private var reader: DirectoryReader? = null

    fun reader(): DirectoryReader {
        fun openReader(): DirectoryReader {
            return DirectoryReader.open(directory).apply {
                reader = this
            }
        }

        return reader ?: openReader()
    }

    fun closeReader() {
        reader?.close()
        reader = null
    }

    fun matches(url: URL): List<Category> {
        val result = mutableListOf<Category>()
        for (category in categories) {
            if (category.filter.accept(url)) {
                result.add(category)
            }
        }
        return result
    }

    fun analyzeWord(word: String): String? {
        val reader: Reader = StringReader(word)
        var tokenStream: TokenStream? = null
        try {
            tokenStream = analyzer.tokenStream("content", reader)
            val charTermAttribute: CharTermAttribute = tokenStream.addAttribute(CharTermAttribute::class.java)
            tokenStream.reset()
            while (tokenStream.incrementToken()) {
                return charTermAttribute.toString()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        } finally {
            try {
                if (tokenStream != null) {
                    tokenStream.end()
                    tokenStream.close()
                }
                reader.close()
            } catch (e: Exception) {
                // Do nothing
                e.printStackTrace()
            }
        }
        return null
    }
}
