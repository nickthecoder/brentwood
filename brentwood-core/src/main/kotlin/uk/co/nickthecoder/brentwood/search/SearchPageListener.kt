package uk.co.nickthecoder.brentwood.search

import uk.co.nickthecoder.brentwood.PageListener

class SearchPageListener(val search: Search, val prefix: String) : PageListener {
    override fun pageChanged(localURL: String) {
        search.scanSinglePage(prefix + localURL)
    }
}
