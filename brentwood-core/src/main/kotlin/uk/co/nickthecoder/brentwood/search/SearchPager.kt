package uk.co.nickthecoder.brentwood.search

import org.apache.lucene.document.Document
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.search.Query
import org.apache.lucene.search.TopDocs
import org.apache.lucene.search.TopScoreDocCollector
import uk.co.nickthecoder.brentwood.util.Pager

/**
 * NOTE. I upgraded to a more recent version of Lucene, and I'm now using a simpler TopScoreDocCollector,
 * which may not be appropriate for large result sets. However, it fits my needs, so I haven't bothered to
 * try to use the new API for "proper" paged collectors.
 *
 * In particular, if a query produces many results, and you click to the "last page", then a HUGE array is
 * created (populated with sentinel values).
 * This isn't a problem for my use case, but this isn't a good pattern in general. Sorry. You've been warned!
 */
class SearchPager(
    private val searcher: IndexSearcher,
    query: Query,
    private val resultsPerPage: Int,
    override val href: String,
    pageNumber: Int,
    override val pageParameter: String = "page"

) : Pager<Document> {

    // Not good for large results. See class's documentation.
    private val collector = TopScoreDocCollector.create(resultsPerPage * pageNumber, resultsPerPage * pageNumber)

    init {
        searcher.search(query, collector)
    }

    override fun pageItems(pageNumber: Int): List<Document> {
        val topDocs: TopDocs = collector.topDocs((pageNumber - 1) * resultsPerPage, resultsPerPage)
        val scoreDocs = topDocs.scoreDocs
        return scoreDocs.map { searcher.doc(it.doc) }
    }

    override fun pageCount() = collector.totalHits / resultsPerPage

}
