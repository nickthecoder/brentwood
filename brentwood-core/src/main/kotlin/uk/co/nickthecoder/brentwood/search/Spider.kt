package uk.co.nickthecoder.brentwood.search

import org.apache.commons.text.StringEscapeUtils
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.protocol.HttpClientContext
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.lucene.document.*
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.index.Term
import org.apache.lucene.search.Query
import org.htmlcleaner.ContentNode
import org.htmlcleaner.HtmlCleaner
import org.htmlcleaner.HtmlNode
import org.htmlcleaner.TagNode
import java.io.InputStream
import java.net.URL
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.ConcurrentLinkedQueue
import java.util.concurrent.LinkedBlockingQueue
import kotlin.collections.HashSet

/**
 * Creates a background thread, which performs two types of task :
 * [fullScan] and [singlePage] sequentially.
 *
 * [fullScan] visits all pages on the web site, parsing the HTML and storing the results using lucene.
 * We start from a set of "origins". Typically this will be the web site's "/".
 *
 * We parse the HTML, looking for all a tags, adding each to the "pending" list of places to visit.
 * When the pending list is empty, we are done!
 *
 * [singlePage] updates the search index for a single page of this web site.
 *
 */
class Spider(

    private val config: SearchConfiguration

) {
    /**
     * A Thread safe queue. The background thread takes entries from the queue.
     * [fullScan] and [singlePage] adds new entries to the queue (via [Queue.offer])
     */
    private val queue: BlockingQueue<Runnable> = LinkedBlockingQueue()

    var isRunning: Boolean = false
        private set

    var isPurging = false
        private set

    /**
     * A list of URL yet to be processed by [fullScan].
     */
    private val pending = HashSet<URL>()

    /**
     * A list of URLs that have already been processed by [fullScan], so that it doesn't
     * add the same url to the [pending] again, after it has already been processed.
     */
    private val processed = HashSet<URL>()

    /**
     * A simple counter, so that we can see progress on the "spider" web page.
     */
    var documentsAdded = 0
        private set

    /**
     * A single lucene [IndexWriter]. It is never closed.
     * At the end of each [fullScan] and [singlePage], the writer is committed.
     */
    private val writer = IndexWriter(
        config.directory,
        IndexWriterConfig(config.analyzer).apply {
            openMode = IndexWriterConfig.OpenMode.CREATE_OR_APPEND
        }
    )

    private val thread = Thread {
        while (true) {
            queue.take().run()
        }
    }.apply {
        name = "SpiderThread"
        isDaemon = true
    }.start()

    /**
     * Process a single page, without following any of the links.
     * When editing a wiki page, call this to update the index for that page.
     *
     * The processing is performed in a background thread, and this method returns without waiting.
     * If a [fullScan] is in progress (or queued prior to this [singlePage]), then it may take a while
     * for the changes to be reflected in the index.
     */
    fun singlePage(url: String) {
        queue.put(Runnable {
            try {
                processURL(URL(url))
            } finally {
                writer.commit()
                config.closeReader()
            }
        })
    }

    /**
     * Updates the index, by visiting every page of the web site.
     *
     * The processing is performed in a background thread, and this method returns without waiting.
     *
     * As an indication of speed, my development laptop scans 3,000 pages in about 1-2 minutes. YMMV.
     */
    fun fullScan() {
        // Prevent two scans being queue.
        if (isRunning) return

        isRunning = true

        queue.put(Runnable {
            try {
                val startTime = Date().time
                pending.addAll(config.origins)

                while (pending.isNotEmpty()) {
                    val i = pending.iterator()
                    while (i.hasNext()) {
                        try {
                            val url = i.next()
                            i.remove()
                            try {
                                processURL(url)
                            } catch (e: Exception) {
                                System.err.println("** Error while scanning $url")
                                e.printStackTrace()
                            }
                        } catch (e: ConcurrentModificationException) {
                            // Break - The while loop will kick in and a new
                            // Iterator object will be created.
                            break
                        }
                    }
                }

                isPurging = true
                purge(startTime)


            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                isPurging = false
                writer.commit()
                // By closing the reader, future requests will open a new reader, so deleted documents will no
                // longer be visible.
                config.closeReader()
                isRunning = false
            }
        })
    }

    private fun processURL(url: URL) {
        try {
            processed.add(url)

            val httpGet = HttpGet(url.toString())
            val httpClient: CloseableHttpClient = HttpClients.createDefault()
            val context: HttpClientContext = HttpClientContext.create()
            httpClient.execute(httpGet, context)

            // If we are bing redirected, use the redirected url, and abandon this one.
            val redirectURIs = context.redirectLocations
            if (redirectURIs != null && redirectURIs.isNotEmpty()) {
                processURL(redirectURIs[redirectURIs.size - 1].toURL())
                return
            }

            httpClient.execute(httpGet).use { response ->
                if (response.statusLine.statusCode != 200) return

                val entity = response.entity
                if (entity == null) {
                    error("No Entity for $url")
                } else {
                    val contentType = entity.contentType
                    if (contentType != null) {
                        val mimeType: String = contentType.value.split(";")[0].trim()
                        if (mimeType == "text/html") {
                            val input = entity.content
                            try {
                                parseHTML(url, input)
                            } catch (e: Exception) {
                                e.printStackTrace()
                                error("Failed to read/store $url")
                            } finally {
                                input.close()
                            }
                        }
                    }
                }
            }

        } catch (e: Exception) {
            System.err.println("Error processing $url\n    $e")
            //e.printStackTrace()
        }
    }

    private fun parseHTML(url: URL, input: InputStream) {

        val contentIDs = config.contentID
        val ignoredIDs = config.ignoredIDs
        val ignoredCssClass = config.ignoredCssClass

        val buffer = StringBuilder()
        val links = mutableSetOf<URL>()
        val contentLinks = mutableSetOf<URL>()
        var title = ""
        // While walking the tree, set when we enter a tag who's id id contentID
        // Reset when exiting that tag. The tag will usually be a DIV tag, which
        // holds the main content of the page. i.e. it does NOT contain navigation,
        // nor the page header and footer.
        var inContentCount = if (contentIDs.isEmpty()) 1 else 0
        var inIgnoredCount = 0

        val rootNode = HtmlCleaner().clean(input)

        rootNode.walk({ _, htmlNode ->
            // Exit
            if (htmlNode is TagNode) {
                val id = htmlNode.getAttributeByName("id")
                if (contentIDs.contains(id)) {
                    inContentCount--
                }
                if (ignoredIDs.contains(id)) {
                    inIgnoredCount--
                }

                if (ignoredCssClass != null) {
                    htmlNode.getAttributeByName("class")?.split(" ")?.let { cssClasses ->
                        if (cssClasses.contains(ignoredCssClass)) {
                            inIgnoredCount--
                        }
                    }
                }
            }

        }) { parentNode, htmlNode ->
            // Enter
            if (htmlNode is TagNode) {
                val id = htmlNode.getAttributeByName("id")
                if (contentIDs.contains(id)) {
                    inContentCount++
                }
                if (ignoredIDs.contains(id)) {
                    inIgnoredCount++
                }

                if (ignoredCssClass != null) {
                    htmlNode.getAttributeByName("class")?.split(" ")?.let { cssClasses ->
                        if (cssClasses.contains(ignoredCssClass)) {
                            inIgnoredCount++
                        }
                    }
                }
                val tagName = htmlNode.name
                if ("a" == tagName) {
                    var href = tidyHTML(htmlNode.getAttributeByName("href"))
                    // Remove #... from the end of the url if it is present.
                    val hash = href.indexOf('#')
                    if (hash > 0) {
                        href = href.substring(0, hash)
                    }
                    if (href.isNotBlank() && !href.startsWith("#")) {
                        val linkURL = URL(url, href)
                        if (config.urlFilter.accept(linkURL)) {
                            links.add(linkURL)
                            if (inContentCount > 0 && inIgnoredCount == 0) {
                                contentLinks.add(linkURL)
                            }
                        }
                    }
                }
            } else if (htmlNode is ContentNode) {
                if ("title" == parentNode.name) {
                    title = tidyHTML(htmlNode.content)
                } else {
                    // Only record text within the "content" area of the page. Ignore the header/footer/navigation.
                    if (inContentCount > 0 && inIgnoredCount == 0) {
                        // We need to add whitespace between html blocks that have no whitespace between them.
                        // Excess whitespace will be eliminated at the end.
                        buffer.append(" ")
                        buffer.append(tidyHTML(htmlNode.content))
                    }
                }
            }
        }

        // We have now parsed the HTML.
        val content = buffer.toString().replace(Regex("\\s\\s+"), " ")
        store(url, title, content, contentLinks)
        for (link in links) {
            if (!processed.contains(link)) {
                //info("Adding $link")
                pending.add(link)
            }
        }

        documentsAdded++
    }

    private fun tidyHTML(html: String?): String {
        return if (html == null) {
            ""
        } else {
            StringEscapeUtils.unescapeHtml3(html.trim())
        }
    }

    private fun store(url: URL, title: String, content: String, links: Set<URL>) {

        val doc = Document()
        val urlString = url.toString()

        doc.add(TextField("title", title, Field.Store.YES))
        doc.add(TextField("content", content, Field.Store.YES))

        // Non-tokenized fields :
        doc.add(StringField("url", urlString, Field.Store.YES))
        doc.add(LongPoint("lastUpdate", Date().time)) // This is queryable, but not retrievable
        doc.add(StoredField("lastUpdateStoreField", Date().time)) // This retrievable, but not queryable

        for (link in links) {
            doc.add(StringField("link", link.toString(), Field.Store.YES))
        }

        for (category in config.categories) {
            if (category.filter.accept(url)) {
                doc.add(StringField("category", category.id, Field.Store.YES))
            }
        }

        writer.updateDocument(Term("url", urlString), doc)
    }

    /**
     * Removes all documents from the index who's lastUpdate is smaller than [time].
     * We can start the spider running, and after it has finished purge all documents
     * that were not updated while the spider was running.
     * This will remove any documents not encountered (or deliberately skipped over)
     * by the spider.
     */
    private fun purge(time: Long) {

        try {
            val reader = DirectoryReader.open(writer)
            val numDocs = reader.numDocs()
            for (i in 0 until numDocs) {
                if (reader.document(i).get("url").isNullOrBlank()) {
                    writer.tryDeleteDocument(reader, i)
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        try {
            val query: Query = LongPoint.newRangeQuery("lastUpdate", 0L, time - 1)
            writer.deleteDocuments(query)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun TagNode.walk(endVisitor: (TagNode, HtmlNode) -> Unit, visitor: (TagNode, HtmlNode) -> Unit) {
        if (parent != null) {
            visitor(parent, this)
        }
        for (child in this.allChildren) {
            when (child) {
                is TagNode -> {
                    child.walk(endVisitor, visitor)
                }
                is ContentNode -> {
                    visitor(this, child)
                }
            }
        }
        if (parent != null) {
            endVisitor(parent, this)
        }
    }

}
