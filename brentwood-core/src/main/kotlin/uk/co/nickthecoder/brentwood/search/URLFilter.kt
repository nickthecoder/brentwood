package uk.co.nickthecoder.brentwood.search

import uk.co.nickthecoder.brentwood.util.Filter
import java.net.URL

class HostFilter(val hostname: String) : Filter<URL> {
    override fun accept(item: URL) = item.host == hostname
}

class RegexURLPathFilter(val regex: Regex) : Filter<URL> {
    constructor(regex: String) : this(Regex(regex))

    override fun accept(item: URL) = regex.matches(item.path)
}

class RegexURLPathAndQueryFilter(val pathRegex: Regex, val queryRegex: Regex) : Filter<URL> {
    constructor(pathRegex: String, queryRegex: String) : this(Regex(pathRegex), Regex(queryRegex))

    override fun accept(item: URL): Boolean {
        if (!pathRegex.matches(item.path)) {
            return false
        }
        val q = item.query ?: return false
        return queryRegex.matches(q)
    }
}

class RegexURLFilter(val regex: Regex) : Filter<URL> {
    constructor(regex: String) : this(Regex(regex))

    override fun accept(item: URL) = regex.matches(item.toString())
}
