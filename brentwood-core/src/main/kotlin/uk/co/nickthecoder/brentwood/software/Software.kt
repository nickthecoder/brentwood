package uk.co.nickthecoder.brentwood.software

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.files.Files
import uk.co.nickthecoder.brentwood.files.FilesConfig
import uk.co.nickthecoder.brentwood.files.fileOrDirectory
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.layout.PageTemplate
import uk.co.nickthecoder.brentwood.util.respondHtmlTemplate2
import uk.co.nickthecoder.brentwood.util.safePathParts
import uk.co.nickthecoder.brentwood.util.staticResources
import java.io.File

/**
 * A catalogue of my software projects.
 * The home page lists all software with a summary of _README.md_ and an optional thumbnail image.
 *
 * Each project has it own page, which displays the whole of the _README.md_.
 * From here, you can browse the code base, view the API documentation (Dokka/Javadoc),
 * visit the _download_ page (if there are pre-compiled binaries).
 */
class Software(

    layout: Layout,
    webPath: String = "/software",
    private val config: SoftwareConfiguration

) : LayoutComponent(layout, webPath) {

    override fun route(route: Route) {
        with(route) {
            get("/") { softwareHome(call) }
            staticResources("/resources", Files::class.java.packageName)

            val projects = mutableSetOf<SoftwareProject>()
            for (category in config.categories) {
                projects.addAll(category.projects)
            }

            for (project in projects) {

                get("/${project.id}") { project(call, project) }

                get("/${project.id}/files/{path...}") {
                    val pathParts = call.safePathParts("path")
                    fileOrDirectory(
                        call,
                        layout,
                        FilesConfig(
                            "$webPath/${project.id}/files", project.filesDirectory,
                            project.label, "$webPath/${project.id}",
                            rawText = false
                        ),
                        pathParts
                    )
                }

                if (project.hasAPI) {
                    get("/${project.id}/api/{path...}") {
                        val pathParts = call.safePathParts("path")
                        fileOrDirectory(
                            call,
                            layout,
                            FilesConfig(
                                "$webPath/${project.id}/api", project.apiDirectory,
                                project.label, "$webPath/${project.id}"
                            ),
                            pathParts
                        )
                    }
                }

                if (project.hasDownloads) {
                    get("/${project.id}/download/{path...}") {
                        val pathParts = call.safePathParts("path")
                        fileOrDirectory(
                            call,
                            layout,
                            FilesConfig(
                                "$webPath/${project.id}/download", project.downloadDirectory,
                                project.label, "$webPath/${project.id}"
                            ),
                            pathParts
                        )
                    }
                }
            }
        }
    }

    private suspend fun softwareHome(call: ApplicationCall) {
        val showDeprecated: Boolean = call.parameters["deprecated"] != null
        val uri = call.request.uri
        call.respondHtmlTemplate2(layout.createTemplate(fullWidth = false, uri)) {
            pageTitle { + "Software" }
            pageHeading { + "Software" }
            pageTools {
                if (showDeprecated) {
                    layout.textTool(this, "${webPath}/") { + "Hide Old Projects" }
                } else {
                    layout.textTool(this, "${webPath}/?deprecated=1") { + "Show All" }
                }
            }

            navigation {
                for (category in config.categories) {
                    val projects =
                        if (showDeprecated) category.projects else category.projects.filter { ! it.deprecated }
                    if (projects.isNotEmpty()) {
                        h2 {
                            style = "font-size: 16px;"
                            + category.label
                        }
                        div("menu") {
                            for (project in projects) {
                                a("$webPath/${project.id}") { + project.label }
                            }
                        }
                    }
                }
            }

            content {
                for (category in config.categories) {

                    val projects =
                        if (showDeprecated) category.projects else category.projects.filter { ! it.deprecated }
                    if (projects.isNotEmpty()) {
                        h2 { + category.label }
                        for (project in projects) {
                            h3 { a("$webPath/${project.id}") { + project.label } }
                            div("editSection editSection2") {
                                tools(project)
                            }
                            if (project.thumbnailFile.exists()) {
                                div("floatLeft") {
                                    a("$webPath/${project.id}") {
                                        img("thumbnail", "$webPath/${project.id}/files/thumbnail.png")
                                    }
                                }
                            }
                            div("markdown") {
                                unsafe { + project.summary(config.markdownFlavour) }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun project(call: ApplicationCall, project: SoftwareProject) {
        val uri = call.request.uri
        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            extraHeader {
                // Ensure links within the markdown resolve correctly.
                base { href = "${webPath}/${project.id}/files/" }
            }
            pageTitle { + project.label }
            pageHeading { + project.label }

            pageTools(project)

            content {
                div("markdown") {
                    unsafe { + project.readme(config.markdownFlavour) }
                }
            }
        }
    }

    private fun FlowContent.tools(project: SoftwareProject) {
        layout.textTool(this, "$webPath/${project.id}/files/") { + "files" }
        if (project.hosted != null) {
            layout.textTool(this, project.hosted) {
                if (project.hosted.contains("hithub")) {
                    + "github"
                } else {
                    + "gitlab"
                }
            }
        }
        if (project.hasAPI) {
            layout.textTool(this, "$webPath/${project.id}/api/${project.latestVersion}") { + "api" }
        }
        if (project.hasDownloads) {
            layout.textTool(this, "$webPath/${project.id}/download/") { + "download" }
        }
    }

    private fun PageTemplate.pageTools(project: SoftwareProject) {
        pageTools {
            tools(project)
        }
    }
}
