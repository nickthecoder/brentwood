package uk.co.nickthecoder.brentwood.software

import org.intellij.markdown.flavours.gfm.GFMFlavourDescriptor

class SoftwareConfiguration(
    val categories: List<SoftwareCategory>
) {
    var markdownFlavour = GFMFlavourDescriptor() //CommonMarkFlavourDescriptor()
}

class SoftwareCategory(
    val label: String,
    projects: List<SoftwareProject>
) {
    val projects = projects.sortedBy { it.label }
}
