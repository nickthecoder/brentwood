package uk.co.nickthecoder.brentwood.software

import org.intellij.markdown.IElementType
import org.intellij.markdown.flavours.MarkdownFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import java.io.File

data class SoftwareProject(
    val id: String,
    val label: String,
    val directory: File,
    /**
     * A link to external hosting (such as gitlab.com or github.com).
     */
    val hosted: String? = null,
    val deprecated: Boolean = false
) {

    val filesDirectory = File(directory, id)

    val thumbnailFile = File(filesDirectory, "thumbnail.png")

    val apiDirectory = File(directory, "api")
    val hasAPI = apiDirectory.exists()
    val latestVersion = apiDirectory.list()?.maxOrNull() ?: ""

    val downloadDirectory = File(directory, "download")
    val hasDownloads = downloadDirectory.exists()

    private var markdownFlavour: MarkdownFlavourDescriptor? = null
    private var readme: String? = null
    private var summary: String? = null

    /**
     * Converts the README.md file to HTML.
     * The result is cached.
     */
    fun readme(markdownFlavour: MarkdownFlavourDescriptor): String {
        if (markdownFlavour != this.markdownFlavour) {
            readme = null
            summary = null
        }
        readme?.let { return it }
        this.markdownFlavour = markdownFlavour

        val file = File(File(directory, id), "README.md")
        if (! file.exists()) {
            readme = ""
            return ""
        }

        val lines = file.readText().split("\n")
        var builder = StringBuilder()
        var lookingForHeader = true
        for (line in lines) {
            if (lookingForHeader && line.startsWith("#")) {
                lookingForHeader = false
            } else if (lookingForHeader && (line.startsWith("--") || line.startsWith("=="))) {
                // Throw away the previous line (it was a title)
                builder = StringBuilder()
                lookingForHeader = false
            } else {
                builder.append(line)
                builder.append("\n")
            }
        }
        val markdown = builder.toString()
        val parsedTree = MarkdownParser(markdownFlavour).parse(IElementType("ROOT"), markdown)
        val html = HtmlGenerator(markdown, parsedTree, markdownFlavour).generateHtml()
        readme = html
        return html
    }

    /**
     * Creates HTML for the `summary` of README.md.
     * A summary consists of all text, excluding heading, up until the first blank line.
     * The results are cached.
     */
    fun summary(markdownFlavour: MarkdownFlavourDescriptor): String {
        if (markdownFlavour != this.markdownFlavour) {
            readme = null
            summary = null
        }
        summary?.let { return it }
        this.markdownFlavour = markdownFlavour

        val file = File(File(directory, id), "README.md")
        if (! file.exists()) {
            summary = ""
            return ""
        }
        val lines = file.readText().split("\n")
        var builder = StringBuilder()
        for (line in lines) {
            if (line.isBlank()) {
                if (builder.isNotEmpty()) {
                    break
                }
            } else if (line.startsWith("#")) {
                // Ignore the title
            } else if (line.startsWith("--") || line.startsWith("==")) {
                // Throw away the previous line (it was a title)
                builder = StringBuilder()
            } else {
                builder.append(line)
                builder.append("\n")
            }
        }

        val markdown = builder.toString()
        val parsedTree = MarkdownParser(markdownFlavour).parse(IElementType("ROOT"), markdown)
        val html = HtmlGenerator(markdown, parsedTree, markdownFlavour).generateHtml()
        summary = html
        return html
    }

    init {
        if (! directory.exists()) {
            println("WARNING : Software '$id' not found : '$directory'")
        }
        val readmeFile = File(File(directory, id), "README.md")
        if (! readmeFile.exists()) {
            println("WARNING : README.md for software '$id' not found : '$readmeFile'")
        }
    }

}

