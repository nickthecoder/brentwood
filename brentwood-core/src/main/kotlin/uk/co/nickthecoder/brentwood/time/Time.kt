package uk.co.nickthecoder.brentwood.time

import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import uk.co.nickthecoder.brentwood.Component
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Outputs the current date and time as plain text.
 * This is useful for an IoT (internet of things) device that needs a simple way to
 * update its clock on a periodic basis.
 *
 * The default format is spread over 3 lines :
 *
 *     hh:mm:ss
 *     yyyy-mm-dd
 *     timezone     (+ or -, then a 2 digit hour offset from GMT)
 */
class Time(
    webPath: String = "/time",
    val dateFormat: DateFormat = SimpleDateFormat("HH:mm:ss\nyyyy-MM-dd\nX")
) : Component(webPath) {

    override fun route(route: Route) {

        route.get("/") {
            call.response.headers.append(HttpHeaders.CacheControl, "no-cache, no-store")
            time(call)
        }
    }

    private suspend fun time(call: ApplicationCall) {
        val now = Date()
        call.respondText(dateFormat.format(now))
    }

}
