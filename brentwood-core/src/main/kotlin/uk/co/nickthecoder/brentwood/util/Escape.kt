package uk.co.nickthecoder.brentwood.util

import kotlinx.html.*
import java.net.URLDecoder
import java.net.URLEncoder
import java.nio.charset.StandardCharsets

private val UTF8 = StandardCharsets.UTF_8.toString()

/**
 * Using [URLEncoder.encode], but it escapes a space using "+", and I prefer to use "%20"
 * AFAIK both are acceptable, but KTOR doesn't unescape "+", whereas it does unescape "%20C".
 * Also, web browser automatically show %20C as spaces, but show "+" as "+".
 */
fun escapeURLParam(param: String): String = URLEncoder.encode(param, UTF8).replace("+", "%20")
fun unescapeURLParam(param: String): String = URLDecoder.decode(param, UTF8)//.replace("+", "%20")

fun escapeURLPathParams(parts: List<String>): String = parts.joinToString(separator = "/") { escapeURLParam(it) }

fun escapeChildURLPathParams(parentParts: List<String>, child: String) = if (parentParts.isEmpty()) {
    escapeURLParam(child)
} else {
    "${parentParts.joinToString(separator = "/") { escapeURLParam(it) }}/${escapeURLParam(child)}"
}

fun ByteArray.toHex(): String = joinToString(separator = "") { eachByte -> "%02x".format(eachByte) }

fun javascriptString(str: String) = "'" + str.replace("'", "\\'") + "'"

/**
 * Strips leading spaces from all lines in a javascript source code.
 */
fun String.shrinkJavascript() = this.split("\n").joinToString(separator = "\n") { it.trim() }

@HtmlTagMarker
fun FlowOrMetaDataOrPhrasingContent.inlineScript(src: String): Unit =
    SCRIPT(emptyMap(), consumer).visit { comment(src) }

@HtmlTagMarker
fun FlowOrMetaDataOrPhrasingContent.script(src: String) = script(src = src) {}
