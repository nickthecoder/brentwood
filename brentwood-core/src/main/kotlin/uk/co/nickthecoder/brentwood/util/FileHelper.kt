package uk.co.nickthecoder.brentwood.util

import java.io.File

fun File.listOnlyFiles() =
    this.listFiles { child: File ->
        child.isFile && !child.isHidden
    }?.sorted() ?: emptyList()

fun File.listFilesWithExtension(extensions: List<String>) =
    this.listFiles { child: File ->
        child.isFile && !child.isHidden && extensions.contains(child.extension.lowercase())
    }?.sorted() ?: emptyList()

fun File.listOnlyDirectories() =
    this.listFiles { child: File ->
        child.isDirectory && !child.isHidden
    }?.sorted() ?: emptyList()
