package uk.co.nickthecoder.brentwood.util

interface Filter<T> {
    fun accept(item: T): Boolean

    operator fun not() = NotFilter(this)

    infix fun and(other: Filter<T>) = AndFilter(listOf(this, other))
    infix fun or(other: Filter<T>) = OrFilter(listOf(this, other))
}

class NotFilter<T>(val condition: Filter<T>) : Filter<T> {
    override fun accept(item: T) = !condition.accept(item)
}

class AndFilter<T>(val children: List<Filter<T>>) : Filter<T> {
    override fun accept(item: T): Boolean {
        for (child in children) {
            if (!child.accept(item)) return false
        }
        return true
    }

    override infix fun and(other: Filter<T>) = AndFilter(children.toMutableList().apply { add(other) })
}


class OrFilter<T>(val children: List<Filter<T>>) : Filter<T> {
    override fun accept(item: T): Boolean {
        for (child in children) {
            if (child.accept(item)) return true
        }
        return false
    }

    override infix fun or(other: Filter<T>) = OrFilter(this.children.toMutableList().apply { add(other) })

}

