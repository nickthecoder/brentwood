package uk.co.nickthecoder.brentwood.util

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.layout.Layout

/**
 * Helps to group a list of items by their initial letter.
 * The UI will have a list of all the initial letters, and clicking one
 * will jump to the relevant part of the page (using named anchor tags).
 *
 * The doesn't actually create the HTML, that is left to the [Layout]
 */
class GroupBy<T>(allItems: List<T>, firstLetter: (T) -> Char) {

    private val itemsByInitial = mutableMapOf<Char, MutableList<T>>()

    init {
        // Prime the map with empty lists for A..Z
        for (c in 'A'..'Z') {
            itemsByInitial[c] = mutableListOf()
        }
        for (item in allItems) {
            val initial = firstLetter(item).uppercaseChar()
            var list = itemsByInitial[initial]
            if (list == null) {
                list = mutableListOf()
                itemsByInitial[initial] = list
            }
            list.add(item)
        }
    }

    /**
     * Are there items with the given initial [letter]?
     */
    fun isUsed(letter: Char): Boolean = itemsByInitial[letter]?.isNotEmpty() ?: false

    /**
     * A list of used letters.
     */
    fun usedInitials(): List<Char> = itemsByInitial.filter { it.value.isNotEmpty() }.map { it.key }.sorted()

    fun allInitials(): List<Char> = itemsByInitial.keys.sorted()

    fun itemsForInitial(c: Char) = itemsByInitial[c]!!
}

fun FlowContent.groupByLinks(groupBy: GroupBy<*>, layout: Layout) = layout.groupByInitials(this, groupBy)
fun FlowContent.groupByAnchor(groupBy: GroupBy<*>, layout: Layout, initial: Char) =
    layout.groupByAnchor(this, groupBy, initial)
