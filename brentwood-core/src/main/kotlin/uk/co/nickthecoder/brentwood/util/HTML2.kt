package uk.co.nickthecoder.brentwood.util

import kotlinx.html.*
import kotlinx.html.attributes.Attribute
import kotlinx.html.attributes.StringAttribute

/**
 * In kotlinx's HTML DSL, the TH tag is NOT a HtmlBlockTag, and therefore cannot hold the same kind of content as a TD.
 * I don't know why, but I need my TH's to be near identical to TD's.
 * So I copied the TD code, and replaced the tag name to "th".
 */
open class TH2(initialAttributes: Map<String, String>, override val consumer: TagConsumer<*>) :
    HTMLTag("th", consumer, initialAttributes, null, false, false),
    HtmlBlockTag {
    var headers: String
        get() = attributeStringString.get(this, "headers")
        set(newValue) {
            attributeStringString.set(this, "headers", newValue)
        }

    var rowSpan: String
        get() = attributeStringString.get(this, "rowspan")
        set(newValue) {
            attributeStringString.set(this, "rowspan", newValue)
        }

    var colSpan: String
        get() = attributeStringString.get(this, "colspan")
        set(newValue) {
            attributeStringString.set(this, "colspan", newValue)
        }

}

internal val attributeStringString: Attribute<String> = StringAttribute()

inline fun TR.th2(classes: String? = null, crossinline block: TH2.() -> Unit = {}): Unit =
    TH2(attributesMapOf("class", classes), consumer).visit(block)