package uk.co.nickthecoder.brentwood.util

import uk.co.nickthecoder.brentwood.wiki.WikiImageScaler
import java.io.File

class ImageScaler(val width: Int, val height: Int, val quality: Int = 80) {

    fun scaleImage(sourceFile: File, destFile: File) {

        if (!sourceFile.exists()) return

        if (!destFile.exists() || destFile.lastModified() < sourceFile.lastModified()) {
            JobQueue.add {

                // Check again, because the file may have been created while we were waiting in the queue.
                if (!destFile.exists() || destFile.lastModified() < sourceFile.lastModified()) {
                    destFile.parentFile.mkdirs()

                    commandToString(
                        WikiImageScaler.convertPath,
                        "-quality", quality.toString(),
                        "-thumbnail",
                        "${width}x${height}",
                        sourceFile.absolutePath,
                        destFile.absolutePath
                    )
                }
            }
        }
    }

}
