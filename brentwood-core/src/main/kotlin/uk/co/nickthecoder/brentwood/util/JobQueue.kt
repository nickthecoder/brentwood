package uk.co.nickthecoder.brentwood.util

import uk.co.nickthecoder.brentwood.util.JobQueue.currentThread
import uk.co.nickthecoder.brentwood.util.JobQueue.queue
import java.util.*

/**
 * Thread safe by synchronising on the [queue].
 * All access to [queue] and [currentThread] are synchronized with [queue].
 * The processing of each item is NOT within the synchronized block.
 */
object JobQueue {

    private var currentThread: Thread? = null

    private val queue = ArrayDeque<Runnable>()

    fun add(runnable: Runnable) {
        synchronized(queue) {
            queue.add(runnable)
            if (currentThread == null) {
                startThread()
            }
        }
    }

    private fun startThread() {
        currentThread = Thread {

            while (true) {
                try {
                    val item = synchronized(queue) {
                        if (queue.isEmpty()) {
                            currentThread = null
                            null
                        } else {
                            queue.pop()
                        }
                    } ?: break
                    item.run()
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }

        }
        currentThread!!.start()
    }

}