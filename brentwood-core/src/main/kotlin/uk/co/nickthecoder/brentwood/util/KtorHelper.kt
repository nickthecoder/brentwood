package uk.co.nickthecoder.brentwood.util

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.routing.*
import java.net.URL

fun RequestConnectionPoint.urlString(): String {
    return buildString {
        append(scheme)
        append("://")
        append(host)
        if (port != 80 && port != 443) {
            append(":")
            append(port)
        }
        append(uri)
    }
}

fun RequestConnectionPoint.url() = URL(urlString())

fun Route.staticResources(remotePath: String, resourcePackage: String): Route =
    static(remotePath) { resources(resourcePackage) }

fun Route.redirect(old: String, new: String, permanent: Boolean = true) {
    get(old) {
        call.respondRedirectReplace(old, new, permanent)
    }
}

fun Route.redirectParameter(old: String, new: String, oldParam: String, newParam: String, permanent: Boolean = true) {
    get(old) {
        val value = call.parameters[oldParam]
        if (value == null) {
            call.respondRedirect(new, permanent)
        } else {
            call.respondRedirect("$new?$newParam=$value", permanent)
        }
    }
}

fun Route.redirectAll(old: String, new: String, permanent: Boolean = true) {
    get("$old/{...}") {
        call.respondRedirectReplace(old, new, permanent)
    }
}

fun Route.optionalAuthenticate(auth: List<String>?, block: Route.() -> Unit) {
    if (auth == null) {
        block()
    } else {
        authenticate(* auth.toTypedArray()) {
            block()
        }
    }
}

suspend fun ApplicationCall.respondRedirectReplace(old: String, new: String, permanent: Boolean = true) {
    val uri = request.origin.uri
    if (uri.startsWith(old)) {
        return respondRedirect(new + uri.substring(old.length), permanent)
    } else {
        throw IllegalArgumentException("url : $uri does not start with '$old'")
    }
}

