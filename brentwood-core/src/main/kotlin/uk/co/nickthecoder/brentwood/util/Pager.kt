package uk.co.nickthecoder.brentwood.util

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.layout.Layout

/**
 * Helps paginate items in a list across multiple pages. Each page uses a request parameter "page".
 *
 * This call does not create the html, this is left to the [Layout].
 * Therefore the look and feel of the pager can be customised by creating
 * a new implementation of [Layout], and the "control" logic remains untouched.
 *
 * Note, page numbers always start at 1 (there is not page 0).
 *
 */
interface Pager<T> {

    val href: String

    val pageParameter: String

    fun pageItems(pageNumber: Int): List<T>

    fun pageCount(): Int
}

class ListPager<T>(
    override val href: String,
    var allItems: List<T>,
    val itemsPerPage: Int,
    override val pageParameter: String = "page"
) : Pager<T> {

    /**
     * Returns a subset of [allItems] based on the [pageNumber] and [itemsPerPage].
     */
    override fun pageItems(pageNumber: Int): List<T> {
        val from = Math.max(0, Math.min(itemsPerPage * (pageNumber - 1), allItems.size))
        val to = Math.min(from + itemsPerPage, allItems.size)

        return allItems.subList(from, to)
    }

    /**
     * Returns the number of pages based on the size of [allItems] and [itemsPerPage].
     */
    override fun pageCount() = Math.ceil(allItems.size.toDouble() / itemsPerPage).toInt()

}


fun FlowContent.pager(pager: Pager<*>, layout: Layout, pageNumber: Int) = layout.pager(this, pager, pageNumber)
