package uk.co.nickthecoder.brentwood.util

import io.ktor.application.*
import io.ktor.http.*
import uk.co.nickthecoder.brentwood.BrentwoodException

class MissingParameter(name: String) : Exception("Missing parameter : $name")

fun ApplicationCall.optionalIntParameter(
    parameterName: String,
    defaultValue: Int,
    parameters: Parameters = this.parameters
): Int {
    return try {
        parameters[parameterName]?.toInt() ?: defaultValue
    } catch (e: Exception) {
        defaultValue
    }
}

fun ApplicationCall.requiredStringParameter(
    parameterName: String,
    parameters: Parameters = this.parameters
): String {
    return parameters[parameterName] ?: throw MissingParameter(parameterName)

}

fun ApplicationCall.requiredIntParameter(
    parameterName: String,
    parameters: Parameters = this.parameters
): Int {
    return parameters[parameterName]?.toIntOrNull() ?: throw MissingParameter(parameterName)

}

fun ApplicationCall.requiredPartsParameter(
    parameterName: String,
    parameters: Parameters = this.parameters
): String {
    val result = parameters.getAll(parameterName) ?: throw MissingParameter(parameterName)

    return result.joinToString(separator = "/")
}

fun Parameters.getAllParts(parameterName: String) = getAll(parameterName)?.joinToString(separator = "/")

fun ApplicationCall.optionalStringParameter(
    parameterName: String,
    defaultValue: String,
    parameters: Parameters = this.parameters
): String {
    return parameters[parameterName] ?: defaultValue
}

fun List<String>.joinPath() = joinToString(prefix = "/", separator = "/")

/**
 * When a parameter will be used as a directory name, we cannot allow it to contain the
 * special characters '/' ".." or '\'
 */
fun ApplicationCall.safePart(name: String): String {
    val value = requiredStringParameter(name)
    if (value.contains('/') || value.contains('\\')) {
        throw IllegalArgumentException("Name contains a slash")
    }
    if (value.startsWith("..")) {
        throw IllegalArgumentException("Name contains ..")
    }
    return value
}

fun ApplicationCall.safePathParts(name: String): List<String> {
    val result = parameters.getAll(name) ?: emptyList()
    if (result.firstOrNull() { it.contains('/') || it.contains('\\') || it.startsWith("..") } != null) {
        throw IllegalArgumentException("Name contains '/' or '..'")
    }
    return result
}

fun ApplicationCall.joinedSafeParts(name: String): String {
    val value = requiredPartsParameter(name)
    if (value.contains("/..") || value.contains("\\..") || value.startsWith("..")) {
        throw IllegalArgumentException("Name contains ..")
    }
    return value
}


fun String.addParameter(nameAndValue: String) =
    if (this.contains('?')) {
        "$this&$nameAndValue"
    } else {
        "$this?$nameAndValue"
    }


fun parameterID(name: String, call: ApplicationCall, parameters: Parameters? = null): Int {
    return (call.parameters[name] ?: parameters?.get(name))?.toIntOrNull()
        ?: throw BrentwoodException("Parameter $name not specified")
}
