package uk.co.nickthecoder.brentwood.util

import java.io.InputStreamReader

/**
 * Reverses the order of Thread's constructor, so that we can pass the runnable as
 * a lambda as the last argument (and therefore outside of the parentheses)
 */
fun newThread(name: String, runnable: Runnable) = Thread(runnable, name)

/**
 * Waits for the Process to end, consumes stdout and sinks stderr
 * Returns the stdout stream as a string.
 *
 * If you want to include stderr in the result, use [ProcessBuilder.redirectErrorStream] (true)
 * when building the [Process].
 *
 * If you want stderr as a separate result, tough! That's not supported.
 *
 * NOTE, this method will block until the process and the thread consuming
 * stdout are finished. Therefore you should almost always call this
 * from a new thread (perhaps via [JobQueue])
 */
fun Process.stdoutToString(): String {

    val result = StringBuilder()

    // We must sink the stderr thread so that the process does not block if stderr's buffer is filled.
    // We just throw away the output.
    newThread("stderrSink") {
        try {
            val buffer = ByteArray(1024)
            with(this.errorStream) {
                while (read(buffer, 0, buffer.size) >= 0) {
                    Unit // Do nothing. stderr is thrown away.
                }
            }
        } catch (e: Exception) {
        }
    }.start()

    val stdOutThread = newThread("stdoutSink") {
        try {
            val buffer = CharArray(1024)
            with(InputStreamReader(this.inputStream)) {
                var count = read(buffer, 0, buffer.size)
                while (count >= 0) {
                    result.append(buffer, 0, count)
                    count = read(buffer, 0, buffer.size)
                }
            }
        } catch (e: Exception) {
        }
    }.apply {
        start()
    }

    // Wait for the process to finish
    try {
        waitFor()
    } catch (e: InterruptedException) {
    }

    // There's a chance that the thread sinking stdout hasn't finished yet.
    // Without this the result may be truncated.
    try {
        stdOutThread.join()
    } catch (e: InterruptedException) {
    }
    // NOTE, we do not care if the stderr thread has finished.

    return result.toString()
}

/**
 * A quick an easy way to run a command and get the stdout stream as a string.
 * NOTE this will BLOCK until the process has finished, so usually run this
 * in a new thread (see [JobQueue]).
 *
 * NOTE, I also use this when I don't care about the result, as it is a very easy way
 * to run a command. Don't do it if the command produces lots of output though!
 */
fun commandToString(vararg command: String): String {
    return ProcessBuilder().apply {
        command(*command)
    }.start().stdoutToString()
}
