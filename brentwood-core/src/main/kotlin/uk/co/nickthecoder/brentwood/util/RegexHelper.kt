package uk.co.nickthecoder.brentwood.util

val REGEX_CLASS = Regex("[a-zA-Z]*")
val REGEX_BOOLEAN = Regex("true|false")
val REGEX_NAMESPACE_NAME = Regex("[a-zA-Z]*")
val REGEX_PAGE_NAME = Regex(".*")
val REGEX_NUMBER = Regex("[0-9]*")
val REGEX_PERCENT = Regex("[0-9]*%")
val REGEX_NUMBER_OR_PERCENT = Regex("[0-9]*%?")
val REGEX_LEFT_RIGHT = Regex("left|right")