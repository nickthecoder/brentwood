package uk.co.nickthecoder.brentwood.util

import io.ktor.application.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.response.*
import io.ktor.util.cio.*
import io.ktor.utils.io.*
import kotlinx.html.HTML
import kotlinx.html.html
import kotlinx.html.stream.appendHTML

// This file is a near identical copy of RespondHtml.kt in the ktor-html-builder library.
// The only difference is the ability to turn pretty printing on/off.

/**
 * A global, which affects all calls to [respondHtml2]. When false, superfluous whitespace is not included.
 * Use true for debugging, and false for production when speed is preferred over readablity of the generated html.
 *
 * Alas, turning this off isn't as safe as I'd like. I have <li> tags which are display:inline.
 * The writer thinks they are blocks, and therefore the whitespace between then can be removed, but
 * the css means they aren't blocks, and therefore there is a different between no whitespace and some whitespace.
 */
var prettyPrint = true

/**
 * Like the regular [ApplicationCall.respondHtml], but creates compact HTML if [prettyPrint] == false
 */
suspend fun ApplicationCall.respondHtml2(status: HttpStatusCode = HttpStatusCode.OK, block: HTML.() -> Unit) {
    respond(HtmlContent2(status, block))
}


/**
 * Like the regular [ApplicationCall.respondHtmlTemplate], but creates compact HTML if [prettyPrint] == false
 */
suspend fun <TTemplate : Template<HTML>> ApplicationCall.respondHtmlTemplate2(template: TTemplate, status: HttpStatusCode = HttpStatusCode.OK, body: TTemplate.() -> Unit) {
    template.body()
    respondHtml2(status) { with(template) { apply() } }
}

/**
 * Represents an [OutgoingContent] using `kotlinx.html` builder.
 */
class HtmlContent2(
    override val status: HttpStatusCode? = null, private val builder: HTML.() -> Unit
) : OutgoingContent.WriteChannelContent() {

    override val contentType: ContentType
        get() = ContentType.Text.Html.withCharset(Charsets.UTF_8)

    override suspend fun writeTo(channel: ByteWriteChannel) {
        channel.bufferedWriter().use {
            it.append("<!DOCTYPE html>\n")
            it.appendHTML(prettyPrint = prettyPrint).html(block = builder)
        }
    }
}
