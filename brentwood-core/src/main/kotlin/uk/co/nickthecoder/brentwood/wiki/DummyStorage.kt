package uk.co.nickthecoder.brentwood.wiki

import java.io.InputStream

class DummyStorage : Storage {

    override fun exists(pageName: PageName) = false
    override fun mediaExists(pageName: PageName) = false
    override fun load(pageName: PageName) = throw NotImplementedError()
    override fun date(pageName: PageName) = throw NotImplementedError()
    override fun versions(pageName: PageName): List<PageName> = emptyList()
    override fun save(pageName: PageName, source: String) = throw NotImplementedError()
    override fun mediaFile(pageName: PageName) = throw NotImplementedError()
    override fun list(namespace: Namespace) = throw NotImplementedError()
    override fun delete(pageName: PageName) = throw NotImplementedError()
    override fun saveMedia(pageName: PageName, inputStream: InputStream) = throw NotImplementedError()
    override fun rename(pageName: PageName, newTitle: String) = throw NotImplementedError()

}
