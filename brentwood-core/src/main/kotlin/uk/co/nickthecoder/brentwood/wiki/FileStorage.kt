package uk.co.nickthecoder.brentwood.wiki

import java.io.File
import java.io.InputStream
import java.util.*

/**
 * File structure for a page called "ExampleImage.png"
 * It has a related page called "thumbnail"
 * There are two versions of ExampleImage.png, and also two version of the thumbnail.
 *
 *     BASE_DIR
 *         ExampleImage.png.wiki
 *         ExampleImage.png
 *
 *         .relations
 *              thumbnail.png
 *                  ExampleImage.png.thumbnail.png.wiki
 *                  ExampleImage.png.thumbnail.png
 *         .history
 *             ExampleImage.png
 *                 1
 *                     ExampleImage.png.wiki
 *                     ExampleImage.png
 *             .relations
 *                 thumbnail.png
 *                     ExampleImage.png.thumbnail.png
 *                         1
 *                             ExampleImage.png.thumbnail.png.wiki
 *                             ExampleImage.png.thumbnail.png
 */
class FileStorage(val baseDir: File) : Storage {

    val historyDir = File(baseDir, ".history")

    val relationsDir = File(baseDir, ".relations")

    val historyRelationsDir = File(historyDir, ".relations")

    /**
     * The directory which holds the file for the wiki page, and the media (if the page has media).
     */
    private fun directory(pageName: PageName): File {

        return if (pageName.version == null) {
            if (pageName.relation == null) {
                baseDir
            } else {
                File(relationsDir, pageName.relation)
            }
        } else {
            if (pageName.relation == null) {
                File(File(historyDir, squishRelation(pageName)), pageName.version.toString())
            } else {
                File(File(File(historyRelationsDir, pageName.relation), squishRelation(pageName)), pageName.version.toString())
            }
        }
    }

    private fun wikiFile(pageName: PageName) =
        File(directory(pageName), "${squishRelation(pageName)}.wiki")

    override fun mediaFile(pageName: PageName) =
        File(directory(pageName), squishRelation(pageName))

    override fun exists(pageName: PageName) = wikiFile(pageName).exists()

    override fun mediaExists(pageName: PageName) = mediaFile(pageName).exists()

    override fun date(pageName: PageName) = Date(wikiFile(pageName).lastModified())

    override fun versions(pageName: PageName): List<PageName> {
        if (pageName.version != null) return listOf(pageName)

        val dir = if (pageName.relation == null) {
            File(historyDir, pageName.title)
        } else {
            File(File(historyRelationsDir, pageName.relation), squishRelation(pageName))
        }

        // List all the sub-directories, which are integers. Sort them numerically.
        // Return them as WikiNames with the version set.
        val oldVersions = dir.listFiles { file -> file.isDirectory && file.name.toIntOrNull() != null }
            ?.map { it.name.toInt() }
            ?.sorted()
            ?.map { PageName(pageName.namespace, pageName.title, pageName.relation, it) }
            ?: emptyList()

        // We also need to add the "current" version to the end (which does not have a version number).
        return oldVersions.toMutableList().apply { add(pageName) }
    }

    override fun load(pageName: PageName) = wikiFile(pageName).readText()

    private fun squishRelation(pageName: PageName) = if (pageName.relation == null) {
        pageName.title
    } else {
        "${pageName.title}.${pageName.relation}"
    }

    /**
     * When saving a file, we need to copy the *current* version into the .history directory.
     * What is the name of this new directory? (as an int).
     */
    private fun nextHistoryNumber(pageName: PageName): Int {
        val numbersDir = File(
            historyDir,
            if (pageName.relation == null) {
                pageName.title
            } else {
                "${pageName.title}.${pageName.relation}"
            }
        )
        val historyVersion =
            numbersDir.list { _, filename: String ->
                filename.toIntOrNull() != null
            }?.map {
                it.toInt()
            }?.maxOrNull() ?: 0

        return historyVersion + 1
    }

    override fun save(pageName: PageName, source: String) {
        if (exists(pageName) && load(pageName) == source) return

        val file = wikiFile(pageName)
        file.parentFile.mkdirs()
        if (file.exists()) {
            val newHistoryDirectory =
                File(File(historyDir, squishRelation(pageName)), nextHistoryNumber(pageName).toString())
            newHistoryDirectory.mkdirs()
            file.renameTo(File(newHistoryDirectory, file.name))
        }

        file.writeText(source)
    }

    override fun list(namespace: Namespace): List<PageName> {
        val files = baseDir.listFiles { file -> file.isFile && file.extension == "wiki" }?.sorted() ?: emptyList()
        return files.map { PageName(namespace, it.nameWithoutExtension) }
    }

    override fun delete(pageName: PageName) {
        if (pageName.version != null) {
            throw IllegalArgumentException("Cannot delete a specific version number")
        }

        val file = wikiFile(pageName)
        if (file.exists()) {
            file.delete()
        }
        val mediaFile = mediaFile(pageName)
        if (mediaFile.exists()) {
            mediaFile.delete()
        }

        val historyDir = if (pageName.relation == null) {
            File(historyDir, pageName.title)

        } else {
            File(File(historyRelationsDir, pageName.relation), squishRelation(pageName))
        }
        if (historyDir.exists()) {
            historyDir.deleteRecursively()
        }
    }

    override fun saveMedia(pageName: PageName, inputStream: InputStream) {
        val file = mediaFile(pageName)
        file.outputStream().buffered().use {
            inputStream.copyTo(it)
        }
    }

    override fun rename(pageName: PageName, newTitle: String) {

        if (pageName.relation != null) {
            throw IllegalArgumentException("Cannot rename a related page")
        }
        if (pageName.version != null) {
            throw IllegalArgumentException("Cannot rename a page with a version number")
        }

        val newName = PageName(pageName.namespace, newTitle)
        val oldFile = wikiFile(pageName)
        val newFile = wikiFile(newName)
        oldFile.renameTo(newFile)

        if (pageName.isMedia() && mediaExists(pageName)) {
            val oldMedia = mediaFile(pageName)
            val newMedia = mediaFile(newName)
            oldMedia.renameTo(newMedia)
        }

    }
}
