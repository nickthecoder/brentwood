package uk.co.nickthecoder.brentwood.wiki

import uk.co.nickthecoder.brentwood.wiki.tree.ExternalLinkNode
import uk.co.nickthecoder.brentwood.wiki.tree.ParentNode

interface LinkProtocol {

    fun url(from: String): String

    fun createLinkNode(line: Int, column: Int, from: String): ParentNode

    fun postColon(href: String) = href.substring(href.indexOf(':') + 1)
}

interface NonWikiLinkProtocol : LinkProtocol {

    override fun createLinkNode(line: Int, column: Int, from: String): ParentNode {
        return ExternalLinkNode(line, column, url(from))
    }
}

/**
 * Creates ExternalLinkNodes, using the whole of the href.
 * Used to allow external links from this wiki. By default the [WikiEngine]
 * allows external links starting with  http: and https:
 */
class ExternalLinkProtocol : NonWikiLinkProtocol {

    override fun url(from: String) = from

    companion object {
        val instance = ExternalLinkProtocol()
    }
}

/**
 * Creates ExternalLinkNodes using only the portion AFTER "local:".
 * Therefore it creates links without specifying the protocol, hostname or port.
 * One would usually use it for links such as :
 *
 *     local:/foo/blah
 *
 * Where /foo/blah is found on the web server, but we do not have to "hard-code"
 * the hostname into the link.
 */
class LocalProtocol : NonWikiLinkProtocol {

    override fun url(from: String): String {
        val postColon = postColon(from)
        return if (postColon.startsWith("/")) postColon else "/$postColon"
    }

}

/**
 * Allows a short form for any webserver. For example I use :
 *
 *    wikiEngine.protocolHandler["ntc"] = AliasProtocol( "http://nickthecoder.co.uk" )
 *
 * Example wiki syntax which links to http://nickthecoder.co.uk/map
 *
 *     [ntc:/map]
 *
 * We can also omit the slash (NOT recommended, because it will look like a relative path, but isn't).
 */
class AliasProtocol(val protocolAndHostname: String) : NonWikiLinkProtocol {

    override fun url(from: String): String {
        val postColon = postColon(from)
        return if (postColon.startsWith('/')) {
            "${protocolAndHostname}$postColon"
        } else {
            "${protocolAndHostname}/$postColon"
        }
    }

}


/**
 * This can be useful when using the wiki as part of a database driven application,
 * and you want wiki links to point to a particular row in a table.
 * The created link will be the URL of the table's "view", with the unique key
 * at the end.
 *
 * Suppose we have a table called Person, and the url to view person # 2 is
 * /person/2
 *
 * Then use IDProtocol( "/person", "NOT_FOUND" )
 *
 * If we send junk (non-integers), then [notFound] is used on its own.
 */
class IDProtocol(val found: String, val notFound: String) : NonWikiLinkProtocol {

    override fun url(from: String): String {
        val id = postColon(from).toIntOrNull()
        return if (id == null) {
            notFound
        } else {
            "$found/$id"
        }
    }

}
