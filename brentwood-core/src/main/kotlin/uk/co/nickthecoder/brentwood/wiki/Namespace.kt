package uk.co.nickthecoder.brentwood.wiki

class Namespace(
    val name: String,
    val storage: Storage,
    val showBackLinks: Boolean = false,
    val isHidden : Boolean = false
) {

    /**
     * The urls for the default namespace do not include the namespace's name.
     * There must be at most 1 default namespace.
     */
    var isDefault = false

    /**
     * Pages which are not listed in "BackLinksSyntax", nor the "IndexSyntax".
     * These are typically index pages.
     *
     * Note, pages titles which start with a lower case are also hidden.
     * Therefore I use lower case titles for navigation pages, but as I want my index page
     * titles to look nice
     */
    val hiddenTitles = mutableListOf("Index", "Images")

    /**
     * When parsing a page name, if the namespace given is not defined in the WikiEngine,
     * then an "error" namespace will be used.
     */
    fun isError() = name == "error"

    fun list(): List<PageName> = storage.list(this)
}
