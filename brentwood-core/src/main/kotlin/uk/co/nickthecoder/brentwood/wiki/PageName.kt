package uk.co.nickthecoder.brentwood.wiki

/**
 * The name of a wiki page (or part of a page).
 *
 * [namespaceName] The title of the [Namespace]. We can find the namespace using the [namespace] function.
 *
 * [title] The plain name of the page.
 *
 * [relation] Pages can have additional pages, such as a "talk" page in the WikiMedia wiki.
 * We use them here to automatically create smaller versions of images.
 *
 * [version] If set, then this is a specific version of the page, otherwise it is the latest version of the page.
 *
 */
data class PageName(
    val namespace: Namespace,
    val title: String,
    val relation: String? = null,
    val version: Int? = null

) : Comparable<PageName> {

    /**
     * Throws an exception if this name is not valid.
     * Call this before loading or saving wiki pages.
     */
    fun ensureIsValid() {
        if (title.contains("..")) {
            throw IllegalArgumentException("Page titles cannot contain '..'")
        }
        // Check that the name is valid.
        if (title.contains('/') || title.contains(':')) {
            throw IllegalArgumentException("Page titles cannot contain '/', or ':'")
        }
        val open = title.indexOf('{')
        val close = title.indexOf('}')
        if ((open < 0 && close >= 0) || (close < 0 && open >= 0) || (open >= 0 && close >= 0 && close < open)) {
            throw  IllegalArgumentException("Mismatched '{' and '}'")
        }
    }

    fun isHidden() = title.startsWith(".") || title.firstOrNull()?.isLowerCase() == true ||
            namespace.isHidden || namespace.hiddenTitles.contains(title)

    fun isMedia() = extension().isNotBlank()

    fun extension(): String {
        val s = relation ?: title
        val dot = s.lastIndexOf('.')
        // Note, a title with only a dot at the beginning has NO extension.
        // The dot indicates it is a HIDDEN file.
        return if (dot > 0) {
            s.substring(dot + 1)
        } else {
            ""
        }
    }

    fun exists() = namespace.storage.exists(this)
    fun mediaExists() = namespace.storage.mediaExists(this)

    fun date() = namespace.storage.date(this)

    fun versions() = namespace.storage.versions(this)

    fun version(v: Int) = PageName(namespace, title, relation, v)

    fun load() = namespace.storage.load(this)

    fun mediaFile() = namespace.storage.mediaFile(this)

    fun isRelation() = relation != null

    fun unrelated() = PageName(namespace, title)

    fun relation(relation: String) = PageName(namespace, title, relation)

    fun titleAndRelation() = if (relation == null) title else "$title{$relation}"

    override fun toString() = if (namespace.isDefault) {
        titleAndRelation()
    } else {
        "${namespace.name}/${titleAndRelation()}"
    }

    override fun compareTo(other: PageName): Int {

        return if (namespace == other.namespace) {
            if (title == other.title) {
                if (relation == null) {
                    if (other.relation == null) {
                        0
                    } else {
                        -1
                    }
                } else if (other.relation == null) {
                    1
                } else {
                    relation.compareTo(other.relation)
                }
            } else {
                title.compareTo(other.title)
            }
        } else {
            namespace.name.compareTo(other.namespace.name)
        }
    }
}
