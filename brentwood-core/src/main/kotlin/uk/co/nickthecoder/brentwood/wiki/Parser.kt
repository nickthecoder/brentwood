package uk.co.nickthecoder.brentwood.wiki

import uk.co.nickthecoder.brentwood.wiki.syntax.*
import uk.co.nickthecoder.brentwood.wiki.tree.*
import java.io.LineNumberReader
import java.io.StringReader

class Parser(val engine: WikiEngine) {

    val lineSyntaxList = mutableListOf<WikiLineSyntax>()
    val nonLineSyntaxList = mutableListOf<WikiSyntax>()

    init {

        add(
            ListItemSyntax("***", 3, false),
            ListItemSyntax("**", 2, false),
            ListItemSyntax("*", 1, false),

            ListItemSyntax("###", 3, true),
            ListItemSyntax("##", 2, true),
            ListItemSyntax("#", 1, true),

            CommentSyntax(),
            LinkSyntax(),

            NewParagraphSyntax(),
            HorizontalRuleSyntax(),
            LineBreakSyntax(), // Preferred syntax

            HeadingSyntax2("=====", "=====", 4),
            HeadingSyntax2("====", "====", 3),
            HeadingSyntax2("===", "===", 2),
            HeadingSyntax2("==", "==", 1),

            BoldSyntax(),
            ItalicSyntax(),
            UnderscoreSyntax(),
            StrikeThroughSyntax(),
            SpanSyntax("``","``","keyword"),
            PreSyntax(),
            ImgSyntax(),
            ImageSyntax(),

            IndexSyntax(),
            ImageIndexSyntax(),

            TableSyntax(),
            TableNewRowSyntax(),
            TableCellSyntax.heading,
            TableCellSyntax(),

            NoWikiSyntax(),
            FullWidthSyntax()

        )
    }

    fun add(vararg syntaxes: WikiSyntax) {
        for (syntax in syntaxes) {
            if (syntax is WikiLineSyntax) {
                lineSyntaxList.add(syntax)
            } else {
                nonLineSyntaxList.add(syntax)
            }

        }
    }

    fun addFirst(vararg syntaxes: WikiSyntax) {
        for (syntax in syntaxes) {
            if (syntax is WikiLineSyntax) {
                lineSyntaxList.add(0, syntax)
            } else {
                nonLineSyntaxList.add(0, syntax)
            }

        }
    }

    fun parse(pageName: PageName, source: String) = 
        ParserWorker(pageName, pageName, source).parse()
        
    fun parse(pageName: PageName, originalPageName : PageName, source : String) =
        ParserWorker(pageName, originalPageName, source).parse()
        
    fun parseSummary(pageName: PageName, source: String) =
        ParserWorker(pageName, pageName, source, true).parse()

    /**
     * Used as a placeholder for the "root"
     */
    private class DummyRootSyntax : WikiSyntax {
        override fun matches(line: String, fromColumn: Int) = -1
        override fun matchesClose(lineStr: String, fromColumn: Int) = -1
        override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails) =
            throw NotImplementedError()

        override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails) = fromColumn
        override fun expectUnclosed() = true
        override val isDeprecated: Boolean
            get() = false
    }

    private class DummyListSyntax : WikiSyntax {
        override fun matches(line: String, fromColumn: Int) = -1
        override fun matchesClose(lineStr: String, fromColumn: Int) = -1
        override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails) =
            throw NotImplementedError()

        override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails) = fromColumn
        override fun expectUnclosed() = true
        override val isDeprecated: Boolean
            get() = false
    }

    private inner class ParserWorker(
        private val pageName: PageName,
        private val originalPageName : PageName,
        private val source: String,
        private val summaryOnly: Boolean = false
    ) {

        private lateinit var currentParentNode: ParentNode
        private lateinit var reader: LineNumberReader
        private var rememberedText = StringBuilder()

        private val unterminated = mutableListOf<Pair<WikiSyntax, ParentNode>>()

        // Set to true if onlySummary == true, and we find a heading or table of contents.
        private var summaryFinished = false

        private lateinit var details: ParserDetails

        fun parse(): ParserDetails {

            if (engine.textExtensions.contains(pageName.extension())) {
                val root = PreNode(0, 0).apply {
                    children.add(PlainText(0, 0, source))
                }

                details = ParserDetails(pageName, originalPageName, root, engine)
                return details
            }

            reader = LineNumberReader(StringReader(source))
            val root = AutoParagraphBlock(0, 0)
            currentParentNode = root
            unterminated.add(Pair(DummyRootSyntax(), root))

            details = ParserDetails(pageName, originalPageName, root, engine)

            var line = 0
            var isFirst = true

            while (!summaryFinished) {

                val lineStr = reader.readLine() ?: break

                // Do not remember the final new line.
                if (isFirst) {
                    isFirst = false
                } else {
                    rememberNewLine()
                }

                try {
                    parseLine(line++, lineStr)
                } catch (e: WikiException) {
                    throw e
                } catch (e: Exception) {
                    throw WikiException(line, 0, details, "Unexpected error ${e.message}")
                }
            }
            addRememberedText()

            unterminated.lastOrNull { !it.first.expectUnclosed() }?.let { unclosed ->
                val node = unclosed.second
                throw WikiException(
                    node.line, node.column, details,
                    "Unterminated wiki syntax : ${unclosed.first.name()}"
                )
            }
            return details

        }

        private fun beginNode(line: Int, column: Int, syntax: WikiSyntax, node: Node) {
            if (summaryOnly && syntax is EndsSummary) {
                summaryFinished = true
                return
            }

            if (syntax.isDeprecated) {
                details.deprecatedNode = node
            }

            // Does this cause any of the unterminated to be closed?
            // e.g. If syntax is a TableCell, then a previous TableCell will be closed.
            for (pair in unterminated.reversed()) {
                if (pair.first.closedBy(syntax)) {
                    terminate(pair.first)
                    break
                }
            }

            // If we are currently inside a ListNode, then we must terminate it
            // as soon as we see something that isn't a ListItemNode, nor whitespace.
            if (currentParentNode is ListNode) {
                if (node !is ListItemNode) {
                    terminate(unterminated.last().first)
                }
            }

            // When we first come across a list item, we need to create a ListNode for it
            // i.e. We need a node which renders the "ul" or "ol", but there is no
            // corresponding wiki syntax for this node, so we create a DummyListSyntax.
            if (node is ListItemNode) {
                if (currentParentNode !is ListNode) {
                    beginNode(line, column, DummyListSyntax(), ListNode(line, column))
                }
            }

            // Do not allow a block item within a non-block parent
            if (!currentParentNode.isBlock() && node.isBlock()) {
                throw WikiException(
                    currentParentNode.line,
                    currentParentNode.column,
                    details,
                    "${currentParentNode.name()} must be terminated before the block element : ${node.name()}"
                )
            }

            currentParentNode.children.add(node)

            if (node is ParentNode) {
                unterminated.add(Pair(syntax, node))

                currentParentNode = node
            }
        }

        private fun parseLineSyntax(line: Int, lineStr: String): Int {

            // Do NOT look for new WikiLineSyntax if the start of the line matches
            // the END of an unterminated piece of syntax.
            // Example |} is the end of a table, but without this test would be thought of
            // as a beginning of a cell with "}" as its contents.
            for ((syntax, _) in unterminated) {
                val start = syntax.matchesClose(lineStr, 0)
                if (start == 0) {
                    return 0
                }
            }

            for (lineSyntax in lineSyntaxList) {

                // Look for WikiLineSyntax
                // Note, we have a special case for NewParagraphSyntax, which requires a
                // blank line, and the prefix is blank.
                val foo = lineSyntax.matches(lineStr, 0)
                if (foo >= 0) {
                    addRememberedText()
                    val pair = createNode(lineSyntax, line, 0, lineStr)
                    val node = pair.second
                    beginNode(line, 0, lineSyntax, node)

                    return pair.first
                }
            }
            return 0
        }

        private fun createNode(syntax: WikiSyntax, line: Int, column: Int, lineStr: String): Pair<Int, Node> {
            try {
                return syntax.createNode(line, column, lineStr, details)
            } catch (e: ParameterException) {
                throw WikiException(line, column, details, e.message ?: "Parameter Exception")
            } catch (e: WikiException) {
                throw e
            } catch (e: Exception) {
                throw WikiException(line, column, details, "Unexpected Error")
            }
        }

        private fun parseLine(line: Int, lineStr: String) {
            if (lineStr.isNotEmpty() && lineStr[0] != ' ' && currentParentNode is ListItemNode) {
                terminate(unterminated.last().first)
            }

            var column = 0

            try {
                column = if (currentParentNode.isPlainTextContent()) {
                    0
                } else {
                    parseLineSyntax(line, lineStr)
                }
                if (column < 0) {
                    throw WikiException(line, 0, details, "Internal Error")
                }

                while (true) {

                    // Look along the line for the first WikiSyntax prefix, or unterminated suffix.
                    var bestSyntax: WikiSyntax? = null
                    var bestParentNode: ParentNode? = null
                    var bestStart = Int.MAX_VALUE

                    if (currentParentNode.isPlainTextContent()) {
                        // Only look to close the currentParent
                        val (syntax, node) = unterminated.last()
                        val start = syntax.matchesClose(lineStr, column)
                        if (start >= 0 && start < bestStart) {
                            bestStart = start
                            bestSyntax = syntax
                            bestParentNode = node
                        }
                    } else {
                        // Look to close any of the unterminated, as we may have forgotten
                        // to close a tag, and we want to recover if possible.
                        for ((syntax, node) in unterminated.reversed()) {
                            val start = syntax.matchesClose(lineStr, column)
                            if (start >= 0 && start < bestStart) {
                                bestStart = start
                                bestSyntax = syntax
                                bestParentNode = node
                            }
                        }
                    }

                    if (!currentParentNode.isPlainTextContent()) {
                        // Look to start any non-line WikiSyntax.
                        for (syntax in nonLineSyntaxList) {
                            val start = syntax.matches(lineStr, column)
                            if (start >= 0 && start < bestStart) {
                                bestStart = start
                                bestParentNode = null
                                bestSyntax = syntax
                            }
                        }
                    }

                    if (summaryFinished) break

                    // At this point, we've either need to terminate an existing syntax
                    //    bestParentNode != null && bestSyntax != null
                    // Or we need to start a new syntax
                    //    bestParentNode == null && bestSyntax != null
                    // Or we haven't found anything else on this line
                    //    bestParentNode == null && bestSyntax == null

                    if (bestParentNode == null && bestSyntax == null) {

                        // No wiki syntax till the end of the line

                        rememberText(lineStr.substring(column))
                        column = lineStr.length
                        break

                    } else {

                        // Yes, we did find wiki code
                        // take care of the plain text before the next prefix/suffix.
                        if (bestStart > column) {
                            rememberText(lineStr.substring(column, bestStart))
                            if (bestStart < 0) {
                                throw WikiException(line, column, details, "Internal Error")
                            }

                            column = bestStart
                        }

                        if (bestSyntax != null) {

                            if (bestParentNode != null) {
                                terminate(bestSyntax)

                                val oldColumn = column
                                column = bestSyntax.close(bestParentNode, lineStr, bestStart, details)
                                if (column < 0) {
                                    throw WikiException(line, oldColumn, details, "Internal Error")
                                }

                            } else {
                                if (bestStart > column) {
                                    rememberText(lineStr.substring(column, bestStart))
                                }
                                addRememberedText()
                                val pair = createNode(bestSyntax, line, column, lineStr)//.substring(column) )
                                val node = pair.second
                                beginNode(line, column, bestSyntax, node)
                                if (pair.first <= column) {
                                    throw WikiException(
                                        line,
                                        column,
                                        details,
                                        "Parser went backwards : $bestSyntax"
                                    )
                                }
                                if (pair.first < 0) {
                                    throw WikiException(line, column, details, "Internal Error")
                                }
                                column = pair.first

                                if (node !is ParentNode) {
                                    if (node === unterminated.last().second) {
                                        //unterminated.removeAt(unterminated.size - 1)
                                        terminate(bestSyntax)
                                    }
                                    val oldColumn = column
                                    column = bestSyntax.close(node, lineStr, column, details)
                                    if (column < 0) {
                                        throw WikiException(line, oldColumn, details, "Internal Error")
                                    }
                                }
                            }
                        }
                    }

                }

                // Terminate the WikiLineSyntax if there was one (and if it hasn't already been terminated).
                val last = unterminated.last()
                val lastSyntax = last.first
                if (!summaryFinished && lastSyntax is WikiLineSyntax && lastSyntax.autoCloseAtEndOfLine()) {
                    addRememberedText()
                    terminate(last.first)
                }


                if (!summaryFinished && column < lineStr.length - 1) {
                    rememberText(lineStr.substring(column))
                }

            } catch (e: WikiException) {
                throw e
            } catch (e: Exception) {
                throw WikiException(line, column, details, "Unexpected error")
            }
        }

        private fun terminate(wikiSyntax: WikiSyntax) {
            val i = unterminated.indexOfLast { it.first === wikiSyntax }
            if (i > 0) {
                for (n in unterminated.size - 1 downTo i) {
                    unterminated.removeAt(unterminated.size - 1)
                }
            }
            addRememberedText()
            currentParentNode = unterminated.last().second
        }

        private fun rememberText(text: String) {
            if (text.isNotEmpty()) {
                rememberedText.append(text)
            }
        }

        private fun rememberNewLine() {
            if (!currentParentNode.ignoreWhiteSpace()) {
                rememberedText.append("\n")
            }
        }

        private fun addRememberedText() {
            if (rememberedText.isNotEmpty()) {
                val text = if (currentParentNode.isPlainTextContent()) {
                    rememberedText.toString()
                } else {
                    rememberedText.toString().replace(REGEX_WHITESPACE, " ")
                }
                rememberedText = StringBuilder()

                if (currentParentNode is ListNode) {
                    terminate(unterminated.last().first)
                }

                currentParentNode.children.add(PlainText(-1, -1, text))
            }
        }
    }

    companion object {
        val REGEX_WHITESPACE = Regex("\\s+")
    }

}

fun skipLeadingWhitespace(line: String, fromColumn: Int): Int {
    for (i in fromColumn until line.length) {
        if (!line[i].isWhitespace()) {
            return i
        }
    }
    return line.length
}
