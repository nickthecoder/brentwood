package uk.co.nickthecoder.brentwood.wiki

import uk.co.nickthecoder.brentwood.wiki.tree.Node

/**
 * Information which may be useful to some syntax during parsing.
 */
data class ParserDetails(
    /**
     * The name of the page being parsed.
     */
    val pageName: PageName,
    
    /**
     * When a page is included, this will be the name of the topmost page containing the include,
     * not the included page's name.
     */
    val originalPageName : PageName,
    
    /**
     * The root node. Note, when parsing a section, this will the root of the section,
     * not of the whole page.
     */
    val rootNode: Node,

    val engine: WikiEngine
    
) {

    /**
     * During the parsing process, piece of WikiSyntax can set customData.
     * Each piece of custom data has a name (the key of the map), and a value of any type.
     *
     * If the key "fullWidth" is set, then the view page will not include a sidebar with navigation.
     * This can be handy if the page contains wide tables. See home.PlantChartSyntax.
     */
    val customData = mutableMapOf<String, Any>()

    /**
     * The first position were deprecated syntax was encountered, or -1 if none were found.
     */
    var deprecatedNode: Node? = null
        set(v: Node?) {
            if (field == null) {
                field = v
            }
        }
}
