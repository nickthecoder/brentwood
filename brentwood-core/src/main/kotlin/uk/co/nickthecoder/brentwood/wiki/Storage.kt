package uk.co.nickthecoder.brentwood.wiki

import java.io.File
import java.io.InputStream
import java.util.*

interface Storage {

    fun exists(pageName: PageName): Boolean
    fun mediaExists(pageName: PageName): Boolean

    fun save(pageName: PageName, source: String)
    fun load(pageName: PageName): String

    fun saveMedia(pageName: PageName, inputStream: InputStream)

    fun date(pageName: PageName): Date

    fun versions(pageName: PageName): List<PageName>

    // Hmm, This isn't very flexible - what if we want to store the media elsewhere? e.g. a database or a jar file.
    fun mediaFile(pageName: PageName): File

    fun list(namespace: Namespace): List<PageName>

    fun delete(pageName: PageName)
    fun rename(pageName: PageName, newTitle: String)

}
