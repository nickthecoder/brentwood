package uk.co.nickthecoder.brentwood.wiki

import uk.co.nickthecoder.brentwood.util.addParameter
import uk.co.nickthecoder.brentwood.util.escapeURLParam
import uk.co.nickthecoder.brentwood.util.unescapeURLParam

/**
 * In order for the [WikiEngine], and its parts (such as InternalLinkNode) to be isolated from
 * the web server, [WikiEngine] needs a way to build urls for its pages.
 *
 * The interface allows the [WikiEngine] to do this without knowing HOW.
 *
 * Note that the "edit", "info" and "raw" urls are NOT part of this interface, because the
 * [WikiEngine] doesn't generate those links.
 *
 * At some point, I'll also use the wiki engine as a component in a larger web application,
 * and at that point, the url will be very different.
 */
interface URLFactory {

    fun view(pageName: PageName, full: Boolean = false): String
    fun media(pageName: PageName, full: Boolean = false): String
    fun edit(pageName: PageName, full: Boolean = false): String

    fun slide(pageName: PageName, slideNumber: Int? = null): String = throw NotImplementedError()
    fun info(pageName: PageName, full: Boolean = false): String = throw NotImplementedError()
    fun raw(pageName: PageName, full: Boolean = false): String = throw NotImplementedError()
    fun delete(pageName: PageName, full: Boolean = false): String = throw NotImplementedError()
    fun rename(pageName: PageName, full: Boolean = false): String = throw NotImplementedError()

    fun pageNameFromURL(url: String, wikiEngine: WikiEngine): PageName? = null

}

class DefaultURLFactory(val prefix: String, val webPath: String) : URLFactory {

    override fun view(pageName: PageName, full: Boolean) = url("view", pageName, full)

    override fun slide(pageName: PageName, slideNumber: Int?): String {
        val withoutNumber = url("slides", pageName, false)
        return if (slideNumber == null) {
            withoutNumber
        } else {
            withoutNumber.addParameter("slide=$slideNumber")
        }
    }

    override fun media(pageName: PageName, full: Boolean) = url("media", pageName, full)

    override fun edit(pageName: PageName, full: Boolean) = url("edit", pageName, full)

    override fun info(pageName: PageName, full: Boolean) = url("info", pageName, full)

    override fun raw(pageName: PageName, full: Boolean) = url("raw", pageName, full)

    override fun delete(pageName: PageName, full: Boolean) = url("delete", pageName, full)

    override fun rename(pageName: PageName, full: Boolean) = url("rename", pageName, full)


    private fun url(type: String, pageName: PageName, full: Boolean): String {
        return buildString {
            if (full) {
                append(prefix)
            }
            append(webPath)
            append("/$type/")
            if (!pageName.namespace.isDefault) {
                append(escapeURLParam(pageName.namespace.name))
                append("/")
            }
            append(escapeURLParam(pageName.title))
            if (pageName.relation != null) {
                append("{")
                append(escapeURLParam(pageName.relation))
                append("}")
            }
            if (pageName.version != null) {
                append("?version=")
                append(pageName.version)
            }
        }
    }

    override fun pageNameFromURL(url: String, wikiEngine: WikiEngine): PageName? {
        val slashSlash = url.indexOf("//")
        if (slashSlash < 0) return null

        val nextSlash = url.indexOf("/", slashSlash + 2)
        if (nextSlash < 0) return null
        val pathAndQuery = url.substring(nextSlash)
        val q = pathAndQuery.indexOf("?")
        val path = if (q < 0) {
            pathAndQuery
        } else {
            pathAndQuery.substring(0, q)
        }
        val prefix = "$webPath/view/"
        if (path.startsWith(prefix)) {
            val name = unescapeURLParam(path.substring(prefix.length))

            return wikiEngine.createName(name)
        }

        return null
    }

}
