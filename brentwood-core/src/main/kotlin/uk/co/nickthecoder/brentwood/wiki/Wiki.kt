package uk.co.nickthecoder.brentwood.wiki

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.DoNothingPageListener
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.PageListener
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.search.Search
import uk.co.nickthecoder.brentwood.util.*
import uk.co.nickthecoder.brentwood.wiki.tree.*
import java.lang.Integer.max
import java.lang.Integer.min

/**
 * A component which renders wiki pages, and also allows them to be edited, deleted etc.
 *
 * [Wiki] is separated from [WikiEngine], so that [WikiEngine] can be used without
 * having Ktor routes to the edit/view/... pages.
 * The FamilyAlbum component uses a [WikiEngine], but does not use an instance of this class.
 */
class Wiki(

    layout: Layout,
    webPath: String = "/wiki",
    val wikiEngine: WikiEngine,

    /**
     * Notified when a wiki page has been saved.
     * The implementation of [PageListener] will usually tell the Search component to
     * update its index based on the new contents.
     * If you do not wish to be notified when a page has changed, then you may use the default
     * [DoNothingPageListener].
     */
    private val pageListener: PageListener = DoNothingPageListener(),

    /**
     * If a [Search] component is specified, then we can use it to get "back links",
     * i.e. list the pages which reference the current page.
     * This is an optional feature, therefore [search] can be null.
     */
    val search: Search?,

    /**
     * Either null, or a list of authentication configuration names, such as "auth-basic"
     * as defined in the web server's Application.install(Authentication) block.
     */
    private val authenticate: List<String>? = null

) : LayoutComponent(layout, webPath) {

    /**
     * When viewing a wiki page, there is a navigation section on the left.
     * If the current page's namespace has a page with this title, then
     * it will be used to render this side panel.
     * If the page doesn't exist, then a page with the same title in the
     * DEFAULT namespace is used.
     */
    var navigationPageTitle = "navigation"

    /**
     * The factory MUST be of type DefaultURLFactory, otherwise our routing won't work!
     * We also need access to the other methods not in [URLFactory].
     */
    val urlFactory = wikiEngine.urlFactory as DefaultURLFactory

    override fun route(route: Route) {

        with(route) {

            staticResources("/resources", this@Wiki.javaClass.packageName)

            get("/view/{namespace}/{title...}") {
                view(call, parsePageName(call))
            }

            get("/slides/{namespace}/{title...}") {
                slide(call, parsePageName(call), call.parameters["slide"]?.toIntOrNull() ?: 1)
            }

            get("/info/{namespace}/{title...}") {
                info(call, parsePageName(call))
            }

            get("/raw/{namespace}/{title...}") {
                raw(call, parsePageName(call))
            }

            get("/media/{namespace}/{title...}") {
                media(call, parsePageName(call))
            }

            optionalAuthenticate(authenticate) {

                route("/edit/{namespace}/{title...}") {
                    get { edit(call, parsePageName(call), false) }
                    post { edit(call, parsePageName(call), true) }
                }

                route("/delete/{namespace}/{title...}") {
                    get { deleteForm(call, parsePageName(call)) }
                    post { delete(call, parsePageName(call)) }
                }

                route("/rename/{namespace}/{title...}") {
                    get { renameForm(call, parsePageName(call)) }
                    post { rename(call, parsePageName(call)) }
                }

            }
        }
    }

    private fun parsePageName(call: ApplicationCall): PageName {
        // If the namespace isn't specified, then this route still applies, but {title} will be null,
        // and {namespace} will be the title.

        fun parsePageName(namespace: Namespace, titleAndRelation: String, version: Int?): PageName {
            return if (titleAndRelation.endsWith('}')) {
                val open = titleAndRelation.lastIndexOf('{')
                PageName(
                    namespace,
                    titleAndRelation.substring(0, open),
                    titleAndRelation.substring(open + 1, titleAndRelation.length - 1),
                    version = version
                )
            } else {
                PageName(namespace, titleAndRelation, version = version)
            }
        }

        val namespaceOrTitle = call.requiredStringParameter("namespace")
        val title = call.parameters["title"]
        val version = call.parameters["version"]?.toIntOrNull()
        return if (title == null) {
            parsePageName(wikiEngine.defaultNamespace, namespaceOrTitle, version)
        } else {
            parsePageName(wikiEngine.safeNamespace(namespaceOrTitle), title, version)
        }
    }

    /**
     * When a parser throws an exception, we still want to render as much as possible,
     * and also report the exception.
     * So we catch the exception, and return both it and the root node.
     */
    private fun parseWithError(pageName: PageName, source: String): Pair<ParserDetails, WikiException?> {
        return try {
            Pair(wikiEngine.parser.parse(pageName, source), null)
        } catch (e: WikiException) {
            Pair(e.details, e)
        }
    }

    private suspend fun view(call: ApplicationCall, pageName: PageName) {

        val found = pageName.exists()
        val statusCode = if (found) HttpStatusCode.OK else HttpStatusCode.NotFound
        val source = if (found) {
            pageName.load()
        } else {
            ""
        }
        val (details, wikiException) = parseWithError(pageName, source)
        val node = details.rootNode
        val isFullWidth = details.customData["fullWidth"] == true
        val uri = call.request.uri

        call.respondHtmlTemplate2(layout.createTemplate(isFullWidth, uri), status = statusCode) {

            pageTitle { +pageName.title }
            pageHeading { +pageName.title }

            pageTools {
                if (found) {
                    layout.textTool(this, urlFactory.slide(pageName, 1)) {
                        +"View as Slides"
                    }
                    layout.textTool(this, urlFactory.info(pageName)) {
                        +"Info"
                    }
                    layout.textTool(this, urlFactory.edit(pageName)) {
                        +"Edit"
                    }
                }
            }

            content {
                if (found) {
                    node.render(this, wikiEngine)

                    if (wikiEngine.imageExtensions.contains(pageName.extension()) && pageName.mediaExists()) {
                        div("images") {
                            div("imageContainer") {
                                img(pageName.title, wikiEngine.urlFactory.media(pageName))
                            }
                        }
                    }

                } else {
                    p {
                        +"This page does not exist. Do you want to "
                        a(urlFactory.edit(pageName)) { +"create" }
                        +" it?"
                    }
                }

                wikiException?.let {
                    h3("error") {
                        +"Error at line ${wikiException.line + 1}, ${wikiException.column + 1}"
                    }
                }

                if (pageName.namespace.showBackLinks) {
                    backLinks(this, pageName)
                }

            }

            if (!isFullWidth) {

                var nav = PageName(pageName.namespace, navigationPageTitle)
                if (!nav.exists()) {
                    nav = PageName(wikiEngine.defaultNamespace, navigationPageTitle)
                }
                if (nav.exists()) {
                    val (navDetails, navException) = parseWithError(nav, nav.load())
                    val navRootNode = navDetails.rootNode

                    navigation {

                        navRootNode.render(this, wikiEngine)
                        navException?.let {
                            h3("error") {
                                +"Error at line ${navException.line + 1}, ${navException.column + 1}"
                            }
                        }
                    }
                }
            }
        }

    }

    /**
     * Takes the "root" of a wiki page, and splits it up into a list of "slides".
     * We use [HorizontalRuleNode], [HeadingNode].level == 1 to choose where to split.
     * The horizontal rules are removed, but the headings are not.
     */
    private fun split(parent: ParentNode): List<ParentNode> {

        val result = mutableListOf<ParentNode>()
        var currentParent = AutoParagraphBlock(0, 0)

        fun addSlide() {
            if (currentParent.children.isNotEmpty()) {
                result.add(currentParent)
            }
            currentParent = AutoParagraphBlock(0, 0)
        }

        for (child in parent.children) {
            if (child is HorizontalRuleNode) {
                addSlide()

            } else if (child is HeadingNode && child.level == 1) {
                addSlide()
                currentParent.children.add(child)

            } else {
                if (currentParent.children.isEmpty() && ((child is NewParagraphNode) || (child as? PlainText)?.text?.isBlank() == true)) {
                    // Skip blank text at the start of a slide.
                } else {
                    currentParent.children.add(child)
                }
            }
        }
        // Add the final slide.
        addSlide()
        return result
    }

    /**
     * Instead of showing the entire text on one page, it is split up into smaller sections,
     * an only one section is displayed at a time.
     *
     * The wiki page nodes are split at [HorizontalRuleNode] (wiki syntax ``----``),
     * and also at a level 1 heading (wiki syntax ``==Heading==``).
     */
    private suspend fun slide(call: ApplicationCall, pageName: PageName, pageNumber: Int) {

        val found = pageName.exists()
        val statusCode = if (found) HttpStatusCode.OK else HttpStatusCode.NotFound
        val source = if (found) {
            pageName.load()
        } else {
            ""
        }
        val (details, wikiException) = parseWithError(pageName, source)
        val isFullWidth = true
        val uri = call.request.uri
        val slides = split(details.rootNode as ParentNode)
        val slideIndex = min(max(0, pageNumber - 1), slides.size - 1)

        val slide = if (slides.isEmpty()) {
            details.rootNode
        } else {
            slides[slideIndex]
        }

        call.respondHtmlTemplate2(layout.createTemplate(isFullWidth, uri), status = statusCode) {

            pageTitle { +pageName.title }

            // If the first node on the slide is a heading, then use that as the pageHeading.
            // And remove it from the content.
            val heading = slide.children.firstOrNull() as? HeadingNode
            if (heading != null) {
                pageHeading { +heading.text() }
                slide.children.removeAt(0)
            } else if (slideIndex == 0) {
                pageHeading { +pageName.title }
            }

            val pager = ListPager(wikiEngine.urlFactory.slide(pageName), slides, 1, pageParameter = "slide")
            pageTools {
                if (found) {
                    layout.textTool(this, urlFactory.view(pageName)) {
                        +"Normal View"
                    }
                    layout.textTool(this, urlFactory.info(pageName)) {
                        +"Info"
                    }
                    layout.textTool(this, urlFactory.edit(pageName)) {
                        +"Edit"
                    }
                }
            }

            content {
                if (found) {
                    slides[slideIndex].render(this, wikiEngine)
                    pager(pager, layout, pageNumber)

                } else {
                    p {
                        +"This page does not exist. Do you want to "
                        a(urlFactory.edit(pageName)) { +"create" }
                        +" it?"
                    }
                }

                wikiException?.let {
                    h3("error") {
                        +"Error at line ${wikiException.line + 1}, ${wikiException.column + 1}"
                    }
                }

            }
        }

    }

    private suspend fun info(call: ApplicationCall, pageName: PageName) {
        val uri = call.request.uri

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

            pageTitle { +"Info : ${pageName.title}" }
            pageHeading { +"Info : ${pageName.title}" }

            pageTools {
                layout.textTool(this, wikiEngine.urlFactory.view(pageName)) {
                    +"View"
                }
                layout.textTool(this, urlFactory.raw(pageName)) {
                    +"Raw"
                }
                layout.textTool(this, urlFactory.delete(pageName)) {
                    +"Delete Entire Page"
                }
                layout.textTool(this, urlFactory.rename(pageName)) {
                    +"Rename"
                }
                layout.textTool(this, urlFactory.edit(pageName)) {
                    +"Edit"
                }
            }

            content {

                h2 { +"Versions" }
                val versions = pageName.versions()
                table {
                    tr {
                        th { +"Version" }
                        th { +"Date" }
                        th { +"View" }
                        th { +"Raw" }
                    }
                    for (version in versions.reversed()) {
                        tr {
                            td("right") {
                                +(version.version?.toString() ?: "current")
                            }
                            td { +"${version.date()}" }
                            td { a(urlFactory.view(version)) { +"view" } }
                            td { a(urlFactory.raw(version)) { +"raw" } }
                        }
                    }
                }

                backLinks(this, pageName)
            }

        }

    }


    private suspend fun raw(call: ApplicationCall, pageName: PageName) {
        val source = pageName.load()
        val contentType = when (pageName.extension()) {
            "css" -> ContentType.Text.CSS
            "js" -> ContentType.Text.JavaScript
            else -> ContentType.Text.Plain
        }
        call.respondText(source, contentType)
    }

    private suspend fun edit(call: ApplicationCall, pageName: PageName, isPost: Boolean) {
        var source = if (pageName.exists()) pageName.load() else ""
        var preview = false
        var cancel = false
        var save = false

        // When editing via a link attached to a heading jump to this position in the source code (zero based)
        var line = call.parameters["line"]?.toIntOrNull()
        var column = call.parameters["column"]?.toIntOrNull()

        if (isPost) {
            val multipart = call.receiveMultipart()
            multipart.forEachPart { part ->

                if (part is PartData.FormItem) {
                    when (part.name) {
                        "source" -> source = part.value
                        "preview" -> preview = true
                        "cancel" -> cancel = true
                        "save" -> save = true
                        "line" -> line = part.value.toIntOrNull()
                        "column" -> column = part.value.toIntOrNull()
                    }
                }

                if (part is PartData.FileItem && save) {

                    // use InputStream from part to save file
                    part.streamProvider().use { inputStream ->
                        // copy the stream to the file with buffering
                        pageName.namespace.storage.saveMedia(pageName, inputStream)
                    }
                }

                part.dispose()
            }
        }

        if (cancel) {
            call.respondRedirect(wikiEngine.urlFactory.view(pageName))
            return
        }

        val (details, wikiException) = parseWithError(pageName, source)
        val rootNode = details.rootNode

        // Do not save a page that cannot be parsed without error.
        if (save && wikiException == null) {
            pageName.namespace.storage.save(pageName, source)
            call.respondRedirect(wikiEngine.urlFactory.view(pageName))
            updateSearch(pageName)
            return
        }

        var message = ""
        var caretOffset = -1

        if (line != null && column != null) {
            caretOffset = calcOffset(source, line!!, column!!)
        } else if (wikiException != null) {
            message = wikiException.message
            caretOffset = calcOffset(source, wikiException.line, wikiException.column)
        } else {
            details.deprecatedNode?.let {
                message = "Deprecated syntax @ ${it.line + 1},${it.column + 1} : ${it.javaClass.simpleName}"
                caretOffset = calcOffset(source, it.line, it.column)
            }
        }

        // Either we tried to save, but there's an error, or we are showing a preview,
        // or this is the initial "get".
        // Either way, we need to show the form with the wiki source code.

        call.respondHtmlTemplate2(layout.createFullScreenTemplate()) {
            pageTitle { +"Edit : $pageName" }

            content {
                form(urlFactory.edit(pageName), FormEncType.multipartFormData, FormMethod.post) {
                    id = "editForm"

                    div("splitVertical") {

                        div("top") {

                            textArea {
                                name = "source"
                                id = "editTextArea"
                                autoFocus = true
                                +source
                            }
                            script("$webPath/resources/editWiki.js")

                            if (line != null) {
                                input(InputType.hidden, name = "line") { value = "$line" }
                            }
                            if (column != null) {
                                input(InputType.hidden, name = "column") { value = "$column" }
                            }

                            h3 { +"$pageName" }

                            if (caretOffset >= 0) {
                                h3("error") { +message }
                                inlineScript(positionCaret_JS)
                                inlineScript(
                                    """
                                    window.addEventListener( "load", function() {
                                        positionCaret(document.getElementById( "editTextArea" ), $caretOffset );
                                    });
                                    """.shrinkJavascript()
                                )
                            }

                            div("buttons") {
                                input(InputType.submit, name = "preview", classes = "button preview") {
                                    value = "Preview"
                                }
                                input(InputType.submit, name = "save", classes = "button ok") { value = "Save" }
                                input(InputType.submit, name = "cancel", classes = "button") { value = "Cancel" }
                            }

                            wikiEngine.cheatSheet?.let { cheatSheet ->
                                a(wikiEngine.urlFactory.view(cheatSheet), target = "_blank") { +"Cheat Sheet" }
                            }

                            if (pageName.isMedia()) {
                                input(InputType.file, name = "media") {
                                    id = "file"
                                    size = "60"
                                }
                            }

                        }

                        div("bottom") {

                            // Render the wiki page using the source posted from the textarea.
                            if (preview) {
                                div("previewWikiNarrow") {
                                    rootNode.render(this, wikiEngine)
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    private suspend fun deleteForm(call: ApplicationCall, pageName: PageName) {

        val uri = call.request.uri
        if (!pageName.exists()) {
            throw NotFoundException()
        }
        val (details, _) = parseWithError(pageName, pageName.load())
        val rootNode = details.rootNode

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            pageTitle { +"Delete : $pageName" }
            pageHeading { +pageName.title }

            content {

                div("form") {
                    form(urlFactory.delete(pageName), method = FormMethod.post) {
                        h3 {
                            +"Are you sure you want to delete this page?"
                        }
                        div("buttons") {
                            input(InputType.submit, name = "delete", classes = "button delete") {
                                value = "Delete"
                            }
                            input(InputType.submit, name = "cancel", classes = "button cancel") {
                                value = "Cancel"
                            }
                        }
                    }
                }

                rootNode.render(this, wikiEngine)

            }

        }
    }

    private suspend fun delete(call: ApplicationCall, pageName: PageName) {

        val parameters = call.receiveParameters()
        if (parameters["cancel"] != null) {
            call.respondRedirect(wikiEngine.urlFactory.view(pageName))
            return
        }
        if (!pageName.exists()) {
            throw NotFoundException()
        }
        pageName.namespace.storage.delete(pageName)
        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Deleted" }
            pageHeading { +"Deleted" }
            content {

            }
        }

    }

    private suspend fun renameForm(call: ApplicationCall, pageName: PageName) {
        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Rename $pageName" }
            pageHeading { +"Rename $pageName" }

            content {

                div("form") {
                    form(urlFactory.rename(pageName), method = FormMethod.post) {
                        +"New Name : "
                        input(InputType.text, name = "newName") {
                            autoFocus = true
                            value = pageName.title
                        }

                        div("buttons") {
                            input(InputType.submit, name = "rename", classes = "button ok") {
                                value = "Rename"
                            }
                            input(InputType.submit, name = "cancel", classes = "button cancel") {
                                value = "Cancel"
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun rename(call: ApplicationCall, pageName: PageName) {

        val parameters = call.receiveParameters()
        if (parameters["cancel"] != null) {
            call.respondRedirect(urlFactory.view(pageName))
            return
        }

        val newName = parameters["newName"]
        if (newName == null) {
            call.respondRedirect(urlFactory.rename(pageName))
            return
        }

        val newPageName = PageName(pageName.namespace, newName)
        pageName.namespace.storage.rename(pageName, newName)
        call.respondRedirect(urlFactory.view(newPageName))
        updateSearch(newPageName)
    }


    private suspend fun media(call: ApplicationCall, pageName: PageName) {
        if (pageName.mediaExists()) {
            val file = pageName.mediaFile()
            call.respondFile(file)
        } else {
            throw NotFoundException(pageName.toString())
        }
    }

    private fun backLinks(parent: FlowContent, pageName: PageName) {
        val fullURL = urlFactory.view(pageName, true)
        val backLinks = search?.backLinks(fullURL) ?: emptyList()
        val backPageNames =
            backLinks.mapNotNull { urlFactory.pageNameFromURL(it, wikiEngine) }.filter {
                !it.isHidden()
            }
        if (backLinks.isNotEmpty()) {
            parent.h2 { +"Referenced by :" }
            parent.ul("infoList") {
                for (backName in backPageNames) {
                    li {
                        a(urlFactory.view(backName)) {
                            rel = "nofollow"
                            +backName.title
                        }
                    }
                    +" "
                }
            }
        }
    }

    private fun updateSearch(pageName: PageName) {
        JobQueue.add {
            pageListener.pageChanged(urlFactory.view(pageName))
        }
    }

    private fun calcOffset(source: String, line: Int, column: Int): Int {
        var linesRemaining = line
        for (i in source.indices) {
            val c = source[i]
            if (c == '\n') {
                linesRemaining--
                if (linesRemaining == 0) {
                    return i + column - line + 1
                }
            }
        }
        return source.length
    }

}

private val positionCaret_JS = """
function positionCaret(textArea,position) { 
    textArea.focus();

    const fullText = textArea.value;
    textArea.value = fullText.substring(0, position);
    var scrollTo = textArea.scrollHeight;
    textArea.value = fullText;
    textArea.setSelectionRange(position, position);
    textArea.scrollTop = scrollTo;
}
""".shrinkJavascript()
