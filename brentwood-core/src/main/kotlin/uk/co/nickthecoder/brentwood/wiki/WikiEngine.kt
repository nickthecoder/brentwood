package uk.co.nickthecoder.brentwood.wiki

import uk.co.nickthecoder.brentwood.layout.Layout

class WikiEngine(

    val defaultNamespace: Namespace,
    val urlFactory: URLFactory,
    val layout: Layout

) {

    private var namespaces = mutableMapOf<String, Namespace>()

    var errorNamespace = Namespace("error", DummyStorage())

    var parser = Parser(this)

    val protocolHandlers = mutableMapOf<String, LinkProtocol>(
        "http" to ExternalLinkProtocol.instance,
        "https" to ExternalLinkProtocol.instance
    )

    /**
     * The list of extensions which are considered to be text, and therefore not parsed as wiki source code.
     */
    val textExtensions = mutableListOf("txt", "css")

    val imageExtensions = mutableListOf("png", "jpg", "jpeg")

    /**
     * Used by the ImageIndexNode to display a thumbnail of images
     */
    var thumbnailScaler = WikiImageScaler("thumbnail.png", 166, 166)

    /**
     * Optional : The name of the page containing hints on the wiki syntax.
     * The edit page will have a link to this page.
     */
    var cheatSheet : PageName? = null

    init {
        addNamespace(defaultNamespace)
        defaultNamespace.isDefault = true
    }

    fun addNamespace(namespace: Namespace) {
        if (namespace.isError()) {
            throw Exception("Do NOT add the 'error' namespace into the wiki engin")
        }
        namespaces[namespace.name] = namespace
    }

    fun namespace(namespaceName: String) =
        namespaces[namespaceName] ?: throw Exception("Namespace $namespaceName not found")

    fun safeNamespace(namespaceName: String) =
        namespaces[namespaceName] ?: errorNamespace

    /**
     * The format of a wiki name is :
     *
     *    namespace/title{relation}
     *
     * If the namespace isn't specified, then [contextNamespace] is used.
     */
    fun createName(name: String, contextNamespace: Namespace = defaultNamespace): PageName {
        val trimmed = name.trim()
        val slash = trimmed.indexOf('/')
        val namespace: Namespace
        val titleAndRelation: String
        if (slash >= 0) {
            namespace = safeNamespace(trimmed.substring(0, slash))
            titleAndRelation = trimmed.substring(slash + 1)
        } else {
            namespace = contextNamespace
            titleAndRelation = trimmed
        }
        val title: String
        val relation: String?
        val open = titleAndRelation.indexOf('{')
        if (open >= 0) {
            title = titleAndRelation.substring(0, open)
            relation = if (titleAndRelation.endsWith('}')) {
                titleAndRelation.substring(open + 1, titleAndRelation.length - 2)
            } else {
                titleAndRelation.substring(open + 1)
            }
        } else {
            title = titleAndRelation
            relation = null
        }
        return PageName(namespace, title, relation)
    }
}
