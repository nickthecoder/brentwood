package uk.co.nickthecoder.brentwood.wiki

/**
 * [line] and [column] are both zero based. i.e. the first character is at (0,0).
 */
class WikiException(
    val line: Int,
    val column: Int,
    val details: ParserDetails,
    reason: String,
) : Exception("$reason at (${line + 1},${column + 1})") {
    override val message: String = super.message ?: "Error at ${line + 1}, ${column + 1}"
}
