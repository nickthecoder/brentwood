package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.search.Search
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.BackLinksNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class BackLinksSyntax(val search: Search, prefix: String = "{{backLinks", suffix: String = "}}") :
    AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val url = details.engine.urlFactory.view(details.pageName, true)
        val backLinks = search.backLinks(url).map { link: String ->
            details.engine.urlFactory.pageNameFromURL(link, details.engine)
        }.filterNotNull().filter {
            !it.isHidden()
        }
        return Pair(column + prefix.length, BackLinksNode(line, column, backLinks))
    }

}
