package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.CodeBlockNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class CodeBlockSyntax(

    val config: CodeBlockConfig,
    prefix: String = "<<<",
    suffix: String = ">>>"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, CodeBlockNode(line, column, config))
    }

}

/**
 * Note, the order of the entries is important.
 * e.g. placing ``*`` before ``*=`` will NEVER match ``*=``, because ``*`` is matched first.
 */
class CodeBlockConfig(
    val keywords: List<String>,
    val operators: List<String>,
    val parts: List<CodeBlockPart>,
    val trailingComment: List<String>
) {
    companion object {
        val kotlin = CodeBlockConfig(
            keywords = listOf(
                "as", "break", "boolean", "byte", "catch", "char", "class", "continue", "do", "double",
                "else", "enum", "false", "finally", "float", "for", "fun",
                "if", "in", "is", "int", "interface", "long", "null", "super", "return",
                "throw", "try", "true", "typeOf", "typeAlias", "val", "var", "when", "while"
            ),
            operators = listOf(
                "[", "]",
                "++", "+=", "+",
                "--", "-=", "-",
                "*=", "*",
                "/=", "/",
                "%=", "%",
                "!==", "!=", "!",
                "===", "==", "="
            ),
            parts = listOf(
                CodeBlockPart("codeString", "\"\"\"", isMultiline = true),
                CodeBlockPart("codeString", "\""),
                CodeBlockPart("codeString", "\'"),
                CodeBlockPart("codeComment", "/*", "*/", isMultiline = true),
            ),
            trailingComment = listOf("//")

        )
    }
}

/**
 * Definition a string literal or (non-trailing) comments.
 * Note, Kotlin style string expressions are not supported.
 */
class CodeBlockPart(
    val cssClass: String,
    val start: String, val end: String = start, val isMultiline: Boolean = false
)
