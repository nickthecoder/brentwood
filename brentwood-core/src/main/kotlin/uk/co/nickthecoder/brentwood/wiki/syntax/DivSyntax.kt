package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_BOOLEAN
import uk.co.nickthecoder.brentwood.util.REGEX_CLASS
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.DivNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class DivSyntax(

    prefix: String,
    suffix: String,
    val cssClass: String,
    val isAutoParagraphBlock: Boolean = true

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(prefix.length, DivNode(line, column, cssClass, isAutoParagraphBlock))
    }
}

/**
 * Like [DivSyntax], but allows the css class to be determined by the user.
 *
 * The user can also choose if the content should use paragraphs or not.
 * For "regular" content, keep to the default (paragraphs=true),
 * But if the DIV is for a single IMG (or similar), then perhaps
 * paragraphs are not desired.
 */
class Div2Syntax(

    prefix: String,
    suffix: String

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val cssClass = values["class"] ?: ""
        val isAutoParagraphBlock = values["paragraphs"] != "false"

        return Pair(end, DivNode(line, column, cssClass, isAutoParagraphBlock))
    }

    companion object {
        val parameters = mapOf(
            "class" to REGEX_CLASS,
            "paragraphs" to REGEX_BOOLEAN
        )
    }
}
