package uk.co.nickthecoder.brentwood.wiki.syntax

/**
 * Implemented by headings and the table of contents.
 * Indicates to the parser when using "parseSummary" that it should stop, because the summary section has finished.
 */
interface EndsSummary : WikiSyntax
