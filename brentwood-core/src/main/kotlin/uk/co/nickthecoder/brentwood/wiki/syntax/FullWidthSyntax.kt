package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.NothingNode

/**
 * Hints that the page should take up the full width (no navigation sidebar).
 */
class FullWidthSyntax(prefix: String = "{{fullWidth", suffix: String = "}}") : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        details.customData["fullWidth"] = true
        return Pair(column + prefix.length, NothingNode(line, column))
    }

}
