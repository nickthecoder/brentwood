package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.skipLeadingWhitespace
import uk.co.nickthecoder.brentwood.wiki.tree.HeadingNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class HeadingSyntax2(

    prefix: String,
    val suffix: String,
    val level: Int

) : AbstractWikiLineSyntax(prefix), EndsSummary {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val editLink = if (level < 3) {
            "${details.engine.urlFactory.edit(details.pageName)}?line=$line&column=$column"
        } else {
            null
        }
        return Pair(skipLeadingWhitespace(lineStr, column + prefix.length), HeadingNode(line, column, level, editLink))
    }

    override fun matchesClose(lineStr: String, fromColumn: Int): Int =
        if (suffix.isBlank()) {
            -1
        } else {
            lineStr.indexOf(suffix, fromColumn)
        }

    override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails) =
        if (lineStr.substring(fromColumn).startsWith(suffix)) {
            fromColumn + suffix.length
        } else {
            -1
        }

}


class HeadingLineSyntax(

    prefix: String,
    val level: Int

) : AbstractWikiLineSyntax(prefix), EndsSummary {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val editLink = if (level < 3) {
            "${details.engine.urlFactory.edit(details.pageName)}?line=$line&column=$column"
        } else {
            null
        }
        return Pair(skipLeadingWhitespace(lineStr, column + prefix.length), HeadingNode(line, column, level, editLink))
    }
}
