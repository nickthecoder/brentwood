package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.HorizontalRuleNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class HorizontalRuleSyntax(
        prefix: String = "----"
) : AbstractWikiLineSyntax(prefix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, HorizontalRuleNode(line, column))
    }

}
