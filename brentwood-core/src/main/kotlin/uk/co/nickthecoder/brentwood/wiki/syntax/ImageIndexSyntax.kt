package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_NAMESPACE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.ImageIndexNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class ImageIndexSyntax(

    prefix: String = "{{imageIndex",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val namespace = details.engine.safeNamespace(values["namespace"] ?: details.engine.defaultNamespace.name)

        return Pair(end, ImageIndexNode(line, column, namespace))
    }

    companion object {

        private val parameters = mapOf(
            "namespace" to REGEX_NAMESPACE_NAME
        )

    }
}