package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.ImageNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node


class ImageSyntax(

    prefix: String = "{{image",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)

        val pageValue = values["page"] ?: "none"
        val colon = pageValue.indexOf(":")
        val protocolHandler = if (colon >= 0) details.engine.protocolHandlers[pageValue.substring(0, colon)] else null
        val imagePageName = if (protocolHandler == null) {
            details.engine.createName(pageValue, details.pageName.namespace)
        } else {
            null
        }
        val imageUrl = protocolHandler?.url(pageValue) ?: details.engine.urlFactory.media(imagePageName!!)

        val floatValue = values["float"]
        val alignValue = values["align"]
        val renderLink = values["renderLink"] != "false"

        val cssClass = when (floatValue) {
            "left" -> "floatLeft"
            "right" -> "floatRight"
            else -> when (alignValue) {
                "left" -> "left"
                "right" -> "right"
                "center" -> "center"
                "char" -> "char"
                else -> null
            }
        }
        val pageLink = if (renderLink && imagePageName != null) {
            details.engine.urlFactory.view(imagePageName)
        } else {
            null
        }
        val imageNode = ImageNode(line, column, imagePageName, imageUrl, cssClass, pageLink)

        return Pair(end, imageNode)
    }

    companion object {

        private val parameters = mapOf(
            "page" to Regex(".*"),
            "renderLink" to Regex("true|false"),
            "float" to Regex("left|right|none"),
            "align" to Regex("left|center|right|justify|char")
        )
    }

}
