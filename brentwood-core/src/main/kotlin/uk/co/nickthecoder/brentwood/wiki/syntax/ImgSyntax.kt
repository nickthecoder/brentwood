package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.ImgNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

private val parameters = mapOf(
    "page" to Regex(".*")
)

/**
 */
class ImgSyntax(
    prefix: String = "{{img",
    suffix: String = "}}"
) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)

        val pageValue = values["page"] ?: "none"
        val colon = pageValue.indexOf(":")
        val protocolHandler = if (colon >= 0) details.engine.protocolHandlers[pageValue.substring(0, colon)] else null
        val imagePageName = if (protocolHandler == null) {
            details.engine.createName(pageValue, details.pageName.namespace)
        } else {
            null
        }
        val imageUrl = if (protocolHandler == null) {
            details.engine.urlFactory.media(imagePageName!!)
        } else {
            protocolHandler.url(pageValue)
        }

        return Pair(end, ImgNode(line, column, imagePageName, imageUrl))
    }

}
