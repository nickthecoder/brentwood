package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.WikiException
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class IncludeSyntax(

    prefix: String = "{{include",
    suffix: String = "}}",
    /**
     * Is this treated as a new page, or is it considered merely a part of the current page?
     * If the included page contains a link with no namespace given, then isNewPage will
     * determine which namespace is used.
     * If isNewPage, then it will be the namespace of the included page. If false, then
     * it will be the namespace of the page containing the [IncludeSyntax].
     */
    val isNewPage: Boolean = true

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val includeName = details.engine.createName(values["page"]!!, details.pageName.namespace)

        if (!includeName.exists()) {
            throw WikiException(line, column, details, "Page not found : '${values["page"]}'")
        }

        try {
            val node = if (isNewPage) {
                details.engine.parser.parse(includeName, details.originalPageName, includeName.load()).rootNode
            } else {
                details.engine.parser.parse(details.pageName,  details.originalPageName, includeName.load()).rootNode
            }
            return Pair(end, node)

        } catch (e: WikiException) {
            throw WikiException(line, column, details, "Include failed")
        }
    }

    companion object {
        private val parameters = mapOf(
            "page" to REGEX_PAGE_NAME
        )
    }

}
