package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_BOOLEAN
import uk.co.nickthecoder.brentwood.util.REGEX_NAMESPACE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.IndexNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class IndexSyntax(

    prefix: String = "{{index",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val namespace = details.engine.safeNamespace(values["namespace"] ?: details.engine.defaultNamespace.name)
        val includeMedia = values["includeMedia"] == "true"
        val includeHidden = values["includeHidden"] == "true"

        return Pair(end, IndexNode(line, column, namespace, includeMedia, includeHidden))
    }

    companion object {

        private val parameters = mapOf(
            "namespace" to REGEX_NAMESPACE_NAME,
            "includeMedia" to REGEX_BOOLEAN,
            "includeHidden" to REGEX_BOOLEAN
        )

    }
}