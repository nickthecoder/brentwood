package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.LineBreakNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class LineBreakSyntax(
        prefix: String = "<br>"
) : AbstractWikiSyntax(prefix, "") {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, LineBreakNode(line, column))
    }
}
