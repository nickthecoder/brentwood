package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.WikiException
import uk.co.nickthecoder.brentwood.wiki.tree.InternalLinkNode
import uk.co.nickthecoder.brentwood.wiki.tree.LinkNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.PlainText

class LinkSyntax(

    prefix: String = "[",
    suffix: String = "]",
    val separator: String = "|"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun matchesClose(lineStr: String, fromColumn: Int): Int {
        val superMatch = super.matchesClose(lineStr, fromColumn)
        // If we don't have a close, then don't bother looking for a separator.
        if (superMatch < 0) {
            return superMatch
        }

        val separatorMatch = lineStr.indexOf(separator, fromColumn)
        return if (separatorMatch >= 0) {
            Math.min(separatorMatch, superMatch)
        } else {
            superMatch
        }
    }

    /**
     * We can close the syntax if we find the separator OR the suffix.
     * If we find the suffix, then the link's text is the same as link's ref.
     */
    override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails): Int {
        if (node !is LinkNode) return -1

        val close = lineStr.indexOf(suffix, fromColumn)
        if (close < 0) {
            // We shouldn't get here (matchClose takes care of that!)
            throw WikiException(-1, fromColumn, details, "Link not closed")
        }

        val sepLength = separator.length
        val closedBySep = fromColumn > sepLength && lineStr.substring(fromColumn).startsWith(separator)

        val rawRef = if (closedBySep) {
            lineStr.substring(fromColumn + sepLength, close)
        } else {
            // We have only a ref. The content will be the same as the ref.
            node.text()
        }
        val ref = replaceParameters( rawRef, details )

        val colon = ref.indexOf(':')
        if (colon < 0) {
            // A regular page name, not an external link
            val linkName = details.engine.createName(ref, details.pageName.namespace)
            val href = details.engine.urlFactory.view(linkName)

            val internalLink = InternalLinkNode(-1, -1, linkName, href)
            if (ref == node.text()) {
                // Only display the page title (not the namespace).i.e. [Foo/Bar] will display as Bar.
                internalLink.children.add(PlainText(-1, -1, linkName.titleAndRelation()))
            } else {
                internalLink.children.addAll(node.children)
            }
            node.children.clear()
            node.children.add(internalLink)

        } else {
            // An external link (maybe a special type of link, such as local:
            // but more often, just http://example.com...
            val protocol = ref.substring(0, colon)

            val handler = details.engine.protocolHandlers[protocol] ?: throw WikiException(
                node.line, node.column, details, "Unknown protocol '$protocol' for location : '$ref'"
            )
            val externalLinkNode = handler.createLinkNode(node.line, node.column, ref)

            externalLinkNode.children.addAll(node.children)
            node.children.clear()
            node.children.add(externalLinkNode)
        }

        return close + suffix.length
    }

    /**
     * We can include the page's title / namespace within an external url by using {title} within the url.
     * {namespace} is also replaced.
     * 
     * I added this so that included pages can use the page name as part of a URL.
     * In particular, I wanted an include, which contained a link to premierseedsdirect.com search page.
     * 
     * NOTE. URLS are not allowed to include characters '{' nor '}', so we don't have to worry about escaping.
     *
     */
    
   private fun replaceParameters(ref: String, details: ParserDetails): String {
       val buffer = StringBuilder()
       var from = 0
       var open = ref.indexOf('{', from)
       while (open >= 0) {
           val close = ref.indexOf('}', open)
           if (close > open) {
               val name = ref.substring(open + 1, close)
               buffer.append(ref.substring(from, open))
               when (name) {
                   "title" -> buffer.append(details.originalPageName.title)
                   "namespace" -> buffer.append(details.originalPageName.namespace.name)
                   else -> throw Exception("Unknown replacement : $name")
               }
               from = close + 1
           }
           open = ref.indexOf('{', from)
       }
       return if (buffer.isEmpty()) {
           ref
       } else {
           buffer.append(ref.substring(from))
           buffer.toString()
       }
   }
   

    /*
     * So that I don't have to convert all of my old wiki pages, I want my new wiki to be able to parse
     * my old syntax. This is problematic for the LinkSyntax.
     *
     * The format is either : [pageName] or [url] or [label|url]
     *
     * label can be arbitrary wiki markup, and can include images (or any other non-block elements).
     * Should LinkNode allow arbitrary wiki markup? Well that depends on whether a '|' comes later!
     * This is a bodge, it just looks for the next | or ] It works well enough, but if I were to design this
     * from scratch, I'd consider changing the syntax of links; putting the url first always.
     */
    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val end = lineStr.indexOf(suffix, column)
        val sep = lineStr.indexOf(separator, column)
        val isLabelled = (sep >= 0 && sep < end) || (sep >= 0 && end < 0)

        return Pair(column + prefix.length, LinkNode(line, column, isLabelled))
    }

}
