package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.skipLeadingWhitespace
import uk.co.nickthecoder.brentwood.wiki.tree.ListItemNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

/**
 * Creates a ListItemNode till the end of the line.
 * This implementation does NOT allow list item wiki code to span more than one line.
 *
 * The parser has special code to automatically add a ListNode before a set of ListItems,
 * as well as terminating the ListNode after non-listItem wiki markup is found.
 *
 * NOTE. Unlike HTML, multi-level lists will have only one ListNode, and the ListItemNode
 * keeps track of the indentation level.
 * It is up to the ListNode to begin and end each HTML UL and OL.
 */
class ListItemSyntax(
    prefix: String,
    val level: Int,
    val isOrdered: Boolean
) : AbstractWikiLineSyntax(prefix) {

    override fun autoCloseAtEndOfLine() = false

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(
            skipLeadingWhitespace(lineStr, column + prefix.length),
            ListItemNode(line, column, level, isOrdered)
        )
    }

    override fun expectUnclosed() = true
}
