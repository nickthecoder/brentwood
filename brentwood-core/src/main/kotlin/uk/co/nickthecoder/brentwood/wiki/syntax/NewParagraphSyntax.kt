package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.NewParagraphNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class NewParagraphSyntax(prefix: String = "") : AbstractWikiLineSyntax(prefix) {

    override fun matches(line: String, fromColumn: Int) =
        if (prefix == "") {
            if (line.isBlank()) {
                0
            } else {
                -1
            }
        } else {
            super.matches(line, fromColumn)
        }

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, NewParagraphNode(line, column))
    }
}
