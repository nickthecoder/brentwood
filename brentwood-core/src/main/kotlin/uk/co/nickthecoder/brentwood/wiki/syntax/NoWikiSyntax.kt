package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.NoWikiNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class NoWikiSyntax(

    prefix: String = "<nowiki>",
    suffix: String = "</nowiki>"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, NoWikiNode(line, column))
    }

}
