package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.PlainText

/**
 * Includes the current page's title as plain text.
 * I created this for pages which were "sourced" i.e. the source code is included, so we don't know the
 * page's name.
 * However, I wanted to use it within a link's URL, which really isn't sensible!
 * We shouldn't be evaluating wiki code when parsing a url!
 * So instead, I added code within LinkSyntax itself. But I've left this here... unused.
 */
class PageTitleSyntax(

    prefix: String = "{{title",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, PlainText(line, column, details.pageName.title))
    }

}
