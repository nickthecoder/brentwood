package uk.co.nickthecoder.brentwood.wiki.syntax

// The state values. This is the next symbol we are expecting.
private const val OPEN_BRACKET = 0
private const val NAME = 1
private const val OPEN_QUOTE = 2
private const val VALUE_WITHOUT_QUOTES = 3
private const val VALUE = 4
private const val TRAILING_WHITESPACE = 5

class ParameterException(message: String) : Exception(message)

fun parseParameters(parameters: Map<String, Regex>, line: String, fromColumn: Int): Pair<Int, Map<String, String>> {

    var foundOpen = false
    var state = OPEN_BRACKET
    val result = mutableMapOf<String, String>()
    var buffer = StringBuilder()
    lateinit var name: String
    // The position where we last had a completely clean parse.
    // If we discover text which might be a name, but not named in the parameters, then we can
    // return this position
    var cleanColumn = fromColumn

    //println("Parsing $line from $fromColumn")

    // Return true if we should return from parseParameters
    fun endOfValue(nextIndex: Int): Boolean {
        val value = buffer.toString()
        buffer = StringBuilder()
        val regex = parameters[name] ?: return true

        if (regex.matches(value)) {
            result[name] = value
            state = NAME
            cleanColumn = nextIndex
        } else {
            throw ParameterException("Bad parameter value for parameter $name : '$value'  (regex=$regex)")
        }
        return false
    }

    for (i in fromColumn until line.length) {
        val c = line[i]
        when (state) {

            OPEN_BRACKET -> {
                if (c.isWhitespace()) continue
                if (c == '(') {
                    foundOpen = true
                    state = NAME
                } else if (c.isLetterOrDigit()) {
                    state = NAME
                    buffer = StringBuilder()
                    buffer.append(c)
                } else {
                    // Didn't find any parameters
                    return Pair(fromColumn, result)
                }
            }

            NAME -> {
                // skip spaces before the parameter name
                if (c.isWhitespace()) continue
                // Skip commas between the previous value and the next name
                // Note, commas are optional, and I don't tend to use them.
                if (c == ',' && result.isNotEmpty()) continue

                if (foundOpen && c == ')') {
                    if (i == line.length - 1) return Pair(line.length, result)
                    state = TRAILING_WHITESPACE
                } else if (c.isLetterOrDigit()) {
                    buffer.append(c)
                } else if (c == '=') {
                    state = OPEN_QUOTE
                    name = buffer.toString()
                    buffer = StringBuilder()
                    if (parameters[name] == null) {
                        if (foundOpen) {
                            throw ParameterException("Unexpected parameter name $name")
                        } else {
                            return Pair(cleanColumn, result)
                        }
                    }
                } else {
                    return Pair(cleanColumn, result)
                }
            }

            OPEN_QUOTE -> {
                if (c.isWhitespace()) continue
                if (c == '"') {
                    state = VALUE
                } else {
                    state = VALUE_WITHOUT_QUOTES
                    buffer = StringBuilder()
                    buffer.append(c)
                }
            }

            VALUE_WITHOUT_QUOTES -> {
                if (c == ' ') {
                    if (endOfValue(i)) return Pair(cleanColumn, result)
                } else {
                    buffer.append(c)
                }
            }

            VALUE -> {
                if (c == '"') {
                    if (endOfValue(i + 1)) return Pair(cleanColumn, result)
                } else {
                    buffer.append(c)
                }
            }

            TRAILING_WHITESPACE -> {
                if (!c.isWhitespace()) {
                    return Pair(i, result)
                }
            }

        }

    }

    // We have got to the end of the string
    val end = when (state) {
        OPEN_BRACKET -> fromColumn
        NAME -> cleanColumn
        VALUE_WITHOUT_QUOTES -> {
            endOfValue(0)
            line.length
        }
        else -> line.length
    }

    return Pair(end, result)

}
