package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.PreNode

class PreSyntax(

    prefix: String = "{{{",
    suffix: String = "}}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        return Pair(column + prefix.length, PreNode(line, column))
    }
}
