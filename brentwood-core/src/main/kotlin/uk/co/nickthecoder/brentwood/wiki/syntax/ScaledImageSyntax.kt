package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.WikiImageScaler
import uk.co.nickthecoder.brentwood.util.REGEX_BOOLEAN
import uk.co.nickthecoder.brentwood.util.REGEX_LEFT_RIGHT
import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.ImgNode
import uk.co.nickthecoder.brentwood.wiki.tree.InternalLinkNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class ScaledImageSyntax(

    prefix: String = "{{thumbnail",
    suffix: String = "}}",
    val imageScaler: WikiImageScaler

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val imagePageName = details.engine.createName(values["page"] ?: "none", details.pageName.namespace)

        val renderImagePageName = imageScaler.scale(imagePageName)

        val imgNode = ImgNode(line, column, renderImagePageName, details.engine.urlFactory.media(renderImagePageName))

        if (values["renderLink"] != "false") {
            return Pair(
                end,
                InternalLinkNode(line, column, imagePageName, details.engine.urlFactory.view(imagePageName)).apply {
                    children.add(
                        imgNode
                    )
                })
        } else {
            return Pair(end, imgNode)
        }
    }

    companion object {

        private val parameters = mapOf(
            "renderLink" to REGEX_BOOLEAN,
            "page" to REGEX_PAGE_NAME,
            "float" to REGEX_LEFT_RIGHT
        )

    }
}
