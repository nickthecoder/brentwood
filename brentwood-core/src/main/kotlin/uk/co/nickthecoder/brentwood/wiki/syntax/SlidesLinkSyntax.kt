package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_NUMBER
import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.WikiException
import uk.co.nickthecoder.brentwood.wiki.tree.InternalLinkNode
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.PlainText

class SlidesLinkSyntax(

    prefix: String = "{{slides",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val pageName = details.engine.createName(values["page"]!!, details.pageName.namespace)
        val slideNumber = values["slide"]?.toIntOrNull() ?: 1

        if (!pageName.exists()) {
            throw WikiException(line, column, details, "Page not found : '${values["page"]}'")
        }

        val node = InternalLinkNode(line, column, pageName, details.engine.urlFactory.slide(pageName, slideNumber))
        node.children.add(PlainText(0, 0, pageName.title))
        return Pair(end, node)

    }

    companion object {
        private val parameters = mapOf(
            "page" to REGEX_PAGE_NAME,
            "slide" to REGEX_NUMBER
        )
    }

}