package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_BOOLEAN
import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.WikiException
import uk.co.nickthecoder.brentwood.wiki.tree.*

/**
 * Similar to [IncludeSyntax], but instead of including the whole page, it only includes the first part of
 * the page, stopping before the table of contents or a heading.
 *
 * I use this when I want a "contents" page, with links to articles, and rather than having duplicated
 * sentences, the contents page can include the summary of each article.
 */
class SummarySyntax(

    prefix: String = "{{summary",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val pageName = details.engine.createName(values["page"]!!, details.pageName.namespace)

        if (!pageName.exists()) {
            throw WikiException(line, column, details, "Page not found : '${values["page"]}'")
        }

        val more = values["more"] != "false"
        val includeTitle = values["includeTitle"] != "false"

        try {
            val node = details.engine.parser.parseSummary(pageName, pageName.load()).rootNode
            if (node is AutoParagraphBlock) {

                if (includeTitle) {
                    node.children.add(0,
                        HeadingNode(line, column, 1).apply {
                            children.add(PlainText(line, column, pageName.title))
                        }
                    )
                }

                if (more) {
                    node.children.add(
                        DivNode(line, column, "summaryMore", false).apply {
                            children.add(
                                InternalLinkNode(line, column, pageName, details.engine.urlFactory.view(pageName))
                                    .apply {
                                        children.add(PlainText(line, column, "More..."))
                                    }
                            )
                        }
                    )

                }
            }
            return Pair(end, node)

        } catch (e: WikiException) {
            throw WikiException(line, column, details, "Summary failed")
        }
    }

    companion object {
        private val parameters = mapOf(
            "page" to REGEX_PAGE_NAME,
            "includeTitle" to REGEX_BOOLEAN,
            "more" to REGEX_BOOLEAN
        )
    }

}
