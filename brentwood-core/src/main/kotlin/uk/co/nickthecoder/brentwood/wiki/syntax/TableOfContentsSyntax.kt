package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.TableOfContentsNode

/**
 * This class is a stub. Not implemented yet.
 * When finished, move out of "home"
 */
class TableOfContentsSyntax(

    prefix: String = "{{toc",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix), EndsSummary {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val levels = values["levels"]?.toIntOrNull() ?: 2

        return Pair(end, TableOfContentsNode(line, column, details.rootNode, levels))
    }

    companion object {

        private val parameters = mapOf(
            "levels" to Regex("[0-9]*")
        )

    }

}
