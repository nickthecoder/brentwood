package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.util.REGEX_CLASS
import uk.co.nickthecoder.brentwood.util.REGEX_NUMBER
import uk.co.nickthecoder.brentwood.util.REGEX_NUMBER_OR_PERCENT
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node
import uk.co.nickthecoder.brentwood.wiki.tree.TableCellNode
import uk.co.nickthecoder.brentwood.wiki.tree.TableNewRowNode
import uk.co.nickthecoder.brentwood.wiki.tree.TableNode


class TableSyntax(

    prefix: String = "{|",
    suffix: String = "|}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, prefix.length)
        val cssClass = values["class"]

        return Pair(end, TableNode(line, column, cssClass))
    }

    companion object {
        val parameters = mapOf(
            "class" to REGEX_CLASS,
            "width" to REGEX_NUMBER_OR_PERCENT
        )
    }
}

interface ClosesTableCell

class TableNewRowSyntax(

    prefix: String = "|-"

) : AbstractWikiLineSyntax(prefix), ClosesTableCell {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val cssClass = values["class"]

        return Pair(end, TableNewRowNode(line, column, cssClass))
    }

    companion object {
        val parameters = mapOf(
            "class" to REGEX_CLASS
        )
    }
}

class TableCellSyntax(

    prefix: String = "|",
    val isHeading: Boolean = false

) : AbstractWikiSyntax(prefix, ""), ClosesTableCell {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)
        val cssClass = values["class"]
        val rowSpan = values["rowspan"]?.toIntOrNull() ?: 1
        val colSpan = values["colspan"]?.toIntOrNull() ?: 1

        return Pair(end, TableCellNode(line, column, cssClass, isHeading, rowSpan, colSpan))
    }

    /**
     * Calls can span multiple lines of source code.
     * See [closedBy}
     */
    // override fun autoCloseAtEndOfLine() = false

    /**
     * Cells are closed by another cell, or by the start of a new row.
     */
    override fun closedBy(syntax: WikiSyntax) = syntax is ClosesTableCell

    companion object {
        val parameters = mapOf(
            "class" to REGEX_CLASS,
            "width" to REGEX_NUMBER_OR_PERCENT,
            "rowspan" to REGEX_NUMBER,
            "colspan" to REGEX_NUMBER
        )

        val heading = TableCellSyntax("||", true)
    }
}
