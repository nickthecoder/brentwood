package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node

/**
 * Defines the syntax for wiki syntax which can only appear at the beginning of
 * a line
 */
interface WikiLineSyntax : WikiSyntax {

    /**
     * If this is false, then [closedBy] is the only way that this gets closed.
     */
    fun autoCloseAtEndOfLine() = true

    override fun matchesClose(lineStr: String, fromColumn: Int): Int = -1

    override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails) = -1

}

abstract class AbstractWikiLineSyntax(

    val prefix: String

) : WikiLineSyntax {

    override var isDeprecated = false

    override fun matches(line: String, fromColumn: Int): Int {
        return if (fromColumn == 0 && line.startsWith(prefix)) {
            0
        } else {
            -1
        }
    }

    override fun toString() = "${this.javaClass.simpleName} $prefix ..."
}
