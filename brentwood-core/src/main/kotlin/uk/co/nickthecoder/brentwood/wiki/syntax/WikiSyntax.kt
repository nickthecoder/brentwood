package uk.co.nickthecoder.brentwood.wiki.syntax

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.tree.Node

interface WikiSyntax {

    fun name() = this.javaClass.simpleName.replace("Syntax", "")

    /**
     * Returns the first position that this syntax matches the line, or -1 if there is no match.
     */
    fun matches(line: String, fromColumn: Int): Int

    /**
     * Returns the position within the line after [fromColumn], which this syntax is closed,
     * or -1 if the line does not contain a terminator.
     */
    fun matchesClose(lineStr: String, fromColumn: Int): Int

    /**
     * Returns the end column of the syntax prefix and the [Node]
     * At this point, the node may not be fully formed (if it is a parent node)
     *
     */
    fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node>

    /**
     * The body of the syntax has been parsed.
     * [fromColumn] will be taken from [createNode]'s return value, and is the position of the closing syntax.
     * Return the position within the line to resume parsing. This will often be [fromColumn] + suffix.length
     */
    fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails): Int

    /**
     * If we encounter [syntax], should this syntax be closed automatically?
     */
    fun closedBy(syntax: WikiSyntax) = false

    /**
     * When we have parsed the whole document, is it ok if this syntax has not been closed.
     */
    fun expectUnclosed() = false

    /**
     * During preview, if there are no errors, then the first deprecated syntax will be highlighted.
     */
    val isDeprecated: Boolean
}

abstract class AbstractWikiSyntax(
    val prefix: String,
    val suffix: String
) : WikiSyntax {

    override var isDeprecated = false

    override fun matches(line: String, fromColumn: Int): Int {
        return line.indexOf(prefix, fromColumn)
    }

    override fun matchesClose(lineStr: String, fromColumn: Int): Int =
        if (suffix.isBlank()) {
            -1
        } else {
            lineStr.indexOf(suffix, fromColumn)
        }

    override fun close(node: Node, lineStr: String, fromColumn: Int, details: ParserDetails) =
        if (lineStr.substring(fromColumn).startsWith(suffix)) {
            fromColumn + suffix.length
        } else {
            -1
        }

    override fun toString() = "${this.javaClass.simpleName} $prefix ... $suffix"
}
