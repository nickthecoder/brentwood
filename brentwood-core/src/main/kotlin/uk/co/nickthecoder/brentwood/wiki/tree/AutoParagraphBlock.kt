package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

/**
 * A [ParentNode] which may contain blocks or non-blocks, or a combination.
 * non-blocks should be wrapped inside paragraph tags, which should be automatically
 * closed when a block is found.
 */
open class AutoParagraphBlock(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun render(parent: FlowContent, engine: WikiEngine) {
        render(children, parent, engine)
    }

    override fun isBlock() = true

    companion object {

        /**
         * By placing this in a companion object other ParentNodes can render their children in the same way
         * as an [AutoParagraphBlock].
         */
        fun render(children: List<Node>, parent: FlowContent, engine: WikiEngine) {
            var paragraph: ParagraphNode? = null
            for (child in children) {
                if (child.isBlock()) {
                    if (paragraph != null) {
                        // We need to end the current paragraph before rendering the next block.
                        paragraph.render(parent, engine)
                        paragraph = null
                    }
                    child.render(parent, engine)
                } else {
                    // We haven't started a paragraph, but we found text, which should be placed into one.
                    if (paragraph == null) {
                        paragraph = ParagraphNode(-1, -1)
                    }
                    // We do NOT render the child, only add it to the paragraph. It will get rendered
                    // either when a block element is found, or at the end of this method.
                    paragraph.children.add(child)
                }
            }
            paragraph?.render(parent, engine)
        }

    }
}
