package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.a
import kotlinx.html.li
import kotlinx.html.ul
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class BackLinksNode(line: Int, column: Int, val backLinks: List<PageName>) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.ul("infoList") {
            for (backLink in backLinks.sorted()) {
                li {
                    a(engine.urlFactory.view(backLink)) { +backLink.title }
                }
                + " "
            }
        }
    }

}
