package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.b
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class BoldNode(line: Int, column: Int) : SimpleParentNode(line, column) {
    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.b {
            renderChildren(this, engine)
        }
    }

}
