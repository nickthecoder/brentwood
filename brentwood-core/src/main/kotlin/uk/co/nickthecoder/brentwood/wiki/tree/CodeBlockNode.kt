package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.wiki.WikiEngine
import uk.co.nickthecoder.brentwood.wiki.syntax.CodeBlockConfig
import uk.co.nickthecoder.brentwood.wiki.syntax.CodeBlockPart

class CodeBlockNode(line: Int, column: Int, val config: CodeBlockConfig) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun isPlainTextContent() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        var openPart: CodeBlockPart? = null
        parent.div(classes = "codeBlock") {
            ol {
                for (child in children) {
                    if (child is PlainText) {
                        val lines = child.text.split("\n")
                        for (i in lines.indices) {
                            val line = lines[i]
                            if ((i > 0 && i < lines.size - 1) || line.isNotBlank()) {
                                li {
                                    openPart = renderLine(line, openPart)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Returns a [CodeBlockPart] if a multi-line part hasn't been closed.
     */
    private fun FlowContent.renderLine(line: String, multilinePart: CodeBlockPart?): CodeBlockPart? {
        var from = 0

        // Find the closing part of the previously opened multi-line CodeBlockPart.
        if (multilinePart != null) {
            val i = line.indexOf(multilinePart.end)
            if (i >= 0) {
                from = i + multilinePart.end.length
                span(classes = multilinePart.cssClass) {
                    +line.substring(0, i + multilinePart.end.length)
                }
                // Note, we don't return here, as the multi-line has been closed.
            } else {
                span(classes = multilinePart.cssClass) {
                    +line
                }
                // Multi-line hasn't been closed, so return without looking for keywords etc.
                return multilinePart
            }
        }

        var openPart: CodeBlockPart? = null

        do {
            // The index of the left-most keyword
            var firstIndex = Int.MAX_VALUE
            // The style class for the left-most keyword etc
            var cssClass = openPart?.cssClass ?: ""
            // The text that matched the left-most keyword etc
            var found = ""

            // Find the first keyword, trailing etc.
            // For each match, we compare the found match with firstIndex,
            // and only process those that are before the previously found match.

            for (keyword in config.keywords) {
                val i = line.indexOf(keyword, from)
                if (i >= 0 && i < firstIndex) {
                    val end = i + keyword.length
                    if (i == 0 || !line[i - 1].isLetter()) {
                        if (end >= line.length || !line[end].isLetter()) {
                            cssClass = "codeKeyword"
                            firstIndex = i
                            found = keyword
                            openPart = null
                        }
                    }
                }
            }

            for (trailing in config.trailingComment) {
                val i = line.indexOf(trailing, from)
                if (i >= 0 && i < firstIndex) {
                    cssClass = "codeComment"
                    firstIndex = i
                    found = line.substring(i)
                    openPart = null
                }
            }

            for (codeBlockPart in config.parts) {
                val i = line.indexOf(codeBlockPart.start, from)
                if (i >= 0 && i < firstIndex) {
                    openPart = codeBlockPart
                    firstIndex = i
                }
            }

            for (operator in config.operators) {
                val i = line.indexOf(operator, from)
                if (i >= 0 && i < firstIndex) {
                    cssClass = "codeSpecial"
                    firstIndex = i
                    found = operator
                    openPart = null
                }
            }

            // firstIndex will now either be MAX_VALUE if no keyword etc was found, or the position
            // of the keyword etc. In which case, cssClass and found are also set.

            if (firstIndex != Int.MAX_VALUE) {
                if (from < line.length) {
                    val before = line.substring(from, firstIndex)
                    if (before.isNotEmpty()) {
                        +before
                    }
                }

                if (openPart == null) {
                    span(classes = cssClass) {
                        +found
                    }
                    from = firstIndex + found.length
                } else {

                    val i = line.indexOf(openPart.end, firstIndex + openPart.start.length)
                    val classes = openPart.cssClass
                    if (i >= 0) {
                        found = line.substring(firstIndex, i + openPart.end.length)
                        from = i + openPart.end.length
                        openPart = null
                    } else {
                        // Not closed!
                        if (!openPart.isMultiline) {
                            openPart = null // Oops, not closed correctly, but ignore that!
                        }
                        found = line.substring(firstIndex)
                        from = line.length
                    }
                    span(classes = classes) {
                        +found
                    }
                }

            }

        } while (found.isNotEmpty() && from < line.length)

        val remainder = line.substring(from)

        if (remainder.isNotBlank()) {
            +remainder
        }

        return openPart
    }


}
