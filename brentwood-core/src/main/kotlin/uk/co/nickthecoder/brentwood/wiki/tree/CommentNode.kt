package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class CommentNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun isPlainTextContent() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        // The DSL doesn't have a method to create a comment with a lambda, only plain text.
        // However, the children of the node should be just a single PlainText, so there is
        // no problem. We won't end up with multiple comments.
        for (child in children) {
            if (child is PlainText) {
                parent.comment(child.text)
            }
        }
    }
}