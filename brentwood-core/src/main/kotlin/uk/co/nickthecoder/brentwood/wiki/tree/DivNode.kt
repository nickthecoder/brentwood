package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.div
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class DivNode(

    line: Int,
    column: Int,
    val cssClass: String,
    val isAutoParagraphBlock: Boolean

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.div(cssClass) {
            if (isAutoParagraphBlock) {
                AutoParagraphBlock.render(children, this, engine)
            } else {
                renderChildren(this, engine)
            }
        }
    }

}
