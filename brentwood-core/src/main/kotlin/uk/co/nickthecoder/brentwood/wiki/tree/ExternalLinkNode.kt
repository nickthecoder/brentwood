package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.a
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ExternalLinkNode(

    line: Int,
    column: Int,
    val url: String

) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.a(url) {
            renderChildren(this, engine)
        }
    }

}
