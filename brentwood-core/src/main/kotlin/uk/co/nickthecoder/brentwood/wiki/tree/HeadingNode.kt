package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class HeadingNode(
    line: Int,
    column: Int,
    val level: Int,
    val editLink: String? = null

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    fun anchor() = text().filter { it.isLetterOrDigit() }

    override fun render(parent: FlowContent, engine: WikiEngine) {

        when (level) {

            // NOTE h1 is for the page title, so wiki headings start at h2.
            1 -> parent.h2 {
                id = anchor()
                renderChildren(this, engine)
            }
            2 -> parent.h3 {
                id = anchor()
                renderChildren(this, engine)
            }
            3 -> parent.h4 {
                id = anchor()
                renderChildren(this, engine)
            }
            4 -> parent.h5 {
                id = anchor()
                renderChildren(this, engine)
            }
            else -> parent.h6 {
                id = anchor()
                renderChildren(this, engine)
            }
        }
        if (editLink != null) {
            parent.div("noSearch editSection editSection$level") {
                a(editLink) {
                    +"Edit"
                }
            }
        }

    }
}
