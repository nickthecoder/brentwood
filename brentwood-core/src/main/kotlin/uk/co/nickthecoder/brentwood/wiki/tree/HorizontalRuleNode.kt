package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.hr
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class HorizontalRuleNode(line: Int, column: Int) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.hr()
    }

}
