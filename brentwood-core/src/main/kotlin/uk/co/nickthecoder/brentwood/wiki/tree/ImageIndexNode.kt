package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.a
import kotlinx.html.img
import uk.co.nickthecoder.brentwood.util.GroupBy
import uk.co.nickthecoder.brentwood.wiki.Namespace
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ImageIndexNode(

    line: Int,
    column: Int,
    val namespace: Namespace

) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {

        val names = namespace.list().filter {
            it.isMedia() && engine.imageExtensions.contains(it.extension()) && !it.isRelation() && !it.isHidden()
        }

        val groupBy = GroupBy(names) { it.title.first() }

        engine.layout.groupByInitials(parent, groupBy)

        for (initial in groupBy.usedInitials()) {
            engine.layout.groupByAnchor(parent, groupBy, initial)

            engine.layout.thumbnails(parent) {

                for (imagePageName in groupBy.itemsForInitial(initial)) {

                    val renderImagePageName = engine.thumbnailScaler.scale(imagePageName, false)

                    engine.layout.thumbnail(this, main = {
                        a(engine.urlFactory.view(imagePageName)) {
                            img(imagePageName.title, engine.urlFactory.media(renderImagePageName))
                        }
                    }) {
                        a(engine.urlFactory.view(imagePageName)) {
                            +imagePageName.title
                        }
                    }
                }
            }
        }
    }

}
