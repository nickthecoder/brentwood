package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

/**
 * Renders an img tag inside a figure tag.
 * If this has children, then they are added to a figcaption tag.
 */
class ImageNode(

    line: Int,
    column: Int,
    val pageName: PageName?,
    val url: String,
    val cssClass: String?,
    val pageLink: String?

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun text(): String {
        return pageName.toString()
    }

    override fun render(parent: FlowContent, engine: WikiEngine) {
        if (cssClass != null) {
            parent.div(classes = cssClass) {
                render2(this, engine)
            }
        } else {
            render2(parent, engine)
        }
    }

    private fun render2(parent: FlowContent, engine: WikiEngine) {
        if (pageLink == null) {
            render3(parent, engine)
        } else {
            parent.a(pageLink) {
                render3(this, engine)
            }
        }
    }

    private fun render3(parent: FlowContent, engine: WikiEngine) {
        parent.figure {
            img(pageName?.toString() ?: "", url)
            if (children.isNotEmpty()) {
                figcaption {
                    renderChildren(this, engine)
                }
            }
        }
    }
}
