package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.img
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

/**
 * Renders a simple img tag.
 */
class ImgNode(

    line: Int,
    column: Int,
    val pageName: PageName?,
    val url: String

) : SimpleNode(line, column) {

    override fun isBlock() = false

    override fun text(): String {
        return pageName?.toString() ?: url
    }

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.img(pageName?.toString() ?: "", url)
    }
}
