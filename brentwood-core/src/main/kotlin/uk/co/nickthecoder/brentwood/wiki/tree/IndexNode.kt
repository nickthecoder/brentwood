package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.util.GroupBy
import uk.co.nickthecoder.brentwood.wiki.Namespace
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class IndexNode(
    line: Int,
    column: Int,
    val namespace: Namespace,
    val includeMedia: Boolean,
    val includeHidden: Boolean

) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {

        var names = namespace.list().filter { !it.isRelation() }
        if (!includeHidden) {
            names = names.filter { !it.isHidden() }
        }
        if (!includeMedia) {
            names = names.filter { !it.isMedia() }
        }


        val groupBy = GroupBy(names) { it.title.first() }

        engine.layout.groupByInitials(parent, groupBy)

        for (initial in groupBy.usedInitials()) {
            engine.layout.groupByAnchor(parent, groupBy, initial)
            parent.div("index_group") {

                ul("infoList") {
                    for (name in groupBy.itemsForInitial(initial)) {

                        li {
                            a(engine.urlFactory.view(name)) {
                                +name.title
                            }
                        }
                        + " "
                    }
                }
            }
        }
    }

}

