package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.a
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class InternalLinkNode(

    line: Int,
    column: Int,
    val pageName: PageName,
    val url: String

) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.a(url, classes = if (pageName.exists()) "" else "notFound") {
            renderChildren(this, engine)
        }
    }

}
