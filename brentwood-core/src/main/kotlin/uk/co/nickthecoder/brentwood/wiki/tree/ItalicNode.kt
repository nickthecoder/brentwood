package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.i
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ItalicNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.i {
            renderChildren(this, engine)
        }
    }

}
