package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.br
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class LineBreakNode(line: Int, column: Int) : SimpleNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.br()
    }

}
