package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

/**
 * A LinkNode will end up having a single child, which is either an ExternalLinkNode, or an InternalLinkNode.
 * The child is added when LinkSyntax.close(...) is called.
 */
class LinkNode(line: Int, column: Int, val isLabelled : Boolean) : SimpleParentNode(line, column) {

    override fun isPlainTextContent() = !isLabelled

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        renderChildren(parent, engine)
    }

}
