package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.OL
import kotlinx.html.UL
import kotlinx.html.li
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ListItemNode(

    line: Int,
    column: Int,
    val level: Int,
    val isOrdered: Boolean

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    //override fun ignoreWhiteSpace() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {

        if (parent is UL) {
            parent.li {
                renderChildren(this, engine)
            }
        } else if (parent is OL) {
            parent.li {
                renderChildren(this, engine)
            }
        }
    }
}
