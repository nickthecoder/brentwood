package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ListNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun ignoreWhiteSpace() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        deeperList(parent, 0, 0, engine)
    }

    fun deeperList(parent: FlowContent, from: Int, currentLevel: Int, engine: WikiEngine) {
        if (from >= children.size) return
        val first = children[from] as? ListItemNode ?: return
        if (first.level <= currentLevel) return
        val level = first.level

        var isFirst = true
        var isOrdered = true
        for (i in from until children.size) {
            val child = children[i] as? ListItemNode ?: continue

            if (child.level < currentLevel) return

            if (child.level == level && (isFirst || child.isOrdered != isOrdered)) {
                isFirst = false
                isOrdered = child.isOrdered
                if (child.isOrdered) {
                    parent.ol {
                        renderListItems(this, i, child.isOrdered, child.level, engine)
                    }
                } else {
                    parent.ul {
                        renderListItems(this, i, child.isOrdered, child.level, engine)
                    }
                }
            }
        }
    }

    fun renderListItems(parent: FlowContent, from: Int, isOrdered: Boolean, currentLevel: Int, engine: WikiEngine) {

        for (i in from until children.size) {
            val child = children[i]
            if (child !is ListItemNode) continue

            if (child.level < currentLevel) {
                return
            }

            if (child.level == currentLevel) {
                if (child.isOrdered == isOrdered) {
                    if (parent is OL) {
                        parent.li {
                            child.renderChildren(this, engine)
                            // We must now check for a following higher level, and if so, start a new nested list.
                            deeperList(this, i + 1, child.level, engine)
                        }
                    } else if (parent is UL) {
                        parent.li {
                            child.renderChildren(this, engine)
                            deeperList(this, i + 1, child.level, engine)
                        }
                    }

                } else {
                    return
                }
            }

        }
    }

}
