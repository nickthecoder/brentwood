package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class NewParagraphNode(line: Int, column: Int) : SimpleNode(line, column) {

    override fun isBlock() = true

    /**
     *  Do nothing.
     * The mere presence of this node will cause the [AutoParagraphNode] to
     * end the old node, and start a new one.
     */
    override fun render(parent: FlowContent, engine: WikiEngine) {
    }
}
