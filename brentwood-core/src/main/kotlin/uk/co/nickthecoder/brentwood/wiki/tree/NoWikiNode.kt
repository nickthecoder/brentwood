package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class NoWikiNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isPlainTextContent() = true

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        renderChildren(parent, engine)
    }

}
