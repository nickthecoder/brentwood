package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine


/**
 * Any piece of wiki syntax
 */
interface Node {

    /**
     * Zero based line number where the syntax began
     */
    val line: Int

    /**
     * Zero based column number where the syntax began
     */
    val column: Int

    fun render(parent: FlowContent, engine: WikiEngine)

    fun isBlock(): Boolean

    fun text(buffer: StringBuilder) {}

    fun text() = buildString {
        text(this)
    }

    fun name() = this.javaClass.simpleName.replace("Node", "")


}


fun Node.toTreeString(inFull: Boolean = false): String {
    return buildString {
        toTreeString(this, 0, inFull)
    }
}

fun Node.toTreeString(buffer: StringBuilder, indent: Int, inFull: Boolean) {
    if (inFull) {
        buffer.appendLine("${"    ".repeat(indent)}$this")
    } else {
        buffer.appendLine("${"    ".repeat(indent)}${this.javaClass.simpleName}")
    }
    if (this is ParentNode) {
        for (child in children) {
            child.toTreeString(buffer, indent + 1, inFull)
        }
    }
}
