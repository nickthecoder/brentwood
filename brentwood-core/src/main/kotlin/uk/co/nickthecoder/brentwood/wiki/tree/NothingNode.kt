package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class NothingNode(line: Int, column: Int) : SimpleNode(line, column) {
    override fun isBlock() = false
    override fun render(parent: FlowContent, engine: WikiEngine) {}
}
