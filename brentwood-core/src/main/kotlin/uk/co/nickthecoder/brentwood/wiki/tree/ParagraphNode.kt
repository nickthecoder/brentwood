package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.p
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class ParagraphNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock(): Boolean = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.p {
            renderChildren(parent, engine)
        }
    }

}
