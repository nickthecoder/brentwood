package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

interface ParentNode : Node {

    val children: MutableList<Node>

    /**
     * Should the source within this be treated as plain-text, and not parsed as wiki code?
     */
    fun isPlainTextContent() = false

    /**
     * Should white space be ignored. For example, tables don't care about white space
     */
    fun ignoreWhiteSpace() = false

    fun renderChildren(container: FlowContent, engine: WikiEngine) {
        for (child in children) {
            child.render(container, engine)
        }
    }

    override fun text(buffer: StringBuilder) {
        for (child in children) {
            child.text(buffer)
        }
    }

}
