package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class PlainText(

    line: Int,
    column: Int,
    val text: String

) : SimpleNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        with(parent) {
            +text
        }
    }

    override fun text(buffer: StringBuilder) {
        buffer.append(text)
    }

    override fun toString() = "PlainText : '$text'"
}
