package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.pre
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class PreNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun isPlainTextContent() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.pre {
            for (child in children) {
                child.render(this, engine)
            }
        }
    }

}
