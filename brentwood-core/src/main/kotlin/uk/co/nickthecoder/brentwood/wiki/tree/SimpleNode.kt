package uk.co.nickthecoder.brentwood.wiki.tree

abstract class SimpleNode(

    override val line: Int,
    override val column: Int

) : Node
