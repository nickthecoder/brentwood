package uk.co.nickthecoder.brentwood.wiki.tree

abstract class SimpleParentNode(line: Int, column: Int) : SimpleNode(line, column), ParentNode {

    override val children = mutableListOf<Node>()

    override fun toString() = "${this.javaClass.simpleName} children=${children.size}"
}
