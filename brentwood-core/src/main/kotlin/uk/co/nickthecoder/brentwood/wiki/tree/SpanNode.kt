package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.span
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class SpanNode(line: Int, column: Int, val cssClass: String) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.span(cssClass) {
            renderChildren(this, engine)
        }
    }

}
