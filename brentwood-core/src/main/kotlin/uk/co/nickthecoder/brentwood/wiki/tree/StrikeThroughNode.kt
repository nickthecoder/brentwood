package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.del
import uk.co.nickthecoder.brentwood.wiki.WikiEngine


class StrikeThroughNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.del {
            renderChildren(this, engine)
        }
    }

}
