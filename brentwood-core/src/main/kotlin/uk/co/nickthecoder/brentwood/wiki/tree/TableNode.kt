package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.util.th2
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

/**
 * A TableNode will contains [TableCellNode]s and [TableNewRowNode] in a FLAT structure.
 * The [TableNewRowNode] are not parents, and do NOT contain table cells. They merely
 * mark the place that a new row should start.
 */
class TableNode(

    line: Int,
    column: Int,
    val cssClass: String?

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun ignoreWhiteSpace() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {

        parent.table(cssClass) {
            var from = 0
            while (from >= 0) {
                val rowClass = if (from > 0) {
                    (children[from - 1] as? TableNewRowNode)?.cssClass
                } else {
                    null
                }

                tr(rowClass) {
                    from = renderCells(this, engine, from)
                }
            }
        }
    }

    /**
     * Returns the index of the next table cell, or -1 if we got to the end of the table.
     */
    private fun renderCells(parent: TR, engine: WikiEngine, from: Int): Int {
        for (i in from until children.size) {
            val child = children[i]

            if (child is TableNewRowNode) return i + 1

            if (child is TableCellNode) {

                if (child.isHeading) {
                    parent.th2(child.cssClass) {
                        if (child.rowSpan != 1) {
                            rowSpan = child.rowSpan.toString()
                        }
                        if (child.colSpan != 1) {
                            colSpan = child.colSpan.toString()
                        }
                        child.renderChildren(this, engine)
                    }
                } else {
                    parent.td(child.cssClass) {
                        child.renderChildren(this, engine)
                    }
                }
            }

        }

        return -1
    }
}

/**
 * Note, this is a SEPARATOR, is does NOT hold a set of table cells,
 * it merely tells TableNode to start a new row.
 */
class TableNewRowNode(line: Int, column: Int, val cssClass: String?) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        // The TableNode renders us, and our children, so we need not do anything.
    }

    override fun toString(): String = this.javaClass.simpleName

}


class TableCellNode(
    line: Int, column: Int,
    val cssClass: String?,
    val isHeading: Boolean,
    val rowSpan: Int,
    val colSpan: Int

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        // The TableNode renders us, and our children, so we need not do anything.
    }

    override fun ignoreWhiteSpace() = true

}

