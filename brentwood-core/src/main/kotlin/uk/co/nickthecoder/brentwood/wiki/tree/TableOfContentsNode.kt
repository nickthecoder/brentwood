package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.div
import uk.co.nickthecoder.brentwood.wiki.WikiEngine

class TableOfContentsNode(

    line: Int,
    column: Int,
    val rootNode: Node,
    val levels: Int

) : SimpleNode(line, column) {

    override fun isBlock() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {
        val list = ListNode(line, column)

        fun scan(parent: Node) {
            if (parent !is ParentNode) return
            for (child in parent.children) {
                if (child is HeadingNode && child.level <= levels) {

                    list.children.add(
                        ListItemNode(line, column, child.level, true).apply {
                            children.add(

                                ExternalLinkNode(line, column, "#${child.anchor()}").apply {
                                    children.add(
                                        PlainText(line, column, child.text())
                                    )
                                }
                            )
                        }
                    )
                } else if (child is ParentNode) {
                    scan(child)
                }
            }
        }
        scan(rootNode)

        parent.div("toc") {
            list.render(this, engine)
        }


    }

}
