package uk.co.nickthecoder.brentwood.wiki.tree

import kotlinx.html.FlowContent
import kotlinx.html.span
import uk.co.nickthecoder.brentwood.wiki.WikiEngine


class UnderscoreNode(line: Int, column: Int) : SimpleParentNode(line, column) {

    override fun isBlock() = false

    override fun render(parent: FlowContent, engine: WikiEngine) {
        parent.span("underscore") {
            renderChildren(this, engine)
        }
    }

}
