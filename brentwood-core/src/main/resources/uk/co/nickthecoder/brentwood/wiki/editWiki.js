// When "tab" is pressed...
// Indent the block of text containing the selection (with 4 spaces per line).
// We NEVER insert spaces in the middle of a line (most text editors do!)
// even when the selection length is 0.
function indent( textArea ) {
    const text = textArea.value;
    var selectionStart = textArea.selectionStart;

    // Find the start of the line
    var start = textArea.selectionStart;
    while ( start > 0 && text.charAt(start-1) != "\n" ) {
        start --;
    }

    const block = text.substring( start, textArea.selectionEnd );
    const replacement = "    " + block.split("\n").join("\n    ");
    textArea.value = text.substring( 0, start ) + replacement + text.substring( textArea.selectionEnd );
    textArea.setSelectionRange( selectionStart + 4, start + replacement.length );
}
// When shift+tab is pressed...
// Un-indent the block of text containing the selection (with up to 4 spaces per line).
// If you hit, tab and then shift+tab, you will ALWAYS be back to where you started.
// Most text-editors don't have this property!
function outdent( textArea ) {
    const text = textArea.value;
    var selectionStart = textArea.selectionStart;

    // Find the start of the line
    var start = textArea.selectionStart;
    while ( start > 0 && text.charAt(start-1) != "\n" ) {
        start --;
    }

    // Find the end of line after selectionEnd
    var end = textArea.selectionEnd;
    while ( end < text.length-1 && text[end] != "\n" ) {
        end ++
    }

    const block = text.substring( start, end );
    const lines = block.split("\n");
    var indentedBy = 0;
    const regex =  / {0,4}/;
    for (let i = 0; i < lines.length; i ++) {
        const replacement = lines[i].replace( regex, "" );
        if (i == 0) indentedBy = lines[i].length - replacement.length;
        lines[i] = replacement;
    }
    const replacement = lines.join("\n");
    textArea.value = text.substring( 0, start ) + replacement + text.substring( end );
    textArea.setSelectionRange( start, start + replacement.length );
 }

window.addEventListener('load', (event) => {
    document.getElementById("editTextArea").addEventListener('keydown', (e) => {
        if (e.key == "Tab") {
            if (e.shiftKey) {
                outdent(editTextArea);
            } else {
                indent(editTextArea);
            }
            e.preventDefault();
        }
    });
});
