package uk.co.nickthecoder.brentwood.parser

import junit.framework.TestCase
import uk.co.nickthecoder.brentwood.util.escapeURLParam

class TestEscape : TestCase() {

    fun testEscapeUrlParam() {
        assertEquals("Hello%20World", escapeURLParam("Hello World"))

        assertEquals("Morissette%2C%20Alanis", escapeURLParam("Morissette, Alanis"))
    }


}
