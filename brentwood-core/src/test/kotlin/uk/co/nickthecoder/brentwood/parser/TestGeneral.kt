package uk.co.nickthecoder.brentwood.parser

import junit.framework.TestCase
import uk.co.nickthecoder.brentwood.wiki.DummyStorage
import uk.co.nickthecoder.brentwood.wiki.Namespace
import uk.co.nickthecoder.brentwood.wiki.PageName

class TestGeneral : TestCase() {

    fun testComparePageName() {

        val namespaceDefault = Namespace("default", DummyStorage())
        val namespaceRecipe = Namespace("recipe", DummyStorage())

        /**
         * We don't actually care about the VALUE of the compare, only if it is zero,
         * less than zero or greater than zero.
         */
        fun compare(a: PageName, b: PageName): Int {
            val value = a.compareTo(b)
            return if (value == 0) {
                0
            } else if (value < 0) {
                -1
            } else {
                1
            }
        }

        val home = PageName(namespaceDefault, "Home")
        val homeAbout = PageName(namespaceDefault, "Home", "About")
        val homeFoo = PageName(namespaceDefault, "Home", "Foo")

        val moan = PageName(namespaceDefault, "Moan")

        val recipeHome = PageName(namespaceRecipe, "Home")

        // Same name
        assertEquals(0, compare(home, home))

        // Only the titles are different
        assertEquals(1, compare(moan, home))
        assertEquals(-1, compare(home, moan))

        // Related page is after the "main" page.
        assertEquals(1, compare(homeAbout, home))
        assertEquals(-1, compare(home, homeAbout))

        // Related pages are ordered by their relation.
        assertEquals(1, compare(homeFoo, homeAbout))
        assertEquals(-1, compare(homeAbout, homeFoo))

        // Same title, different namespace name
        assertEquals(1, compare(recipeHome, home))
        assertEquals(-1, compare(home, recipeHome))
    }


}