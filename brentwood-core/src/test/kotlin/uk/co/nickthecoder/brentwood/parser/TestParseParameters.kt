package uk.co.nickthecoder.brentwood.parser

import junit.framework.TestCase
import uk.co.nickthecoder.brentwood.util.REGEX_BOOLEAN
import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.syntax.parseParameters

class TestParseParameters : TestCase() {

    fun testNoParameters() {
        assertEquals(2, parseParameters(emptyMap(), "|| ||", 2).first)
    }

    fun testOneParamNoBrackets() {
        assertEquals(
            19,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo hello=\"world\" }}", 5).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo hello=\"world\" }}", 5).second
        )
    }

    fun testOneParamNoBrackets2() {
        assertEquals(
            19,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo hello=\"world\" xxx}}", 5).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo hello=\"world\" xxx}}", 5).second
        )
    }

    fun testMissingClose() {
        assertEquals(
            20,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\" xxx}}", 5).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\" xxx}}", 5).second
        )
    }

    fun testClosed() {
        assertEquals(
            22,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\") xxx}}", 5).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\") xxx}}", 5).second
        )
    }

    fun testEndOfLine() {
        assertEquals(
            20,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\"", 5).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "{{foo( hello=\"world\"", 5).second
        )
    }

    fun testEndOfLineNoParams() {
        assertEquals(
            2,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "|| ", 2).first
        )
        assertEquals(
            emptyMap<String, String>(),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "|| ", 2).second
        )
    }

    /**
     */
    fun testEndOfLineParamWithoutQuotes() {
        assertEquals(
            14,
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "|| hello=world", 2).first
        )
        assertEquals(
            mapOf("hello" to "world"),
            parseParameters(mapOf("hello" to REGEX_PAGE_NAME), "|| hello=world", 2).second
        )
    }

    /**
     * As above, but "hello" is not a named parameter.
     * Therefore the text should NOT be considered parameters, and instead be the cell's content.
     */
    fun testEndOfLineParamWithoutQuotesUnknown() {
        assertEquals(
            2,
            parseParameters(mapOf("xxx" to REGEX_PAGE_NAME), "|| hello=world", 2).first
        )
        assertEquals(
            emptyMap<String, String>(),
            parseParameters(mapOf("xxx" to REGEX_PAGE_NAME), "|| hello=world", 2).second
        )
    }

    fun testFoo() {
        assertEquals(
            1,
            parseParameters(emptyMap(), "|A", 1).first
        )
    }

    fun testWithoutComma() {
        val params = mapOf("page" to REGEX_PAGE_NAME, "test" to REGEX_BOOLEAN)
        val (end, values) = parseParameters(params, "|(page=\"foo\"  test=\"true\") Hello", 1)

        assertEquals(27, end)
        assertEquals("foo", values["page"])
        assertEquals("true", values["test"])
    }
    fun testComma() {
        val params = mapOf("page" to REGEX_PAGE_NAME, "test" to REGEX_BOOLEAN)
        val (end, values) = parseParameters(params, "|(page=\"foo\", test=\"true\") Hello", 1)

        assertEquals(27, end)
        assertEquals("foo", values["page"])
        assertEquals("true", values["test"])
    }

    fun testSpacesInName() {
        val params = mapOf("page" to REGEX_PAGE_NAME)
        val (end, values) = parseParameters(params, "|(page=\"Kalas Garden Day 0b.jpg\") Hello", 1)

        assertEquals(34, end)
        assertEquals("Kalas Garden Day 0b.jpg", values["page"])
    }

}