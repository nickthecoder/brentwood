package uk.co.nickthecoder.brentwood.parser

import junit.framework.TestCase
import kotlinx.html.body
import kotlinx.html.consumers.DelayedConsumer
import kotlinx.html.html
import kotlinx.html.stream.HTMLStreamBuilder
import uk.co.nickthecoder.brentwood.layout.ExampleLayout
import uk.co.nickthecoder.brentwood.wiki.*

/**
 * Tests the parser by rendering the html to a string buffer, and then comparing that to
 * the expected text.
 *
 * For these tests, I have turned pretty print off (which makes it easier write the expected text!).
 *
 * NOTE, the parser currently places a space at the beginning and end of some paragraphs, where, ideally,
 * there would be nothing. However this doesn't affect the final rendering of the HTML.
 * At the moment, these tests use a simple "assertEquals", and therefore I have manually added these extra
 * spaces. When the Parser is changed, the spaces may change, and therefore the test will fail.
 * At which point, rejig the expected values to place the spaces in the new positions, or
 * change the tests to regex, and state in the tests where spaces are allowed using "\\s?".
 *
 * But that would make the tests harder to read.
 *
 */
class TestParser : TestCase() {

    private val defaultNamespace = Namespace("default", DummyStorage())

    private val layout = ExampleLayout()

    private val engine = WikiEngine(defaultNamespace, DefaultURLFactory("http://localhost:8282", "/wiki"), layout)

    private val pageName = PageName(defaultNamespace, "Test")


    private fun buildHtml(source: String): String {
        val details = engine.parser.parse(pageName, source)
        val node = details.rootNode

        return buildString {
            val consumer = HTMLStreamBuilder<StringBuilder>(this, prettyPrint = false, xhtmlCompatible = false)
            val consumer2 = DelayedConsumer(consumer)
            consumer2.html {
                body {
                    node.render(this, engine)
                }
            }
        }.replace("<html><body>", "")
            .replace("</body></html>", "")
    }

    // Should there be a newline. If one at the end, why not one at the front?
    // The test suite creates non-pretty html.
    fun testSimpleText() {
        assertEquals(
                "<p>Hello</p>",
                buildHtml("Hello")
        )
    }

    fun testTestWithNewLine() {
        assertEquals(
            "<p>Hello World</p>",
            buildHtml("Hello World")
        )
    }


    fun testHeadings() {
        assertEquals(
            "<h2 id=\"Heading2\">Heading2</h2><div class=\"noSearch editSection editSection1\"><a href=\"/wiki/edit/Test?line=0&amp;column=0\">Edit</a></div>",
            buildHtml("==Heading2==")
        )
    }

    fun testPre() {
        assertEquals(
                "<pre>This is a pre block</pre>",
                buildHtml("{{{This is a pre block}}}")
        )
    }

    fun testPre2() {
        // The == should NOT be interpreted as wiki code, it should appear in the pre block as-is.
        assertEquals(
                "<pre>\n==Not a heading==\n</pre>",
                buildHtml("{{{\n==Not a heading==\n}}}")
        )
    }


    fun testBold() {
        assertEquals(
                "<p>This is <b>bold</b> text</p>",
                buildHtml("This is ++bold++ text")
        )
    }

    fun testItalic() {
        assertEquals(
                "<p>This is <i>italic</i> text</p>",
                buildHtml("This is ''italic'' text")
        )
    }

    fun testUnderscore() {
        assertEquals(
                "<p>This is <span class=\"underscore\">underscore</span> text</p>",
                buildHtml("This is __underscore__ text")
        )
    }

    fun testStrikeThrough() {
        assertEquals(
                "<p>This is <del>strike through</del> text</p>",
                buildHtml("This is --strike through-- text")
        )
    }

    fun testLineBreak() {
        assertEquals(
                "<p>This is one line<br> This is another</p>",
                buildHtml("This is one line<br>\nThis is another")
        )
    }

    fun testHorizontalRule() {
        assertEquals(
                "<p>This is one line </p><hr><p> This is another</p>",
            buildHtml("This is one line\n----\nThis is another")
        )
    }

    fun testNewParagraph() {
        assertEquals(
            "<p>This is one paragraph </p><p> This is another</p>",
            buildHtml("This is one paragraph\n\nThis is another")
        )
    }

    fun testComment() {
        assertEquals(
            "<p>Before the comment<!-- ++comment++ -->after the comment</p>",
            buildHtml("Before the comment<!-- ++comment++ -->after the comment")
        )
    }

    fun testMultiLineComment() {
        assertEquals(
            "<p>Before the comment<!-- comment1\ncomment2\ncomment3\n -->after the comment</p>",
            buildHtml("Before the comment<!-- comment1\ncomment2\ncomment3\n -->after the comment")
        )
    }

    fun testInternalLink() {
        assertEquals(
            "<p>Page <a href=\"/wiki/view/foo\" class=\"notFound\">foo</a> is good.</p>",
            buildHtml("Page [foo] is good.")
        )
    }

    fun testInternalLink2() {
        assertEquals(
            "<p>Page <a href=\"/wiki/view/foo\" class=\"notFound\">FOO</a> is good.</p>",
            buildHtml("Page [FOO|foo] is good.")
        )
    }

    fun testExternalLink() {
        assertEquals(
            "<p>Page <a href=\"http://google.com\">http://google.com</a> harvests your data.</p>",
            buildHtml("Page [http://google.com] harvests your data.")
        )
    }

    fun testExternalLink2() {
        assertEquals(
            "<p>Page <a href=\"http://example.com/++hi++/\">http://example.com/++hi++/</a> is not bold!</p>",
            buildHtml("Page [http://example.com/++hi++/] is not bold!")
        )
    }

    fun testUnorderedList() {
        assertEquals(
            "<p>Before the list </p><ul><li>Red </li><li>Green </li><li>Blue </li></ul><p>After the list</p>",
            buildHtml("Before the list\n*Red\n*Green\n*Blue\nAfter the list")
        )
    }

    /**
     * The same as [testUnorderedList], but with extra whitespace after the *
     */
    fun testUnorderedList2() {
        assertEquals(
            "<p>Before the list </p><ul><li>Red </li><li>Green </li><li>Blue </li></ul><p>After the list</p>",
            buildHtml("Before the list\n* Red\n*Green\n*  Blue\nAfter the list")
        )
    }

    /**
     * The same as [testUnorderedList], but with end the document after the list.
     */
    fun testUnorderedList3() {
        assertEquals(
            "<p>Before the list </p><ul><li>Red </li><li>Green </li><li>Blue</li></ul>",
            buildHtml("Before the list\n*Red\n*Green\n*Blue\n")
        )
    }

    /**
     * The same as [testUnorderedList], but with extra whitespace after the *
     */
    fun testMultiLevelUnorderedList() {
        assertEquals(
            "<p>Before the list </p><ul><li>Red <ul><li>A </li><li>B </li></ul></li><li>Green <ul><li>C </li><li>D </li></ul></li></ul><p>After the list</p>",
            buildHtml("Before the list\n* Red\n** A\n** B\n* Green\n**C\n** D\nAfter the list")
        )
    }

    fun testOrderedList() {
        assertEquals(
            "<p>Before the list </p><ol><li>Red </li><li>Green </li><li>Blue </li></ol><p>After the list</p>",
            buildHtml("Before the list\n#Red\n#Green\n#Blue\nAfter the list")
        )
    }

    /**
     * The same as [testUnorderedList], but with extra whitespace after the *
     */
    fun testOrderedList2() {
        assertEquals(
            "<p>Before the list </p><ol><li>Red </li><li>Green </li><li>Blue </li></ol><p>After the list</p>",
            buildHtml("Before the list\n# Red\n#Green\n#  Blue\nAfter the list")
        )
    }
    /**
     * The same as [testUnorderedList], but with extra whitespace after the *
     */
    fun testMultiLevelOrderedList() {
        assertEquals(
            "<p>Before the list </p><ol><li>Red <ol><li>A </li><li>B </li></ol></li><li>Green <ol><li>C </li><li>D </li></ol></li></ol><p>After the list</p>",
            buildHtml("Before the list\n# Red\n## A\n## B\n# Green\n##C\n## D\nAfter the list")
        )
    }

    fun testMixedList() {
        assertEquals(
            "<p>Before the list </p><ol><li>Red </li><li>Green </li></ol><ul><li>A </li><li>B </li></ul><p>After the list</p>",
            buildHtml("Before the list\n# Red\n# Green\n* A\n* B\nAfter the list")
        )
    }

    /**
     * Without the nowiki syntax, the "++" should be interpreted as a BOLD syntax,
     */
    fun testNoWiki() {
        assertEquals(
            "<p>Do you like C++? C++ is naff!</p>",
            buildHtml("Do you like <nowiki>C++? C++</nowiki> is naff!")
        )
    }

    fun testTableCell() {
        assertEquals(
            "<table><tr><td>A</td><td>B</td></tr></table>",
            buildHtml(
                """
                {|
                |A
                |B
                |}
                """.trimIndent()
            )
        )
    }
    fun testTableHeaderCell() {
        assertEquals(
            "<table><tr><th>A</th><th>B</th></tr></table>",
            buildHtml(
                """
                {|
                ||A
                ||B
                |}
                """.trimIndent()
            )
        )
    }

    fun testTable() {
        assertEquals(
            """
            <table class="plantChart"><tr><th></th><th>J</th><th>F</th></tr><tr class="line"><th rowspan="2">1st Year</th><td class="sowIndoors"></td><td class="sowIndoors"></td></tr><tr><td></td><td></td></tr></table>
            """.trimIndent(),
            buildHtml(
                """
                {|(class="plantChart")
                ||
                ||J
                ||F
                |-(class="line")
                ||(rowspan="2")1st Year
                |(class="sowIndoors")
                |(class="sowIndoors")
                |-
                |
                |
                |}
                """.trimIndent()
            )
        )
    }

    fun testTable2() {
        assertEquals(
            """
            <table class="plantChart"><tr><th> </th><th>J</th><th>F</th></tr><tr class="line"><th rowspan="2">1st Year</th><td class="sowIndoors"></td><td class="sowIndoors"></td></tr><tr><td></td><td></td></tr></table>
            """.trimIndent(),
            buildHtml(
                """
                {|(class="plantChart")
                || ||J||F
                |-(class="line")
                ||(rowspan="2")1st Year
                |(class="sowIndoors")
                |(class="sowIndoors")
                |-
                |
                |
                |}
                """.trimIndent()
            )
        )
    }

    fun testTableLooped() {
        assertEquals(
            "<table><tr><td>The Valley of Death</td><td>384</td><td>24</td><td>end</td><td>-ve</td></tr></table>",
            buildHtml(
                """
                    {|
                    |The Valley of Death|384|24|end|-ve
                    |}
                """.trimIndent()
            )
        )
    }

    /*
     * URLs can have substitutions {title} and {namespace}.
     * In our tests the page is default/Test
     */
    /*
    I've removed this feature, so commented out the test case.
    fun testTitleInLink() {
       assertEquals(
           "<p><a href=\"http://example.com/Test.jsp\">Hello</a></p>",
           buildHtml("[Hello|http://example.com/{title}.jsp]")
       )
       assertEquals(
           "<p><a href=\"http://example.com/default.jsp\">Hello</a></p>",
           buildHtml("[Hello|http://example.com/{namespace}.jsp]")
       )
    }
    */


    /**
     * Two links with a single space in between them, the space was ommitted from the html.
     * The caused the two links to touch each other.
     */
    fun testTwoLinksWithX() {
        assertEquals(
            "<p><a href=\"/wiki/view/A\" class=\"notFound\">A</a> X <a href=\"/wiki/view/B\" class=\"notFound\">B</a></p>",
            buildHtml("[A] X [B]")
        )
    }
    fun testTwoLinkWithSpace() {
        assertEquals(
            "<p><a href=\"/wiki/view/A\" class=\"notFound\">A</a> <a href=\"/wiki/view/B\" class=\"notFound\">B</a></p>",
            buildHtml("[A] [B]")
        )
    }
    fun testTwoLinkWithNewLine() {
        assertEquals(
            "<p><a href=\"/wiki/view/A\" class=\"notFound\">A</a> <a href=\"/wiki/view/B\" class=\"notFound\">B</a></p>",
            buildHtml("[A]\n[B]")
        )
    }

}
