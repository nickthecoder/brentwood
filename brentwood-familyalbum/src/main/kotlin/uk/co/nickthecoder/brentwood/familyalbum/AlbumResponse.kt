package uk.co.nickthecoder.brentwood.familyalbum

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.familyalbum.model.*
import uk.co.nickthecoder.brentwood.util.parameterID
import uk.co.nickthecoder.brentwood.util.respondHtmlTemplate2
import java.io.File


internal suspend fun FamilyAlbum.albums(call: ApplicationCall) {

    val familyID = parameterID("familyID", call)

    suspendTransaction {

        val family = Family.findById(familyID) ?: throw NotFoundException("Family $familyID")

        val albums = Album.find {
            Albums.familyID eq familyID
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"${family.label} : Albums" }
            pageHeading { +"${family.label} : Albums" }
            pageTools {
                add("$webPath/addAlbum/$familyID", "Add Photo Album")
            }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                if (albums.empty()) {
                    p { +"No albums" }
                } else {
                    layout.thumbnails(this, plain = true) {
                        for (album in albums) {
                            layout.thumbnail(this, {
                                a(album) { thumbnail(album) }
                            }, {
                                a(album) { +album.label }
                            })
                        }
                    }
                }
            }

        }
    }
}

internal suspend fun FamilyAlbum.album(call: ApplicationCall) {

    val albumID = parameterID("albumID", call)

    suspendTransaction {

        val album = Album.findById(albumID) ?: throw NotFoundException("Album $albumID")

        val photos = Photo.find {
            Photos.albumID eq albumID
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Album : ${album.label}" }

            preContent {
                secondaryNavigation(album.family.id.value)
            }

            pageHeading {
                +album.label
                jumpToNotes(album.notes)
            }

            pageTools {
                delete("$webPath/deleteAlbum/$albumID", "Delete Album")
                add("$webPath/addPhoto/$albumID", "Upload a Photo")
                edit("$webPath/editAlbum/$albumID", "Edit Album")
            }

            content {

                if (photos.empty()) {
                    p { +"No photos" }
                } else {
                    layout.thumbnails(this) {
                        for (photo in photos) {
                            layout.thumbnail(this, {
                                a(photo) { thumbnail(photo) }
                            }, {
                                a(photo) { +photo.name }
                            })
                        }
                    }
                }

                notesSection(album.notes)
            }

        }
    }

}

internal suspend fun FamilyAlbum.editAlbum(
    isNew: Boolean,
    call: ApplicationCall,
    parameters: Parameters = call.parameters,
    error: String? = null
) {

    val albumID = if (isNew) 0 else parameterID("albumID", call, parameters)

    suspendTransaction {
        val album = if (isNew) {
            null
        } else {
            Album.findById(albumID) ?: throw NotFoundException("Album $albumID not found")
        }

        val familyID = album?.family?.id?.value ?: parameterID("familyID", call, parameters)
        val pageTitle = if (isNew) "New Photo Album" else "Edit Photo Album"

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +pageTitle }
            pageHeading { +pageTitle }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                if (error != null) {
                    div("error") {
                        p { +error }
                    }
                }
                val action = if (isNew) {
                    "$webPath/addAlbum/$familyID"
                } else {
                    "$webPath/editAlbum/$albumID"
                }
                form(action, method = kotlinx.html.FormMethod.post) {

                    div("form") {

                        +"Label"
                        input(InputType.text, name = "label") {
                            value = parameters["label"] ?: album?.label ?: ""
                        }

                        +"Notes"
                        notesTextArea(parameters["notes"] ?: album?.notes ?: "")

                        if (!isNew) {
                            +"Default Image"
                            br()
                            thumbnail(album!!, "defaultThumbnail")
                            input(InputType.hidden) {
                                name = "defaultPhotoID"
                                id = "defaultPhotoID"
                                value = parameters["defaultPhotoID"] ?: album.defaultPhoto?.id?.value?.toString()
                                        ?: ""
                            }
                        }

                        saveCancelButtons(if (isNew) "Create" else "Save")
                    }

                    if (album?.photos?.empty() == false) {
                        h2 { +"Photos (Click to select the default photo for the album)" }

                        layout.thumbnails(this, plain = true) {
                            album.photos.forEach { photo ->
                                layout.thumbnail(this, {
                                    a("#defaultThumbnail") {
                                        onClick =
                                            "document.getElementById(\"defaultPhotoID\").value=\"${photo.id.value}\"; document.getElementById(\"defaultThumbnail\").src=\"${photo.thumbnail()}\";"
                                        thumbnail(photo)
                                    }
                                }, {
                                    input(InputType.submit) {
                                        value = "Delete"
                                        name = "deletePhoto:${photo.id.value}"
                                    }
                                })

                            }
                        }
                    }

                }
            }

        }
    }
}

internal suspend fun FamilyAlbum.saveAlbum(isNew: Boolean, call: ApplicationCall) {

    val parameters = call.receiveParameters()
    var albumID = if (isNew) 0 else parameterID("albumID", call, parameters)
    val defaultPhotoID = parameters["defaultPhotoID"]?.toIntOrNull()

    if (parameters["cancel"] != null) {
        if (isNew) {
            call.respondRedirect("$webPath/albums/${parameterID("familyID", call, parameters)}")
        } else {
            call.respondRedirect("$webPath/album/$albumID")
        }
        return
    }

    if (!isNew) {
        // Check for "deletePhoto:{ID}"
        for (name in parameters.names()) {
            if (name.startsWith("deletePhoto:")) {
                name.substring(12).toIntOrNull()?.let { photoID ->
                    suspendTransaction {
                        Photo.findById(photoID)?.let { photo ->
                            for (sip in photo.subjectsInPhoto) {
                                deleteImages(sip)
                                sip.delete()
                            }
                            deleteImages(photo)
                            photo.delete()
                        }
                    }
                    editAlbum(isNew, call, parameters)
                    return
                }
            }
        }
    }

    val label = parameters["label"]
    val notes = parameters["notes"] ?: ""

    if (label.isNullOrBlank()) {
        editAlbum(isNew, call, parameters, "Label is missing")
        return
    }
    suspendTransaction {
        if (isNew) {
            val familyID = parameterID("familyID", call, parameters)
            val family = Family[familyID]
            val album = Album.new2 { album ->
                album.label = label
                album.notes = notes
                album.directory = ""
                album.family = family
            }
            albumID = album.id.value
            album.directory =
                "family${idNumberFormat.format(familyID)}/album${idNumberFormat.format(albumID)}"
            File(baseDir, album.directory).mkdirs()

        } else {
            val album = Album.findById(albumID) ?: throw NotFoundException("Album $albumID not found")

            album.label = label
            album.notes = notes

            album.defaultPhoto = defaultPhotoID?.let { Photo.findById(it) }
        }
    }

    // Update the search index
    pageListener.pageChanged("$webPath/album/$albumID")

    call.respondRedirect("$webPath/album/$albumID")
}


internal suspend fun FamilyAlbum.confirmDeleteAlbum(call: ApplicationCall) {

    val albumID = parameterID("albumID", call)

    suspendTransaction {
        val album = Album.findById(albumID) ?: throw NotFoundException("Album $albumID")

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Delete Photo Album" }
            pageHeading { +"Delete Photo Album" }

            content {

                form(action = "$webPath/deleteAlbum/$albumID", method = kotlinx.html.FormMethod.post) {

                    div("form") {
                        h3 { +album.label }

                        deleteCancelButtons()
                    }
                }

                if (!album.photos.empty()) {
                    h2 { +"These photos will be permanently deleted" }
                    layout.thumbnails(this) {
                        for (photo in album.photos) {
                            layout.thumbnail(this, { thumbnail(photo) })
                        }
                    }
                }
            }

        }
    }

}

internal suspend fun FamilyAlbum.deleteAlbum(call: ApplicationCall) {
    val parameters = call.receiveParameters()
    val albumID = parameterID("albumID", call, parameters)

    if (parameters["ok"] != null) {
        suspendTransaction {
            val album = (Album.findById(albumID) ?: throw NotFoundException("Album $albumID"))
            val familyID = album.family.id.value
            for (photo in album.photos) {
                for (sip in photo.subjectsInPhoto) {
                    deleteImages(sip)
                    sip.delete()
                }
                deleteImages(photo)
                photo.delete()
            }
            album.delete()
            deleteImages(album)
            call.respondRedirect("$webPath/albums/$familyID")
        }
    } else {
        call.respondRedirect("$webPath/album/$albumID")
    }
}
