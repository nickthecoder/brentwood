package uk.co.nickthecoder.brentwood.familyalbum

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.html.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.html.*
import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.PageListener
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.*
import uk.co.nickthecoder.brentwood.wiki.*
import uk.co.nickthecoder.brentwood.familyalbum.model.*
import java.io.File
import java.text.DecimalFormat

class FamilyAlbum(

    layout: Layout,
    webPath: String = "/familyalbum",
    val baseDir: File,
    val db : Database,
    val pageListener: PageListener

) : LayoutComponent(layout, webPath) {

    var defaultFamilyID = 1

    private val wikiURLFactory = object : URLFactory {
        override fun view(pageName: PageName, full: Boolean) = "$webPath/NOT_FOUND"
        override fun edit(pageName: PageName, full: Boolean) = "$webPath/NOT_FOUND"
        override fun media(pageName: PageName, full: Boolean) = throw NotImplementedError()
        override fun pageNameFromURL(url: String, wikiEngine: WikiEngine): PageName? = null
    }

    val wikiEngine = WikiEngine(Namespace("default", DummyStorage()), wikiURLFactory, layout)

    internal val idNumberFormat = DecimalFormat("00000")

    init {
        wikiEngine.protocolHandlers.put("family", IDProtocol("$webPath/family", webPath))
        wikiEngine.protocolHandlers.put("album", IDProtocol("$webPath/album", webPath))
        wikiEngine.protocolHandlers.put("photo", IDProtocol("$webPath/photo", webPath))
        wikiEngine.protocolHandlers.put("subject", IDProtocol("$webPath/subject", webPath))
    }

    override fun route(route: Route) {

        with(route) {

            // Resources from the jar file
            staticResources("/resources", this@FamilyAlbum.javaClass.packageName)

            // Images from the filesystem (path is relative to baseDir)
            get("/image/{path...}") {
                val path = call.joinedSafeParts("path")
                val file = File(baseDir, path)
                if (file.exists() && file.isFile && !file.isHidden) {
                    call.respondFile(file)
                } else {
                    throw NotFoundException(path)
                }
            }

            // VIEW ONLY
            get("/family/{familyID}") { familyDetails(call) }
            get("/albums/{familyID}") { albums(call) }
            get("/album/{albumID}") { album(call) }
            get("/photo/{photoID}") { photo(call) }
            get("/familyMembers/{familyID}") { subjects(call, 1) }
            get("/pets/{familyID}") { subjects(call, 2) }
            get("/friends/{familyID}") { subjects(call, 3) }
            get("/subject/{subjectID}") { subject(call) }

            get("/") { call.respondRedirect("$webPath/familyMembers/$defaultFamilyID") }

            // EDITING - Requires authentication.
            authenticate("auth-basic") {

                route("/editFamily/{familyID}") {
                    get { editFamily(call) }
                    post { saveFamily(call) }
                }

                route("/editAlbum/{albumID}") {
                    get { editAlbum(false, call) }
                    post { saveAlbum(false, call) }
                }

                route("/addAlbum/{familyID}") {
                    get { editAlbum(true, call) }
                    post { saveAlbum(true, call) }
                }
                route("/deleteAlbum/{albumID}") {
                    get { confirmDeleteAlbum(call) }
                    post { deleteAlbum(call) }
                }

                route("/editPhoto/{photoID}") {
                    get { editPhoto(call, call.parameters) }
                    post { savePhoto(call, call.receiveParameters()) }
                }
                route("/addPhoto/{albumID}") {
                    get { addPhoto(call) }
                    post { uploadPhoto(call) }
                }
                route("/tagPhoto/{photoID}") {
                    get { tagPhoto(call) }
                    post { savePhotoTags(call) }
                }


                route("/editSubject/{subjectID}") {
                    get { editSubject(false, call, call.parameters) }
                    post { saveSubject(false, call, call.receiveParameters()) }
                }
                route("/addSubject/{familyID}") {
                    get { editSubject(true, call, call.parameters) }
                    post { saveSubject(true, call, call.receiveParameters()) }
                }
                route("/deleteSubject/{subjectID}") {
                    get { confirmDeleteSubject(call) }
                    post { deleteSubject(call) }
                }
                route("/addRelationship/{subjectID}") {
                    get { addRelationship(call) }
                    post { newRelationship(call) }
                }


            }

        }

    }


    // **** Delete Images ****

    internal fun deleteImages(sip: SubjectInPhoto) {
        val file = File(baseDir, sip.filename)
        file.delete()
        File(File(file.parentFile, ".thumbnails"), file.name).delete()
    }

    internal fun deleteImages(photo: Photo) {
        val file = File(baseDir, photo.filename)
        file.delete()
        File(File(file.parentFile, ".thumbnails"), file.name).delete()
    }

    internal fun deleteImages(album: Album) {
        val albumDir = File(baseDir, album.directory)
        File(albumDir, ".thumbnails").deleteRecursively()
        File(albumDir, "subjects").deleteRecursively()
        albumDir.delete()
    }


    // **** ALBUM ****

    private fun Album.thumbnail(): String {
        val photo = defaultPhoto?.thumbnail()
        return if (photo?.endsWith("photo.png") != false) {
            "$webPath/resources/album.png"
        } else {
            photo
        }
    }

    internal fun FlowContent.thumbnail(album: Album, imgID: String? = null) {
        img(album.label, album.thumbnail()) {
            if (imgID != null) {
                id = imgID
            }
        }
    }

    internal fun FlowContent.a(album: Album, body: A.() -> Unit) {
        a(album.href()) {
            body()
        }
    }

    private fun Album.href() = "$webPath/album/$id"

    // **** PHOTO ****

    internal fun Photo.thumbnail(): String {
        val path = "$directory/.thumbnails/$name"
        val file = File(baseDir, path)
        return if (file.exists()) {
            "$webPath/image/$path"
        } else {
            val sourceFile = File(baseDir, filename)
            if (sourceFile.exists()) {
                JobQueue.add {
                    if (!file.exists()) {
                        createThumbnail(sourceFile, file)
                    }
                }
            }
            "$webPath/resources/photo.png"
        }
    }

    internal fun Photo.image(): String {
        val file = File(baseDir, filename)
        return if (file.exists()) {
            "$webPath/image/$filename"
        } else {
            "$webPath/resources/photo.png"
        }
    }

    internal fun FlowContent.thumbnail(photo: Photo) {
        img(photo.name, photo.thumbnail())
    }

    internal fun FlowContent.image(photo: Photo) {
        img(photo.name, photo.image())
    }

    internal fun FlowContent.a(photo: Photo, body: A.() -> Unit) {
        a(photo.href()) {
            body()
        }
    }

    internal fun Photo.href() = "$webPath/photo/$id"

    // **** SUBJECT ****

    private fun Subject.thumbnail(): String {
        return defaultSubjectInPhoto?.thumbnail() ?: "$webPath/resources/sipThumb.png"
    }

    private fun Subject.image(): String {
        return defaultSubjectInPhoto?.image() ?: "$webPath/resources/sipImage.png"
    }

    internal fun FlowContent.thumbnail(subject: Subject, imgID: String? = null) {
        img("${subject.name1} ${subject.name2}", subject.thumbnail(), classes = "sipThumb") {
            if (imgID != null) {
                id = imgID
            }
        }
    }

    internal fun FlowContent.img(subject: Subject) {
        img("${subject.name1} ${subject.name2}", subject.image(), classes = "sip")
    }

    internal fun FlowContent.a(subject: Subject, body: A.() -> Unit) {
        a(subject.href()) {
            body()
        }
    }

    internal fun Subject.href() = "$webPath/subject/$id"

    // **** SubjectInPhoto ****

    internal fun SubjectInPhoto.image(): String {
        val imgFile = File(baseDir, filename)
        return if (imgFile.exists()) {
            "$webPath/image/$filename"
        } else {
            val sourceFile = File(baseDir, photo.filename)
            if (sourceFile.exists()) {
                JobQueue.add {
                    if (sourceFile.exists()) {
                        createSIP(sourceFile, imgFile, x, y, radius)
                    }
                }
            }
            "$webPath/resources/sipImage.png"
        }
    }

    internal fun SubjectInPhoto.thumbnailPath(): String {
        val foo = File(filename)
        return "${foo.parent}/.thumbnails/${foo.nameWithoutExtension}.jpg"
    }

    internal fun SubjectInPhoto.thumbnail(): String {
        val path = thumbnailPath()
        val thumbFile = File(baseDir, thumbnailPath())
        return if (thumbFile.exists()) {
            "$webPath/image/$path"
        } else {
            val sourceFile = File(baseDir, photo.filename)
            if (sourceFile.exists()) {
                JobQueue.add {
                    if (sourceFile.exists()) {
                        createSIPThumbnail(sourceFile, thumbFile, x, y, radius)
                    }
                }
            }
            "$webPath/resources/sipThumb.png"
        }
    }

    internal fun FlowContent.image(sip: SubjectInPhoto) {
        img(sip.filename, sip.image(), classes = "sip")
    }

    internal fun FlowContent.thumbnail(sip: SubjectInPhoto) {
        img(sip.filename, sip.thumbnail(), classes = "sipThumb")
    }

    // **** Extras ****

    var convertProgram = "convert"
    var thumbnailQuality = 80

    /**
     * The max width and height of the thumbnails of the photos. These will be rectangles.
     */
    var photoThumbnailSize = 166

    /**
     * The diameter of the subject-in-photo thumbnails. These will be circular, and therefore the
     * css border-radius must be set appropriately too. (The radius must be half the [sipThumbnailSize]
     * plus the img's padding.
     */
    var sipThumbnailSize = 100

    private fun createThumbnail(sourceFile: File, destFile: File) {
        destFile.parentFile.mkdirs()
        commandToString(
            convertProgram,
            "-quality", thumbnailQuality.toString(),
            "-thumbnail",
            "${photoThumbnailSize}x${photoThumbnailSize}",
            sourceFile.absolutePath,
            destFile.absolutePath
        )
    }

    private fun createSIP(sourceFile: File, destFile: File, ox: Int, oy: Int, radius: Int) {
        destFile.parentFile.mkdirs()
        commandToString(
            convertProgram,
            "-crop", "${radius * 2}x${radius * 2}+${ox - radius}+${oy - radius}",
            "-quality", thumbnailQuality.toString(),
            sourceFile.absolutePath,
            destFile.absolutePath
        )
    }

    private fun createSIPThumbnail(sourceFile: File, destFile: File, ox: Int, oy: Int, radius: Int) {
        destFile.parentFile.mkdirs()
        commandToString(
            convertProgram,
            "-crop", "${radius * 2}x${radius * 2}+${ox - radius}+${oy - radius}",
            "-quality", thumbnailQuality.toString(),
            "-thumbnail",
            "${sipThumbnailSize}x${sipThumbnailSize}",
            sourceFile.absolutePath,
            destFile.absolutePath
        )
    }

    // **** General Purpose HTML Generators ****

    internal fun FlowContent.secondaryNavigation(familyID: Int) {
        div("center") {
            ul("infoList") {
                li {
                    a("$webPath/family/$familyID") { +"Family Details" }
                }
                +" "
                li {
                    a("$webPath/albums/$familyID") { +"Photo Albums" }
                }
                +" "
                li {
                    a("$webPath/familyMembers/$familyID") { +"Family Members" }
                }
                +" "
                li {
                    a("$webPath/friends/${familyID}") { +"Friends" }
                }
                +" "
                li {
                    a("$webPath/pets/${familyID}") { +"Pets" }
                }
            }
        }
    }

    internal fun FlowContent.jumpToNotes(notes: String?) {
        if (!notes.isNullOrBlank()) {
            +" "
            a("#notes") {
                title = "Jump to notes"
                accessKey = "n"
                img("notes", "$webPath/resources/notes.png")
            }
        }
    }

    internal fun FlowContent.notesSection(notes: String?) {
        if (!notes.isNullOrBlank()) {
            h2 {
                id = "notes"
                +"Notes"
            }
            wiki(notes)
        }
    }

    internal fun FlowContent.wiki(source: String) {
        val pageName = PageName(wikiEngine.defaultNamespace, "Text")
        try {
            val details = wikiEngine.parser.parse(pageName, source)
            details.rootNode.render(this, wikiEngine)
        } catch (e: WikiException) {
            div("error") {
                +e.message
            }
            p { +source }
        } catch (e: Throwable) {
            div("error") {
                +"Unexpected Error"
            }
            p { +source }
        }
    }

    internal fun FlowContent.edit(href: String, aTitle: String = "Edit") {
        layout.imageTool(this, href) {
            title = aTitle
            img("edit", "$webPath/resources/edit.png")
        }
    }

    internal fun FlowContent.tagPhoto(href: String) {
        layout.imageTool(this, href) {
            title = "Tag People in the Photo"
            img("tag photo", "$webPath/resources/tagPhoto.png")
        }
    }

    internal fun FlowContent.delete(href: String, aTitle: String = "Delete") {
        layout.imageTool(this, href) {
            title = aTitle
            img("delete", "$webPath/resources/delete.png")
        }
    }

    internal fun FlowContent.add(href: String, aTitle: String = "Add") {
        layout.imageTool(this, href) {
            title = aTitle
            img("add", "$webPath/resources/add.png")
        }
    }

    internal fun FlowContent.notesTextArea(notes: String) {
        textArea {
            rows = "6"
            name = "notes"
            +notes
        }
    }

    internal fun <T> FlowContent.choice(
        formName: String,
        selectValue: T,
        choices: List<Pair<T, String>>,
        selectID: String? = null
    ) {
        select {
            name = formName
            if (selectID != null) id = selectID
            // Add a "blank" value if the selectValue is not in the lit of choices.
            if (choices.firstOrNull { it.first == selectValue } == null) {
                option {
                    label = "Choose..."
                    value = ""
                    selected = true
                }
            }
            for (choice in choices) {
                option {
                    label = choice.second
                    value = choice.first.toString()
                    if (selectValue == choice.first) {
                        selected = true
                    }
                }
            }
        }
    }

    internal fun FlowContent.saveCancelButtons(okLabel: String = "Save") {
        div("buttons") {
            input(InputType.submit, name = "ok", classes = "button ok") { value = okLabel }
            input(InputType.submit, name = "cancel", classes = "button cancel") { value = "Cancel" }
        }
    }

    internal fun FlowContent.deleteCancelButtons() {
        div("buttons") {
            input(InputType.submit, name = "ok", classes = "button delete") { value = "Delete" }
            input(InputType.submit, name = "cancel", classes = "button cancel") { value = "Cancel" }
        }
    }

    // **** Misc ****

    internal suspend fun <T> suspendTransaction(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO, db = db) {
            block()
        }

    /**
     * The "normal" new method changes the receiver to the new Entity instance.
     * This can be very annoying, because I end up wanting to do things like :
     *
     *     label = label
     *
     * where the left is referring to "this" and the right to a local variable.
     * So instead, I like the entity to be a PARAMETER of the lambda, not
     * a receiver.
     *
     * As a bonus, now adding a new row looks similar to updating an existing one.
     */
    internal fun <T : Entity<Int>> EntityClass<Int, T>.new2(init: (T) -> Unit) = new(null) { init(this) }

}
