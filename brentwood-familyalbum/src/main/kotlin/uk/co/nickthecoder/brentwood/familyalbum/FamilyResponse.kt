package uk.co.nickthecoder.brentwood.familyalbum

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.familyalbum.model.Family
import uk.co.nickthecoder.brentwood.util.parameterID
import uk.co.nickthecoder.brentwood.util.respondHtmlTemplate2


internal suspend fun FamilyAlbum.familyDetails(call: ApplicationCall) {

    val familyID = parameterID("familyID", call)

    suspendTransaction {
        val family = Family.findById(familyID) ?: throw NotFoundException("Family $familyID not found")
        val albumsCount = family.albums.count()
        val familyCount = family.subjects.count { it.subjectTypeID == 1 }
        val petsCount = family.subjects.count { it.subjectTypeID == 2 }
        val friendCount = family.subjects.count { it.subjectTypeID == 3 }
        var photoCount = 0L
        for (album in family.albums) {
            photoCount += album.photos.count()
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Family Details" }
            pageHeading { +"Family" }
            pageTools {
                edit("$webPath/editFamily/$familyID", "Edit Family Details")
            }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                p { +"${family.label} " }

                h2 { +"Details" }

                p {
                    +"There are "
                    b { +"$albumsCount" }
                    +" "
                    a("$webPath/albums/$familyID") { +"Photo Albums" }
                    +", containing a total of "
                    b { +"$photoCount" }
                    +" photos."
                }

                p {
                    +"With "
                    b { +"$familyCount" }
                    +" "
                    a("$webPath/familyMembers/$familyID") { +"Family Members" }
                    +", "

                    b { +"$friendCount" }
                    +" "
                    a("$webPath/friends/$familyID") { +"Friends" }

                    +" and "
                    b { +"$petsCount" }
                    +" "
                    a("$webPath/pets/$familyID") { +"Pets" }
                }

                notesSection(family.notes)

            }

        }
    }
}


internal suspend fun FamilyAlbum.editFamily(
    call: ApplicationCall,
    parameters: Parameters = call.parameters,
    error: String? = null
) {

    val familyID = parameterID("familyID", call, parameters)

    suspendTransaction {
        val family = Family.findById(familyID) ?: throw NotFoundException("Family $familyID not found")

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +"Edit Family Details" }
            pageHeading { +"Edit Family Details" }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                if (error != null) {
                    div("error") {
                        p { +error }
                    }
                }

                form("$webPath/editFamily/$familyID", method = kotlinx.html.FormMethod.post) {
                    div("form") {
                        +"Label"
                        input(InputType.text, name = "label") {
                            autoFocus = true
                            value = parameters["label"] ?: family.label
                        }

                        +"Notes"
                        notesTextArea(parameters["notes"] ?: family.notes)

                        saveCancelButtons()
                    }
                }
            }

        }
    }
}

internal suspend fun FamilyAlbum.saveFamily(call: ApplicationCall) {

    val parameters = call.receiveParameters()
    val familyID = parameterID("familyID", call, parameters)

    if (parameters["cancel"] != null) {
        call.respondRedirect("$webPath/family/$familyID")
        return
    }

    val label = parameters["label"]?.trim()
    val notes = parameters["notes"] ?: ""

    if (label.isNullOrBlank()) {
        editFamily(call, parameters, "Label is missing")
        return
    }

    suspendTransaction {
        val family = Family.findById(familyID) ?: throw NotFoundException("Family $familyID not found")
        family.label = label
        family.notes = notes
    }

    // Update the search index
    pageListener.pageChanged("$webPath/family/$familyID")
    
    call.respondRedirect("$webPath/family/$familyID")
}
