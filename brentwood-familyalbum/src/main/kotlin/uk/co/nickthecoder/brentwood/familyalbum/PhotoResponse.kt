package uk.co.nickthecoder.brentwood.familyalbum

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import kotlinx.html.*
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.familyalbum.model.*
import uk.co.nickthecoder.brentwood.util.*
import java.io.File
import java.util.*


internal suspend fun FamilyAlbum.photo(call: ApplicationCall) {

    val photoID = parameterID("photoID", call)

    suspendTransaction {

        val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID")
        val album = photo.album
        val subjectsInPhoto = photo.subjectsInPhoto

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Photo : ${photo.nameWithoutExtension} in Album : ${album.label}" }

            extraHeader {
                script(type = "text/javascript", src = "$webPath/resources/tagPhoto.js") {}
            }

            pageHeading {
                +"Photo : ${photo.nameWithoutExtension}"
                jumpToNotes(photo.notes)
            }
            pageTools {
                tagPhoto("$webPath/tagPhoto/$photoID")
                edit("$webPath/editPhoto/$photoID", "Edit Photo")
            }
            preContent {
                secondaryNavigation(album.family.id.value)
            }

            content {
                h3 {
                    +"In Album : "
                    a(album) { +album.label }
                }
                div("images") {
                    div("imageContainer") {
                        div {
                            id = "glass"
                        }
                        img(photo.name, photo.image()) {
                            id = "photo"
                        }
                    }
                }

                layout.thumbnails(this, plain = true) {
                    for (subjectInPhoto in subjectsInPhoto) {
                        val subject = subjectInPhoto.subject
                        layout.thumbnail(this, {
                            a(subject) { thumbnail(subjectInPhoto) }
                        }, {
                            a(subject) { +"${subject.name1} ${subject.name2}" }
                        })

                    }
                }

                notesSection(photo.notes)

                val js = buildString {
                    append("\n")
                    append("window.addEventListener( 'load', function() {\n")
                    for (sip in photo.subjectsInPhoto) {
                        append(
                            "addSIP(${sip.id.value}, ${sip.subject.id.value}, ${sip.x}, ${sip.y}, ${sip.radius},${
                                javascriptString(sip.subject.name1 + " " + sip.subject.name2)
                            });\n"
                        )
                    }
                    append("showTags(${javascriptString(webPath)}, false);\n");
                    append("} );\n")
                    append("\n")
                }
                inlineScript(js)

            }
        }
    }
}


internal suspend fun FamilyAlbum.editPhoto(call: ApplicationCall, parameters: Parameters, error: String? = null) {

    val photoID = parameterID("photoID", call, parameters)

    suspendTransaction {
        val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID not found")


        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +"Edit Photo" }
            pageHeading { +"Edit Photo" }

            preContent {
                secondaryNavigation(photo.album.family.id.value)
            }

            content {

                thumbnail(photo)

                if (error != null) {
                    div("error") {
                        p { +error }
                    }
                }

                form("$webPath/editPhoto/$photoID", method = kotlinx.html.FormMethod.post) {

                    div("form") {
                        +"Year"
                        input(InputType.text, name = "year") {
                            value = parameters["year"] ?: photo.year?.toString() ?: ""
                        }

                        +"Year Accuracy"
                        span("info") { +" (0 if the date is exact, 1 if the date may be off by 1 year etc)" }
                        input(InputType.text, name = "yearAccuracy") {
                            value = parameters["yearAccuracy"] ?: photo.yearAccuracy?.toString() ?: ""
                        }

                        +"Notes"
                        notesTextArea(parameters["notes"] ?: photo.notes)

                        saveCancelButtons()
                    }

                    layout.thumbnails(this, plain = true) {
                        for (subjectInPhoto in photo.subjectsInPhoto) {
                            layout.thumbnail(this, {
                                thumbnail(subjectInPhoto)
                            }, {
                                input(InputType.submit) {
                                    value = "Delete"
                                    name = "deleteSIP:${subjectInPhoto.id.value}"
                                }
                            })

                        }
                    }

                }
            }

        }
    }
}

internal suspend fun FamilyAlbum.savePhoto(call: ApplicationCall, parameters: Parameters) {

    val photoID = parameterID("photoID", call, parameters)

    if (parameters["cancel"] != null) {
        call.respondRedirect("$webPath/photo/$photoID")
        return
    }

    // Check for "deleteSIP:{ID}"
    for (name in parameters.names()) {
        if (name.startsWith("deleteSIP:")) {
            name.substring(10).toIntOrNull()?.let { sipID ->
                suspendTransaction {
                    SubjectInPhoto.findById(sipID)?.delete()
                }

                editPhoto(call, parameters)
                return
            }
        }
    }

    val year = parameters["year"]?.toIntOrNull()
    val yearAccuracy = parameters["yearAccuracy"]?.toIntOrNull()
    val notes = parameters["notes"] ?: ""

    suspendTransaction {
        val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID not found")
        photo.year = year
        photo.yearAccuracy = yearAccuracy
        photo.notes = notes
    }

    // Update the search index
    pageListener.pageChanged("$webPath/photo/$photoID")

    call.respondRedirect("$webPath/photo/$photoID")

}

internal suspend fun FamilyAlbum.addPhoto(call: ApplicationCall) {
    val albumID = parameterID("albumID", call)

    call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
        pageTitle { +"Upload a Photo" }
        pageHeading { +"Upload a Photo" }

        content {

            form("$webPath/addPhoto/$albumID", FormEncType.multipartFormData, method = FormMethod.post) {

                div("form") {

                    +"File to Upload "
                    span("info") { +"(Note. the filename cannot be changed after it is uploaded)" }
                    input(InputType.file, name = "file")

                    br()
                    br()
                    label() {
                        htmlFor = "useYear"
                        +"Use Year in Image MetaData"
                    }
                    input(InputType.checkBox, name = "useYear") {
                        id = "useYear"
                        checked = true
                    }

                    div("buttons") {
                        input(InputType.submit, name = "ok", classes = "button ok") { value = "Upload" }
                        // NOTE. No cancel button, because cancel would upload the photo to the server
                        // which would then ignore it!
                    }
                }

            }
        }
    }

}

internal suspend fun FamilyAlbum.uploadPhoto(call: ApplicationCall) {
    val albumID = parameterID("albumID", call)
    var albumDirectory = ""
    var relativePath = ""
    var useYearMetaData = false

    suspendTransaction {
        val album = Album.findById(albumID) ?: throw NotFoundException("Album : $albumID")
        albumDirectory = album.directory
    }

    val multipart = call.receiveMultipart()
    multipart.forEachPart { part ->

        if (part is PartData.FileItem) {

            val filename = part.originalFileName ?: "${Date()}.jpg"

            // use InputStream from part to save file
            part.streamProvider().use { inputStream ->
                relativePath = "$albumDirectory/$filename"
                val file = File(baseDir, relativePath)
                file.outputStream().buffered().use {
                    inputStream.copyTo(it)
                }
            }
        }

        if (part is PartData.FormItem) {
            if (part.name == "useYear") {
                useYearMetaData = true
            }
        }
        part.dispose()

    }


    if (relativePath != "") {
        var photoID = 0

        suspendTransaction {
            val album = Album.findById(albumID) ?: throw NotFoundException("Album : $albumID")
            val photo = Photo.new2 { photo ->
                photo.album = album
                photo.filename = relativePath
                photo.notes = ""
            }
            photoID = photo.id.value
            photo.thumbnail() // Side effect will create the thumbnail.
        }

        if (useYearMetaData) {
            // If there is a date meta-data, then update Photo's year and yearAccuracy.
            // Queued as this uses a command line program (blocking).
            JobQueue.add {
                val dateTime = commandToString("identify", "-format", "%[EXIF:DateTime]", "$baseDir/$relativePath")
                if (dateTime.length > 4) {
                    dateTime.substring(0, 4).toIntOrNull()?.let { year ->
                        transaction(db) {
                            Photo.findById(photoID)?.let {
                                it.year = year
                                it.yearAccuracy = 0
                            }
                        }
                    }
                }
            }

            call.respondRedirect("$webPath/photo/$photoID")

        } else {
        
            call.respondRedirect("$webPath/editPhoto/$photoID")

        }

        // Update the search index
        pageListener.pageChanged("$webPath/photo/$photoID")
        pageListener.pageChanged("$webPath/album/$albumID")

        return
    }

    // Hmm, we didn't receive a file, go back to the form.
    call.respondRedirect("$webPath/addPhoto/$albumID")

}


internal suspend fun FamilyAlbum.tagPhoto(call: ApplicationCall) {
    val photoID = parameterID("photoID", call)

    suspendTransaction {
        val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID")
        val allSubjects = Subject.all().sortedBy { it.name1 }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            extraHeader {
                script(type = "text/javascript", src = "$webPath/resources/tagPhoto.js") {}
            }

            pageTitle { +"Tag Photo" }
            pageHeading { +"Tag Photo ${photo.name}" }

            pageTools {
                layout.imageTool(this, "#") {
                    id = "newSIPButton"
                    title = "Tag another Person"
                    onClick = "newSIP(); return false;"
                    img("add", "$webPath/resources/add.png")
                }
            }

            content {

                form("$webPath/tagPhoto/$photoID", method = FormMethod.post) {
                    div("images") {
                        div("imageContainer") {
                            id = "imageContainer"
                            div {
                                id = "glass"
                            }
                            img(photo.name, photo.image()) {
                                id = "photo"
                            }
                        }
                    }

                    for (sip in photo.subjectsInPhoto) {
                        input(InputType.hidden, name = "x_${sip.id.value}") {
                            id = "x_${sip.id.value}"
                            value = "${sip.x}"
                        }
                        input(InputType.hidden, name = "y_${sip.id.value}") {
                            id = "y_${sip.id.value}"
                            value = "${sip.y}"
                        }
                        input(InputType.hidden, name = "r_${sip.id.value}") {
                            id = "r_${sip.id.value}"
                            value = "${sip.radius}"
                        }

                    }

                    input(InputType.hidden, name = "newX") {
                        id = "x_new"
                        value = ""
                    }
                    input(InputType.hidden, name = "newY") {
                        id = "y_new"
                        value = ""
                    }
                    input(InputType.hidden, name = "newRadius") {
                        id = "r_new"
                        value = ""
                    }

                    div("form") {
                        div("hidden") {
                            id = "newSIPDetails"
                            chooseName(this, allSubjects, "newSubjectID")
                        }
                        saveCancelButtons()
                    }

                }

                val js = buildString {
                    append("\n")
                    append("window.addEventListener( 'load', function() {\n")
                    for (sip in photo.subjectsInPhoto) {
                        append(
                            "addSIP(${sip.id.value}, ${sip.subject.id.value}, ${sip.x}, ${sip.y}, ${sip.radius},${
                                javascriptString(sip.subject.name1 + " " + sip.subject.name2)
                            });\n"
                        )
                    }
                    append("showTags(${javascriptString(webPath)}, true);\n");
                    append("} );\n")
                    append("\n")
                }
                inlineScript(js)
            }
        }

    }
}

internal suspend fun FamilyAlbum.savePhotoTags(call: ApplicationCall) {
    val photoID = parameterID("photoID", call)
    val parameters = call.receiveParameters()

    suspendTransaction {
        val photo = Photo.findById(photoID) ?: throw NotFoundException("Photo $photoID")

        // Update existing sips
        for (sip in photo.subjectsInPhoto) {
            val x = parameters["x_${sip.id.value}"]?.toIntOrNull()
            val y = parameters["y_${sip.id.value}"]?.toIntOrNull()
            val radius = parameters["r_${sip.id.value}"]?.toIntOrNull()

            if (x != null && y != null && radius != null && (x != sip.x || y != sip.y || radius != sip.radius)) {
                sip.x = x
                sip.y = y
                sip.radius = radius

                // Delete sip image and thumbnail (if they exist)
                File(baseDir, sip.thumbnailPath()).delete()
                File(baseDir, sip.filename).delete()

                // Regenerates the thumbnail - but maybe not in time for the very next request,
                // so the user may have to refresh the page.
                sip.thumbnail()
                // We only need to (re)generate the full-size sip image if it is the default image for the subject.
                if (sip.subject.defaultSubjectInPhoto?.id?.value == photoID) {
                    sip.image() // Side effect will regenerate the sip image.
                }
            }
        }

        // Insert a new sip if needed
        parameters["newSubjectID"]?.toIntOrNull()?.let { newSubjectID ->
            val newSubject = Subject.findById(newSubjectID)
            val newX = parameters["newX"]?.toIntOrNull()
            val newY = parameters["newY"]?.toIntOrNull()
            val newRadius = parameters["newRadius"]?.toIntOrNull()
            if (newSubject != null && newX != null && newY != null && newRadius != null) {
                val sip = SubjectInPhoto.new2 { sip ->
                    sip.photo = photo
                    sip.subject = newSubject
                    sip.filename = "${sip.photo.album.directory}/subjects/${photo.nameWithoutExtension}-${
                        idNumberFormat.format(newSubjectID)
                    }.jpg"

                    sip.x = newX
                    sip.y = newY
                    sip.radius = newRadius

                }
                // If this is the first SIP added, then use it as the default image for this person.
                if (newSubject.defaultSubjectInPhoto == null) {
                    newSubject.defaultSubjectInPhoto = sip
                }
            }
        }

    }

    // Update the search index
    pageListener.pageChanged("$webPath/photo/$photoID")

    call.respondRedirect("$webPath/photo/$photoID")
}
