package uk.co.nickthecoder.brentwood.familyalbum

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.request.*
import io.ktor.response.*
import kotlinx.html.*
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.and
import uk.co.nickthecoder.brentwood.BrentwoodException
import uk.co.nickthecoder.brentwood.familyalbum.model.*
import uk.co.nickthecoder.brentwood.util.*

private fun surnameInitial(surname: String): Char {
    return if (surname.length > 2 && surname.startsWith("O'")) {
        surname[2]
    } else if (surname.length > 2 && surname.startsWith("Mc") && surname[2].isUpperCase()) {
        surname[2]
    } else if (surname.length > 3 && surname.startsWith("Mac") && surname[3].isUpperCase()) {
        surname[3]
    } else {
        surname.firstOrNull() ?: '?'
    }
}

internal suspend fun FamilyAlbum.subjects(call: ApplicationCall, subjectTypeID: Int = 1) {

    val familyID = parameterID("familyID", call)

    suspendTransaction {
        val subjectType =
            SubjectType.findById(subjectTypeID) ?: throw NotFoundException("Subject Type $subjectTypeID")

        val subjects = Subject.find {
            (Subjects.familyID eq familyID) and (Subjects.subjectTypeID eq subjectTypeID)
        }.orderBy(Pair(Subjects.name2, SortOrder.ASC), Pair(Subjects.name1, SortOrder.ASC))

        val groupBy = GroupBy(subjects.toList()) { surnameInitial(it.name2) }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +subjectType.plural }
            pageHeading { +subjectType.plural }

            pageTools {
                add("$webPath/addSubject/$familyID", "Add $subjectType")
            }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                layout.groupByInitials(this, groupBy)

                for (initial in groupBy.usedInitials()) {

                    layout.groupByAnchor(this, groupBy, initial)
                    layout.thumbnails(this, plain = true) {

                        for (subject in groupBy.itemsForInitial(initial)) {
                            layout.thumbnail(this, {
                                a(subject) { thumbnail(subject) }
                            }, {
                                a(subject) { +"${subject.name2}, ${subject.name1}" }
                            })
                        }
                    }
                }
            }
        }

    }
}


internal suspend fun FamilyAlbum.subject(call: ApplicationCall) {

    val subjectID = parameterID("subjectID", call)

    fun FlowContent.relationships(rels: List<Relationship>?) {
        // h3 { +label }
        if (rels != null) {
            for (rel in rels) {
                val other = rel.toSubject
                a("${other.href()}#subject") {
                    title = "${rel.relationshipType.label} : ${rel.toSubject.name1} ${rel.toSubject.name2}"
                    thumbnail(other)
                }

            }
        }
    }

    suspendTransaction {

        val subject = Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID")
        val subjectsInPhoto = subject.subjectsInPhotos
        val defaultSIP = subject.defaultSubjectInPhoto ?: subjectsInPhoto.firstOrNull()
        val groupedRelationships =
            subject.relationships.groupBy { relationshipTypeToGroup[it.relationshipType.id.value] }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +"${subject.name1} ${subject.name2}" }
            pageHeading {
                +"${subject.name1} ${subject.name2}"
                jumpToNotes(subject.notes)
            }
            pageTools {
                delete("$webPath/deleteSubject/$subjectID", "Delete Subject")
                edit("$webPath/editSubject/$subjectID", "Edit Subject")
            }

            preContent {
                secondaryNavigation(subject.family.id.value)
            }

            content {

                div("family") {
                    id = "subject"

                    div("center") {
                        relationships(groupedRelationships["Parents"])
                    }

                    div("generation") {
                        div("subject") {
                            // Center area
                            div("center") {
                                if (defaultSIP != null) {
                                    image(defaultSIP)
                                } else {
                                    img("no photo", "$webPath/resources/sipImage.png", classes = "sip")
                                }
                                h2 { +"${subject.name1} ${subject.name2}" }
                            }
                        }
                        div("partners") {
                            relationships(groupedRelationships["Partners"])
                        }
                        div("siblings") {
                            relationships(groupedRelationships["Siblings"])
                        }
                    }
                    div("center") {
                        relationships(groupedRelationships["Children"])
                    }
                }

                if (!groupedRelationships["Friends"].isNullOrEmpty()) {
                    h2 { +"Friends" }
                    relationships(groupedRelationships["Friends"])
                }
                if (!groupedRelationships["Other"].isNullOrEmpty()) {
                    h2 { +"Pets" }
                    relationships(groupedRelationships["Other"])
                }

                notesSection(subject.notes)

                h2 { +"Appearances in Photos" }

                for (sip in subjectsInPhoto.sortedBy { it.photo.year ?: 3000 }) {
                    a(sip.photo.href()) {
                        title = sip.photo.yearString()
                        thumbnail(sip)
                    }
                }

            }

        }

    }
}


private val sexes = listOf('M' to "Male", 'F' to "Female", '?' to "Other")

private val subjectTypes = listOf(1 to "Family Member", 3 to "Friend", 2 to "Pet")

internal suspend fun FamilyAlbum.editSubject(
    isNew: Boolean,
    call: ApplicationCall,
    parameters: Parameters,
    error: String? = null
) {

    val subjectID = if (isNew) 0 else parameterID("subjectID", call, parameters)

    suspendTransaction {
        val subject = if (isNew) {
            null
        } else {
            Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID")
        }
        val familyID = if (isNew) parameterID("familyID", call, parameters) else subject?.family?.id?.value
            ?: throw BrentwoodException("FamilyID Not specified")


        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            val pageTitle = if (isNew) "New Person" else "Edit ${subject!!.name1} ${subject.name2}"
            pageTitle { +pageTitle }
            pageHeading { +pageTitle }

            preContent {
                secondaryNavigation(familyID)
            }

            content {

                if (error != null) {
                    div("error") {
                        p { +error }
                    }
                }

                val action = if (isNew) "$webPath/addSubject/$familyID" else "$webPath/editSubject/$subjectID"
                form(action, method = kotlinx.html.FormMethod.post) {

                    div("form") {
                        +"Forename(s)"
                        input(InputType.text, name = "name1") {
                            autoFocus = true
                            value = parameters["name1"] ?: subject?.name1 ?: ""
                        }

                        +"Surname"
                        input(InputType.text, name = "name2") {
                            value = parameters["name2"] ?: subject?.name2 ?: ""
                        }

                        +"Sex"
                        choice("sex", parameters["sex"] ?: subject?.sex, sexes)

                        +"Notes"
                        notesTextArea(parameters["notes"] ?: subject?.notes ?: "")

                        +"Family Member / Friend / Pet"
                        choice("subjectTypeID", parameters["subjectTypeID"] ?: subject?.subjectTypeID, subjectTypes)

                        if (!isNew) {
                            +"Default Image"
                            br()
                            thumbnail(subject!!, "defaultThumbnail")
                            input(InputType.hidden) {
                                name = "defaultSIP"
                                id = "defaultSIP"
                                value = parameters["defaultSIP"] ?: subject.defaultSubjectInPhoto?.id?.value?.toString()
                                        ?: ""
                            }
                        }

                        saveCancelButtons()
                    }


                    if (!isNew) {

                        h2 {
                            add("$webPath/addRelationship/$subjectID", "Add a Relationship")
                            +"Relationships"
                        }
                        if (!subject!!.relationships.empty()) {
                            layout.thumbnails(this) {
                                for (rel in subject.relationships) {
                                    layout.thumbnail(this, {
                                        thumbnail(rel.toSubject)
                                    }, {
                                        input(InputType.submit, name = "deleteRel:${rel.id.value}") {
                                            value = "Delete"
                                        }
                                    })
                                }
                            }
                        } else {
                            +"None"
                        }

                        if (!subject.subjectsInPhotos.empty()) {
                            h2 { +"Click to choose the default image" }
                            for (sip in subject.subjectsInPhotos.sortedBy { it.photo.year ?: 3000 }) {
                                a("#defaultThumbnail") {
                                    onClick =
                                        "document.getElementById(\"defaultSIP\").value=\"${sip.id.value}\"; document.getElementById(\"defaultThumbnail\").src=\"${sip.thumbnail()}\";"
                                    thumbnail(sip)
                                }
                            }
                        }
                    }
                }
            }
        }

    }

}

internal suspend fun FamilyAlbum.saveSubject(isNew: Boolean, call: ApplicationCall, parameters: Parameters) {

    var subjectID = if (isNew) 0 else parameterID("subjectID", call, parameters)
    if (parameters["cancel"] != null) {
        if (isNew) {
            val familyID = parameterID("familyID", call, parameters)
            call.respondRedirect("$webPath/familyMembers/$familyID")
        } else {
            call.respondRedirect("$webPath/subject/$subjectID#subject")
        }
        return
    }

    // Check if any of the relationship Delete buttons were pressed.
    if (!isNew) {
        for (name in parameters.names()) {
            if (name.startsWith("deleteRel:")) {
                name.substring(10).toIntOrNull()?.let { relID ->
                    suspendTransaction {
                        Relationship.findById(relID)?.let { rel ->
                            val fromSubject = rel.fromSubject
                            val toSubject = rel.toSubject

                            // Delete ALL of the relationships between the two people.
                            // In both directions.
                            // Most of the time, each loop will delete exactly 1 row.
                            for (a in fromSubject.relationships) {
                                if (a.toSubject.id.value == toSubject.id.value) {
                                    a.delete()
                                }
                            }
                            for (a in toSubject.relationships) {
                                if (a.toSubject.id.value == fromSubject.id.value) {
                                    a.delete()
                                }
                            }

                        }
                    }
                    // Return back to the edit form.
                    call.respondRedirect("$webPath/editSubject/$subjectID")
                    return
                }
            }
        }
    }

    val name1 = parameters["name1"]?.trim()
    val name2 = parameters["name2"]?.trim()
    val notes = parameters["notes"] ?: ""
    val sex = parameters["sex"]
    val subjectTypeID = parameters["subjectTypeID"]?.toIntOrNull()
    val defaultSIP = parameters["defaultSIP"]?.toIntOrNull()

    if (name1.isNullOrBlank()) {
        editSubject(isNew, call, parameters, "Forename is missing")
        return
    }
    if (name2.isNullOrBlank()) {
        editSubject(isNew, call, parameters, "Surname is missing")
        return
    }
    if (sex.isNullOrBlank()) {
        editSubject(isNew, call, parameters, "Sex is not specified")
        return
    }
    if (subjectTypeID == null) {
        editSubject(isNew, call, parameters, "Choose if this is a Family Member, Friend or a Pet")
        return
    }

    var familyID: Int

    suspendTransaction {
        if (isNew) {
            familyID = parameterID("familyID", call, parameters)
            val family = Family.findById(familyID) ?: throw NotFoundException("Family $familyID")

            val subject = Subject.new2 { subject ->
                subject.family = family
                subject.name1 = name1
                subject.name2 = name2
                subject.disambiguation = ""
                subject.sex = sex.first()
                subject.subjectTypeID = subjectTypeID
                subject.notes = notes
            }
            subjectID = subject.id.value
        } else {
            val subject = Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID not found")
            subject.name1 = name1
            subject.name2 = name2
            subject.sex = sex.first()
            subject.subjectTypeID = subjectTypeID
            subject.notes = notes

            val sip = defaultSIP?.let { SubjectInPhoto.findById(it) }
            subject.defaultSubjectInPhoto = sip
            familyID = subject.family.id.value
        }

        // Update the search index
        pageListener.pageChanged("$webPath/subject/$subjectID")
        pageListener.pageChanged("$webPath/subjects/$familyID")
    }

    call.respondRedirect("$webPath/subject/$subjectID#subject")
}


internal suspend fun FamilyAlbum.confirmDeleteSubject(call: ApplicationCall) {

    val subjectID = parameterID("subjectID", call)

    suspendTransaction {
        val subject = Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID")

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
            pageTitle { +"Delete ${subject.name1} ${subject.name2}" }
            pageHeading { +"Delete ${subject.name1} ${subject.name2}" }

            content {

                form(action = "$webPath/deleteSubject/$subjectID", method = kotlinx.html.FormMethod.post) {

                    div("form") {
                        h2 { +"${subject.name1} ${subject.name2}" }

                        +"This person currently appears in ${subject.subjectsInPhotos.count()} photos, and has ${subject.relationships.count()} relationships."

                        deleteCancelButtons()
                    }
                }


                if (!subject.relationships.empty()) {
                    h2 { +"Relationships with the following will be removed" }
                    layout.thumbnails(this) {
                        for (rel in subject.relationships) {
                            layout.thumbnail(this, { thumbnail(rel.toSubject) })
                        }
                    }
                }

                if (!subject.subjectsInPhotos.empty()) {
                    h2 { +"The person will be removed from these photos" }
                    layout.thumbnails(this) {
                        for (sip in subject.subjectsInPhotos) {
                            layout.thumbnail(this, { thumbnail(sip) })
                        }
                    }
                }
            }

        }
    }

}

internal suspend fun FamilyAlbum.deleteSubject(call: ApplicationCall) {
    val parameters = call.receiveParameters()
    val subjectID = parameterID("subjectID", call, parameters)

    if (parameters["ok"] != null) {
        suspendTransaction {
            val subject = (Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID"))
            val familyID = subject.family.id.value
            for (sip in subject.subjectsInPhotos) {
                deleteImages(sip)
                sip.delete()
            }
            for (rel in subject.relationships) {
                rel.delete()
            }
            for (rel in subject.backRelationships) {
                rel.delete()
            }
            subject.delete()
            call.respondRedirect("$webPath/familyMembers/$familyID")
        }
    } else {
        call.respondRedirect("$webPath/subject/$subjectID")
    }
}

internal suspend fun FamilyAlbum.addRelationship(call: ApplicationCall) {
    val subjectID = parameterID("subjectID", call)

    suspendTransaction {
        val subject = Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID")
        val allSubjects = Subject.all().sortedBy { it.name1 }
        val relationshipTypes = RelationshipType.all().filter {
            it.dualUse || it.sex == subject.sex
        }.map { it.id.value to it.label }

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +"Add a New Relationship" }
            pageHeading { +"Add a New Relationship" }

            content {

                form("$webPath/addRelationship/$subjectID", method = FormMethod.post) {
                    div("form") {

                        h3 { +"${subject.name1} ${subject.name2} is related to ..." }

                        br()
                        br()
                        chooseName(this, allSubjects, "toSubjectID")

                        +"Relationship Type. ${subject.name1} ${subject.name2} is their ..."
                        choice("relationshipTypeID", 0, relationshipTypes, "relationshipTypes")
                    }

                    saveCancelButtons("Create")
                }

            }
        }
    }

}

/**
 * Lets the user pick a person (or pet) by selecting a surname from a list,
 * then select a forename from a list (which has just magically been populated based on the choice of surname).
 */
internal fun FamilyAlbum.chooseName(div: FlowContent, allSubjects: Iterable<Subject>, idName: String) {
    with(div) {
        val surnames = allSubjects.groupBy { it.name2 }.keys.sorted().map { Pair(it, it) }

        +"Surname"
        choice("surname", "", surnames, "chooseSurname")

        +"Forename(s)"
        choice(idName, "", emptyList(), "choosePerson")

        val bySurname = buildString {
            append("\n")
            append("var bySurname = new Map();\n")
            allSubjects.groupBy { it.name2 }.forEach { surname, foo ->
                append(
                    "bySurname.set( \"$surname\", ${
                        foo.joinToString(prefix = "[", separator = ",", postfix = "]") {
                            "{value:${it.id.value}, text:\"${it.name1}\" }"
                        }
                    });\n"
                )
            }
        }
        inlineScript(subject_JS)
        inlineScript(bySurname)
    }
}

private val subject_JS =
    """
window.addEventListener( "load", function() {
    document.getElementById( "chooseSurname" ).onchange = function() {
        console.log( "Value= " + this.value )
        var list = bySurname.get(this.value);
        var choosePerson = document.getElementById( "choosePerson" );
        while( choosePerson.options.length > 0 ) {
            choosePerson.remove(0);
        }
        var option = document.createElement('option');
        option.text = "Choose...";
        option.value = ""
        choosePerson.add( option, null );
        
        for (var i = 0; i < list.length; i ++ ) {
            var item = list[i];
            var option = document.createElement('option');
            option.text = item.text;
            option.value = item.value
            choosePerson.add( option, null );
        }
    };
});

""""".shrinkJavascript()

internal suspend fun FamilyAlbum.newRelationship(call: ApplicationCall) {
    val parameters = call.receiveParameters()
    val subjectID = parameterID("subjectID", call, parameters)
    val toSubjectID = parameterID("toSubjectID", call, parameters)
    val relationshipTypeID = parameterID("relationshipTypeID", call, parameters)

    suspendTransaction {

        val subject = Subject.findById(subjectID) ?: throw NotFoundException("Subject $subjectID")
        val toSubject = Subject.findById(toSubjectID) ?: throw NotFoundException("Subject $toSubjectID")
        val relationshipType = RelationshipType.findById(relationshipTypeID)
            ?: throw NotFoundException("RelationshipType $relationshipTypeID")
        val reverse = when (toSubject.sex) {
            'M' -> relationshipType.reverseMale
            'F' -> relationshipType.reverseFemale
            else -> relationshipType.reverseUnspecifiedSex
        }
        Relationship.new2 { rel ->
            rel.fromSubject = subject
            rel.toSubject = toSubject
            rel.relationshipType = reverse
        }
        Relationship.new2 { rel ->
            rel.fromSubject = toSubject
            rel.toSubject = subject
            rel.relationshipType = relationshipType
        }

    }
    call.respondRedirect("$webPath/editSubject/$subjectID")

}

private val relationshipTypeToGroup = mapOf(
    1 to "Parents",
    2 to "Parents",
    3 to "Parents",

    4 to "Children",
    5 to "Children",
    6 to "Children",

    7 to "Siblings",
    8 to "Siblings",
    9 to "Siblings",

    10 to "Partners",
    11 to "Partners",
    12 to "Partners",
    13 to "Partners",
    14 to "Partners",

    15 to "Friends",

    16 to "Other",
    17 to "Other"
)
