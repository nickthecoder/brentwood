package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Albums : IntIdTable("album", "albumID") {

    val familyID = reference("familyID", Families)
    val defaultPhotoID = reference("defaultPhotoID", Photos).nullable()

    val label = varchar("label", 100)
    val directory = varchar("directory", 200)
    val notes = text("notes")
}

class Album(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Album>(Albums)

    var family by Family referencedOn Albums.familyID
    var defaultPhoto by Photo optionalReferencedOn Albums.defaultPhotoID

    var label by Albums.label
    var directory by Albums.directory
    var notes by Albums.notes

    val photos by Photo referrersOn Photos.albumID
}
