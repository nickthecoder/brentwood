package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.io.IOException
import java.sql.DriverManager

fun createFamilyAlbumDB(sqliteDB: File) {

    if (sqliteDB.exists()) {
        throw IOException("File $sqliteDB already exists.")
    }

    val db = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$sqliteDB") })
    transaction(db) {
        SchemaUtils.create(
            Families,
            Albums,
            Photos,
            Subjects,
            SubjectsInPhotos,
            Relationships,
            RelationshipTypes,
            SubjectTypes
        )
    }

    addRelationshipTypes(db)
    addSubjectTypes(db)
}

fun dumpSchema(sqliteDB: File) {

    ProcessBuilder(
        "sh",
        "-c",
        "sqlite3 $sqliteDB .schema | sed -e 's/(/(\\n    /'  -e 's/, /,\\n    /g' -e 's/);/\\n);\\n/' -e 's/REFERENCES/\\n        REFERENCES/g' -e 's/CONSTRAINT/\\n    CONSTRAINT/g'\n"
    ).apply {
        inheritIO()
    }.start()
}

/**
 * Creates all the rows for RelationShipType.
 *
 * I've added non-binary relationships - a subject can be of sex 'M' 'F' or non-binary '?'.
 * And then each relationship type has three entries. e.g. Father (M), Mother (F) and Parent (?).
 * Currently nobody in my family identifies as non-binary, so this feature isn't well tested, or used.
 */
fun addRelationshipTypes(db: Database) {

    transaction(db) {
        RelationshipTypes.deleteAll()
        // NOTE, the later transactions assume that sqlite_sequence["relationshipType"] = 0,
        // but this won't be true if you run the program twice!
        // So I am manually resetting by deleting the appropriate row (it will be recreated
        // when we add the first row into the table relationshipType).
        sqlite_sequence.deleteWhere { sqlite_sequence.name eq "relationshipType" }
    }

    // Parent child relationships
    transaction(db) {
        // 1
        RelationshipTypes.insert {
            it[label] = "Parent"
            it[sex] = '?'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 4 // Child
            it[reverseFemaleID] = 5 // Daughter
            it[reverseMaleID] = 6 // Son
        }
        // 2
        RelationshipTypes.insert {
            it[label] = "Mother"
            it[sex] = 'F'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 4 // Child
            it[reverseFemaleID] = 5 // Daughter
            it[reverseMaleID] = 6 // Son
        }
        // 3
        RelationshipTypes.insert {
            it[label] = "Father"
            it[sex] = 'M'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 4 // Child
            it[reverseFemaleID] = 5 // Daughter
            it[reverseMaleID] = 6 // Son
        }

        // 4
        RelationshipTypes.insert {
            it[label] = "Child"
            it[sex] = '?'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 1 // Parent
            it[reverseFemaleID] = 2 // Mother
            it[reverseMaleID] = 3 // Daughter
        }
        // 5
        RelationshipTypes.insert {
            it[label] = "Daughter"
            it[sex] = 'F'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 1 // Parent
            it[reverseFemaleID] = 2 // Mother
            it[reverseMaleID] = 3 // Daughter
        }
        // 6
        RelationshipTypes.insert {
            it[label] = "Son"
            it[sex] = 'M'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 1 // Parent
            it[reverseFemaleID] = 2 // Mother
            it[reverseMaleID] = 3 // Daughter
        }
    }

    // Siblings
    transaction(db) {
        // 7
        RelationshipTypes.insert {
            it[label] = "Sibling"
            it[sex] = '?'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 7 // Sibling
            it[reverseFemaleID] = 8 // Sister
            it[reverseMaleID] = 9 // Brother
        }
        // 8
        RelationshipTypes.insert {
            it[label] = "Brother"
            it[sex] = 'M'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 7 // Sibling
            it[reverseFemaleID] = 8 // Sister
            it[reverseMaleID] = 9 // Brother
        }
        // 9
        RelationshipTypes.insert {
            it[label] = "Sister"
            it[sex] = 'F'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 7 // Sibling
            it[reverseFemaleID] = 8 // Sister
            it[reverseMaleID] = 9 // Brother
        }
    }

    // Partnerships
    transaction(db) {
        // 10
        RelationshipTypes.insert {
            it[label] = "Partner"
            it[sex] = '?'
            it[reverseUnspecifiedSexID] = 10 // Partner
            // NOTE. This entry is used in two different scenarios
            // 1) The person identifies as non-binary sex
            // 2) The person is not married (of any sex, including non-binary)
            // NOTE. Our language may evolve to introduce a new term to mean "married non-binary"
            // i.e. on par with Husband and Wife.
            // At which point, we could have "false" here, and "Partner" would only mean
            // not-married to a person of any sex. We would also need another row for the new term.
            // But for now, "Partner" also means "Married to a person of non-binary sex".
            it[dualUse] = true
            it[reverseFemaleID] = 11 // Wife
            it[reverseMaleID] = 12 // Husband
        }
        // 11
        RelationshipTypes.insert {
            it[label] = "Wife"
            it[sex] = 'F'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 10 // Partner
            it[reverseFemaleID] = 11 // Wife
            it[reverseMaleID] = 12 // Husband
        }
        // 12
        RelationshipTypes.insert {
            it[label] = "Husband"
            it[sex] = 'M'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 10 // Partner
            it[reverseFemaleID] = 11 // Wife
            it[reverseMaleID] = 12 // Husband
        }
        // 13
        RelationshipTypes.insert {
            it[label] = "Girlfriend"
            it[sex] = 'F'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 10 // Partner
            it[reverseFemaleID] = 13 // Girlfriend
            it[reverseMaleID] = 14 // Boyfriend
        }
        // 14
        RelationshipTypes.insert {
            it[label] = "Boyfriend"
            it[sex] = 'M'
            it[dualUse] = false
            it[reverseUnspecifiedSexID] = 10 // Partner
            it[reverseFemaleID] = 13 // Girlfriend
            it[reverseMaleID] = 14 // Boyfriend
        }

    }

    // Friends
    transaction {
        // 15
        RelationshipTypes.insert {
            it[label] = "Friend"
            it[sex] = '?'
            it[dualUse] = true
            it[reverseUnspecifiedSexID] = 15 // Friend
            it[reverseFemaleID] = 15 // Friend
            it[reverseMaleID] = 15 // Friend
        }
    }

    // Pet/Owner
    transaction {
        // 16
        RelationshipTypes.insert {
            it[label] = "Pet"
            it[sex] = '?'
            it[dualUse] = true
            it[reverseUnspecifiedSexID] = 17 // Owner
            it[reverseFemaleID] = 17 // Owner
            it[reverseMaleID] = 17 // Owner
        }
        // 17
        RelationshipTypes.insert {
            it[label] = "Owner"
            it[sex] = '?'
            it[dualUse] = true
            it[reverseUnspecifiedSexID] = 16 // Pet
            it[reverseFemaleID] = 16 // Pet
            it[reverseMaleID] = 16 // Pet
        }
    }

}

fun addSubjectTypes(db: Database) {

    transaction(db) {
        SubjectTypes.deleteAll()
        sqlite_sequence.deleteWhere { sqlite_sequence.name eq "subjectType" }

        // 1
        SubjectTypes.insert {
            it[label] = "Family Member"
            it[plural] = "Family Members"
        }
        // 2
        SubjectTypes.insert {
            it[label] = "Pet"
            it[plural] = "Pets"
        }
        // 3
        SubjectTypes.insert {
            it[label] = "Friend"
            it[plural] = "Friends"
        }
    }
}

/**
 * A kotlin representation of a row of the special sqlite table "sqlite_sequence", which
 * holds the next id for all tables which use autoincrement on their primary key.
 */
object sqlite_sequence : Table() {
    val name = text("name")
    val seq = integer("seq")

    override val primaryKey = PrimaryKey(name)
}

/**
 * Creates the tables in the (empty?) database called familyAlbum.db in the current directory.
 *
 * Prints the resulting schema.
 */
fun main(/*vararg args: String*/) {
    createFamilyAlbumDB(File("familyAlbum.db"))
    dumpSchema(File("familyAlbum.db"))
}
