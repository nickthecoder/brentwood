package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Families : IntIdTable("family", "familyID") {
    val label = varchar("label", 100)

    val notes = text("notes")
}

class Family(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Family>(Families)

    var label by Families.label
    var notes by Families.notes

    val albums by Album referrersOn Albums.familyID
    val subjects by Subject referrersOn Subjects.familyID

}
