package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Photos : IntIdTable("photo", "photoID") {

    val albumID = reference("albumId", Albums)

    val filename = varchar("filename", 200)
    val notes = text("notes")
    val year = integer("year").nullable()
    val yearAccuracy = integer("yearAccuracy").nullable()
}

class Photo(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Photo>(Photos)

    var album by Album referencedOn Photos.albumID

    var filename by Photos.filename
    var notes by Photos.notes
    var year by Photos.year
    var yearAccuracy by Photos.yearAccuracy

    val subjectsInPhoto by SubjectInPhoto referrersOn SubjectsInPhotos.photoID

    val directory: String
        get() {
            val slash = filename.lastIndexOf('/')
            return if (slash > 0) {
                filename.substring(0, slash)
            } else {
                "/"
            }
        }

    val name: String
        get() {
            val slash = filename.lastIndexOf('/')
            return if (slash > 0) {
                filename.substring(slash + 1)
            } else {
                filename
            }
        }

    val nameWithoutExtension: String
        get() {
            val name = name
            val dot = name.lastIndexOf('.')
            return if (dot > 0) {
                name.substring(0, dot)
            } else {
                name
            }
        }

    val extension: String
        get() {
            val name = name
            val dot = name.lastIndexOf('.')
            return if (dot > 0) {
                name.substring(dot + 1)
            } else {
                ""
            }
        }


    fun yearString(): String {
        return if (year == null) {
            "Date Unknown"
        } else {
            if (yearAccuracy == null || yearAccuracy!! < 2) {
                year.toString()
            } else {
                "$year ish"
            }
        }
    }
}
