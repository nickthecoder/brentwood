package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Relationships : IntIdTable("relationship", "relationshipID") {

    val fromSubjectID = reference("fromSubjectID", Subjects).index()
    val toSubjectID = reference("toSubjectID", Subjects).index()

    val relationshipTypeID = reference("relationshipTypeID", RelationshipTypes)
}

class Relationship(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Relationship>(Relationships)

    var fromSubject by Subject referencedOn Relationships.fromSubjectID
    var toSubject by Subject referencedOn Relationships.toSubjectID

    var relationshipType by RelationshipType referencedOn Relationships.relationshipTypeID
}
