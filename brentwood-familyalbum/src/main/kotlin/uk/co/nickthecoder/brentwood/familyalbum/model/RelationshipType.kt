package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object RelationshipTypes : IntIdTable("relationshipType", "relationshipTypeID") {

    val label = varchar("label", 50)
    val sex = char("sex")
    val dualUse = bool("dualUse")

    val reverseUnspecifiedSexID = reference("reverseUnspecifiedID", RelationshipTypes)
    val reverseMaleID = reference("reverseMaleID", RelationshipTypes)
    val reverseFemaleID = reference("reverseFemaleID", RelationshipTypes)
}

class RelationshipType(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, RelationshipType>(RelationshipTypes)

    var label by RelationshipTypes.label

    /**
     * 'M' (male), 'F' (female) or '?' for unspecified.
     */
    var sex by RelationshipTypes.sex
    // Note

    /**
     * When true, this relationship is used for non-binary sex and also when the sex of the person isn't relevant.
     * For example "Partner" can be used when they are NOT married.
     * It can also be used when the the sex of the partner is non-binary and they ARE married
     * (as we don't yet have a term for a married partner of non-binary sex AFAIK).
     */
    var dualUse by RelationshipTypes.dualUse

    var reverseUnspecifiedSex by RelationshipType referencedOn RelationshipTypes.reverseUnspecifiedSexID
    var reverseMale by RelationshipType referencedOn RelationshipTypes.reverseMaleID
    var reverseFemale by RelationshipType referencedOn RelationshipTypes.reverseFemaleID
}
