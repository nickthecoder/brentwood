package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Subjects : IntIdTable("subject", "subjectID") {

    val familyID = reference("familyID", Families)

    val name1 = varchar("name1", 100)
    val name2 = varchar("name2", 100)
    val disambiguation = varchar("disambiguation", 100)
    val notes = text("notes")
    val sex = char("sex")

    val subjectTypeID = integer("subjectTypeID")
    val defaultSubjectInPhotoID = reference("defaultSubjectInPhotoID", SubjectsInPhotos).nullable()
}

class Subject(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, Subject>(Subjects)

    var family by Family referencedOn Subjects.familyID

    var name1 by Subjects.name1
    var name2 by Subjects.name2
    var disambiguation by Subjects.disambiguation
    var notes by Subjects.notes
    var sex by Subjects.sex

    var defaultSubjectInPhoto by SubjectInPhoto optionalReferencedOn Subjects.defaultSubjectInPhotoID
    var subjectTypeID by Subjects.subjectTypeID

    val subjectsInPhotos by SubjectInPhoto referrersOn SubjectsInPhotos.subjectID
    val relationships by Relationship referrersOn Relationships.fromSubjectID
    val backRelationships by Relationship referrersOn Relationships.toSubjectID

}
