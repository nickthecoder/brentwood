package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object SubjectsInPhotos : IntIdTable("subjectInPhoto", "subjectInPhotoID") {

    val subjectID = reference("subjectID", Subjects)
    val photoID = reference("photoID", Photos)

    val filename = varchar("filename", 300)
    val x = integer("x")
    val y = integer("y")
    val radius = integer("radius")
}

class SubjectInPhoto(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, SubjectInPhoto>(SubjectsInPhotos)

    var subject by Subject referencedOn SubjectsInPhotos.subjectID
    var photo by Photo referencedOn SubjectsInPhotos.photoID

    var filename by SubjectsInPhotos.filename
    var x by SubjectsInPhotos.x
    var y by SubjectsInPhotos.y
    var radius by SubjectsInPhotos.radius

}
