package uk.co.nickthecoder.brentwood.familyalbum.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


/**
 * The subjects in a family are either 1: family members, 2: pets, 3: friends,
 */
object SubjectTypes : IntIdTable("subjectType", "subjectTypeID") {
    val label = varchar("label", 30)
    val plural = varchar("plural", 30)
}

class SubjectType(id: EntityID<Int>) : Entity<Int>(id) {
    companion object : EntityClass<Int, SubjectType>(SubjectTypes)

    var label by SubjectTypes.label
    var plural by SubjectTypes.plural
}
