var sips = new Array();
var photo = null;
var glass = null;
var imageContainer = null;
var ratio = 1;
var offsetX = 0;

var isDraggingEdge = false;
var isDraggingCenter = false;
var draggingSIP = null;
var dragOffsetX = 0; // In pixels.
var dragOffsetY = 0;

function addSIP( sipID, subjectID, x, y, radius, label ) {
    sips.push( { sipID: sipID, subjectID: subjectID, x : x, y : y, radius : radius, label : label } );
}
function showTags( webPath, isEditing) {
    glass = document.getElementById( "glass" );
    photo = document.getElementById( "photo" );
    imageContainer = document.getElementById( "imageContainer" );

    ratio = photo.width / photo.naturalWidth;
    offsetX = Math.floor((glass.clientWidth - photo.width)/2);

    // This prevents the mouse interacting with the photo by placing a
    // div above it. Without this, when dragging the mouse, I would sometimes
    // get the browser's weird behaviour of dragging a thumbnail of the image.
    var back = document.createElement("div");
    back.className = "tagPhotoBack";
    back.style.position = 'absolute';
    back.style.left = '0px';
    back.style.top = '0px';
    back.style.width = glass.clientWidth + 'px';
    back.style.height = photo.clientHeight +'px';
    glass.appendChild( back );

    for ( var i = 0; i < sips.length; i ++ )  {
        var sip = sips[i];
        var highlight = document.createElement("div");
        highlight.className = isEditing ? "existingSIP" : "hiddenSIP";
        highlight.style.position = 'absolute';
        highlight.style.left = Math.floor( ratio * (sip.x - sip.radius) + offsetX) + 'px';
        highlight.style.top = Math.floor( ratio * (sip.y - sip.radius)) + 'px';
        highlight.style.width = Math.floor( ratio * sip.radius*2 ) + 'px';
        highlight.style.height = Math.floor( ratio * sip.radius*2 ) + 'px';
        highlight.style.borderRadius = Math.floor( sip.radius * ratio ) + 'px';
        highlight.title = sip.label;
        highlight.subjectID = sip.subjectID;
        if (!isEditing) {
            highlight.style.cursor = "pointer";
            var subjectID = sip.subjectID;
            highlight.onclick = function() {
                window.location = webPath + "/subject/" + this.subjectID
            };
        }

        sip.highlight = highlight;

        glass.appendChild( highlight );
    }

    if (isEditing) {
        imageContainer.onmousedown = function( event ) {

            var rect = imageContainer.getBoundingClientRect();
            var x = event.clientX - rect.left - offsetX;
            var y = event.clientY - rect.top;

            for ( var i = 0 ; i < sips.length; i ++ ) {
                var sip = sips[i];
                var dx = x - ratio * sip.x;
                var dy = y - ratio * sip.y;
                var dist = Math.sqrt(dx * dx + dy * dy); // Distance from center of the circle

                // If we start dragging near the middle of the circle (to adjust the center)
                if (dist < sip.radius * ratio * 0.5) {
                    dragOffsetX = dx;
                    dragOffsetY = dy;
                    draggingSIP = sip;
                    isDraggingCenter = true;
                    return;
                }

                // If we start dragging near the edge of a circle (to adjust the radius)
                var delta = Math.abs(dist - ratio * sip.radius) // Distance from the circumference
                if (delta < 10) {
                    // Close enough to the edge of a circle!
                    draggingSIP = sip;
                    isDraggingEdge = true;
                    return;
                }
            }
        };

        document.body.onmouseup = function( event ) {
            isDraggingEdge = false;
            isDraggingCenter = false;
            draggingSIP = null;
        };

        imageContainer.onmousemove = function( event ) {

            var rect = imageContainer.getBoundingClientRect();
            var x = event.clientX - rect.left - offsetX;
            var y = event.clientY - rect.top;


            if (draggingSIP != null) {
                var sip = draggingSIP;

                if (isDraggingEdge) {
                    var dx = x/ratio - sip.x;
                    var dy = y/ratio - sip.y;
                    var dist = Math.sqrt(dx * dx + dy * dy);
                    sip.radius = dist;

                } else if (isDraggingCenter) {
                    sip.x = (x - dragOffsetX) / ratio;
                    sip.y = (y - dragOffsetY) / ratio;
                }

                var highlight = sip.highlight;
                highlight.style.left = Math.floor( ratio * (sip.x - sip.radius) + offsetX ) + 'px';
                highlight.style.top = Math.floor( ratio * (sip.y - sip.radius)) + 'px';
                highlight.style.width = Math.floor( ratio * sip.radius*2 ) + 'px';
                highlight.style.height = Math.floor( ratio * sip.radius*2 ) + 'px';
                highlight.style.borderRadius = Math.floor( sip.radius * ratio ) + 'px';

                document.getElementById( "x_" + sip.sipID ).value = Math.floor(sip.x);
                document.getElementById( "y_" + sip.sipID ).value = Math.floor(sip.y);
                document.getElementById( "r_" + sip.sipID ).value = Math.floor(sip.radius);

                event.stopPropagation();
            }
        };
    }
}

function newSIP() {
    var sip = { sipID: "new", x : 20, y : 20, radius : 100, label : "new" }
    sips.push( sip );

    document.getElementById( "newSIPButton" ).style.display = "none";
    document.getElementById( "newSIPDetails" ).style.display = "block";

    var highlight = document.createElement("div");
    highlight.className = "newSIP";
    highlight.style.position = 'absolute';
    highlight.style.left = Math.floor( ratio * (sip.x - sip.radius) + offsetX) + 'px';
    highlight.style.top = Math.floor( ratio * (sip.y - sip.radius)) + 'px';
    highlight.style.width = Math.floor( ratio * sip.radius*2 ) + 'px';
    highlight.style.height = Math.floor( ratio * sip.radius*2 ) + 'px';
    highlight.style.borderRadius = Math.floor( sip.radius * ratio ) + 'px';
    highlight.title = sip.label;

    sip.highlight = highlight;

    glass.appendChild( highlight );
}
