package uk.co.nickthecoder.brentwood.home

import io.ktor.routing.*
import uk.co.nickthecoder.brentwood.component
import uk.co.nickthecoder.brentwood.files.Files
import uk.co.nickthecoder.brentwood.home.games.Games
import uk.co.nickthecoder.brentwood.home.garden.EditScheduleSyntax
import uk.co.nickthecoder.brentwood.home.garden.ScheduleData
import uk.co.nickthecoder.brentwood.home.garden.ScheduleSyntax
import uk.co.nickthecoder.brentwood.home.map.Maps
import uk.co.nickthecoder.brentwood.home.threed.ThreeD
import uk.co.nickthecoder.brentwood.images.Images
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.search.RegexURLPathAndQueryFilter
import uk.co.nickthecoder.brentwood.search.RegexURLPathFilter
import uk.co.nickthecoder.brentwood.search.Search
import uk.co.nickthecoder.brentwood.software.Software
import uk.co.nickthecoder.brentwood.software.SoftwareCategory
import uk.co.nickthecoder.brentwood.software.SoftwareConfiguration
import uk.co.nickthecoder.brentwood.software.SoftwareProject
import uk.co.nickthecoder.brentwood.time.Time
import uk.co.nickthecoder.brentwood.util.*
import uk.co.nickthecoder.brentwood.wiki.FileStorage
import uk.co.nickthecoder.brentwood.wiki.Namespace
import uk.co.nickthecoder.brentwood.wiki.WikiEngine
import uk.co.nickthecoder.brentwood.wiki.WikiImageScaler
import uk.co.nickthecoder.brentwood.wiki.syntax.*
import java.io.File

/**
 * Common Ktor routes for both the Public and Private web sites.
 */
fun Route.routeCommon(layout: Layout) {

    val maps = Maps(
        "/map",
        "AIzaSyCpZxghYNTdsc5KXL483zoCMIN9tRLzHWw",
        "pk.eyJ1Ijoibmlja3RoZWNvZGVyIiwiYSI6ImNpaDJubWZhODAwcXJ2cWtyNW14bTRyZW4ifQ.-eeDoDI8ky5KjLLp3GLl1Q"
    )

    // To be backwards compatible with the old website, redirect urls to the new ones.
    legacyRedirects(this)


    fun software(
        id: String, label: String,
        hosted: String? = "https://gitlab.com/nickthecoder/$id",
        deprecated: Boolean = false
    ) = SoftwareProject(
        id, label, File("/gidea/software/$id"), hosted, deprecated = deprecated
    )

    val softwareConfig = SoftwareConfiguration(
        listOf(

            SoftwareCategory(
                "Tools", listOf(
                    software("door", "Door"),
                    software("feather2", "Feather2"),
                    software("feather", "Feather", deprecated = true),
                    software("fxessentials", "FXEssentials", deprecated = true),
                    software("glok", "Glok"),
                    software("harbourfx", "HarbourFX", deprecated = true),
                    software("ink2scad", "ink2scad", deprecated = true),
                    software("jguifier", "jguifier", deprecated = true),
                    software("metabuild", "Meta Build"),
                    software("paratask", "ParaTask", deprecated = true),
                    software("prioritydoc", "Priority Doc", deprecated = true),
                    software("scarea", "Scarea", deprecated = true),
                    software("tedi", "Tedi", deprecated = true),
                    software("toolapp", "ToolApp", deprecated = true),
                    software("wrkfoo", "wrkfoo", deprecated = true),
                )
            ),

            SoftwareCategory(
                "Desktop Applications", listOf(
                    software("blokart", "Blok Art"),
                    software("featureful", "Featureful"),
                    software("fizzy", "Fizzy", deprecated = true),
                    software("foocad", "FooCAD"),
                    software("goko", "Goko"),
                    software("vectorial", "Vectorial"),
                )
            ),

            SoftwareCategory(
                "Web Applications", listOf(
                    software("brentwood", "Brentwood"),
                    // TODO Rename to gamesCupboard
                    // TODO Move from nickthecoders to nickthecoder
                    software("gamesCupboard", "Games Cupboard"),
                    software("lockeddown", "Locked-down Games", deprecated = true),
                    software("tilda", "Tilda", deprecated = true),
                    software("webwidgets", "WebWidgets", deprecated = true),
                    software("gidea", "Gidea", deprecated = true),
                    software("familyalbum", "Family Album", deprecated = true),
                    software("ichneutae", "Ichneutae", deprecated = true),
                    software("ntc", "ntc", deprecated = true),
                    software("ntcslash", "ntcslash", deprecated = true),
                )
            ),

            SoftwareCategory(
                "Games", listOf(
                    software("burton", "Burton"),
                    software("cavernQuest", "Cavern Quest"),
                    software("dodgeEm", "Dodge 'Em"),
                    software("dollsHouse", "Dolls House"),
                    software("higgsAnomaly", "Higgs Anomaly"),
                    software("rapidragdoll", "Rapid Rag Doll", deprecated = true),
                    software("tickle", "Tickle"),
                    software("drunkInvaders", "Drunk Invaders"),
                    software("drunkInvaders-groovy", "Drunk Invaders (groovy)", deprecated = true),
                    software("pillPopper", "Pill Popper", deprecated = true),
                    software("itchy", "Itchy", deprecated = true),
                    software("jame", "Jame", deprecated = true),
                    software("ithrust", "iThrust", deprecated = true),
                    software("gamesCupboard", "Games Cupboard"),
                )
            ),

            SoftwareCategory(
                "Electronics", listOf(
                    software("abstractIO", "Abstract IO"),
                    software("eplayground", "e Playground"),
                )
            )
        )
    )

    val software = Software(layout, "/software", softwareConfig)

    // Routing shared by both the public and private websites
    staticResources("/style", "${NickTheCoder::class.java.packageName}.style")
    staticResources("/resources", "${NickTheCoder::class.java.packageName}.resources")
    staticResources("/fileResources", Files::class.java.packageName)

    component(Games(layout, "/games"))
    component(software)
    component(maps)
    component(Time())
    component(ThreeD(layout, "/3d", File("/home/nick/documents/3D"), ImageScaler(150, 150)))

}

/**
 * The ImageScaler used by the [Images] component for the thumbnails
 */
val thumbnailScaler = ImageScaler(150, 150)

/**
 * The ImageScaler used by the [Images] component for the "quick" images.
 * These are big enough for casual viewing, and reduces image size compared to the full-size photos.
 */
val quickScaler = ImageScaler(640, 640)

/**
 * Filters out URLs which should not be indexed by the [Search] Spider.
 */
val searchFilter = RegexURLPathFilter("/music/playMusic/.*") or
        RegexURLPathFilter("/music/.*\\.(png|jpg|jpeg)") or
        RegexURLPathFilter("/images/.*\\.(png|jpg|jpeg)") or
        RegexURLPathFilter("/images/\\.people.*") or
        RegexURLPathFilter("images/.*/editNotes/.*") or
        // Only index the "normal" size images, not the thumbnails, nor the full size images.
        RegexURLPathAndQueryFilter("/images/.*", ".*size=.*") or
        RegexURLPathFilter("/games/resources/.*") or
        RegexURLPathFilter("/wiki/media/.*") or
        RegexURLPathFilter("/wiki/slides/.*") or
        RegexURLPathFilter("/wiki/info/.*") or
        RegexURLPathFilter("/wiki/raw/.*") or
        RegexURLPathFilter("/wiki/edit/.*") or
        RegexURLPathFilter("/player/.*") or // Do not index the music PLAYER - it has its own search.
        RegexURLPathFilter("/search.*") or
        RegexURLPathFilter("/public/.*") or
        RegexURLPathFilter("/software/.*/files/.*/.*") or
        RegexURLPathFilter("/software/.*/download/.*") or
        RegexURLPathFilter("/software/.*/api/.*")

fun addCustomWikiSyntax(engine: WikiEngine, search: Search?) {

    engine.parser.add(

        SlidesLinkSyntax(),

        CodeBlockSyntax(CodeBlockConfig.kotlin),

        DivSyntax("{{center", "}}", "center"),
        DivSyntax("{{left", "}}", "left"),
        DivSyntax("{{right", "}}", "right"),
        DivSyntax("{{noSearch", "}}", "noSearch"),
        DivSyntax("{{menu", "}}", "menu"),
        ScaledImageSyntax("{{teaser", "}}", WikiImageScaler("teaser.png", 300, 300)),
        ScaledImageSyntax("{{thumbnail", "}}", WikiImageScaler("thumbnail.png", 166, 166)),
        IncludeSyntax(),
        IncludeSyntax("{{source", "}}", false),
        TableOfContentsSyntax(),
        DivSyntax("{{plantDetails", "}}", "noSearch plantDetails"),
        DivSyntax("{{plantData", "}}", "noSearch plantData"),

        // Deprecated Syntax. These can be removed after all pages have been updated.
        // Deprecated because I don't want to allow arbitrary css classes to be used within the wiki.
        Div2Syntax("{{[", "]}}").apply { isDeprecated = true },
        IndexSyntax("{{:index", "}}").apply { isDeprecated = true }, // Legacy syntax
        HeadingLineSyntax("!!!!", 4).apply { isDeprecated = true },
        HeadingLineSyntax("!!!", 3).apply { isDeprecated = true },
        HeadingLineSyntax("!!", 2).apply { isDeprecated = true },
        HeadingLineSyntax("!", 1).apply { isDeprecated = true },
        LineBreakSyntax("\\\\").apply { isDeprecated = true }, // Legacy syntax. The same as <br>
        ScheduleSyntax(),
        EditScheduleSyntax(),
        ScheduleSyntax(
            "{{plantChart", "}}", ScheduleData(
                tableClass = "noSearch plantChart",
                headings = listOf("J", "F", "M", "A", "M", "J", "J", "A", "S", "O", "N", "D")
            ), isFullWidth = false
        ),
        SummarySyntax()
    )

    if (search != null) {
        engine.parser.add(BackLinksSyntax(search))
    }
}

/**
 * Ensure that urls specific to the old site get redirected to the new urls,
 * so that bookmarks and links still work.
 */
private fun legacyRedirects(route: Route) {
    with(route) {

        redirectParameter("/gidea/map.jsp", "/map", "address", "s")
        redirectParameter("/map.jsp", "/map", "address", "s")

        redirect("/gidea/games/crosswordSolver.jsp", "/games/crosswordSolver")

        redirectAll("/pinkwino", "/wiki")
        redirectAll("/iwiki", "/wiki")
        redirectAll("/gidea/images", "/images")
        redirectAll("/gidea/music", "/music")
    }
}


/**
 * Creates a Storage. This used to be more complex.
 * (Involving the BridgingStorage and LegacyStorage).
 * But now it creates a simple [FileStorage].
 */
fun createWikiNamespace(documentDir: File, name: String, directoryName: String = name): Namespace {
    val storage = FileStorage(File(File(documentDir, "namespaces"), directoryName))
    val showBackLinks = name == "ingredient" || name == "category"
    return Namespace(name, storage, showBackLinks, isHidden = name == "category").apply {
        if (name == "recipe") {
            hiddenTitles.addAll(listOf("Ingredients", "Recipes"))
        }
    }
}

