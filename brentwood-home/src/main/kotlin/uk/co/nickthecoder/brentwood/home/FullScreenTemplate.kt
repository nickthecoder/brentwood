package uk.co.nickthecoder.brentwood.home

import io.ktor.html.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.layout.PageTemplate


open class FullScreenTemplate() : PageTemplate() {

    override fun HTML.apply() {

        head {
            title { insert(pageTitle) }
            meta(name = "viewport", content = "width=device-width, initial-scale=1")
            link(rel = "stylesheet", type = "text/css", href = "/style/style.css")
            link(rel = "icon", href = "/style/icon.png")
            insert(extraHeader)
        }

        body("fullScreen") {
            insert(preContent)
            div("content") {
                insert(pageTools)
                insert(content)
            }
            insert(postContent)
        }
    }

}
