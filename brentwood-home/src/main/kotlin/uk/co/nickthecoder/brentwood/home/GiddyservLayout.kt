package uk.co.nickthecoder.brentwood.home

import kotlinx.html.DIV
import kotlinx.html.div
import kotlinx.html.id

open class GiddyservLayout : NickTheCoderLayout() {

    override fun createTemplate(fullWidth: Boolean, uri: String) =
        NickTheCoderTemplate(this, fullWidth, isPrivate = true, isMusicOnly = true, uri)

    override fun DIV.tabs(uri: String) {
        div {
            id = "tabs"

            tab(uri, "Wiki", "/wiki/view/Home", "w", Regex("/wiki/view/[^/]*"))
            tab(uri, "Photos", "/images/photos", "p", Regex("/images/photos.*"))
            tab(uri, "Music", "/music", "m", Regex("/music.*"))
            tab(uri, "Player", "/player", "y", Regex("/player.*"))
            tab(uri, "Software", "/software", null, Regex("/software.*"))
            tab(uri, "Games", "/wiki/view/game/Games", null, Regex("/games.*"), Regex("/wiki/view/game/.*"))
            tab(uri, "3D", "/3d", null, Regex("/3d.*"))
            tab(uri, "Recipes", "/wiki/view/recipe/Recipes", "r", Regex("/wiki/view/(recipe|ingredient)/.*"))
            tab(uri, "Garden", "/wiki/view/garden/Garden", "g", Regex("/wiki/view/garden/.*"))
        }
    }

}
