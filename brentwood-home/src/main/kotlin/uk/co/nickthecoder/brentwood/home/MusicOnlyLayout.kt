import kotlinx.html.DIV
import kotlinx.html.div
import kotlinx.html.id
import uk.co.nickthecoder.brentwood.home.NickTheCoderLayout
import uk.co.nickthecoder.brentwood.home.NickTheCoderTemplate
import uk.co.nickthecoder.brentwood.home.tab

open class MusicOnlyLayout : NickTheCoderLayout() {

    override fun createTemplate(fullWidth: Boolean, uri: String) =
        NickTheCoderTemplate(this, fullWidth, isPrivate = false, isMusicOnly = true, uri)

    override fun DIV.tabs(uri: String) {
        div {
            id = "tabs"

            tab(uri, "Music", "/music", "m", Regex("/music.*"))
            tab(uri, "Player", "/player", "y", Regex("/player.*"))

        }
    }

}
