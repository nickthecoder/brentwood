package uk.co.nickthecoder.brentwood.home

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.util.*
import org.jetbrains.exposed.sql.Database
import uk.co.nickthecoder.brentwood.CompoundPageListener
import uk.co.nickthecoder.brentwood.familyalbum.model.createFamilyAlbumDB
import uk.co.nickthecoder.brentwood.images.Images
import uk.co.nickthecoder.brentwood.images.createImagesDB
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.player.model.createPlayerDB
import uk.co.nickthecoder.brentwood.search.Search
import uk.co.nickthecoder.brentwood.util.prettyPrint
import uk.co.nickthecoder.brentwood.util.toHex
import uk.co.nickthecoder.brentwood.wiki.Wiki
import java.io.File
import java.io.IOException
import java.sql.DriverManager
import kotlin.system.exitProcess

/**
 * This is the main entry point for my website.
 * There are no configuration files. Everything is hard coded.
 * So if you want to use this for your own site, then create a similar class and main entry point,
 * and add the routing for your particular site.
 * You will also want to create a new implementation of [Layout].
 *
 * This creates two very similar, websites.
 *
 * * "nickthecoder.co.uk" is the external, publicly available site. See Public.kt
 * * "giddyserv" is the private site only available on my intranet. See Private.kt
 *
 * The aspects which both sites share are in Common.kt.
 */
object NickTheCoder {

    /**
     * The main entry point to start my website.
     * Parses command line args
     */
    @JvmStatic
    fun main(vararg argv: String) {

        var endedFlags = false
        var paramName = ""
        var uid = -1
        var gid = -1
        var isDevelopment = false
        var isMusicOnly = false
        var createDB = false
        var port = 80

        fun unnamedParameter(value: String) {
            if (value == "--" && !endedFlags) {
                endedFlags = true
            } else {
                error("Unexpected parameter : $value")
            }
        }

        for (arg in argv) {
            if (endedFlags) {
                unnamedParameter(arg)
            } else {
                when (arg) {
                    "--compact" -> prettyPrint = false
                    "--createDB" -> createDB = true
                    "--development" -> isDevelopment = true
                    "--musicOnly" -> {
                        isMusicOnly = true
                        isDevelopment = true
                    }

                    "--port" -> paramName = "port"
                    "--uid" -> paramName = "uid"
                    "--gid" -> paramName = "gid"
                    "--help", "-h" -> {
                        println(USAGE)
                        exitProcess(0)
                    }

                    else -> {
                        when (paramName) {
                            "port" -> port = arg.toInt()
                            "uid" -> uid = arg.toInt()
                            "gid" -> gid = arg.toInt()
                            else -> {
                                unnamedParameter(paramName)
                            }
                        }
                        paramName = ""
                    }
                }
            }
        }

        /**
         * The directory which contains the SQLite database files, the lucene indices and the wiki namespaces.
         * The later two are subdirectories.
         * This directory must be WRITABLE by the user account running the web server, because SQLite
         * needs to create files in it.
         */
        val documentDir = if (isDevelopment) {
            File("/home/nick/tmp/nickthecoder")
        } else {
            File("/gidea/documents/nickthecoder.co.uk/")
        }

        if (createDB) {

            println("Creating database files (if they don't already exist")
            dropPrivileges(uid, gid)
            createDatabases(documentDir)

        } else {
            createServer(port, documentDir, isDevelopment, isMusicOnly).start(false)
            dropPrivileges(uid, gid)

            if (Privileges.getuid() == 0 || Privileges.getgid() == 0) {
                System.err.println("Running as root is not allowed. Aborting.")
                exitProcess(1)
            }
            if (isDevelopment) {
                println("Development servers : http://fakepublic:$port/ and http://fakeprivate:$port/")
            }
        }
    }
}

private fun createDatabases(documentDir: File) {

    if (!documentDir.exists()) {
        throw IOException("Document directory does not exist : $documentDir")
    }

    val familyAlbumFile = File(documentDir, "familyAlbum.db")
    val imagesDB = File(documentDir, "images.db")
    val playerDB = File(documentDir, "player.db")

    if (!familyAlbumFile.exists()) {
        println("Creating $familyAlbumFile")
        createFamilyAlbumDB(familyAlbumFile)
    }
    if (!imagesDB.exists()) {
        println("Creating $imagesDB")
        createImagesDB(imagesDB)
    }
    if (!playerDB.exists()) {
        println("Creating $playerDB")
        createPlayerDB(playerDB)
    }

}

private fun createServer(
    port: Int,
    documentDir: File,
    isDevelopment: Boolean,
    isMusicOnly: Boolean
): ApplicationEngine {

    val authMap = readPasswordFile(File(documentDir, "users.txt"))
    val hashingAlgorithm = getDigestFunction("SHA-256") { "ntc${it.length}" }

    return embeddedServer(Netty, port) {

        install(DefaultHeaders)
        install(ContentNegotiation)
        install(CachingHeaders) {

            val days: Int = 60 * 60 * 24 // Number of seconds in a day.

            options { outgoingContent ->
                when (val ct = outgoingContent.contentType) {
                    null -> null

                    ContentType.Text.JavaScript -> CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 7 * days))
                    ContentType.Text.CSS -> CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 7 * days))

                    else -> {
                        if (ct.match(ContentType.Image.Any) || ct.match(ContentType.Audio.Any)) {
                            CachingOptions(CacheControl.MaxAge(maxAgeSeconds = 7 * days))
                        } else {
                            null
                        }
                    }
                }
            }
        }
        install(IgnoreTrailingSlash)
        install(Compression)
        install(Authentication) {
            basic("auth-basic") {
                realm = "nickthecoder.co.uk"
                validate { credentials ->
                    if (authMap[credentials.name] == hashingAlgorithm.invoke(credentials.password).toHex()) {
                        UserIdPrincipal(credentials.name)
                    } else {
                        null
                    }
                }
            }
        }

        routing {

            /**
             * The [Images] component is common to both sites, sharing the same database.
             */
            val imagesDBFile = File(documentDir, "images.db")
            val imagesDB = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$imagesDBFile") })

            /**
             * When a wiki page is edited, we need to tell the [Search] spider to re-index that page.
             * The private and public sites share many wiki namespaces, but have different [Search]
             * instances.
             * So we both sites add a SearchPageListener, and therefore changes via either [Wiki]
             * instance will cause BOTH [Search] spiders to update their index.
             */
            val pageListener = CompoundPageListener()

            if (isMusicOnly) {
                routeMusic(documentDir)
            } else {
                routePublic(documentDir, isDevelopment, port, pageListener, imagesDB)
                routePrivate(documentDir, isDevelopment, port, pageListener, imagesDB)
            }
        }

    }
}

private fun dropPrivileges(uid: Int, gid: Int) {
    if (gid > 0) {
        Privileges.setgid(gid)
    }
    if (uid > 0) {
        Privileges.setuid(uid)
    }
}

/**
 * Reads a file where each line consists of a username space password_hash.
 * The password_hash should be the users password hashed using SHA256, with a salt
 * of : "ntc${password.length}"
 *
 * So the hash of "password" is '4e00df21bd781f1f9876e53fefae13694f088bd2b4541e079a0bfc26326fec7b'
 *
 * To generate a hash from the command line :
 *
 *      echo -n 'ntcMY_PASSWORD_LENGTH'MY_PASSWORD' | sha256sum
 */
private fun readPasswordFile(file: File): Map<String, String> {
    val map = mutableMapOf<String, String>()
    for (line in file.readLines()) {
        if (line.startsWith("#") || line.startsWith("//")) {
            continue
        }
        val trimmed = line.trim()
        val space = trimmed.indexOf(' ')
        if (space > 0) {
            map[line.substring(0, space).trim()] = line.substring(space).trim()
        }
    }
    return map
}


private const val USAGE = """
Usage:
    brentwood --help            Shows this message
    brentwood --createDB        Creates empty databases (if they don't already exist)
    brentwood [OPTIONS]...      Starts the web server

    Options :
    
        --port              The default port is 8282 (so that it can be run easily without root privileges).
        --uid  N            After the webserver has been created, drop back user privileges
        --gid  N            After the webserver has been created, drop back group privileges
        --development       uses different config values suitable for development
        --musicOnly         
        --compact           Omits unnecessary whitespace in the HTML

"""



