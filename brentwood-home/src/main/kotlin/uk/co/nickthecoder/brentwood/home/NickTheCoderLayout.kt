package uk.co.nickthecoder.brentwood.home

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.*

open class NickTheCoderLayout : Layout {

    override fun createTemplate(fullWidth: Boolean, uri: String) =
        NickTheCoderTemplate(this, fullWidth, isPrivate = false, isMusicOnly = false, uri)

    override fun createFullScreenTemplate() = FullScreenTemplate()

    open fun DIV.tabs(uri: String) {
        div {
            id = "tabs"

            tab(uri, "Wiki", "/wiki/view/Home", "w", Regex("/wiki/view/[^/]*"))
            tab(uri, "Photos", "/images/photos", "p", Regex("/images.*"))
            tab(uri, "Family Album", "/familyalbum", "f", Regex("/familyalbum.*"))
            tab(uri, "Software", "/software", null, Regex("/software.*"))
            tab(uri, "Games", "/wiki/view/game/Games", null, Regex("/games.*"), Regex("/wiki/view/game/.*"))
            tab(uri, "3D", "/3d", null, Regex("/3d.*"))
            tab(uri, "Recipes", "/wiki/view/recipe/Recipes", "r", Regex("/wiki/view/(recipe|ingredient)/.*"))
            tab(uri, "Garden", "/wiki/view/garden/Garden", "g", Regex("/wiki/view/garden/.*"))
        }
    }

    override fun groupByInitials(parent: FlowContent, groupBy: GroupBy<*>) {
        parent.div("noSearch groupByInitials") {
            ul("infoList") {
                for (initial in groupBy.allInitials()) {
                    li {
                        if (groupBy.isUsed(initial)) {
                            a("#groupBy_$initial") {
                                id = "groupByInitial_$initial"
                                +"$initial"
                            }
                        } else {
                            +"$initial"
                        }
                    }
                    +" "
                }
            }
            inlineScript(GROUP_BY_INITIALS_JS)
        }
    }


    override fun pager(parent: FlowContent, pager: Pager<*>, pageNumber: Int) {

        if (pager.pageCount() > 1) {
            parent.div("pager") {

                if (pageNumber > 1) {
                    a(pager.href.addParameter("${pager.pageParameter}=${pageNumber - 1}"), classes = "prev") {
                        accessKey = ","
                        +"Previous Page"
                    }
                    +" "
                }

                for (n in 1..pager.pageCount()) {
                    if (n == pageNumber) {
                        span("currentPage") { +"$n" }
                    } else {
                        a(pager.href.addParameter("${pager.pageParameter}=$n")) { +"$n" }
                    }
                    +" "
                }

                if (pageNumber < pager.pageCount()) {
                    a(pager.href.addParameter("${pager.pageParameter}=${pageNumber + 1}"), classes = "next") {
                        accessKey = "."
                        +"Next Page"
                    }
                }

                inlineScript(PAGER_SHORTCUTS_JS)

            }
        }

    }

    override fun groupByAnchor(parent: FlowContent, groupBy: GroupBy<*>, initial: Char) {
        parent.h2 {
            id = "groupBy_$initial"
            +"$initial"
        }
    }

}


internal fun FlowContent.tab(
    uri: String,
    name: String,
    href: String,
    shortcut: String? = null,
    vararg matching: Regex
) {
    var matches = false
    for (m in matching) {
        if (m.matches(uri)) {
            matches = true
            break
        }
    }
    div(if (matches) "tab tabSelected" else "tab") {

        a(href = href) {
            if (shortcut != null) {
                accessKey = shortcut
            }
            +name
        }
    }
}

/**
 * Adds keyboard shorts for the left and right arrow keys.
 */
private val PAGER_SHORTCUTS_JS = """
function pagerFollowLink( className ) {
    const pagers = document.getElementsByClassName( "pager" );
    for ( let p = 0; p < pagers.length; p ++ ) {
        const links = pagers[p].getElementsByClassName( className );
        for (let l = 0; l < links.length; l++) {
            window.location = links[l].href;
            return
        }
    }
}
document.addEventListener('keydown', (e) => {
    if (e.ctrlKey || e.altKey || e.shiftKey) return;
    const focus = document.activeElement;
    if ( focus == null || ! (focus instanceof HTMLInputElement) ) {
        if (e.key=="ArrowLeft") {
            pagerFollowLink( "prev" )
        }
        if (e.key=="ArrowRight") {
            pagerFollowLink( "next" )
        }
    }
}, false);

""".shrinkJavascript()

private val GROUP_BY_INITIALS_JS = """
document.addEventListener('keydown', (e) => {
    if (e.ctrlKey || e.altKey || e.shiftKey) return;
    const focus = document.activeElement;
    const key = e.key.toUpperCase();
    if ( focus == null || ! (focus instanceof HTMLInputElement) ) {
        const groupBys = document.getElementsByClassName( "groupByInitials" );
        for ( let g = 0; g < groupBys.length; g ++ ) {
            const links = groupBys[g].getElementsByTagName( "a" );
            for ( let i  = 0; i < links.length; i ++ ) {
                const link = links[i];
                if (link.innerHTML == key) {
                    window.location = link.href;
                    return;
                }
            }
        }
    }
}, false);
""".shrinkJavascript()
