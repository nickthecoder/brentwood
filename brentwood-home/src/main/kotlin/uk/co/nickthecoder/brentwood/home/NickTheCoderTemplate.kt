package uk.co.nickthecoder.brentwood.home

import io.ktor.html.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.layout.PageTemplate
import uk.co.nickthecoder.brentwood.util.escapeURLParam
import uk.co.nickthecoder.brentwood.util.script

open class NickTheCoderTemplate(

    val layout: NickTheCoderLayout,
    val fullWidth: Boolean,
    val isPrivate: Boolean,
    val isMusicOnly: Boolean,
    val uri: String

) :
    PageTemplate() {

    private fun ntcURI(): String {
        val colon = uri.indexOf(':')
        if (colon < 0) return "http://nickthecoder.co.uk$uri"
        val slash = uri.indexOf('/', colon)
        if (slash < 0) return uri
        return "http://nickthecoder.co.uk" + uri.substring(slash)
    }

    override fun HTML.apply() {

        head {
            title { insert(pageTitle) }
            meta(name = "viewport", content = "width=device-width, initial-scale=1")
            link(rel = "stylesheet", type = "text/css", href = "/style/style.css")
            link(rel = "icon", href = "/style/icon.png")

            insert(extraHeader)
        }

        body(if (isPrivate) "private" else "public") {
            script("/resources/ntc.js")

            a(href = "#") {
                id = "normalView"
                title = "Exit Plain View (Alt+F11)"
                +"Exit Full View"
            }
            div {
                id = "whole"
                div {
                    id = "header"
                    if (isPrivate && !isMusicOnly) {
                        div("serverName") {
                            a(ntcURI()) { +"ntc" }
                            +" "
                            a("/search/info?url=${escapeURLParam(uri)}") { +"(i)" }
                        }
                    }
                    a(href = "#") {
                        id = "plainView"
                        title = "Remove Clutter (Alt+F11)"
                        +"Full View"
                    }
                    if (!isMusicOnly) {
                        form(method = FormMethod.get, action = "/search") {
                            id = "search"
                            input(type = InputType.text, name = "q") {
                                id = "searchText"
                                size = "15"
                                placeholder = "search"
                                title = "Search (shortcut S)"
                            }
                            input(type = InputType.submit) {
                                id = "searchSubmit"
                                value = "»"
                                accessKey = "s"
                            }
                        }
                    }

                    with(layout) {
                        tabs(uri)
                    }

                    div {
                        id = "belowTabs"
                    }
                }

                div {
                    id = "main"

                    div {
                        id = "preContent"
                        insert(preContent)
                    }

                    if (fullWidth) {
                        div {
                            id = "full"

                            div {
                                id = "content"

                                div("noSearch tools") {
                                    insert(pageTools)
                                }
                                h1 {
                                    insert(pageHeading)
                                }
                                insert(content)
                            }
                        }
                    } else {

                        div {
                            id = "columns"

                            div {
                                id = "content"

                                div("noSearch tools") {
                                    insert(pageTools)
                                }
                                h1 {
                                    insert(pageHeading)
                                }
                                insert(content)
                            }

                            div {
                                id = "navigation"
                                insert(navigation)
                            }

                        }
                    }

                    div {
                        id = "belowColumns"
                        insert(postContent)
                    }
                }

                div {
                    id = "footer"
                    +"© "
                    a(href = "http://nickthecoder.co.uk") { +"nickthecoder.co.uk" }
                }

            }
        }
    }

}
