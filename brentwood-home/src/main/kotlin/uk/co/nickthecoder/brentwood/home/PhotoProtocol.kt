package uk.co.nickthecoder.brentwood.home

import uk.co.nickthecoder.brentwood.wiki.NonWikiLinkProtocol

/**
 * We can link to "quick" or "thumbnail" versions of photo, without having to know its actual url.
 */
class PhotoProtocol(val base: String, val quickOrThumbnails: String) : NonWikiLinkProtocol {

    override fun url(from: String): String {
        val postColon = postColon(from)
        val lastSlash = postColon.lastIndexOf("/")

        return if (lastSlash >= 0) {
            val dir =
                if (postColon.startsWith("/")) postColon.substring(1, lastSlash) else postColon.substring(0, lastSlash)
            val name = postColon.substring(lastSlash + 1)
            "${base}/${dir}/$quickOrThumbnails/$name"

        } else {

            if (postColon.startsWith('/')) {
                "${base}$postColon"
            } else {
                "${base}/$postColon"
            }
        }
    }
}
