package uk.co.nickthecoder.brentwood.home

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.Database
import uk.co.nickthecoder.brentwood.CompoundPageListener
import uk.co.nickthecoder.brentwood.component
import uk.co.nickthecoder.brentwood.images.Images
import uk.co.nickthecoder.brentwood.layout.layout
import uk.co.nickthecoder.brentwood.music.Music
import uk.co.nickthecoder.brentwood.player.Player
import uk.co.nickthecoder.brentwood.player.SoxBackend
import uk.co.nickthecoder.brentwood.search.*
import uk.co.nickthecoder.brentwood.wiki.*
import java.io.File
import java.net.URL
import java.sql.DriverManager

fun Route.routePrivate(
    documentDir: File,
    isDevelopment: Boolean,
    port: Int,
    pageListener: CompoundPageListener,
    imagesDB: Database
) {
    val hostName = if (isDevelopment) "fakeprivate" else "giddyserv"
    val ipAddress = if (isDevelopment) "127.0.0.0" else "192.168.1.2"
    val root = "http://$hostName${if (port == 80)"" else ":$port"}"
    val layout = GiddyservLayout()

    val playerDBFile = File(documentDir, "player.db")
    val playerDB = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$playerDBFile") })
    val playerPaths = if (isDevelopment) {
        listOf(File("/home/nick/music/ogg/default"))
    } else {
        listOf(File("/gidea/music/categories/default"))
    }

    // Search component
    val searchConfig = SearchConfiguration(
        dir = File(documentDir, "giddyservSearch"),
        origins = listOf(URL("$root/")),
        urlFilter = HostFilter(hostName) and !searchFilter,
        categories = listOf(
            Category("music", "Music", RegexURLPathFilter("/music/.*")),
            Category("images", "Images", RegexURLPathFilter("/images/.*")),
            Category("wiki", "Wiki", RegexURLPathFilter("/wiki/.*")),
            Category("family", "Family Album", RegexURLPathFilter("/familyalbum/.*"))

        ),
        contentID = listOf("content"),
        ignoredIDs = emptyList(),
        ignoredCssClass = "noSearch",
        resultsPerPage = 20
    )
    val search = Search(layout, "/search", searchConfig)
    pageListener.children.add(SearchPageListener(search, root))

    // Wiki component
    val wikiEngine =
        WikiEngine(
            createWikiNamespace(documentDir, "default", "default_private"),
            DefaultURLFactory(root, "/wiki"),
            layout
        ).apply {
            for (name in listOf("common", "nick", "nalin", "linux", "garden", "ingredient", "recipe", "category", "game")) {
                addNamespace(createWikiNamespace(documentDir, name))
            }
            textExtensions.addAll(listOf("osf", "json", "plan"))
            protocolHandlers["local"] = LocalProtocol()
            protocolHandlers["ntc"] = AliasProtocol("http://nickthecoder.co.uk")
            protocolHandlers["photo"] = AliasProtocol("/images/photos/media")
            protocolHandlers["quick"] = PhotoProtocol("/images/photos/media", ".quick")
            protocolHandlers["thumb"] = PhotoProtocol("/images/photos/media", ".thumbnails")
            cheatSheet = PageName(defaultNamespace, "Cheat Sheet")
        }
    addCustomWikiSyntax(wikiEngine, search)

    // Images component
    // Note, the public site *also* has an Images component, but from /gidea/images/photos,,
    // instead of all of /gidea/images.
    // However, the public version uses a webPath of /images/photos, so the urls are equal (after the hostname).
    val images = Images(
        layout, "/images", File("/gidea/images"),
        thumbnailScaler,
        quickScaler,
        imagesDB
    )

    // Player component
    val playerLuceneDir = File(documentDir, "playerSearch")

    // Music component
    val musicPath = File(if (isDevelopment) "/home/nick/music/ogg" else "/gidea/music/categories")
    val music = Music(layout, "/music", musicPath)

    val player = Player(layout, "/player", playerPaths, playerDB, playerLuceneDir, SoxBackend())

    val wiki = Wiki(layout, "/wiki", wikiEngine, pageListener, search)

    host(listOf(hostName, ipAddress)) {
        get("/") { call.respondRedirect("/wiki/view/Home") }
        routeCommon(layout)

        layout(layout)
        component(search)
        component(images)
        component(music)
        component(player)
        component(wiki)
    }

}

