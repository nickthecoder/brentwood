package uk.co.nickthecoder.brentwood.home

import com.sun.jna.Library
import com.sun.jna.Native

object Privileges {

    fun getuid() = CLibrary.INSTANCE.getuid()
    fun getgid() = CLibrary.INSTANCE.getgid()

    fun setgid(gid: Int) {
        if (CLibrary.INSTANCE.setgid(gid) != 0) {
            throw Exception("Failed to set gid to $gid")
        }
    }

    fun setuid(uid: Int) {
        if (CLibrary.INSTANCE.setuid(uid) != 0) {
            throw Exception("Failed to set gid to $uid")
        }
    }

    interface CLibrary : Library {
        fun getuid(): Int
        fun getgid(): Int

        fun setgid(gid: Int): Int
        fun setuid(uid: Int): Int

        companion object {
            val INSTANCE = Native.load("c", CLibrary::class.java) as CLibrary
        }
    }
}
