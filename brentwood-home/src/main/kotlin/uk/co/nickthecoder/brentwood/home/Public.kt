package uk.co.nickthecoder.brentwood.home

import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.Database
import uk.co.nickthecoder.brentwood.CompoundPageListener
import uk.co.nickthecoder.brentwood.component
import uk.co.nickthecoder.brentwood.familyalbum.FamilyAlbum
import uk.co.nickthecoder.brentwood.files.Files
import uk.co.nickthecoder.brentwood.files.FilesConfig
import uk.co.nickthecoder.brentwood.images.Images
import uk.co.nickthecoder.brentwood.layout.layout
import uk.co.nickthecoder.brentwood.search.*
import uk.co.nickthecoder.brentwood.software.Software
import uk.co.nickthecoder.brentwood.software.SoftwareCategory
import uk.co.nickthecoder.brentwood.software.SoftwareConfiguration
import uk.co.nickthecoder.brentwood.software.SoftwareProject
import uk.co.nickthecoder.brentwood.util.staticResources
import uk.co.nickthecoder.brentwood.wiki.*
import java.io.File
import java.net.URL
import java.sql.DriverManager


fun Route.routePublic(
    documentDir: File,
    isDevelopment: Boolean,
    port: Int,
    pageListener: CompoundPageListener,
    imagesDB: Database
) {

    val hostName = if (isDevelopment) "fakepublic" else "nickthecoder.co.uk"
    val root = "http://$hostName${if (port == 80) "" else ":$port"}"
    val layout = NickTheCoderLayout()

    val familyAlbumDBFile = File(documentDir, "familyAlbum.db")
    val familyAlbumDB = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$familyAlbumDBFile") })
    val familyAlbumDir = File(documentDir, "Family Album")

    // Search component
    val searchConfig = SearchConfiguration(
        dir = File(documentDir, "nickthecoderSearch"),
        origins = listOf(URL("$root/")),
        urlFilter = HostFilter(hostName) and !searchFilter,
        categories = listOf(
            Category("music", "Music", RegexURLPathFilter("/music/.*")),
            Category("images", "Images", RegexURLPathFilter("/images/.*")),
            Category("wiki", "Wiki", RegexURLPathFilter("/wiki/.*")),
            Category("family", "Family Album", RegexURLPathFilter("/familyalbum/.*"))
        ),
        contentID = listOf("content"),
        ignoredIDs = emptyList(),
        ignoredCssClass = "noSearch",
        resultsPerPage = 20
    )

    val search = Search(layout, "/search", searchConfig)
    pageListener.children.add(SearchPageListener(search, root))

    // Wiki component
    val wikiEngine = WikiEngine(
        createWikiNamespace(documentDir, "default", "default_public"),
        DefaultURLFactory(root, "/wiki"), layout
    ).apply {

        for (name in listOf(
            "common", "garden", "ingredient", "recipe", "category", "game"
        )) {
            addNamespace(createWikiNamespace(documentDir, name))
        }
        textExtensions.addAll(listOf("osf", "json", "plan"))
        protocolHandlers["local"] = LocalProtocol()
        protocolHandlers["ntc"] = AliasProtocol("http://nickthecoder.co.uk")
        protocolHandlers["photo"] = AliasProtocol("/images/media/photos")
        protocolHandlers["quick"] = PhotoProtocol("/images/media/photos", ".quick")
        protocolHandlers["thumb"] = PhotoProtocol("/images/media/photos", ".thumbnails")

        cheatSheet = PageName(defaultNamespace, "Cheat Sheet")
    }
    addCustomWikiSyntax(wikiEngine, search)

    // Images component
    // Note, the private site *also* has an Images, but the whole of /gidea/images is available,
    // instead of just /gidea/images/photos.
    // However, by using a webPath of /images/photos, the common parts have the same url (after the hostname).
    val images = Images(
        layout, "/images/photos", File("/gidea/images/photos"),
        thumbnailScaler,
        quickScaler,
        imagesDB,
        listOf("auth-basic")
    )

    val wiki = Wiki(layout, "/wiki", wikiEngine, pageListener, search, listOf("auth-basic"))

    val familyAlbum =
        FamilyAlbum(layout, "/familyalbum", familyAlbumDir, familyAlbumDB, SearchPageListener(search, root))
    val files = Files(layout, FilesConfig("/public", File("/gidea/documents/public"), prefix = "Public"))
    val protectedFiles =
        Files(layout, FilesConfig("/protected", File("/gidea/documents/protected"), prefix = "Protected"))


    host(listOf(hostName)) {
        get("/") { call.respondRedirect("/wiki/view/Home") }
        get("/robots.txt") { call.respondText(ROBOTS) }
        routeCommon(layout)

        layout(layout)
        component(search)
        component(images)
        component(wiki)
        component(familyAlbum)
        component(files)
        authenticate("auth-basic") { component(protectedFiles) }
    }
}

private const val ROBOTS = """
User-agent: *
Disallow: /search
Disallow: /search/
Disallow: /music/
Disallow: /public/
Disallow: /private/
Disallow: /wiki/info/
Disallow: /wiki/slides/
Disallow: /wiki/edit/
Disallow: /wiki/delete/
Disallow: /wiki/rename/
Disallow: /wiki/raw/
Disallow: /images/people/
Disallow: /images/.people/
Disallow: /images/textures
"""
