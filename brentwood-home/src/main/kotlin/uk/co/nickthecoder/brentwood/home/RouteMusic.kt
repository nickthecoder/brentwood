package uk.co.nickthecoder.brentwood.home

import MusicOnlyLayout
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.Database
import uk.co.nickthecoder.brentwood.CompoundPageListener
import uk.co.nickthecoder.brentwood.component
import uk.co.nickthecoder.brentwood.layout.layout
import uk.co.nickthecoder.brentwood.music.Music
import uk.co.nickthecoder.brentwood.player.Player
import uk.co.nickthecoder.brentwood.player.SoxBackend
import uk.co.nickthecoder.brentwood.search.Category
import uk.co.nickthecoder.brentwood.search.HostFilter
import uk.co.nickthecoder.brentwood.search.RegexURLPathFilter
import uk.co.nickthecoder.brentwood.search.SearchConfiguration
import uk.co.nickthecoder.brentwood.wiki.AliasProtocol
import uk.co.nickthecoder.brentwood.wiki.DefaultURLFactory
import uk.co.nickthecoder.brentwood.wiki.LocalProtocol
import uk.co.nickthecoder.brentwood.wiki.WikiEngine
import java.io.File
import java.net.URL
import java.sql.DriverManager

/**
Creates a website with only the "music" component.
 */
fun Route.routeMusic(
    documentDir: File
) {
    val hostName = "localhost"
    val ipAddress = "127.0.0.0"
    val layout = MusicOnlyLayout()

    val playerDBFile = File(documentDir, "player.db")
    val playerDB = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$playerDBFile") })
    val playerPaths = listOf(File("/home/nick/music/ogg/default"))

    // Player component
    val playerLuceneDir = File(documentDir, "playerSearch")

    // Music component
    val musicPath = File("/home/nick/music/ogg")

    host(listOf(hostName, ipAddress)) {
        get("/") { call.respondRedirect("/music") }
        routeCommon(layout)

        layout(layout)
        component(Music(layout, "/music", musicPath))
        component(Player(layout, "/player", playerPaths, playerDB, playerLuceneDir, SoxBackend()))
    }

}

