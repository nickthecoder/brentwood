package uk.co.nickthecoder.brentwood.home.games

import io.ktor.application.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.*

class Games(layout: Layout, webPath: String = "/games") : LayoutComponent(layout, webPath) {

    override fun route(route: Route) {

        route.staticResources("/resources", this@Games.javaClass.packageName)

        route.get("/crosswordSolver") { crosswordSolver(call) }

        route.get("/crosswordSolver.jsp") {
            val text = call.parameters["text"]
            if (text == null) {
                call.respondRedirect("/crosswordSolver")
            } else {
                call.respondRedirect("/crosswordSolver?text=$text")
            }
        }

        route.get("/slidingBlocks") {
            slidingBlocksList(call)
        }

        route.get("/slidingBlocks/{puzzleID}") {
            val puzzleID = call.requiredStringParameter("puzzleID")
            slidingBlocksPuzzle(call, puzzleID)
        }


    }

    private val puzzleNames = mapOf(
        "123" to "123",
        "blockTen" to "Block Ten",
        "boyAndDog" to "Boy and Dog",
        "climb12" to "Climb 12",
        "cow" to "Cow",
        "egg" to "Egg",
        "manAndWoman" to "Man and Woman",
        "panda" to "Panda"
    )

    private suspend fun slidingBlocksList(call: ApplicationCall) {
        val uri = call.request.uri

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            pageTitle { +"Sliding Blocks Puzzles" }
            pageHeading { +"Sliding Block Puzzles" }

            content {
                layout.thumbnails(this) {
                    for ((puzzleID, title) in puzzleNames) {
                        layout.thumbnail(this, {
                            a("$webPath/slidingBlocks/$puzzleID") {
                                img(title, "$webPath/resources/slidingBlocks/puzzles/$puzzleID/small.png")
                            }
                        }) {
                            a("$webPath/slidingBlocks/$puzzleID") { +title }
                        }
                    }
                }
            }
        }
    }


    private suspend fun slidingBlocksPuzzle(call: ApplicationCall, puzzleID: String) {
        val uri = call.request.uri
        val puzzlePath = "$webPath/resources/slidingBlocks/puzzles/$puzzleID"

        val title = puzzleNames[puzzleID] ?: "?"

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            pageTitle { +title }
            pageHeading { +title }

            extraHeader {
                script("text/javascript", "$webPath/resources/slidingBlocks/code.js") {}
                script("text/javascript", "$puzzlePath/data.js") {}
            }

            pageTools {

                layout.textTool(this, "#") {
                    onClick = "return board.controller.undo();"
                    +"Undo"
                }
                layout.textTool(this, "#") {
                    onClick = "return board.controller.redo();"
                    +"Redo"
                }
                layout.textTool(this, "#") {
                    onClick = "return board.controller.cheat( window.solution );"
                    +"Cheat"
                }
            }

            content {
                inlineScript(
                    """
                    // Begin
                    window.addEventListener( "load", function() {
                        board.generate();
                        board.positionPieces();
                        document.body.onmouseup = function(event) { board.controller.mouseUp(event) };
                    } );
                    // End
                    """.shrinkJavascript()
                )

                div {
                    id = "playArea"
                    div {
                        id = "board"
                        onMouseMove = "return board.controller.mouseMove( event );"
                        onMouseDown = "return board.controller.mouseDown( event );"
                        //onMouseUp is applied to document.body
                    }
                }

                div("instructions") {
                    h2 { + "Instructions" }
                    p {
                        + "Drag the pieces above with the mouse."
                        + "The aim is to make the board appear as shown below."
                    }
                    div("center") {
                        a("$puzzlePath/full.png") {
                            img("Completed", "$puzzlePath/small.png")
                            br()
                            +"Click to enlarge"
                        }
                    }
                }
                div("clear")
                div { id = "moves" }

            }
        }
    }

    private suspend fun crosswordSolver(call: ApplicationCall) {

        val uri = call.request.uri
        val text = call.parameters["text"]
        val solutions = if (!text.isNullOrBlank()) {
            solveCrossword(text)
        } else {
            emptyList()
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {
            pageTitle { +"Crossword Solver" }
            pageHeading { +"Crossword Solver" }
            content {

                p { +"Use full-stops (periods) in place of unknown letters" }
                form("crosswordSolver", method = FormMethod.get) {
                    input(InputType.text, name = "text") {
                        autoFocus = true
                        value = text ?: ""
                    }
                    input(InputType.button, name = "go") { value = "Go" }
                }
                p {
                    +"For example \"pu..le\" will find the words : "
                    i { +"puzzle" }
                    +", "
                    i { +"purple" }
                    +" and "
                    i { +"puddle." }
                }

                if (!text.isNullOrBlank()) {
                    h2 { +"Results" }
                    div("crosswordSolverResults") {
                        for (word in solutions) {
                            // We used "split" which adds a blank item at the end of the list, ignore it!
                            if (word.isNotBlank()) {
                                a("https://en.wiktionary.org/w/index.php?title=Special%3ASearch&search=$word") { +word }
                                br()
                            }
                        }
                    }
                }

            }
        }
    }

    private fun solveCrossword(text: String): List<String> {
        text.map { if (it.isLetter()) it else '.' }

        return commandToString(grepCommand, "^$text$", wordListFile).split("\n")
    }

    var grepCommand = "grep"
    var wordListFile = "/gidea/documents/wordlist-threeOrMore.txt"
}
