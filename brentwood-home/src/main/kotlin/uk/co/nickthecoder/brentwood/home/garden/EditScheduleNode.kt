package uk.co.nickthecoder.brentwood.home.garden

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.util.inlineScript
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine
import uk.co.nickthecoder.brentwood.wiki.tree.SimpleParentNode

class EditScheduleNode(

    line: Int,
    column: Int,
    private val prefix: String,
    private val suffix: String,
    private val myPageName: PageName,
    private val schedulePageName: PageName

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun isPlainTextContent() = true

    override fun render(parent: FlowContent, engine: WikiEngine) {

        // Include the schedule (without anything filled in).
        val node = engine.parser.parse(schedulePageName, schedulePageName.load()).rootNode
        node.render(parent, engine)

        parent.form(
            method = FormMethod.post,
            encType = FormEncType.multipartFormData,
            action = (engine.urlFactory).edit(myPageName)
        ) {

            input(type = InputType.hidden, name = "source") {
                id = "scheduleSource"
            }
            input(type = InputType.submit, name = "save") {
                onClick = "saveSchedule();"
                value = "Save Schedule"
            }
        }

        val data = children.firstOrNull()?.text()?.replace("\"", "\\\"")?.replace("\n", "\\n") ?: ""

        parent.inlineScript(
            """
function makeCellsClickable() {
    var cells = document.getElementsByClassName("scheduleCell");
    var cellCount = cells.length;
    for (var i = 0; i < cellCount; i++) {
        cells[i].onclick = function(event) {
            var value = this.innerHTML;
            if (value == "Y") {
                this.innerHTML = "N";
            } else if (value == "N" ) {
                this.innerHTML = "&nbsp;";
            } else {
                this.innerHTML = "Y";
            }
            event.stopPropagation();
        };
    } 
}

function updateTables( data ) {
    var map = new Map();
    var lines = data.split( "\n");
    for (l = 0; l < lines.length; l ++ ) {
        var line = lines[l];
        var pipe = line.indexOf( "|" );
        if (pipe > 0) {
            var name = line.substring( 0, pipe ).trim();
            var months = line.substring( pipe +1 ).trim().split( " " );
            map.set(name,months);
        }
    }
    
    var tables = document.getElementsByClassName( "schedule" );
    for (t = 0; t < tables.length; t ++ ) {
        var table = tables[t];
        var rows = table.rows;
        for (r = 0; r < rows.length; r ++ ) {
            var row = rows[r];
            var cells = row.children;
            var monthIndex = 0;
            var name = "";
            for (c = 0; c < cells.length; c ++ ) {
                var cell = cells[c];
                if (cell.className.includes( "rowHeading" ) ) {
                    name = cell.innerText;
                } else if ( cell.className.includes( "scheduleCell" ) ) {
                    var months = map.get(name);
                    if (months != null && months.length > monthIndex) {
                        var month = months[monthIndex];
                        if (month != ".") {
                            cell.innerText = month;
                        }
                        monthIndex ++;
                    }
                }
            }
        }
    }

}

function saveSchedule() {
    var tables = document.getElementsByClassName( "schedule" );
    var rowData = null;
    var rowsData = "";
    for (t = 0; t < tables.length; t ++ ) {
        var table = tables[t];
        var rows = table.rows;
        for (r = 0; r < rows.length; r ++ ) {
            var row = rows[r];
            var cells = row.children;
            for (c = 0; c < cells.length; c ++ ) {
                var cell = cells[c];
                var text = cell.innerText.trim();
                if (text == "") text = ".";
                    
                if (cell.className.includes( "rowHeading" ) ) {
                    if (rowData != null) {
                        rowsData += rowData
                        rowsData += "\n";
                    }
                    var spaces = 20-text.length;
                    if (spaces < 1) spaces = 1;
                    rowData = text + " ".repeat(spaces) + " |";
                } else if (cell.className.includes( "scheduleCell" ) ) {
                    if (text == "") {
                        rowData += " .";
                    } else {
                        rowData += " " + text;
                    }
                }
            }
        }
    }
    if (rowData != null) {
        rowsData += rowData
        rowsData += "\n";
    }
    //console.log( "rowsData = " + rowsData );
    
    var source = document.getElementById( "scheduleSource" );
    source.value = "$prefix(schedule=\"${schedulePageName}\")\n" + rowsData + "\n$suffix\n";    
}

window.addEventListener( "load", function() {
    makeCellsClickable();
    updateTables( "$data" );
});
"""
        )
    }

}

