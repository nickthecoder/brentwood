package uk.co.nickthecoder.brentwood.home.garden

import uk.co.nickthecoder.brentwood.util.REGEX_PAGE_NAME
import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.WikiException
import uk.co.nickthecoder.brentwood.wiki.syntax.AbstractWikiSyntax
import uk.co.nickthecoder.brentwood.wiki.syntax.parseParameters
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class EditScheduleSyntax(

    prefix: String = "{{editSchedule",
    suffix: String = "}}"

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        val (end, values) = parseParameters(parameters, lineStr, column + prefix.length)

        val schedule =
            values["schedule"] ?: throw WikiException(line, column, details, "Expected parameter : schedule")
        val schedulePage = details.engine.createName(schedule, details.pageName.namespace)
        if (!schedulePage.exists()) {
            throw WikiException(line, column, details, "Schedule page '$schedule' does not exist")
        }

        details.customData["fullWidth"] = true

        return Pair(end, EditScheduleNode(line, column, prefix, suffix, details.pageName, schedulePage))
    }

    companion object {
        val parameters = mapOf(
            "schedule" to REGEX_PAGE_NAME
        )
    }

}
