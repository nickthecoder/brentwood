package uk.co.nickthecoder.brentwood.home.garden

import kotlinx.html.*
import uk.co.nickthecoder.brentwood.wiki.PageName
import uk.co.nickthecoder.brentwood.wiki.WikiEngine
import uk.co.nickthecoder.brentwood.wiki.tree.SimpleParentNode
import kotlin.text.Typography

class ScheduleNode(

    line: Int,
    column: Int,
    val pageName: PageName,
    val scheduleData: ScheduleData

) : SimpleParentNode(line, column) {

    override fun isBlock() = true

    override fun isPlainTextContent() = true


    override fun render(parent: FlowContent, engine: WikiEngine) {
        val lines = text()

        parent.table(scheduleData.tableClass) {
            tr {
                th()
                for (month in scheduleData.headings) {
                    th(classes = "month") { +month }
                }
            }
            for (line in lines.split('\n')) {

                val trimmed = line.trim()
                if (trimmed.isEmpty() || trimmed.startsWith("#") || trimmed.startsWith("//")) {
                    continue
                }

                // The separator, before it is the plant name (which is also the name of a WikiPage).
                val bar1 = trimmed.indexOf('|')
                if (bar1 < 0) continue
                    
                // Optionally, a line may contain a second bar, in which case, the contents between the two bars
                // is treated as plain text, added as an extra column.
                val bar2 = trimmed.indexOf('|',bar1 + 1)
                
                val name = trimmed.substring(0, bar1).trim()
                val months = if (bar2 < 0) {
                    trimmed.substring(bar1 + 1).trim()
                } else {
                    trimmed.substring(bar1 + 1, bar2).trim()
                }
                
                var columnCount = 0
                tr {
                    val plantPageName = engine.createName(name, pageName.namespace)
                    th(classes = "rowHeading") {
                        // The "extra" part written after the months data will be
                        // on a new tr, but AFTER this rowHeading (i.e. below the months)
                        if (bar2 > 0) {
                            rowSpan = "2"
                        }
                        
                        if (plantPageName.exists()) {
                            a(engine.urlFactory.view(plantPageName)) { +plantPageName.title }
                        } else {
                            +plantPageName.title
                        }
                    }
                    
                    for (month in months.split(" ")) {
                        
                        columnCount ++

                        val cssClass = scheduleData.cellClasses[month]
                        td(classes = if (cssClass == null) "scheduleCell" else "scheduleCell $cssClass") {
                            scheduleData.cellTitles[month]?.let {
                                title = it
                            }
                            + "${Typography.nbsp}"
                        }

                    }
                }
                if (bar2>0) {
                    tr {
                        td{
                            colSpan = "$columnCount"
                            + trimmed.substring(bar2 + 1).trim()
                        }
                    }
                }

            }
        }
    }

}
