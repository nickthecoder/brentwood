package uk.co.nickthecoder.brentwood.home.garden

import uk.co.nickthecoder.brentwood.wiki.ParserDetails
import uk.co.nickthecoder.brentwood.wiki.syntax.AbstractWikiSyntax
import uk.co.nickthecoder.brentwood.wiki.tree.Node

class ScheduleSyntax(

    prefix: String = "{{schedule",
    suffix: String = "}}",
    val scheduleData: ScheduleData = ScheduleData(),
    val isFullWidth: Boolean = true

) : AbstractWikiSyntax(prefix, suffix) {

    override fun createNode(line: Int, column: Int, lineStr: String, details: ParserDetails): Pair<Int, Node> {
        if (isFullWidth) {
            details.customData["fullWidth"] = true
        }
        return Pair(column + prefix.length, ScheduleNode(line, column, details.pageName, scheduleData))
    }

}

data class ScheduleData(

    val tableClass: String = "schedule",

    val cellClasses: Map<String, String> = mapOf(
        "o" to "sowOutdoors",
        "i" to "sowIndoors",
        "p" to "plantOut",
        "h" to "harvest",
        "f" to "flowers",
        "x" to "extra",
    ),

    val cellTitles: Map<String, String> = mapOf(
        "o" to "Sow Outdoors",
        "i" to "Sow Indoors",
        "p" to "Plant Out",
        "h" to "Harvest",
        "f" to "Flowers",
        "x" to "Extra"
    ),

    val headings: List<String> = listOf(
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec"
    )
)
