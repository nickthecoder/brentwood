package uk.co.nickthecoder.brentwood.home.map

import io.ktor.application.*
import io.ktor.html.*
import io.ktor.routing.*
import kotlinx.html.*
import uk.co.nickthecoder.brentwood.Component
import uk.co.nickthecoder.brentwood.util.javascriptString
import uk.co.nickthecoder.brentwood.util.respondHtml2
import uk.co.nickthecoder.brentwood.util.staticResources

private const val MAP_TYPE_GOOGLE = 1
private const val MAP_TYPE_GOOGLE_SAT_ONLY = 2 // Not used.
private const val MAP_TYPE_GOOGLE_SAT = 3
private const val MAP_TYPE_MAPBOX = 4
private const val MAP_TYPE_MAPBOX_SAT = 5

class Maps(
    webPath: String = "/map",
    private val googleKey: String,
    private val mapBoxKey: String
) : Component(webPath) {

    override fun route(route: Route) {

        route.staticResources("/resources", this@Maps.javaClass.packageName)

        route.get("/") { map(call) }
    }

    private suspend fun map(call: ApplicationCall) {

        val query = call.parameters["s"] ?: ""
        val mapType = call.parameters["type"]?.toIntOrNull() ?: 1

        val load = if (mapType >= MAP_TYPE_MAPBOX) {
            "mapBox_load(${javascriptString(mapBoxKey)});"
        } else {
            "googleMap_load();"
        }
        val search = if (query.isBlank()) {
            ""
        } else {
            "map.search( ${javascriptString(query)} );"
        }

        call.respondHtml2 {
            head {
                title { +"Map" }
                link("/style/mapicon.png", rel = "icon")
                link("$webPath/resources/map.css", rel = "stylesheet")

                if (mapType >= MAP_TYPE_MAPBOX) {
                    script("text/javascript", "https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.js") {}
                    link("https://api.mapbox.com/mapbox-gl-js/v2.3.1/mapbox-gl.css", "stylesheet")
                } else {
                    script(
                        "text/javascript",
                        "https://maps.googleapis.com/maps/api/js?key=$googleKey&sensor=false"
                    ) {}
                }

                script("text/javascript", "$webPath/resources/map.js") {}
            }

            body {
                onLoad = "$load $search"

                form(webPath) {
                    id = "searchForm"
                    onSubmit = "map.search(this.address.value); return false;"

                    div {
                        id = "tools"
                        input(InputType.text, classes = "searchBox") {
                            name = "s"
                            id = "address"
                            size = "30"
                            placeholder = "search"
                        }
                        input(InputType.submit, classes = "searchButton") {
                            value = "»"
                        }
                        input(InputType.button, classes = "advancedSearch") {
                            value = "…"
                            onClick = "return map.advancedSearch();"
                        }
                        a("#") {
                            id = "hamburger"
                            onClick = "hamburger(); return false;"
                            +"☰"
                        }
                    }
                }

                div("popup") {
                    id = "menu"

                    ul {
                        li {
                            a("#") {
                                onClick = "copyLink(${javascriptString(webPath)}); return false;"
                                +"Copy Web Address"
                            }
                        }
                        li {
                            a("#") {
                                id = "googleMapLink"
                                onMouseOver = "createGoogleMapLink();"
                                onClick = "googleMapLink(); return false;"
                                +"Google Maps"
                            }
                        }
                        li {
                            a("#") {
                                id = "openStreetMapLink"
                                onMouseOver = "createOpenStreetMapLink();"
                                onClick = "openStreetMapLink(); return false;"
                                +"Open Street Map"
                            }
                        }
                        if (mapType != MAP_TYPE_MAPBOX) {
                            li {
                                a("#") {
                                    onMouseOver =
                                        "this.href = getMapUrl( ${javascriptString(webPath)}, $MAP_TYPE_MAPBOX )"
                                    onClick = "followLink( ${javascriptString(webPath)}, $MAP_TYPE_MAPBOX )"
                                    +"MapBox Street View"
                                }
                            }
                        }
                        if (mapType != MAP_TYPE_MAPBOX_SAT) {
                            li {
                                a("#") {
                                    onMouseOver =
                                        "this.href = getMapUrl( ${javascriptString(webPath)}, $MAP_TYPE_MAPBOX_SAT )"
                                    onClick = "followLink( ${javascriptString(webPath)}, $MAP_TYPE_MAPBOX_SAT )"
                                    +"MapBox Satellite View"
                                }
                            }
                        }
                        if (mapType >= MAP_TYPE_MAPBOX) {
                            li {
                                a("#") {
                                    id = "googleView"
                                    onMouseOver =
                                        "this.href = getMapUrl( ${javascriptString(webPath)}, $MAP_TYPE_GOOGLE )"
                                    onClick = "followLink( ${javascriptString(webPath)}, $MAP_TYPE_GOOGLE )"
                                    +"Google View"
                                }
                            }
                        }
                    }

                    hr()

                    h3 {
                        +"Markers"
                        a("#", classes = "right") {
                            onClick = "addMarker(); return false;"
                            +"+"
                        }
                    }
                    ul {
                        id = "markers"
                    }
                }

                div {
                    id = "mapDiv"
                }

                div("popup") {
                    id = "search"
                    div("background") {}
                    div {
                        id = "searchHeading"
                        a("#", classes = "right") {
                            onClick = "showSearchResults(null); return false;"
                            +"X"
                        }
                    }
                    div {
                        id = "searchContent"
                    }
                }
            }
        }

    }

}
