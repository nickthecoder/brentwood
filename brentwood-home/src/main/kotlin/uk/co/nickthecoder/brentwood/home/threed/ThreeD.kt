package uk.co.nickthecoder.brentwood.home.threed

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.http.content.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.html.*
import org.intellij.markdown.IElementType
import org.intellij.markdown.flavours.gfm.GFMFlavourDescriptor
import org.intellij.markdown.html.HtmlGenerator
import org.intellij.markdown.parser.MarkdownParser
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.util.*
import java.io.File

/**
 * Shows my 3D projects.
 * The [baseDirectory] has sub-directories, each of which has ".foocad" files.
 * These foocad files are the source code for threed models.
 *
 * The models are rendered to a .png image with the same basename.
 */
class ThreeD(

    layout: Layout,
    webPath: String = "/3d",

    val baseDirectory: File,

    val thumbnailer: ImageScaler,

    ) : LayoutComponent(layout, webPath) {

    override fun route(route: Route) {
        with(route) {
            get("/{resource...}") {
                fileOrDirectory(call, call.safePathParts("resource"))
            }

            get("/media/{resource...}") {
                val pathParts = call.safePathParts("resource")
                media(call, pathParts)
            }

            get("/model/{resource...}") {
                val pathParts = call.safePathParts("resource")
                model(call, pathParts)
            }

        }
    }


    private fun href2(pathParts: List<String>) =
        "$webPath/${escapeURLPathParams(pathParts)}"


    private fun childHref2(pathParts: List<String>, child: String) =
        "$webPath/${escapeChildURLPathParams(pathParts, child)}"

    /**
     * webPath/resource/{...}
     *
     * Responds with the contents of the file (usually an image).
     */
    private suspend fun media(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()

        val file = File(baseDirectory, path)
        if (file.exists() && file.isFile) {
            if (file.extension == "foocad") {
                val message = LocalFileContent(file, contentType = ContentType.Text.Plain)
                call.respond(message)
            } else {
                call.respondFile(file)
            }
        } else {
            throw NotFoundException("ThreeD media not found : '$path'")
        }
    }

    private suspend fun fileOrDirectory(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()

        val file = File(baseDirectory, path)
        if (file.exists()) {
            if (file.isDirectory) {
                directory(call, pathParts, file)
            } else {
                media(call, pathParts)
            }
        } else {
            throw NotFoundException(file.path)
        }

    }


    private suspend fun directory(call: ApplicationCall, pathParts: List<String>, directory: File) {
        val uri = call.request.uri

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

            pageTitle { +"${pathParts.joinPath()} (3D Models)" }

            pageHeading {
                a(webPath) { +"/" }
                for (i in pathParts.indices) {
                    a(href2(pathParts.subList(0, i + 1))) {
                        +pathParts[i]
                    }
                    if (i < pathParts.size - 1) {
                        +"/"
                    }
                }
            }

            pageTools {

                if (pathParts.isNotEmpty()) {
                    layout.imageTool(this, href2(pathParts.subList(0, pathParts.size - 1))) {
                        title = "Back to Parent Directory"
                        img("Up", "/style/up.png")
                    }
                }
            }


            content {

                layout.thumbnails(this) {
                    for (child in directory.listOnlyDirectories()) {
                        layout.thumbnail(this, main = { directoryThumbnail(pathParts, child) }) {
                            a(childHref2(pathParts, child.name)) {
                                +child.name
                            }
                        }
                    }
                }

                layout.thumbnails(this) {
                    for (child in directory.listFilesWithExtension(listOf("foocad"))) {
                        layout.thumbnail(this, main = { imageThumbnail(pathParts, child) }) {
                            a(childHref2(pathParts, child.name)) {
                                +child.name
                            }
                        }
                    }
                }

            }
        }
    }


    private suspend fun model(call: ApplicationCall, pathParts: List<String>) {
        val path = pathParts.joinPath()
        val file = File(baseDirectory, path)
        val markdownFile = File(file.parentFile, file.nameWithoutExtension + ".md")
        val zipFile = File(file.parentFile, file.nameWithoutExtension + ".zip")

        val uri = call.request.uri
        val parentPathParts = pathParts.subList(0, pathParts.size - 1)

        val sourceCode = file.readText()
        val markdown = if (markdownFile.exists()) {
            markdownFile.readText()
        } else {
            ""
        }

        call.respondHtmlTemplate2(layout.createTemplate(true, uri)) {

            pageTitle { + "${pathParts.joinPath()} (3D Model)" }

            pageHeading {
                a(webPath) { + "/" }
                for (i in pathParts.indices) {
                    a(href2(pathParts.subList(0, i + 1))) {
                        + pathParts[i]
                    }
                    if (i < pathParts.size - 1) {
                        +"/"
                    }
                }
            }

            pageTools {
                if (zipFile.exists()) {
                    layout.imageTool(
                        this,
                        "$webPath/media/${escapeURLPathParams(parentPathParts)}/${escapeURLParam(file.nameWithoutExtension)}.zip"
                    ) {
                        title = "Download .stl files and sources"
                        img("Zip", "/style/zip.png")
                    }
                }

                if (pathParts.isNotEmpty()) {
                    layout.imageTool(this, href2(pathParts.subList(0, pathParts.size - 1))) {
                        title = "Back to Parent Directory"
                        img("Up", "/style/up.png")
                    }
                }
            }


            content {

                div("imageContainer") {
                    img(
                        alt = file.nameWithoutExtension,
                        src = "$webPath/media/${escapeURLPathParams(parentPathParts)}/${escapeURLParam(file.nameWithoutExtension)}.png"
                    )
                }

                if (markdown.isNotBlank()) {
                    div("markdown") {
                        val markdownFlavour = GFMFlavourDescriptor()
                        val parsedTree = MarkdownParser(markdownFlavour).parse(IElementType("ROOT"), markdown)
                        val html = HtmlGenerator(markdown, parsedTree, markdownFlavour).generateHtml()
                        unsafe { + html }
                    }
                }

                a("$webPath/media/${escapeURLPathParams(pathParts)}") {
                    + "FooCAD Source Code"
                }

                pre {
                    + sourceCode
                }
            }
        }
    }

    private fun media(pathParts: List<String>, child: String): String {
        return "$webPath/media/${escapeURLPathParams(pathParts)}/${escapeURLParam(child)}"
    }

    private fun FlowContent.imageThumbnail(parentPathParts: List<String>, file: File) {
        val image = ensureThumbnail(file)
        a("$webPath/model/${escapeURLPathParams(parentPathParts)}/${escapeURLParam(file.name)}") {
            val parts = mutableListOf<String>()
            parts.addAll(parentPathParts)
            parts.add(".thumbnails")
            img(image.name, media(parts, image.name), classes = "thumbnail")
        }
    }

    /**
     * Generates the thumbnail image if it doesn't already exists.
     * NOTE. This may NOT be in time for the current request, so the user will have to refresh the page to see the thumbnails.
     */
    private fun ensureThumbnail(file: File): File {
        val image = if (file.extension == "foocad") {
            File(file.parentFile, "${file.nameWithoutExtension}.png")
        } else {
            file
        }
        val thumbnailFile = File(image.parentFile, ".thumbnails/${image.name}")
        if (image.extension == "png" && image.exists()) {
            thumbnailer.scaleImage(image, thumbnailFile)
        }
        return thumbnailFile
    }

    private fun FlowContent.directoryThumbnail(parentPathParts: List<String>, dir: File) {
        val defaultThumbnail = File(dir, ".thumbnails/default.jpg")
        a(childHref2(parentPathParts, dir.name)) {
            if (defaultThumbnail.exists()) {
                val src = "$webPath/media/${escapeURLPathParams(parentPathParts)}/${
                    escapeURLParam(dir.name)
                }/.thumbnails/default.jpg"
                img(dir.name, src)
                span("emblem") {
                    img(dir.name, "/style/folderEmblem.png", "folder")
                }
            } else {
                img(dir.name, "/style/folderThumbnail.png", classes = "folder")
            }
        }
    }

}
