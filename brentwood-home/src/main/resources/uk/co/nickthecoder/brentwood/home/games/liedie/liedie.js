var DICE = 5;
var SIX = 6; // The maximum value of a die.

function LieDie()
{
    this.held = []; // False for each die which will be rolled when the Roll Dice button is clicked.
    this.values = []; // The actual values of each die
    this.claims = []; // The values of the dice as claimed by the player
    this.claimIndex = 0; // 0..DICE - 1. The index of the die to change
    
    for (var i = 0; i < DICE; i ++ ) {
        this.held[i] = false;
        this.values[i] = 0;
        this.claims[i] = 0;
    }
    this.createDice();
    this.setState('newGame');
    
}

LieDie.prototype = {

setState: function(state)
{
    this.state = state;
    document.getElementById('state').className = state;
},

createDice: function() {
    var html = LieDie.diceHTML;
    var number;
    for ( var i = 0; i < DICE; i ++ ) {
        if (this.held[i]) {
            number = this.values[i];
        } else {
            number = 0;
        }
        html += '<img id="die' + i + '" src="die' + number + '.png" alt=""/ onclick="game.toggle(' + i + ')">\n';
    }
    document.getElementById('dice').innerHTML = html;
},

createClaim: function() {
    var html = LieDie.claimHTML;
    for ( var i = 0; i < DICE; i ++ ) {
        html += '<img id="claim' + i + '" src="die0.png" alt=""/>\n';
    }
    document.getElementById('claim').innerHTML = html;

    html = LieDie.claimButtonsHTML;
    for ( var i = 1; i <= SIX; i ++ ) {
        html += '<button onclick="game.pick(' + i + ');">' + i + '</button>\n';
    }
    html += '<button onclick="game.backspace();">Backspace</button>\n';
    html += '<br>';

    html += '<button onclick="game.done()">Done</button>\n';
    
    document.getElementById('claimButtons').innerHTML = html;
    
},

toggle: function( n ) {
    if ( this.state == 'accepted' ) {
        this.held[n] = ! this.held[n];
        this.createDice();
    }
},

pick: function( n ) {
    if ( this.claimIndex < DICE ) {
        this.claims[this.claimIndex] = n;
        document.getElementById('claim' + this.claimIndex).src = 'die' + n + '.png';
        this.claimIndex += 1;
    }
},

backspace: function() {
    if ( this.claimIndex > 0 ) {
        this.claimIndex -= 1;
        document.getElementById('claim' + this.claimIndex).src = 'die0.png';
    }
},

done: function() {
    if ( this.claimIndex == DICE ) {
        this.setState( 'acceptReject' );
    }
},

roll: function() {
    this.rolledCount = 0;
    this.setState('rolled');
    for ( var i = 0; i < DICE; i ++ ) {
        if (!this.held[i]) {
            this.rolledCount += 1;
            this.values[i] = Math.floor((Math.random()*SIX)+1); 
            this.held[i] = true;
        }
    }
    this.claimIndex = 0;
    this.createDice();
    this.createClaim();
},

accept: function() {

    this.values.sort();
    for (var i = 0; i < DICE; i ++ ) {    
        this.claims[i] = 0;
        this.held[i] = true;
    }
    this.createDice();
    this.setState('accepted');    
    
},

reject: function() {
    this.values.sort();
    this.claims.sort();
    var same = true;
    for (var i = 0; i < DICE; i ++ ) {
        if (this.values[i] != this.claims[i]) {
            same = false;
            break;
        }
    }
    var text = same ? "No, they were telling the truth!" : "Well done, you spotted the lie.";
    document.getElementById('result').innerHTML = text;
        
    this.setState('gameOver');
},

restart: function() {
    game = new LieDie();
}

};

function startGame()
{
    LieDie.diceHTML = document.getElementById('dice').innerHTML;
    LieDie.claimHTML = document.getElementById('claim').innerHTML;
    LieDie.claimButtonsHTML = document.getElementById('claimButtons').innerHTML;
        
    game = new LieDie();
}
var game;

