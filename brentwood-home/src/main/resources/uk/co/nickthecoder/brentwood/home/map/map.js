map = null;

function debugProps( label, o ) {
    console.log( label + " " + o );
    console.log( Object.getOwnPropertyNames( o ) );
    console.log( Object.getOwnPropertyNames( Object.getPrototypeOf(o) ) );
}

function getParameter( name, from )
{
    var results = getParameters( name, from );
    if (results == null) {
        return null;
    } else {
        return results[0];
    }
}

function getParameters( name, from )
{
    if (! from) {
        from = window.location.href;
    } else {
        // When searching an arbitrary string, add & to the beginning so that it will find the first NAME=VALUE.
        from = "&" + from;
    }
    
    // name = name.replace(/[\[]/,"\\[").replace(/[\]]/,"\\]");
    var regexS = "[\\?&]"+name+"=([^&#]*)";
    var regex = new RegExp( regexS, "g" );
    
    var results = [];
    var matched = regex.exec( from );
    while (matched != null) {
        results[results.length] = decodeURIComponent( matched[1] );
        matched = regex.exec( from );
    }
    
    return results;
}

function getNumberParameter( name, defaultValue, from )
{
    var value = getParameter( name, from );
    if (value == null) {
        return defaultValue;
    } else {
        return 1 * value;
    }
}

function escapeHTML(unsafe)
{
    if (unsafe == null) return "";
    
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
}

function hamburger()
{
    var menu = document.getElementById( "menu" );
    showHamburger( ! menu.shown );
}

function showHamburger( show )
{
    var menu = document.getElementById( "menu" );
    if (show) {
        menu.shown = true;
        menu.style.visibility = "visible";
        showSearchResults( null );
    } else {
        menu.shown = false;
        menu.style.visibility = "hidden";
    }
}

function showSearchResults( html )
{
    if (html == null) {
        document.getElementById("search").style.visibility = 'hidden';
    } else {
        showHamburger( false );
        document.getElementById("searchContent").innerHTML = html;
        document.getElementById("search").style.visibility = 'visible';
    }
}

function mapBox_load(token)
{
    mapboxgl.accessToken = token;
    map = new MapBoxWrapper();
    initialiseMap();
}

function googleMap_load()
{
    map = new GoogleMapWrapper();
    initialiseMap();
}

function initialiseMap()
{
    var lat = getNumberParameter( "lat", 51.5743537 );
    var lng = getNumberParameter( "lng", 0.1983946 );
    var zoom = getNumberParameter( "zoom", 11 );
    var mapType = getNumberParameter( "type", 1 );
    
    var label = getParameter( "marker" );
    var mlat = getNumberParameter( "mlat", lat );
    var mlng = getNumberParameter( "mlng", lng );

    map.initialise( lat, lng, zoom, label, mlat, mlng, mapType );
    
    var ms = getParameters( "m" );
    for ( var i = 0; i < ms.length; i ++ ) {
        try {
            parseMarkerParameter( ms[i] );
        } catch(err) {
            // Do nothing
        }
    }    
    buildMarkerList();
}

function parseMarkerParameter( m )
{
    var lat = getNumberParameter( "lat", null, m );
    var lng = getNumberParameter( "lng", null, m );
    var label = getParameter( "l", m );
           
    if ( (lat != null) && (lng != null) && (label != null) ) {
        map.createMarker( label, lat, lng );
    }
}

function copyLink( page )
{
    var url = getMapUrl( page );
    if (window.prompt( "Copy the address using using Ctrl+C", url ) != null) {
        document.location = url;
    }
}

function followLink( page, mapType )
{
    var url = getMapUrl( page, mapType );
    document.location = url;
}

function changeLink( id, page )
{
    var url = getMapUrl( page );
    document.getElementById( id ).href = url;
}

function getMapUrl( page, mapType )
{
    var latLng = map.getCenter();
    var zoom = map.getZoom();

    var url = page + "?lat=" + latLng.lat + "&lng=" + latLng.lng + "&zoom=" + zoom;

    if ( mapType == null ) mapType = map.getMapType();
    if ( mapType != 1 ) {
        url += '&type=' + mapType;
    }

    for ( var i = 0; i < map.markers.length; i ++ ) {
        var marker = map.markers[ i ];
        var pos = marker.getPosition();
        var value = 'lat=' + pos.lat + '&lng=' + pos.lng + '&l=' + encodeURIComponent( marker.getLabel() );
        url += '&m=' + encodeURIComponent( value );
    }
    return url;
}

function createGoogleMapLink()
{
    var latLng = map.getCenter();
    var zoom = map.getZoom();

    var url = "http://maps.google.co.uk/maps?ll=" + latLng.lat + "," + latLng.lng + "&z=" + zoom;
    document.getElementById( "googleMapLink" ).href = url;
    return url;
}

function googleMapLink()
{ 
    document.location = createGoogleMapLink();
}


function createOpenStreetMapLink()
{
    var latLng = map.getCenter();
    var zoom = map.getZoom();

    var url = "http://www.openstreetmap.org/?lat=" + latLng.lat + "&lon=" + latLng.lng + "&zoom=" + zoom;
    document.getElementById( "openStreetMapLink" ).href = url;
    return url;
}

function openStreetMapLink()
{ 
    document.location = createOpenStreetMapLink();
}

function buildMarkerList()
{
    var list = document.getElementById( "markers" );
    var html = "";
    for ( var i = 0; i < map.markers.length; i ++ ) {
        var marker = map.markers[i];
        var li = '<li><a href="#" onclick="selectMarker('+i+'); return false;">' + escapeHTML( marker.getLabel() ) + '</a><a href="#" class="right" onclick="deleteMarker('+i+'); return false;">x</li>';
        html += li;
    }
    list.innerHTML = html;
}

function selectMarker( n )
{
    map.setCenter( map.markers[n].getPosition() );
}

function addMarker()
{
    var label = window.prompt( "Add a Marker", "unnamed" );
    var c = map.getCenter();
    if (label != null) {
        map.createMarker( label, c.lat, c.lng );
        buildMarkerList();
    }
}

function deleteMarker( n )
{
    map.deleteMarker(n);
    buildMarkerList();    
}

// BASE

function AbstractMarkerWrapper()
{
}

AbstractMarkerWrapper.prototype.editMarker = function()
{
    var newLabel = window.prompt( "Marker", this.getLabel() );
    if (newLabel != null) {
        this.setLabel( newLabel );
    }
    buildMarkerList();
}

function AbstractWrapper()
{
}

AbstractWrapper.prototype.initialise = function()
{
    this.markers = [];    
};

AbstractWrapper.prototype.markerCount = function()
{
    return this.markers.length;
}

// MapBox

function MapBoxMarkerWrapper( label, lat, lng )
{
    this.label = label;
    this.marker = new mapboxgl.Marker( { draggable: true } );
    this.marker.setLngLat([lng, lat]);
    this.marker.addTo(map.map);
    //this.marker.setPopup(new mapboxgl.Popup().setText(label));
    var me = this;
    this.marker.getElement().title = label;
    this.marker.getElement().addEventListener('click', function() {
        me.editMarker();
    } );
}

MapBoxMarkerWrapper.prototype = Object.create(AbstractMarkerWrapper.prototype);
MapBoxMarkerWrapper.prototype.constructor = MapBoxMarkerWrapper;


MapBoxMarkerWrapper.prototype.getPosition = function()
{
    var c = this.marker.getLngLat();
    return { lat: c.lat, lng: c.lng };            
};

MapBoxMarkerWrapper.prototype.getLabel = function()
{
    return this.label;
};

MapBoxMarkerWrapper.prototype.setLabel = function( label )
{
    this.label = label;
    this.marker.getElement().title = label;
};

function MapBoxWrapper()
{
}
MapBoxWrapper.prototype = Object.create(AbstractWrapper.prototype);
MapBoxWrapper.prototype.constructor = MapBoxWrapper;

MapBoxWrapper.prototype.initialise = function( lat, lng, zoom, label, mlat, mlng, mapType )
{
    AbstractWrapper.prototype.initialise.call( this );

    var div = document.getElementById( "mapDiv" );
    div.style.height = window.innerHeight + "px";

    this.label = null;
    this.marker = null;
    this.mapType = mapType; // Hmm, should always be 4???

    var style = 'mapbox://styles/mapbox/streets-v11';
    if (mapType == 5) {
        style = 'mapbox://styles/mapbox/satellite-streets-v11';
    }

    this.map = new mapboxgl.Map({
        container: 'mapDiv',
        style: style,
        center: [lng, lat],
        zoom: zoom
    });
    this.map.addControl(new mapboxgl.NavigationControl());

    if (label != null) {
        this.label = label;
        this.createMarker( label, mlat, mlng );
    }
};

MapBoxWrapper.prototype.getCenter = function()
{
    var c = this.map.getCenter();
    return { lat: c.lat, lng: c.lng };
};

MapBoxWrapper.prototype.setCenter = function( pos )
{
    this.map.panTo( [pos.lng, pos.lat] );
};

MapBoxWrapper.prototype.getZoom = function()
{
    return this.map.getZoom();
};

MapBoxWrapper.prototype.getMapType = function()
{
    return this.mapType;
};

MapBoxWrapper.prototype.createMarker = function( label, lat, lng )
{
    this.markers[ this.markers.length ] = new MapBoxMarkerWrapper( label, lat, lng );
};

MapBoxWrapper.prototype.deleteMarker = function( n )
{
    var marker = this.markers[n].marker;
    this.markers.splice( n, 1 );
    this.map.removeLayer( marker );
}

// GOOGLE MAP


function GoogleMarkerWrapper( label, lat, lng )
{
    this.marker = new google.maps.Marker( {
        map: map.map,
        position: new google.maps.LatLng( lat, lng ),
        draggable: true,
        title: label,
        clickable: true,
        animation: google.maps.Animation.DROP
    } );
    var me = this;
    this.marker.addListener( "click", function() {
        me.editMarker();
    } );
}

GoogleMarkerWrapper.prototype = Object.create(AbstractMarkerWrapper.prototype);
GoogleMarkerWrapper.prototype.constructor = GoogleMarkerWrapper;

GoogleMarkerWrapper.prototype.getLabel = function()
{
    return this.marker.getTitle();
}

GoogleMarkerWrapper.prototype.setLabel = function( label )
{
    this.marker.setTitle( label );
};

GoogleMarkerWrapper.prototype.getPosition = function()
{
    var c = this.marker.getPosition();
    return { lat: c.lat(), lng: c.lng() };            
};



function GoogleMapWrapper()
{

}
GoogleMapWrapper.prototype = Object.create(AbstractWrapper.prototype);
GoogleMapWrapper.prototype.constructor = GoogleMapWrapper;

GoogleMapWrapper.prototype.initialise = function( lat, lng, zoom, label, mlat, mlng,mapType )
{
    AbstractWrapper.prototype.initialise.call( this );
    
    var div = document.getElementById( "mapDiv" );
    div.style.height = window.innerHeight + "px";
    
    var mapTypeId = google.maps.MapTypeId.ROADMAP;
    if (mapType == 2) {
      mapTypeId = google.maps.MapTypeId.SATELLITE;
    } else if (mapType == 3) {
      mapTypeId = google.maps.MapTypeId.HYBRID;
    }
    
    lnglat = new google.maps.LatLng( lat, lng );

    var mapOptions = {
        center: lnglat,
        zoom: zoom,
        mapTypeId: mapTypeId,
        scrollwheel: false
    };
    
    this.map = new google.maps.Map(document.getElementById("mapDiv"), mapOptions);

    if (label != null) {
        this.createMarker( label, mlat, mlng );
    }

    this.geocoder = new google.maps.Geocoder();
};

GoogleMapWrapper.prototype.getCenter = function()
{
    var c = this.map.getCenter();
    return { lat: c.lat(), lng: c.lng() };
};

GoogleMapWrapper.prototype.setCenter = function( pos )
{
    this.map.setCenter( new google.maps.LatLng(pos.lat, pos.lng) );
};

GoogleMapWrapper.prototype.getZoom = function()
{
    return this.map.getZoom();
};

GoogleMapWrapper.prototype.getMapType = function() {
    if (this.map.mapTypeId == google.maps.MapTypeId.SATELLITE) {
        return 2;
    } else if (this.map.mapTypeId == google.maps.MapTypeId.HYBRID) {
        return 3;
    }
    return 1;
};

GoogleMapWrapper.prototype.createMarker = function( label, lat, lng )
{
    this.markers[ this.markers.length ] = new GoogleMarkerWrapper( label, lat, lng );
};

GoogleMapWrapper.prototype.deleteMarker = function( n )
{
    var marker = this.markers[n].marker;
    this.markers.splice( n, 1 );
    marker.setMap(null);
}

GoogleMapWrapper.prototype.search = function( address )
{
    if ((address == null) || (address == "") ) {
        return;
    }
  
    var request = { address: address, location: this.map.getCenter(), region: "uk" };
  
    var me = this;
    this.geocoder.geocode( request, function ( results, status ) {
        if ( status == google.maps.GeocoderStatus.OK ) {
            result = results[0];    
            me.map.setCenter( result.geometry.location );    
            me.map.fitBounds( result.geometry.viewport );  
        } else {
            alert( "Location : " + address + " not found." );
        }
    });    
};

GoogleMapWrapper.prototype.advancedSearch = function()
{
    var request = { address: document.getElementById("address").value, location: this.map.getCenter(), region: "uk" };
    
    var me = this;
    this.geocoder.geocode( request, function ( results, status ) {

        me.searchResults = results;
        var html = '<ul>';
        for ( var i = 0; i < results.length; i ++ ) {
            html += '<li><a href="#" onclick="return map.selectedSearch(' + i + ');">' + results[i].formatted_address + '</a></li>';
              
        }
        html += '</ul>';

        showSearchResults( html );
        showHamburger( false );
    });
    
    return false;
};

GoogleMapWrapper.prototype.selectedSearch = function( index )
{
    var result = this.searchResults[index];
    this.map.setCenter( result.geometry.location );    

    this.map.fitBounds( result.geometry.viewport );
    
    return false;
};


