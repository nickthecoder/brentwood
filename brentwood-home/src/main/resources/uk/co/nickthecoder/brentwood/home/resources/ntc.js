function parseCookie(name) { return document.cookie.split('; ').find((row) => row.startsWith(name + '='))?.split('=')[1]; }
function setPlainView( v ) {
    isPlainView = v;
    if (v) {
        document.body.classList.add("plain");
    } else {
        document.body.classList.remove("plain");
    }
    document.cookie = 'isPlainView=' + v + '; SameSite=strict; path=/;'
}

var isPlainView=false;
setPlainView(parseCookie('isPlainView') === 'true');

document.addEventListener('keydown', (e) => {
    if (e.altKey && e.key=="F11") {
        setPlainView( !isPlainView );
    }
    if (e.key=="Escape") {
        setPlainView(false);
    }
}, false);
window.addEventListener('load', (event) => {
    document.getElementById("plainView").addEventListener('click', (event) => { setPlainView( !isPlainView ); event.preventDefault(); } );
    document.getElementById("normalView").addEventListener('click', (event) => { setPlainView( !isPlainView ); event.preventDefault(); } );
});
