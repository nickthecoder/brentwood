package uk.co.nickthecoder.brentwood.player

import java.io.File
import java.util.*

/**
 * IDLE - When a track ends normally. This will cause the Controller to play the next track in the playlist.
 *
 * STOPPING - An intermediate state after PLAYING, until the Streaming Thread has finished.
 *
 * STOPPED - Stopped by the user. Unlike IDLE, the next item in the playlist will NOT be played.
 *
 * PLAYING - Fairly obvious!
 *
 * PAUSED - The Streaming Thread is still active, but doing nothing. It is slightly wasteful of resources
 * to stay paused for long periods. Ideally, after a set period we should move from PAUSED to STOPPED.
 */
enum class BackendState { STOPPED, PLAYING, IDLE, PAUSED }

/**
 * Responsible for playing, pausing and stopping music.
 * This is an interface, so that we can have different implementations.
 * The preferred backend is SoxBackend, but I think this is unavailable on RaspPis???
 * In a previous incarnation of this project, I played music using the Java APIs,
 * but it wasn't very good, and using SoxBackend is much simpler.
 * Also, if something goes wrong, it's easy to "killall play" or "killall -9 play".
 */
interface Backend {

    var controller: Controller?

    val backendState: BackendState

    /**
     * When playing, the position of song in seconds.
     */
    val songPosition: Int

    /**
     * The duration of the currently playing song in seconds
     */
    val songDuration: Int

    fun isPlaying() = backendState == BackendState.PLAYING

    fun isPaused() = backendState == BackendState.PAUSED

    fun isStopped() = backendState == BackendState.STOPPED || backendState == BackendState.IDLE

    fun play(file: File, durationSeconds: Int)

    fun positionTo(file: File, durationSeconds: Int, fromSeconds: Int = 0)

    fun stop()

    fun pause()

    fun unPause()
}

abstract class AbstractBackend : Backend {

    override var controller: Controller? = null

    /**
     * The time in millis, when the song was started.
     */
    protected var startTime: Long = 0L

    /**
     * The time when the latest pause started
     */
    protected var pausedTime: Long = 0L

    /**
     * The accumulation of time in millis of the periods between Playing and Paused.
     * This does NOT include pauses which have not yet been un-paused.
     */
    protected var pausedMillis = 0L

    override val songPosition: Int
        get() {
            return when (backendState) {
                BackendState.PAUSED -> ((pausedTime - startTime - pausedMillis) / 1000).toInt()
                BackendState.PLAYING -> ((Date().time - startTime - pausedMillis) / 1000).toInt()
                else -> 0
            }
        }

    override var songDuration: Int = 0
        get() = if (isPlaying() || isPaused()) field else 0
        protected set(v) {
            field = v
        }

    override var backendState: BackendState = BackendState.IDLE
        protected set(v) {
            if (field != v) {
                // Remember play began
                if (v == BackendState.PLAYING && field != BackendState.PAUSED) {
                    startTime = Date().time
                    pausedMillis = 0L
                }
                // Remember when pause began
                if (v == BackendState.PAUSED) {
                    pausedTime = Date().time
                }

                // Un-paused, accumulate into pausedMillis.
                if (field == BackendState.PAUSED && v == BackendState.PLAYING) {
                    if (pausedTime != 0L) { // Defensive, it should always be non zero.
                        pausedMillis += Date().time - pausedTime
                        pausedTime = 0L
                    }
                }

                val old = field
                field = v

                controller?.requestChangedState(old, v)

            }
        }

}
