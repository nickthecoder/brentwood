package uk.co.nickthecoder.brentwood.player

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.player.model.Setting

val playInterrupts: Boolean
    get() = CachedSettings.PLAY_INTERRUPTS.value
val removeOnPlay: Boolean
    get() = CachedSettings.REMOVE_ON_PLAY.value
val randomPlay: Boolean
    get() = CachedSettings.RANDOM_PLAY.value

/**
 * Caches the values of [Setting]s, so that we don't need to go to the database each time we need
 * to know a setting.
 */
enum class CachedSettings(val humanName: String, val label: String) {

    RANDOM_PLAY("Random Play", "Play random tracks when the playlist is empty"),
    PLAY_INTERRUPTS("Interrupt", "Interrupt the current song, when playing a new one"),
    REMOVE_ON_PLAY("Remove", "Remove the current song when adding a song/album"),
    SHUFFLE("Shuffle", "Pick from the playlist randomly"),
    LOOP("Loop", "Loop back to the top of the playlist");

    var value = false

    fun load() {
        Setting.findById(name)?.let { value = it.value }
    }

    fun save() {
        val setting = Setting.findById(name)
        if (setting == null) {
            Setting.new(name) {
                this.value = this@CachedSettings.value
            }
        } else {
            setting.value = value
        }
    }

    companion object {

        fun update(name: String, value: Boolean) {
            valueOf(name).apply {
                this.value = value
                save()
            }
        }

        fun load(db: Database) {
            transaction(db) {
                for (ks in values()) {
                    ks.load()
                }
                // Delete all old settings.
                for (setting in Setting.all()) {
                    try {
                        valueOf(setting.id.value)
                    } catch (e: Exception) {
                        setting.delete()
                    }
                }
            }
        }
    }
}
