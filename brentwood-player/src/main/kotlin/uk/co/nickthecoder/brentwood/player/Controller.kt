package uk.co.nickthecoder.brentwood.player

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.Transaction
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.player.model.*
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.BlockingQueue
import java.util.concurrent.LinkedBlockingQueue

/**
 * All updates of the current playlist, and the currently playing song go through the [Controller].
 * This must be thread safe, as there could be requests from multiple clients,
 * e.g. two people adding/removing items from the playlist.
 * The [Backend] can also cause state changes when a song ends.
 *
 * I've chosen to implement this using a consumer/producer pattern.
 * We have a [queue] of actions to perform, and a background thread (the consumer) acts on them
 * sequentially.
 *
 * The methods named requestX do not carry out the request immediately, instead they put the request
 * onto the [queue].
 */
class Controller(private val db: Database, private val backend: Backend) {

    private val queue: BlockingQueue<Runnable> = LinkedBlockingQueue()

    private var currentEntryId = -1

    init {
        backend.controller = this
        Thread {
            while (true) {
                queue.take().run()
            }
        }.apply {
            name = "PlayerController"
            isDaemon = true
        }.start()
    }

    /**
     * Adds [work] to the [queue], which the background thread will later consume (act upon).
     *
     * When [work] is run, it is wrapped in its own transaction.
     */
    private fun <T> request(work: Transaction.() -> T) {
        queue.put(Runnable {
            transaction(db = db, statement = work)
        })
    }

    /**
     * Return the current playlist entry, or null, if there is no current entry.
     * This must be called within a transaction!
     */
    fun currentEntry(): Entry? {
        return if (currentEntryId < 0) {
            null
        } else {
            Entry.findById(currentEntryId)
        }
    }

    /**
     * Called by the [Backend] when a song has finished playing.
     * Prefixed by "request" to show that the action is put onto the [queue], rather than
     * acted on immediately.
     */
    fun requestChangedState(was: BackendState, now: BackendState) {
        request {
            if (was == BackendState.PLAYING && now == BackendState.IDLE) {
                skip(true)
            }
        }
    }

    fun requestPlayEntry(entryId: Int) {
        request {
            Entry.findById(entryId)?.let { playEntry(it) }
        }
    }

    private fun playEntry(entry: Entry) {
        if (backend.isPlaying()) {
            backend.stop()
        }
        currentEntryId = entry.id.value
        entry.song.lastPlayed = LocalDateTime.now()
        entry.played = true
        backend.play(entry.song.file, entry.song.seconds)
    }

    fun requestPlay() {
        request {
            play()
        }
    }

    /**
     * If we know the current entry in the playlist ([currentEntryId] >= 0), then
     * play that entry, otherwise, play the first entry in the playlist.
     */
    private fun play() {
        if (backend.isPlaying()) return

        val playlist = Playlist.findCurrent()

        if (currentEntryId >= 0) {
            Entry.findById(currentEntryId)?.let { entry ->
                playEntry(entry)
                return
            }
        }

        if (playlist.entries.empty()) {
            //if (randomPlay) {
            // Let's ALWAYS do random play (or 1 song) when the play button is pressed!
            randomPlay()
            //}
        } else {
            if (CachedSettings.SHUFFLE.value) {
                playRandomEntry(playlist.entries.toList())
            } else {
                // Play the TOP entry
                playlist.entries.minByOrNull { it.orderNumber }?.let { playEntry(it) }
            }
        }
    }

    /**
     *
     */
    private fun playRandomEntry(entries: List<Entry>) {
        if (entries.isEmpty()) return

        var unplayed = entries.filter { !it.played }
        if (unplayed.isEmpty()) {
            if (CachedSettings.LOOP.value) {
                for (entry in entries) {
                    entry.played = false
                }
            }
            unplayed = entries
        }
        if (unplayed.isNotEmpty()) {
            playEntry(unplayed[Random().nextInt(unplayed.size)])
        }
    }


    private fun randomPlay(reshuffle: Boolean = true) {
        Songs.slice(Songs.id, Songs.randomOrder)
            .selectAll()
            .orderBy(Songs.randomOrder, SortOrder.DESC)
            .limit(1).firstOrNull()
            ?.let { row ->
                val songId = row[Songs.id]
                val randomOrder = row[Songs.randomOrder]

                if (randomOrder >= 0) {

                    Song.findById(songId)?.let { song ->
                        song.randomOrder = -1
                        playSong(song)
                    }
                } else {
                    // There are no more songs left with a positive randomOrder
                    if (reshuffle) {
                        Song.all().forEach { song ->
                            song.resetRandomOrder()
                        }
                    }
                    randomPlay(false) // False prevents infinite recursion.

                }
            }
    }

    /**
     * Reorder the playlist randomly.
     */
    fun requestRandomisePlaylist() {
        request {
            val playlist = Playlist.findCurrent()

            for (entry in playlist.entries) {
                entry.played = false
                entry.orderNumber = Random().nextInt()
            }

            var counter = 1
            for (entry in playlist.entries.sortedBy { it.orderNumber }) {
                entry.orderNumber = counter
                counter++
            }
        }
    }

    /**
     * Add all songs in an album in front of the current entry (or at the start if there is no current entry).
     * If there is a song playing, then it is stopped, but NOT removed from the playlist.
     */
    fun requestPlayAlbum(albumId: Int) {
        request {
            Album.findById(albumId)?.let { album ->
                playSongs(album.songs.sortedBy { it.trackNumber })
            }
        }
    }

    fun requestPlaySong(songId: Int) {
        request {
            Song.findById(songId)?.let { song ->
                playSongs(listOf(song))
            }
        }
    }

    private fun playSong(song: Song) {
        playSongs(listOf(song))
    }

    /**
     * Adds songs to the playlist.
     * If there is no "current entry" (currentEntryId < 0), then the songs are added to the
     * start of the playlist (orderNumber = 1).
     * Otherwise we either add before or after the current entry, depending on [CachedSettings.PLAY_INTERRUPTS].
     *
     * If [CachedSettings.REMOVE_ON_PLAY], then the current entry (if there is one) is removed.
     */
    private fun playSongs(songs: List<Song>) {

        if (songs.isEmpty()) return

        val playlist = Playlist.findCurrent()

        val currentEntry = if (currentEntryId >= 0) Entry.findById(currentEntryId) else null
        if (playInterrupts) {
            currentEntryId = -1
            backend.stop()
        }

        // What will be the orderNumber for the first newly added song?
        val startingOrderNumber = when {
            currentEntry == null -> 1
            removeOnPlay -> currentEntry.orderNumber
            else -> currentEntry.orderNumber + 1
        }

        // How much do we need to increase orderNumbers by to make room for the new entries?
        val spaceRequired = if (currentEntry != null && removeOnPlay) {
            songs.size - 1 // As we will be removing an entry, we need 1 less space.
        } else {
            songs.size
        }

        // Make room for the new orderNumbers for the new items.
        for (entry in playlist.entries) {
            if (entry.orderNumber >= startingOrderNumber) {
                entry.orderNumber += spaceRequired
            }
        }
        var nextOrderNumber = startingOrderNumber
        val newEntries = mutableListOf<Entry>()
        for (song in songs) {

            val newEntry = Entry.new {
                this.playlist = playlist
                this.song = song
                this.orderNumber = nextOrderNumber
                this.played = false
            }
            newEntries.add(newEntry)
            nextOrderNumber++
        }

        // Should we play the song right now?
        if (currentEntry == null || playInterrupts) {
            if (CachedSettings.SHUFFLE.value) {
                playRandomEntry(newEntries)
            } else {
                playEntry(newEntries.first())
            }
        }

        if (removeOnPlay && currentEntry != null) {
            currentEntry.delete()
        }
    }

    /**
     * Add all songs in an album to the end of the playlist.
     * Does not affect the playing song.
     */
    fun requestQueueAlbum(albumId: Int) {
        request {
            Album.findById(albumId)?.let { album ->
                queueSongs(album.songs.sortedBy { it.trackNumber })
            }
        }
    }

    /**
     * Add the song to the end of the playlist.
     * Does not affect the playing song.
     */
    fun requestQueueSong(songId: Int) {
        request {
            Song.findById(songId)?.let { song ->
                queueSongs(listOf(song))
            }
        }
    }

    private fun queueSongs(songs: List<Song>) {
        val playlist = Playlist.findCurrent()

        val lastEntry = playlist.entries.maxByOrNull { it.orderNumber }
        var nextOrderNumber = if (lastEntry == null) 1 else {
            lastEntry.orderNumber + 1
        }

        for (song in songs) {
            Entry.new {
                this.playlist = playlist
                this.song = song
                this.orderNumber = nextOrderNumber
                this.played = false
            }
            nextOrderNumber++
        }
    }

    fun requestPositionTo(seconds: Int) {
        request {
            currentEntry()?.let { entry ->
                if (seconds >= 0 && seconds <= entry.song.seconds) {
                    backend.positionTo(entry.song.file, entry.song.seconds, seconds)
                }
            }
        }
    }

    fun requestSkip(removeCurrentEntry: Boolean) {
        request {
            skip(removeCurrentEntry)
        }
    }

    /**
     * Removes the current entry (if [removeCurrentEntry] ), then
     * moves to the "next" entry in the playlist.
     * "next" will be randomly picked if SHUFFLE setting is on.
     *
     * If we reach the end of the playlist, then check LOOP and RANDOM_PLAY settings.
     */
    private fun skip(removeCurrentEntry: Boolean) {

        val playlist = Playlist.findCurrent()
        val oldEntry = Entry.findById(currentEntryId)
        val currentOrderNumber = oldEntry?.orderNumber ?: Int.MAX_VALUE

        if (removeCurrentEntry) {
            oldEntry?.delete()

            // Decrease the order numbers of entries below the current entry
            for (entry in playlist.entries) {
                if (removeCurrentEntry) {
                    if (entry.orderNumber > currentOrderNumber) {
                        entry.orderNumber--
                    }
                }
            }
        }

        backend.stop()
        currentEntryId = -1

        if (CachedSettings.SHUFFLE.value) {

            playRandomEntry(playlist.entries.toList())

        } else {
            // Move to the NEXT item in the playlist.
            playlist.entries
                .filter { it !== oldEntry && it.orderNumber >= currentOrderNumber }
                .minByOrNull { it.orderNumber }?.let { playEntry(it) }
        }

        // If we reached the end of the playlist, then we won't be playing anything yet.
        if (currentEntryId < 0) {

            // We've reached the end of the playlist.
            if (playlist.entries.count() == 0L) {
                // The playlist is empty
                if (randomPlay) {
                    randomPlay()
                }
            } else {
                if (CachedSettings.LOOP.value) {
                    playlist.entries.minByOrNull { it.orderNumber }?.let { playEntry(it) }
                }
            }
        }
    }

    fun requestClearPlaylist() {
        request {
            backend.stop()
            currentEntryId = -1
            val playlist = Playlist.findCurrent()
            for (entry in playlist.entries) {
                entry.delete()
            }
        }
    }

    fun requestUntickEntry(entryId: Int) {
        request {
            Entry.findById(entryId)?.let { entry ->
                entry.played = false
            }
        }
    }

    fun requestRemoveEntry(entryId: Int) {
        request {
            Entry.findById(entryId)?.let { entry ->
                removeEntry(entry)
            }
        }
    }

    private fun removeEntry(entry: Entry) {
        if (currentEntryId == entry.id.value) {
            skip(false)
        }
        for (other in Playlist.findCurrent().entries) {
            if (other.orderNumber > entry.orderNumber) {
                other.orderNumber--
            }
        }
        entry.delete()
    }


}
