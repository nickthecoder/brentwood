package uk.co.nickthecoder.brentwood.player

import io.ktor.application.*
import io.ktor.features.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import kotlinx.coroutines.Dispatchers
import kotlinx.html.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SortOrder
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.selectAll
import org.jetbrains.exposed.sql.transactions.experimental.newSuspendedTransaction
import uk.co.nickthecoder.brentwood.LayoutComponent
import uk.co.nickthecoder.brentwood.layout.Layout
import uk.co.nickthecoder.brentwood.player.model.*
import uk.co.nickthecoder.brentwood.util.*
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

/**
 * A web based music player. The music is played on the server, and is therefore only useful
 * if the computer hosting the website has speakers attached!
 *
 * This class handles the interactions with Ktor.
 * [Controller] is responsible for maintaining the state of the database, and [Backend] is responsible for
 * playing the actual music.
 *
 * [Backend] notifies [Controller] when a song ends, so that it can advance the playlist, and
 * ask the [Backend] to play the "next" track if there is one.
 *
 * Multiple playlists are supported. However, only one playlist is active at any time.
 * You can NOT edit one playlist, while playing a different one.
 *
 * Do not use this for internet facing web sites without authentication!!!
 *
 * NOTE, Post requests such as "play", "stop" etc ''always'' return Http Status 200 : OK,
 * even if the request ultimately fails.
 * This because these requests are not acted on immediately, and instead are ''queued'' by the [Controller].
 *
 * History
 * -------
 *
 * This is the 3rd incarnation. The first, called ripple, was written in Python, but I was forced to rewrite it
 * when libraries that I relied on were no longer supported.
 *
 * The 2nd incarnation, called tilda, was a stand-alone web site, whereas this version can be incorporated into my
 * existing local intranet website (and can therefore share port 80).
 *
 * Unlike its ancestors, this version requires Javascript, and has a nicer looking UI. The older versions
 * were designed for very early "smart phones", whose browsers were quite limited.
 *
 * The database is almost identical for all three versions.
 */
class Player(

    layout: Layout,
    webPath: String = "/player",
    private val musicPaths: List<File>,
    private val db: Database,
    luceneDir: File,
    private val backend: Backend

) : LayoutComponent(layout, webPath) {

    private val controller = Controller(db, backend)

    private val playerSearch = PlayerSearch(luceneDir)

    init {
        CachedSettings.load(db)
    }

    override fun route(route: Route) {

        with(route) {
            // Resources from the jar file
            staticResources("/resources", this@Player.javaClass.packageName)

            get("/") { call.respondRedirect("$webPath/home") }

            get("/home") { home(call) }
            get("/browse") { browse(call) }
            get("/artist") { artist(call) }
            get("/album") { album(call) }
            get("/history") { history(call) }

            // Actions
            post("/play") { play(call) }
            post("/stop") { stop(call) }
            post("/pause") { pause(call) }
            post("/skip") { skip(call) }
            post("/positionTo/{seconds}") { positionTo(call) }
            post("/removeCurrentEntry") { removeCurrentEntry(call) }
            post("/clearPlaylist") { clearPlaylist(call) }
            post("/randomisePlaylist") { randomisePlaylist(call) }

            post("/playAlbum/{album}") { playAlbum(call) }
            post("/queueAlbum/{album}") { queueAlbum(call) }
            post("/playSong/{song}") { playSong(call) }
            post("/queueSong/{song}") { queueSong(call) }

            post("/removeEntry/{entry}") { removeEntry(call) }
            post("/untickEntry/{entry}") { untickEntry(call) }
            post("/playEntry/{entry}") { playEntry(call) }

            // Settings
            get("/settings") { settings(call) }
            post("/changeSetting/{name}/{value}") { changeSetting(call) }
            get("/playlists") { playlists(call) }
            get("/editPlaylist") { editPlaylist(call) }
            post("/savePlaylist") { savePlaylist(call) }
            post("/deletePlaylist/{id}") { deletePlaylist(call) }
            post("/activatePlaylist/{id}") { activatePlaylist(call) }
            post("/permanentPlaylist/{id}/{value}") { permanentPlaylist(call) }

            // Misc
            get("/search") { search(call) }
            get("/thumbnail/{path...}") { thumbnail(call) }
            get("/cover/{path...}") { cover(call) }

            // Scan
            get("/scan") { scan(call) }
            post("/scan") { beginScan(call) }
        }

    }


    private fun FlowContent.pageTools() {
        script("$webPath/resources/player.js")

        layout.imageTool(this, "$webPath/home") {
            id = "homeLink"
            title = "Home: current song and playlist (Shift+H)"
            img("home", "$webPath/resources/home.png")
        }
        layout.imageTool(this, "$webPath/browse") {
            id = "browseLink"
            title = "Browse (Shift+B)"
            img("skip", "$webPath/resources/browse.png")
        }
        layout.imageTool(this, "#") {
            onClick = "return play();"
            title = "Play (Shift+P)"
            img("play", "$webPath/resources/play.png")
        }
        layout.imageTool(this, "#") {
            onClick = "return pause();"
            title = "Pause (Space)"
            img("pause", "$webPath/resources/pause.png")
        }
        layout.imageTool(this, "#") {
            onClick = "return stop();"
            title = "Stop (Shift+S)"
            img("stop", "$webPath/resources/stop.png")
        }
        layout.imageTool(this, "#") {
            onClick = "return skip();"
            title = "Skip to the next song in the playlist (Shift+N)"
            img("skip", "$webPath/resources/skip.png")
        }
        layout.imageTool(this, "#") {
            onClick = "return removeCurrentEntry();"
            title = "Remove the current song from the playlist (Shift+Minus)"
            img("remove", "$webPath/resources/remove.png")
        }
        layout.imageTool(this, "$webPath/settings") {
            title = "Settings"
            img("settings", "$webPath/resources/settings.png")
        }
        layout.imageTool(this, "$webPath/search") {
            title = "Search"
            img("search", "$webPath/resources/search.png")
        }
    }

    private suspend fun home(call: ApplicationCall) {

        suspendTransaction {

            val playlist = Playlist.findCurrent()
            val currentEntry = controller.currentEntry()

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
                pageTitle {
                    if (currentEntry == null) {
                        + "Music Player"
                    } else {
                        + currentEntry.song.album.artist.name
                        + " : "
                        + currentEntry.song.name
                    }
                }
                pageHeading { + "Music Player" }

                pageTools {
                    pageTools()
                }

                content {
                    div(classes = "noSearch tools") {
                        layout.textTool(this, href = "history") {
                            + "History"
                        }
                        layout.textTool(this, href = "home") {
                            onClick = "return clearPlaylist();"
                            +"Clear Playlist"
                        }
                        layout.textTool(this, href = "home") {
                            onClick = "return randomisePlaylist();"
                            +"Randomise Playlist"
                        }
                    }
                    div(classes = "clear")

                    currentEntry?.let {
                        div(classes = "playerCurrentSong") {
                            div(classes = "playerCover") {
                                a(href = "$webPath/album?id=${currentEntry.song.album.id.value}") {
                                    img(src = "$webPath/thumbnail/${currentEntry.song.album.imageFile}") {
                                        alt = "Album Cover"
                                    }
                                }
                            }
                            div {
                                a(href = "$webPath/album?id=${currentEntry.song.album.id.value}") {
                                    title = "View Album"
                                    +currentEntry.song.album.name
                                }
                            }
                            div{
                                +currentEntry.song.truncatedName
                            }
                            div {
                                a(href = "$webPath/artist?id=${currentEntry.song.album.artist.id.value}") {
                                    title = currentEntry.song.nameExtra("View Artist")
                                    +currentEntry.song.album.artist.name
                                }
                            }
                            div {
                                span {
                                    id = "songTimer"
                                    +currentEntry.song.seconds.secondsToString()
                                }
                            }
                            div {
                                id = "sliderBox"
                                div {
                                    id = "sliderButton"
                                    style = "left: 0px;"
                                    img(src = "$webPath/resources/sliderButton.png")
                                }
                                img( src="$webPath/resources/slider.png" )
                            }
                        }
                    }

                    table(classes = "playerPlaylist") {
                        for (entry in playlist.entries.sortedBy { it.orderNumber }) {
                            val current = if (currentEntry == entry) "playerCurrent" else ""
                            tr(classes = current) {
                                td(classes = "playerOrderNumber") {
                                    +"${entry.orderNumber}"
                                }
                                td(classes = "playerEntryDetails") {
                                    div {
                                        a(href = "$webPath/album?id=${entry.song.album.id.value}") {
                                            title = "View Album"
                                            +entry.song.truncatedName
                                        }
                                        + entry.song.nameExtra("")
                                    }
                                    div {
                                        +entry.song.album.artist.name
                                    }
                                }
                                td(classes = "playerButtons") {
                                    if (CachedSettings.SHUFFLE.value && entry.played) {
                                        a(href = "#") {
                                            title = "Played"
                                            onClick = "return untickEntry(${entry.id.value});"
                                            img("played", "$webPath/resources/tickSmall.png")
                                        }
                                    }
                                    a(href = "#") {
                                        title = "Remove Playlist Entry"
                                        onClick = "return removeEntry(${entry.id.value});"
                                        img("remove", "$webPath/resources/removeSmall.png")
                                    }
                                    a(href = "#") {
                                        onClick = "return playEntry(${entry.id.value});"
                                        img("play", "$webPath/resources/playSmall.png")
                                    }
                                }
                            }
                        }
                    }
                    if (backend.isPlaying()) {
                        inlineScript(
                            """
                            window.addEventListener('load', (event) => {
                               beginPlayPosition( ${backend.songPosition}, ${backend.songDuration} );
                           });
                           """
                        )
                    }

                }
            }
        }
    }

    private suspend fun browse(call: ApplicationCall) {
        suspendTransaction {
            val artists = Artist.all().toList().sortedBy { it.name }

            val groupBy = GroupBy(artists) { it.name.firstOrNull() ?: '?' }

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"Browse" }
                pageHeading { +"Browse" }
                pageTools {
                    pageTools()
                }

                content {

                    groupByLinks(groupBy, layout)

                    for (initial in groupBy.usedInitials()) {
                        groupByAnchor(groupBy, layout, initial)

                        for (artist in groupBy.itemsForInitial(initial)) {
                            div("playerArtist") {
                                h3 {
                                    a("$webPath/artist?id=${artist.id}") {
                                        title = "View Artist"
                                        +artist.name
                                    }
                                }
                                layout.thumbnails(this) {
                                    for (album in artist.albums) {
                                        layout.thumbnail(this,
                                            icons = {
                                                a(href = "$webPath/album?id=${album.id.value}") {
                                                    onClick = "return playAlbum(${album.id});"
                                                    title = "Play Album"
                                                    img("play", "$webPath/resources/playSmall.png")
                                                }
                                            }, main = {

                                                a("$webPath/album?id=${album.id.value}") {
                                                    onClick = "return queueAlbum(${album.id});"
                                                    title = "Click to Queue"
                                                    img(
                                                        "cover",
                                                        "$webPath/thumbnail${album.imageFile}"
                                                    )
                                                }
                                            }, label = {
                                                a("$webPath/album?id=${album.id}") {
                                                    title = "View Album"
                                                    +album.name
                                                }
                                            }
                                        )
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private suspend fun artist(call: ApplicationCall) {
        val artistId = call.parameters["id"]?.toIntOrNull() ?: throw NotFoundException()

        suspendTransaction {

            val artist = Artist.findById(artistId) ?: throw NotFoundException("Artist $artistId")

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +artist.name }
                pageHeading { +artist.name }

                pageTools {
                    pageTools()
                }

                content {

                    for (album in artist.albums) {
                        albumDetails(album, isAlone = false)
                    }

                }
            }
        }
    }

    private suspend fun album(call: ApplicationCall) {
        val albumId = call.parameters["id"]?.toIntOrNull() ?: throw NotFoundException()

        suspendTransaction {

            val album = Album.findById(albumId) ?: throw NotFoundException("Album $albumId")

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +album.name }
                pageHeading {
                    +album.name
                    +" "
                    a("#") {
                        onClick = "return playAlbum(${album.id.value});"
                        title = "Play Album"
                        img(alt = "play", src = "$webPath/resources/playSmall.png")
                    }
                    +" "
                    a("#") {
                        onClick = "return queueAlbum(${album.id.value});"
                        title = "Queue Album"
                        img(alt = "queue", src = "$webPath/resources/queueSmall.png")
                    }

                }
                pageTools {
                    pageTools()
                }

                content {
                    albumDetails(album, isAlone = true)
                }
            }
        }
    }

    private fun FlowContent.albumDetails(album: Album, isAlone: Boolean) {

        if (isAlone) {
            h2 {
                a(href = "$webPath/artist?id=${album.artist.id.value}") { +album.artist.name }
            }
        } else {
            h2 {
                a(href = "$webPath/album?id=${album.id.value}") { +album.name }
                +" "
                a("#") {
                    onClick = "return playAlbum(${album.id.value});"
                    title = "Play Album"
                    img(alt = "play", src = "$webPath/resources/playSmall.png")
                }
                +" "
                a("#") {
                    onClick = "return queueAlbum(${album.id.value});"
                    title = "Queue Album"
                    img(alt = "queue", src = "$webPath/resources/queueSmall.png")
                }
            }
        }


        div(classes = "albumCover") {
            a("#") {
                onClick = "return playQueueAlbum(${album.id});"
                title = "Click to Queue Album"
                img(
                    "cover",
                    "$webPath/cover${album.imageFile}"
                )
            }
        }
        div(classes = "trackListing") {
            ol {
                for (song in album.songs.sortedBy { it.trackNumber }) {
                    li {
                        a(href = "#") {
                            title = "Click to Play Track"
                            onClick = "return playSong(${song.id.value});"
                            img("play", src = "$webPath/resources/playSmall.png")
                        }
                        +" "
                        a(classes = "alt", href = "#") {
                            title = song.nameExtra("Click to Queue")
                            onClick = "return queueSong(${song.id.value});"
                            +song.truncatedName
                        }
                    }
                }
            }

        }
    }


    private suspend fun history(call: ApplicationCall) {

        suspendTransaction {
            val history = Song.find { Songs.lastPlayed.isNotNull() }
                .orderBy(Pair(Songs.lastPlayed, SortOrder.DESC))
                .limit(100).toList()

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"History" }
                pageHeading { +"History" }
                pageTools {
                    pageTools()
                }

                content {

                    table(classes = "playerPlaylist") {
                        tr {
                            th {
                                style = "width: 10%;"
                                +"Last Played"
                            }
                            th { +"Song" }
                        }
                        for (song in history) {
                            tr {
                                td(classes = "playerOrderNumber") { song.lastPlayed.let { if (it != null) +it.toNiceString() } }
                                td {
                                    div {
                                        a(href = "$webPath/album?id=${song.album.id.value}") {
                                            title = song.nameExtra("View Album")
                                            +song.truncatedName
                                        }
                                    }
                                    div { +song.album.artist.name }
                                }
                            }
                        }
                    }

                }
            }
        }
    }

    private suspend fun play(call: ApplicationCall) {
        if (backend.backendState == BackendState.PAUSED) {
            backend.unPause()
        } else {
            controller.requestPlay()
        }
        call.respondText("ok")
    }

    /**
     * Stop the currently playing song (if there is one).
     * Has no effect on the playlist, or current playlist entry.
     */
    private suspend fun stop(call: ApplicationCall) {
        backend.stop()
        call.respondText("ok")
    }

    /**
     * Pause/Unpause the currently playing song.
     * Has no effect on the playlist. The position within the song is unchanged, and
     * un-pausing will play the song from the position it was paused.
     *
     * However, if the backend is stopped or idle, then this acts the same as [play],
     * and therefore the current playlist item does change, and the playlist can change
     * (random play will add to the playlist).
     */
    private suspend fun pause(call: ApplicationCall) {
        if (backend.isPaused()) {
            backend.unPause()
            call.respondText("ok")
        } else if (backend.isPlaying()) {
            backend.pause()
            call.respondText("ok")
        } else {
            play(call)
        }
    }

    /**
     * Stop playing the current entry, and begin playing the next entry in the playlist.
     * The old entry may be deleted depending on Playlist.isPermanent and Option.skipDeletes.
     */
    private suspend fun skip(call: ApplicationCall) {
        controller.requestSkip(false)
        call.respondText("ok")
    }

    private suspend fun positionTo(call: ApplicationCall) {
        val seconds = call.parameters["seconds"]?.toIntOrNull() ?: throw NotFoundException()

        controller.requestPositionTo(seconds)
        call.respondText("ok")
    }

    private suspend fun removeCurrentEntry(call: ApplicationCall) {
        controller.requestSkip(true)
        call.respondText("ok")
    }

    private suspend fun clearPlaylist(call: ApplicationCall) {
        controller.requestClearPlaylist()
        call.respondText("ok")
    }

    private suspend fun randomisePlaylist(call: ApplicationCall) {
        controller.requestRandomisePlaylist()
        call.respondText("ok")
    }

    private suspend fun playAlbum(call: ApplicationCall) {
        val albumId = call.parameters["album"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestPlayAlbum(albumId)
        call.respondText("ok")
    }

    private suspend fun queueAlbum(call: ApplicationCall) {
        val albumId = call.parameters["album"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestQueueAlbum(albumId)
        call.respondText("ok")
    }

    private suspend fun playSong(call: ApplicationCall) {
        val songId = call.parameters["song"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestPlaySong(songId)
        call.respondText("ok")
    }

    private suspend fun queueSong(call: ApplicationCall) {
        val songId = call.parameters["song"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestQueueSong(songId)
        call.respondText("ok")
    }

    private suspend fun removeEntry(call: ApplicationCall) {
        val entryId = call.parameters["entry"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestRemoveEntry(entryId)
        call.respondText("ok")
    }

    private suspend fun untickEntry(call: ApplicationCall) {
        val entryId = call.parameters["entry"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestUntickEntry(entryId)
        call.respondText("ok")
    }

    private suspend fun playEntry(call: ApplicationCall) {
        val entryId = call.parameters["entry"]?.toIntOrNull() ?: throw NotFoundException()
        controller.requestPlayEntry(entryId)
        call.respondText("ok")
    }

    /**
     * Before streaming the contents of the file, checks that [file] is within one of the [musicPaths],
     * and there is no "/../" (which may allow a sneaky person to escape out of [musicPaths]).
     * Also, don't allow windows style directory separators (\).
     */
    private suspend fun respondFileSafe(call: ApplicationCall, file: File) {
        val path = file.path
        if (path.contains("/../")) throw NotFoundException()
        if (path.contains("\\")) throw NotFoundException()

        if (file.exists() && file.isFile) {
            for (base in musicPaths) {
                if (file.path.startsWith(base.path)) {
                    call.respondFile(file)
                    return
                }
            }
        }
        throw NotFoundException(file.path)
    }

    private suspend fun thumbnail(call: ApplicationCall) {
        val path = call.parameters.getAll("path")!!.joinToString(prefix = "/", separator = "/")
        val thumbnail = File(File(path).parentFile, "cover_100.jpg")
        respondFileSafe(call, thumbnail)
    }

    private suspend fun cover(call: ApplicationCall) {
        val path = call.parameters.getAll("path")!!.joinToString(prefix = "/", separator = "/")
        respondFileSafe(call, File(path))
    }

    private suspend fun scan(call: ApplicationCall) {

        suspendTransaction {

            val artists = Artists.selectAll().count()
            val albums = Albums.selectAll().count()
            val songs = Songs.selectAll().count()

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {
                pageTitle { +"Scan" }
                pageHeading { +"Scan" }
                pageTools {
                    pageTools()
                }

                content {
                    h3 { + "Database Contains : " }
                    ul {
                        li { +"Artists : $artists" }
                        li { +"Albums : $albums" }
                        li { +"Songs : $songs" }
                    }
                    if (Scanner.isScanning()) {
                        h2 { +"Scanning" }
                        + "Files scanned : ${Scanner.scanCount}"
                    } else {
                        form(action = "$webPath/scan", method = FormMethod.post) {
                            input(InputType.submit, name = "scan", classes = "button ok") { value = "Scan" }
                        }
                    }
                }
            }
        }
    }

    private suspend fun beginScan(call: ApplicationCall) {
        Scanner.scan(musicPaths, playerSearch, true)
        call.respondRedirect("$webPath/scan", false)
    }

    private suspend fun settings(call: ApplicationCall) {

        call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

            pageTitle { +"Settings" }
            pageHeading { +"Settings" }
            pageTools {
                pageTools()
            }

            content {
                div(classes = "noSearch tools") {
                    layout.textTool(this, href = "history") { +"History" }
                    layout.textTool(this, href = "scan") { +"Scan for new Music" }
                    layout.textTool(this, href = "playlists") { +"Edit Playlists" }
                }
                div(classes = "clear") {}

                div(classes = "playerSettings") {
                    table {
                        for (knownSetting in CachedSettings.values()) {
                            tr {
                                td {
                                    checkBoxInput {
                                        onClick = "changeSetting( '${knownSetting.name}', this );"
                                        checked = knownSetting.value
                                    }
                                }
                                td { +knownSetting.humanName }
                                td { +knownSetting.label }
                            }
                        }
                    }

                }

            }
        }

    }

    private suspend fun changeSetting(call: ApplicationCall) {
        val name = call.parameters["name"] ?: throw NotFoundException()
        val value = call.parameters["value"]?.toBoolean() ?: throw NotFoundException()

        suspendTransaction {
            CachedSettings.update(name, value)
        }
        call.respondText("ok")
    }

    private suspend fun search(call: ApplicationCall) {

        val queryText = call.parameters["q"]
        val pageNumber = call.parameters["page"]?.toIntOrNull() ?: 1

        suspendTransaction {

            val searchResults = queryText?.let { playerSearch.search(it, 100) }
            val pager = searchResults?.let { ListPager("search", it, 20) }

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"Search" }
                pageHeading { +"Search" }
                pageTools {
                    pageTools()
                }

                content {
                    div(classes = "playerSearch") {
                        form {
                            searchInput(name = "q") {
                                placeholder = "search"
                                size = "50"
                                autoFocus = true
                                value = queryText ?: ""
                            }
                            +" "
                            submitInput(classes = "button ok", name = "go") {
                                value = "Search"
                            }
                        }
                    }
                    pager?.let { pager ->
                        table(classes = "playerSearch") {
                            for (result in pager.pageItems(pageNumber)) {
                                tr {

                                    td(classes="playerType") {
                                        +result.type
                                    }

                                    result.song?.let { song ->
                                        td(classes = "playerButtons") {
                                            a(href = "#") {
                                                title = "play"
                                                onClick = "return playSong(${song.id.value});"
                                                img("remove", "$webPath/resources/playSmall.png")
                                            }
                                            a(href = "#") {
                                                title = "queue"
                                                onClick = "return queueSong(${song.id.value});"
                                                img("play", "$webPath/resources/queueSmall.png")
                                            }
                                        }
                                        td(classes = "playerResultDetails") {
                                            div {
                                                +song.truncatedName
                                            }
                                            div {
                                                a(href = "album?id=${song.album.id.value}") { +song.album.name }
                                            }
                                            div {
                                                +song.album.artist.name
                                            }
                                        }
                                    }

                                    result.album?.let { album ->
                                        td(classes = "playerButtons") {
                                            a(href = "#") {
                                                title = "play"
                                                onClick = "return playAlbum(${album.id.value});"
                                                img("remove", "$webPath/resources/playSmall.png")
                                            }
                                            a(href = "#") {
                                                title = "queue"
                                                onClick = "return queueAlbum(${album.id.value});"
                                                img("play", "$webPath/resources/queueSmall.png")
                                            }
                                        }
                                        td(classes = "playerResultDetails") {
                                            div {
                                                a(href = "album?id=${album.id.value}") { +album.name }
                                            }
                                            div {
                                                +album.artist.name
                                            }
                                        }
                                    }

                                    result.artist?.let { artist ->
                                        td(classes = "playerButtons") {
                                        }
                                        td(classes = "playerResultDetails") {
                                            div {
                                                a(href = "artist?id=${artist.id.value}") { +artist.name }
                                            }
                                        }
                                    }

                                }
                            }
                        }

                        layout.pager(this, pager, pageNumber)
                    }
                }
            }
        }
    }

    private suspend fun playlists(call: ApplicationCall) {

        suspendTransaction {

            val playlists = Playlist.all()

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"Playlists" }
                pageHeading { +"Playlists" }
                pageTools {
                    pageTools()
                }

                content {
                    table {
                        tr {
                            th { +"Active?" }
                            th { +"Name" }
                            th { +"Is Permanent" }
                            th { }
                        }
                        for (playlist in playlists) {
                            tr {
                                td {
                                    radioInput(name = "isActive") {
                                        onClick = "return activatePlaylist( ${playlist.id.value} );"
                                        checked = playlist.isActive
                                    }
                                }
                                td {
                                    a(href = "editPlaylist?id=${playlist.id}") {
                                        +playlist.name
                                    }
                                }
                                td {
                                    checkBoxInput {
                                        onClick = "permanentPlaylist(${playlist.id.value},this);"
                                        checked = playlist.isPermanent
                                    }
                                }
                                td {
                                    buttonInput(classes = "button delete") {
                                        value = "Delete"
                                        onClick = "deletePlaylist(${playlist.id.value});"
                                    }
                                }
                            }
                        }
                    }

                    h3 {
                        a(href = "editPlaylist") {
                            +"Add New Playlist"
                        }
                    }
                }

            }
        }
    }

    private suspend fun activatePlaylist(call: ApplicationCall) {
        val id = call.parameters["id"]?.toIntOrNull() ?: throw NotFoundException()

        suspendTransaction {
            val playlist = Playlist.findById(id) ?: throw NotFoundException()
            playlist.isActive = true
            for (other in Playlist.find { (Playlists.isActive eq true) and (Playlists.id neq id) }) {
                other.isActive = false
            }
        }
        call.respondText("ok")

    }

    private suspend fun permanentPlaylist(call: ApplicationCall) {
        val id = call.parameters["id"]?.toIntOrNull() ?: throw NotFoundException()
        val value = call.parameters["value"] == "true"

        suspendTransaction {
            val playlist = Playlist.findById(id) ?: throw NotFoundException()
            playlist.isPermanent = value
        }
        call.respondText("ok")

    }

    private suspend fun editPlaylist(call: ApplicationCall) {
        val id = call.parameters["id"]?.toIntOrNull()

        suspendTransaction {

            val playlist = id?.let { Playlist.findById(id) ?: throw NotFoundException() }

            call.respondHtmlTemplate2(layout.createTemplate(true, call.request.uri)) {

                pageTitle { +"Playlists" }
                pageHeading { +"Playlists" }
                pageTools {
                    pageTools()
                }

                content {
                    form(action = "savePlaylist", method = FormMethod.post) {
                        hiddenInput(name = "id") { value = id?.toString() ?: "NEW" }
                        table(classes = "data") {
                            tr {
                                th { +"Name" }
                                td {
                                    input(name = "name") { value = playlist?.name ?: "" }
                                }
                            }
                            tr {
                                th { +"Is Permanent" }
                                td {
                                    checkBoxInput(name = "isPermanent") {
                                        checked = playlist?.isPermanent ?: false
                                        value = "true"
                                    }
                                    +" (If set, songs will not be removed when they finish playing)"
                                }
                            }
                        }
                        div(classes = "buttons") {
                            submitInput(classes = "button ok") { value = "Save" }
                        }
                    }

                }
            }
        }
    }

    private suspend fun savePlaylist(call: ApplicationCall) {
        val parameters = call.receiveParameters()
        suspendTransaction {

            if (parameters["id"] == "NEW") {
                Playlist.new {
                    name = parameters["name"] ?: "unknown"
                    isActive = false
                    loop = parameters["loop"] == "true"
                    isPermanent = parameters["isPermanent"] == "true"
                }
            } else {

                val id = parameters["id"]?.toIntOrNull() ?: throw NotFoundException()
                with(Playlist.findById(id) ?: throw NotFoundException()) {
                    name = parameters["name"] ?: "unknown"
                    loop = parameters["loop"] == "true"
                    isPermanent = parameters["isPermanent"] == "true"
                }

            }
        }

        call.respondRedirect("$webPath/playlists")
    }

    private suspend fun deletePlaylist(call: ApplicationCall) {
        val id = call.parameters["id"]?.toIntOrNull() ?: throw NotFoundException()
        suspendTransaction {
            Playlist.findById(id)?.delete()
            call.respondText("ok")
        }
    }

    private suspend fun <T> suspendTransaction(block: suspend () -> T): T =
        newSuspendedTransaction(Dispatchers.IO, db = db) {
            block()
        }

}

fun Int.secondsToString(): String {
    val mins = this / 60
    val secs = this - mins * 60
    return if (secs < 10) {
        "$mins:0$secs"
    } else {
        "$mins:$secs"
    }
}

fun LocalDateTime.toNiceString(): String {
    val now = LocalDateTime.now()
    return if (now.year == year) {
        if (now.dayOfYear == dayOfYear) {
            format(hoursMinutesFormat)
        } else {
            format(yearMonthDayFormat)
        }
    } else {
        format(yearMonthDayFormat)
    }
}


val hoursMinutesFormat = DateTimeFormatter.ofPattern("HH:mm")
val yearMonthDayFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")
