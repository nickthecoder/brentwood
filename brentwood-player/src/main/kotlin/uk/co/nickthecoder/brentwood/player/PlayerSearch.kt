package uk.co.nickthecoder.brentwood.player

import org.apache.lucene.analysis.en.EnglishAnalyzer
import org.apache.lucene.document.Document
import org.apache.lucene.document.Field
import org.apache.lucene.document.StoredField
import org.apache.lucene.document.TextField
import org.apache.lucene.index.DirectoryReader
import org.apache.lucene.index.IndexWriter
import org.apache.lucene.index.IndexWriterConfig
import org.apache.lucene.index.Term
import org.apache.lucene.queryparser.classic.QueryParser
import org.apache.lucene.search.IndexSearcher
import org.apache.lucene.search.TermQuery
import org.apache.lucene.store.Directory
import org.apache.lucene.store.SimpleFSDirectory
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.player.model.*
import java.io.File

class PlayerSearch(directory: File) {

    private val analyzer = EnglishAnalyzer() //StandardAnalyzer()
    private val index: Directory = SimpleFSDirectory(directory.toPath())

    private var indexReader: DirectoryReader? = null
    private var searcher: IndexSearcher? = null

    fun reader(): DirectoryReader {
        if (indexReader == null) {
            indexReader = DirectoryReader.open(index)
            searcher = IndexSearcher(indexReader)
        }
        return indexReader!!
    }

    fun searcher(): IndexSearcher {
        reader()
        return searcher!!
    }

    init {

        // It the database doesn't exist, this creates it!
        open().close()

        if (reader().numDocs() == 0) {
            addFromDB()
        }
    }


    fun open(): Writer = Writer()

    fun search(searchString: String, maxHits: Int): List<SearchResult>? {
        if (searchString.length < 2) return null

        val query = QueryParser("name", analyzer).parse(searchString)
        val topDocs = searcher().search(query, maxHits)

        return topDocs.scoreDocs.map { transaction { SearchResult(searcher().doc(it.doc)) } }
    }

    private fun closeSearcher() {
        indexReader?.close()
        indexReader = null
        searcher = null
    }

    fun rebuild() {
        closeSearcher()

        val config = IndexWriterConfig(analyzer)
        val writer = IndexWriter(index, config)
        writer.deleteAll()
        writer.commit()
        writer.close()

        addFromDB()
    }

    private fun addFromDB() {
        val writer = open()
        try {
            transaction {
                Artist.all().forEach { artist ->
                    artist.albums.forEach { album ->
                        album.songs.forEach { song ->
                            writer.addSong(song)
                        }
                        writer.addAlbum(album)
                    }
                    writer.addArtist(artist)
                    writer.commit()
                }
            }
        } finally {
            writer.close()
            closeSearcher()

        }
    }

    class SearchResult(doc: Document) {

        val name: String = doc.get("name")

        val id: Int = doc.getField("id").numericValue().toInt()

        val type: String

        init {
            val key = doc.get("key")
            val hash = key?.indexOf("#") ?: -1
            type = if (hash > 0) {
                key.substring(0, hash)
            } else {
                ""
            }
        }

        val song: Song? = if (type == "Song") {
            Song.findById(id)
        } else {
            null
        }

        val album: Album? = if (type == "Album") {
            Album.findById(id)
        } else {
            null
        }

        val artist: Artist? = if (type == "Artist") {
            Artist.findById(id)
        } else {
            null
        }

    }

    inner class Writer {

        val config = IndexWriterConfig(analyzer)
        private val writer = IndexWriter(index, config)

        private fun updateOrDelete(doc: Document) {
            val term = Term("key", doc.get("key"))
            val query = TermQuery(term)

            val existing = searcher().search(query, 1).scoreDocs.firstOrNull()?.let { searcher().doc(it.doc) }
            if (existing != null) {
                if (existing.get("name") != doc.get("name")) {
                    writer.updateDocument(term, doc)
                }
            } else {
                writer.addDocument(doc)
            }
        }

        fun addSong(song: Song) {
            val doc = Document()
            doc.add(StoredField("key", "Song#${song.id.value}"))
            doc.add(StoredField("id", song.id.value))
            doc.add(TextField("name", song.name, Field.Store.YES))

            updateOrDelete(doc)
        }

        fun addAlbum(album: Album) {
            val doc = Document()
            doc.add(StoredField("key", "Album#${album.id.value}"))
            doc.add(StoredField("id", album.id.value))
            doc.add(TextField("name", album.name, Field.Store.YES))

            updateOrDelete(doc)
        }

        fun addArtist(artist: Artist) {
            val doc = Document()
            doc.add(StoredField("key", "Artist#${artist.id.value}"))
            doc.add(StoredField("id", artist.id.value))
            doc.add(TextField("name", artist.name, Field.Store.YES))

            updateOrDelete(doc)
        }

        fun deleteSong(song: Song) {
            val term = Term("key", "Song#${song.id.value}")
            writer.deleteDocuments(term)
        }

        fun deleteAlbum(album: Album) {
            val term = Term("key", "Album#${album.id.value}")
            writer.deleteDocuments(term)
        }

        fun deleteArtist(artist: Artist) {
            val term = Term("key", "Artist#${artist.id.value}")
            writer.deleteDocuments(term)
        }

        fun commit() {
            writer.commit()
        }

        fun close() {
            writer.commit()
            writer.close()
            closeSearcher()
        }
    }

}
