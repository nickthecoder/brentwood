package uk.co.nickthecoder.brentwood.player

import java.io.File
import java.util.*
import java.util.concurrent.TimeUnit


/**
 * Spawns a new process running a command, such as "play".
 */
abstract class ProcessBackend : AbstractBackend() {

    private var playerThread: PlayerThread? = null

    private var playingFile: File? = null

    protected abstract fun processBuilder(file: File, startSeconds: Int): ProcessBuilder

    protected open fun reportNonZeroExitStatus(exitStatus: Int) {
        println("Failed to play - exit code $exitStatus")
    }

    override fun play(file: File, durationSeconds: Int) {
        if (isPlaying() || isPaused()) {
            stop()
        }

        backendState = BackendState.PLAYING
        songDuration = durationSeconds
        playingFile = file

        playerThread = PlayerThread(file, 0).apply { start() }
    }

    override fun positionTo(file: File, durationSeconds: Int, fromSeconds: Int) {
        if (isPlaying() || isPaused()) {
            playerThread?.let {
                // Prevents the Controller being notified of BackendState.IDLE
                // when the process ends.
                playerThread = null

                it.stopPlaying()
            }
        }
        songDuration = durationSeconds
        playingFile = file

        playerThread = PlayerThread(file, fromSeconds).apply { start() }
        backendState = BackendState.PLAYING

        startTime = Date().time - fromSeconds * 1000L
        pausedMillis = 0
    }

    override fun stop() {
        if (backendState == BackendState.PLAYING || backendState == BackendState.PAUSED) {
            playerThread?.let {
                backendState = BackendState.STOPPED
                it.stopPlaying()
            }
        }
    }

    override fun pause() {
        if (backendState == BackendState.PLAYING) {
            playerThread?.let {
                backendState = BackendState.PAUSED
                it.pause()
            }
        }
    }

    override fun unPause() {
        if (backendState == BackendState.PAUSED) {
            playerThread?.let {
                backendState = BackendState.PLAYING
                it.unPause()
            }
        }
    }

    /**
     *
     */
    inner class PlayerThread(val file: File, private val startSeconds: Int) : Thread("PlayerThread") {

        init {
            isDaemon = true
        }

        /**
         * The process which is playing the music
         */
        private lateinit var process: Process

        /**
         * If the player reports a non-zero exit status, we don't care about it if it was because
         * we killed it.
         */
        private var isStopping = false

        override fun run() {

            process = processBuilder(file, startSeconds).start()

            val exitStatus = process.waitFor()
            if (!isStopping && exitStatus != 0) {
                reportNonZeroExitStatus(exitStatus)
            }

            // Update the backendStatus, but not if another PlayerThread has already been started.
            if (playerThread === this) {
                backendState = BackendState.IDLE
            }
        }

        fun pause() {
            if (process.isAlive) {
                process.sigstop()
            }
        }

        fun unPause() {
            if (process.isAlive) {
                process.sigcont()
            }
        }

        fun stopPlaying() {
            isStopping = true
            if (process.isAlive) {
                process.sigint()
            }
            if (process.isAlive) {

                Thread().apply { name = "Kill_Sox" }.run {
                    // Give it 2 seconds to stop, but if that fails, then forcibly kill it
                    process.waitFor(2, TimeUnit.SECONDS)
                    if (process.isAlive) {
                        process.sigkill()
                    }
                }

            }

        }
    }

}
