package uk.co.nickthecoder.brentwood.player

import org.jaudiotagger.audio.AudioFileIO
import org.jaudiotagger.tag.FieldKey
import org.jetbrains.exposed.sql.transactions.transaction
import uk.co.nickthecoder.brentwood.player.model.*
import java.io.File
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.logging.Level
import java.util.logging.Logger

enum class ScannerState { IDLE, SCANNING }

object Scanner {

    /**
     * The count of the files scanned. Final total is the number of songs, plus the number of album covers.
     */
    var scanCount = 0
        private set

    private val musicExtensions = mutableListOf("mp3", "wav", "flac", "ogg")

    private val imageExtensions = mutableListOf("jpg", "png", "gif")

    private val preferredImages = mutableListOf(
        File(".meta${File.separator}cover_400.jpg"),
        File("cover.jpg")
    )

    private var state: ScannerState = ScannerState.IDLE

    // Ensure that the logger isn't gc'd, otherwise setting Level.OFF may be lost.
    // See https://stackoverflow.com/questions/40923754/selenium-many-logs-how-to-remove/40934452#40934452
    private val jAudioLogger = Logger.getLogger("org.jaudiotagger")

    private var searchWriter: PlayerSearch.Writer? = null

    private var privateLatestDir: File? = null

    init {
        jAudioLogger.level = Level.OFF
    }

    fun isScanning(): Boolean = state == ScannerState.SCANNING

    fun scan(musicPaths: List<File>, playerSearch: PlayerSearch, purge: Boolean = false) {

        if (state == ScannerState.IDLE) {
            state = ScannerState.SCANNING
            Thread {
                scanCount = 0
                try {
                    doScan(musicPaths, playerSearch)
                    if (purge) {
                        purge()
                    }

                    playerSearch.rebuild()

                } finally {
                    state = ScannerState.IDLE
                    scanCount = 0
                }
            }.start()
        }
    }

    private fun doScan(musicPaths: List<File>, playerSearch: PlayerSearch) {
        searchWriter = playerSearch.open()

        try {
            for (musicPath in musicPaths) {
                scanDirectory(musicPath)
            }

            // Look for images for each album
            transaction {
                Album.all().forEach { album ->
                    var directory: File? = null
                    album.songs.forEach { song ->
                        val songDir = song.file.parentFile
                        if (songDir != directory) {
                            directory = songDir
                            album.imageFile = findImage(songDir)
                            scanCount ++
                        }
                    }
                }
            }

            // Now check every song, to see if the file exists, and tag it as not "available" it if it doesn't.
            // Do one artists at a time, so that there isn't too many items per transaction.
            transaction {
                Artist.all().toList()
            }.forEach { fromList ->
                transaction {
                    Artist.findById(fromList.id.value)?.let { artist ->
                        var hasAlbum = false
                        artist.albums.forEach { album ->
                            var hasSong= false
                            album.songs.forEach { song ->
                                song.isAvailable = song.file.exists()
                                hasSong = hasSong || song.isAvailable
                                if (!song.isAvailable) {
                                    searchWriter?.deleteSong(song)
                                }
                            }
                            album.isAvailable = hasSong
                            hasAlbum = hasAlbum || album.isAvailable
                            if (!album.isAvailable) {
                                searchWriter?.deleteAlbum(album)
                            }
                        }
                        artist.isAvailable = hasAlbum
                        if (!artist.isAvailable) {
                            searchWriter?.deleteArtist(artist)
                        }
                    }
                }
            }
        } catch (e: Exception) {
            //Tilda.logger.log(Level.SEVERE, "Scan failed")
            e.printStackTrace()
        } finally {
            searchWriter?.commit()
            searchWriter?.close()
        }

    }

    /**
     * Permanently deletes database rows where "available" is false.
     * Therefore any songs/artist/albums that have been deleted from the file system,
     * will be removed from the database also.
     * Without a purge, the rows would only be marked as not "available" by the scan.
     * The user interface would look the same (but may be a little slower without a purge).
     */
    private fun purge() {
        transaction {
            Song.find { Songs.isAvailable eq false }.forEach { it.delete() }
        }
        transaction {
            Album.find { Albums.isAvailable eq false }.forEach { it.delete() }
        }
        transaction {
            Artist.find { Artists.isAvailable eq false }.forEach { it.delete() }
        }
    }

    /**
     * Looks for an image for this album.
     */
    private fun findImage(directory: File): File? {
        for (f in preferredImages) {
            val file = File(directory, f.path)
            if (file.exists()) return file
        }
        directory.listFiles()?.let {
            it.forEach { child ->
                if (imageExtensions.contains(child.extension.lowercase())) {
                    return child
                }
            }
        }
        return null
    }

    private fun scanDirectory(dir: File) {

        privateLatestDir = dir
        dir.listFiles()?.let { filesAndDirs ->

            val files = filesAndDirs.filter { it.isFile }
            if (files.isNotEmpty()) {
                transaction {
                    files.forEach { child ->
                        val ext = child.extension.lowercase()
                        if (musicExtensions.contains(ext)) {
                            scanCount++
                            scanFile(child)
                        }
                    }
                }
            }

            filesAndDirs.filter { it.isDirectory && !it.isHidden }.sortedBy { it.name }.forEach {
                scanDirectory(it)
            }
        }
    }

    private fun scanFile(file: File) {

        val now = LocalDateTime.now()
        val fileDate = LocalDateTime.ofInstant(Instant.ofEpochMilli(file.lastModified()), ZoneId.systemDefault())

        var song = Song.find { Songs.path eq file.path }.firstOrNull()
        if (song != null && song.lastScanned.isAfter(fileDate)) {
            // No need to re-scan it
            song.isAvailable = true
            song.lastModified = now
            return
        }


        val f = try {
            AudioFileIO.read(file)
        } catch (e: Exception) {
            song?.isAvailable = false
            song?.lastModified = now
            return
        }
        val tag = f.tag
        val header = f.audioHeader

        val trackLength = default(0) { header.trackLength }
        val trackNumber = default(0) { tag.getFirst(FieldKey.TRACK).toInt() }

        val artistName = default(guessArtistName(file)) { tag.getFirst(FieldKey.ARTIST) }
        val albumName = default(guessAlbumName(file)) { tag.getFirst(FieldKey.ALBUM) }
        val songName = default(guessSongName(file)) { tag.getFirst(FieldKey.TITLE) }

        val album = findOrCreateAlbum(artistName, albumName)

        if (song == null) {
            song = Song.new {
                this.album = album
                name = songName.maxLength(100)
                this.trackNumber = trackNumber
                seconds = trackLength
                this.file = file
                lastModified = now
                lastScanned = now
                isAvailable = true
                resetRandomOrder()
            }
        } else {

            with(song) {
                this.album = album
                name = songName.maxLength(100)
                this.trackNumber = trackNumber
                seconds = trackLength
                this.file = file
                lastModified = now
                lastScanned = now
                isAvailable = true
                resetRandomOrder()
            }
        }

        searchWriter?.addSong(song)
        searchWriter?.commit()

    }

    private fun findOrCreateAlbum(artistName: String, albumName: String): Album {

        var artist = Artist.find { Artists.name eq artistName }.firstOrNull()
        if (artist == null) {
            artist = Artist.new {
                name = artistName.maxLength(100)
                isAvailable = true
            }
            searchWriter?.addArtist(artist)
        }

        var album = artist.albums.firstOrNull { it.name == albumName }
        if (album == null) {
            album = Album.new {
                this.artist = artist
                name = albumName.maxLength(100)
                isAvailable = true
                imagePath = null
            }
            searchWriter?.addAlbum(album)
        }

        return album
    }

    private fun guessArtistName(file: File): String {
        return file.parentFile.parentFile.name
    }

    private fun guessAlbumName(file: File): String {
        return file.parentFile.name
    }

    private fun guessSongName(file: File): String {
        return file.nameWithoutExtension
    }
}

private fun <T> default(default: T, value: () -> T): T {
    return try {
        value()
    } catch (e: Exception) {
        default
    }
}

private fun default(default: String, value: () -> String): String {
    return try {
        val str = value()
        return str.ifBlank { default }
    } catch (e: Exception) {
        default
    }
}

private fun String.maxLength(l: Int, suffix: String = "…"): String {
    return if (this.length > l) {
        return this.substring(0, l - suffix.length) + suffix
    } else {
        this
    }
}
