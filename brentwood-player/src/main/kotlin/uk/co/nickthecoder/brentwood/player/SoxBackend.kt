package uk.co.nickthecoder.brentwood.player

import java.io.File


class SoxBackend : ProcessBackend() {

    init {
        // Stop eny music currently playing.
        // Useful when restarting the web server, otherwise, you will get two songs playing at once!
        ProcessBuilder("killall", "play").start()
    }

    override fun processBuilder(file: File, startSeconds: Int) =
        if (startSeconds <= 0) {
            ProcessBuilder("play", "-q", file.absolutePath)
        } else {
            ProcessBuilder("play", "-q", file.absolutePath, "trim", "=$startSeconds")
        }

    override fun reportNonZeroExitStatus(exitStatus: Int) {
        // The most common cause of problems with sox is not having the appropriate libraries
        // installed for the file type being played.
        if (exitStatus == 2) {
            println("SoxBackend. play exit status 2. Ensure that you have installed libsox-fmt-all")
        } else if (exitStatus == 137) {
            // Ignore this, it's from being killed.
        } else {
            super.reportNonZeroExitStatus(exitStatus)
        }
    }

}
