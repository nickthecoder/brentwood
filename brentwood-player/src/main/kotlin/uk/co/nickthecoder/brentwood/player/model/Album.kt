package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import java.io.File

object Albums : IntIdTable(name = "album") {
    val name = varchar("name", 100).index()
    val isAvailable = bool("isAvailable")
    val imagePath = varchar("imagePath", 500).nullable()

    val artist = reference("artist", Artists)
}

class Album(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Album>(Albums)

    var name by Albums.name

    var isAvailable by Albums.isAvailable

    var imagePath by Albums.imagePath

    var imageFile: File?
        get() = imagePath?.let { File(it) }
        set(v) {
            imagePath = if (v == null) null else v.absolutePath
        }

    var artist by Artist referencedOn Albums.artist

    val songs by Song referrersOn Songs.album

}
