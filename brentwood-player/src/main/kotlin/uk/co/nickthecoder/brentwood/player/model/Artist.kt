package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable


object Artists : IntIdTable(name = "artist") {
    val name = varchar("name", 100).index()
    val isAvailable = bool("isAvailable")
}

class Artist(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Artist>(Artists)

    var name by Artists.name
    var isAvailable by Artists.isAvailable

    val albums by Album referrersOn Albums.artist
}
