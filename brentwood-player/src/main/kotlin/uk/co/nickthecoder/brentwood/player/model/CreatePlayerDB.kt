package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.transactions.transaction
import java.io.File
import java.io.IOException
import java.sql.DriverManager

fun createPlayerDB(sqliteDB: File) {

    if (sqliteDB.exists()) {
        throw IOException("File $sqliteDB already exists.")
    }

    val db = Database.connect({ DriverManager.getConnection("jdbc:sqlite:$sqliteDB") })
    transaction(db) {
        SchemaUtils.create(
            Albums, Artists, Entries, Playlists, Songs, Settings
        )
    }

}

fun dumpPlayerSchema(sqliteDB: File) {

    ProcessBuilder(
        "sh",
        "-c",
        "sqlite3 $sqliteDB .schema | sed -e 's/(/(\\n    /'  -e 's/, /,\\n    /g' -e 's/);/\\n);\\n/' -e 's/REFERENCES/\\n        REFERENCES/g' -e 's/CONSTRAINT/\\n    CONSTRAINT/g'\n"
    ).apply {
        inheritIO()
    }.start()
}

/**
 * Creates the tables in the (empty?) database called familyAlbum.db in the current directory.
 *
 * Prints the resulting schema.
 */
fun main(/*vararg args: String*/) {
    createPlayerDB(File("player.db"))
    dumpPlayerSchema(File("player.db"))
}
