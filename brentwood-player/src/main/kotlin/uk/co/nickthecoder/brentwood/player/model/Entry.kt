package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Entries : IntIdTable(name = "entry") {

    val playlist = reference("playlist", Playlists)
    val song = reference("song", Songs)

    val orderNumber = integer("orderNumber")
    val played = bool("played")
}

class Entry(id: EntityID<Int>) : IntEntity(id), Comparable<Entry> {

    companion object : IntEntityClass<Entry>(Entries)

    var playlist by Playlist referencedOn Entries.playlist

    var song by Song referencedOn Entries.song

    /**
     * The index of this playlist entry. The first entry is 1, then 2 etc.
     */
    var orderNumber by Entries.orderNumber

    /**
     * Used by Shuffle. It only picks from entries which have not been played.
     */
    var played by Entries.played

    override fun compareTo(other: Entry): Int {
        return orderNumber - other.orderNumber
    }
}
