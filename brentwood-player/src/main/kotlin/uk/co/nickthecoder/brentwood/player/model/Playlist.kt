package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

object Playlists : IntIdTable(name = "playlist") {
    val name = varchar("name", 100)
    val isActive = bool("isActive")
    val isPermanent = bool("isPermanent")
    val loop = bool("loop")
}

class Playlist(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<Playlist>(Playlists) {
        fun findCurrent(): Playlist {
            val existing = find { Playlists.isActive eq true }.firstOrNull()
            return existing ?: Playlist.new {
                name = "Default"
                isActive = true
                isPermanent = false
                loop = false
            }
        }
    }

    var name by Playlists.name

    var isActive by Playlists.isActive

    /**
     * Entries in permanent playlists are NOT removed when the song finishes playing.
     */
    var isPermanent by Playlists.isPermanent

    /**
     * No longer used! There is a [Setting] instead.
     */
    var loop by Playlists.loop

    val entries by Entry referrersOn Entries.playlist

}
