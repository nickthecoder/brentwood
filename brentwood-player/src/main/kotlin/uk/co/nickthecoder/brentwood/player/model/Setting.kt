package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.Entity
import org.jetbrains.exposed.dao.EntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IdTable

object Settings : IdTable<String>(name = "setting") {

    override val id = varchar("id", 30).entityId()
    override val primaryKey = PrimaryKey(id)

    val value = bool("value")
}

class Setting(id: EntityID<String>) : Entity<String>(id) {

    companion object : EntityClass<String, Setting>(Settings)

    var value by Settings.value

}
