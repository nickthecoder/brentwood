package uk.co.nickthecoder.brentwood.player.model

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.`java-time`.datetime
import java.io.File
import java.time.LocalDateTime
import java.util.*

object Songs : IntIdTable(name = "song") {
    val name = varchar("name", 100).index()
    val trackNumber = integer("trackNumber")

    val seconds = integer("seconds")

    val path = varchar("path", 500)
    val lastScanned = datetime("lastScanned")
    val lastModified = datetime("lastModified")
    val lastPlayed = datetime("lastPlayed").nullable()
    val isAvailable = bool("isAvailable")
    val randomOrder = integer("randomOrder").index()

    val album = reference("album", Albums)
}

class Song(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Song>(Songs)

    /**
     * The track number within the album. 1 is the first track.
     */
    var trackNumber by Songs.trackNumber

    /**
     * The duration of the song in seconds.
     */
    var seconds by Songs.seconds

    /**
     * The name of the song. This is taken from the song's title tag, and if not found, then
     * it is derived from the file's path (stripped of extension and a track number prefix if there is one).
     */
    var name by Songs.name

    /**
     * Nalin likes to add extra data, such as writers in the "title" tag.
     * He places this extra data in square brackets.
     * This returns everything *before* the square bracket (if there is one),
     * and [nameExtra] returns everything the square bracket onwards.
     */
    val truncatedName: String
        get() {
            val bracket = name.indexOf('[')
            return if (bracket < 0) name else name.substring(0, bracket)
        }

    /**
     * Returns everything that [truncatedName] omits.
     */
    fun nameExtra(default: String): String {
        val bracket = name.indexOf('[')
        return if (bracket < 0) default else name.substring(bracket)
    }

    /**
     * The full path of the song. See [file].
     */
    var path by Songs.path

    /**
     * The file for this song. This is derived from [path].
     * These should be absolute paths; never relative.
     */
    var file: File
        get() = File(path)
        set(v) {
            path = v.absolutePath
        }

    /**
     * The date and time that the song's meta-data was stored in the database.
     */
    var lastScanned: LocalDateTime by Songs.lastScanned

    /**
     * The data and time that this song was last changed on disk.
     * The scanner compares the file's lastModified date with this to see if it should
     * re-extract the tag meta-data again.
     */
    var lastModified by Songs.lastModified

    /**
     * The date and time that this song was last played.
     * Used for the "History" page.
     */
    var lastPlayed by Songs.lastPlayed

    /**
     * If the song's file doesn't exist.
     * False if the file has been deleted, or its file system has been unmounted.
     */
    var isAvailable by Songs.isAvailable

    /**
     * A random number is given to all new Songs.
     * When a random track is requested the song with the largest randomOrder is chosen,
     * and it's randomOrder is reset to -1.
     * If there are no more songs with positive randomOrders, then all songs are re-assigned
     * a randomOrder.
     * When this song is played randomOrder is set to -1 (regardless of whether it was picked randomly, or
     * chosen by the user).
     * In this way, random play will not repeat a song until, nor miss some tracks.
     */
    var randomOrder by Songs.randomOrder


    var album by Album referencedOn Songs.album

    fun resetRandomOrder() {
        randomOrder = Random().nextInt(Int.MAX_VALUE)
    }
}
