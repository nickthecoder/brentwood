# Launching via SystemD

Here's a sample systemd service configuration file /etc/systemd/system/brentwood.service :

    [Unit]
    Description=The brentwood website
    
    [Service]
    User=root
    WorkingDirectory=/gidea/projects/brentwood/
    ExecStart=/gidea/projects/brentwood/brentwood-home/build/install/brentwood-home/bin/brentwood-home --compact --uid 1002 --gid 507 --port 80
    Restart=always
    
    [Install]
    WantedBy=multi-user.target

Command to restart the webserver :

    systemctl restart brentwood.service

